# Backend APIRI-HC
_Servidor Backend de la Aplicación para la Recolección de Información sobre Heridas Cutáneas (APIRI-HC)_


### Pre-requisitos 📋

### Instalación 🔧

## Despliegue 📦

## Llamadas y Pruebas con POSTMAN 📦

_Para facilitar las pruebas del Backend, en el siguiente link se podra obtener la coleccion POSTMAN de las pruebas realizadas y llamadas al API del Backend APIRI https://www.getpostman.com/collections/34323d2447ded0855ddb_

## Autor ✒️

* **Armando Rodarte Rodríguez*


