﻿use APIRI;

SET IDENTITY_INSERT EtapasVisuales ON ;

Insert into EtapasVisuales (id,NombreFase,Descripcion) values(1,'Hérida sin coagulación','Aún no inicia el proceso cicatrización con manifestaciones visuales, ya que no se aprecia el inicio de coagulación donde ésta sería la primera fase (en la mayoría de los casos la herida acaba de ocurrir).');
Insert into EtapasVisuales (id,NombreFase,Descripcion) values(2,'Formación de costra','La fase de coagulación y fase inflamatoria tienen un equivalente a nivel visual por el ojo humano, esta fase sería la formación de la costra.
Las células sanguíneas especiales llamadas plaquetas intervienen en esta fase visual. Las plaquetas permanecen juntas como un pegamento en el corte, formando un coágulo. Este coágulo es como un vendaje protector sobre el corte que evita que fluyan más sangre y otros líquidos al exterior. Él coágulo también está lleno de otras células sanguíneas y de un material fibroso llamado fibrina, que ayuda a mantener la estructura del coágulo.
A medida que el coágulo empieza a endurecerse y a secarse, se va formando una costra. Las costras suelen tener aspecto de corteza y son de un color rojo oscuro o marrón. Su función es proteger el corte manteniendo alejados a los gérmenes y otras cosas, así ofreciéndole a las células de la piel de debajo la oportunidad de cicatrizar
Esta fase de la curación se caracteriza por la formación de exudado y el enrojecimiento de la piel circundante.  Nada tiene que ver con la infección. El proceso de inflamación produce síntomas clásicos: enrojecimiento, calor, aumento de volumen, dolor y disminución de la función. Las respuestas inflamatorias tempranas y tardías difieren, y cada fase conlleva mediadores bioquímicos distintos.
 ');
Insert into EtapasVisuales (id,NombreFase,Descripcion) values(3,'Cierre de hérida','Igualmente, la fase proliferativa y sus subprocesos de epitelización, granulación y contracción se pueden apreciar visualmente por el inicio de la formación de una cicatriz, por lo que esta fase sería su equivalente a nivel visual por el ojo humano.
En esta fase la costra ha desaparecido. Conforme se disuelve el coágulo se forma el tejido de granulación (subprocesos de epitelización, granulación) dando una coloración rojiza.
En ese momento (subproceso de contracción) se forma la cicatriz y puede tener un aspecto rojizo o rosado (Nota: las heridas cicatrizan de lado a lado y no de extremo a extremo).
Esta fase se caracteriza por la formación, organización y resistencia que obtiene el tejido al formar la cicatriz, lo cual se obtiene de la contracción de la herida generada por los miofibroblastos y la organización de los paquetes de colágeno
');
Insert into EtapasVisuales (id,NombreFase,Descripcion) values(4,'Remodelación de cicatriz','El objetivo es dejar la piel lo más parecido a la piel normal (en su estado y apariencia normal) Esta etapa es traducida clínicamente por la disminución de color cicatrizal (la cicatriz toma un color menos rojizo). Con el tiempo hay un cambio en la coloración en la cicatriz debido a la disminución en la vascularización (de rojo a rosado y finalmente una tonalidad gris). ');
Insert into EtapasVisuales (id,NombreFase,Descripcion) values(5,'Hérida Sana','La cicatriz ha madurado completamente o la fase de remodelación ha terminado dejando la piel sana y restaurada.');
select * from EtapasVisuales;





insert into GradoGravedad(Grado,Infectada) values (1,0)
insert into GradoGravedad(Grado,Infectada) values (1,1)
insert into GradoGravedad(Grado,Infectada) values (2,0)
insert into GradoGravedad(Grado,Infectada) values (2,1)
insert into GradoGravedad(Grado,Infectada) values (3,0)
insert into GradoGravedad(Grado,Infectada) values (3,1)
insert into GradoGravedad(Grado,Infectada) values (4,0)
insert into GradoGravedad(Grado,Infectada) values (4,1)
select * from GradoGravedad;


insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','1');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','2')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','3');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','4')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','5');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','6')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','7');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Quemadura','8')

insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','1');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','2')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','3');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','4')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','5');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','6')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','7');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Ulcera','8')

insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','1');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','2')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','3');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','4')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','5');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','6')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','7');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Cortada Lineal','8')

insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','1');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','2')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','3');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','4')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','5');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','6')
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','7');
insert into TipoHerida(NombreHerida,  GradoGravedadId)values ('Mordedura','8')
select * from TipoHerida;


