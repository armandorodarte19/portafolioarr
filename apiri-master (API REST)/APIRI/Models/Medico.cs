﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
    public class Medico
    {
        public int Id { get; set; } //MedicoId
        [Required]
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        [Required]
        public string NombreMedico { get; set; }
        [Required]
        public char Genero { get; set; }
        [Required]
        [EmailAddress]

        public string Correo { get; set; }
        [Required]

        public long Telefono { get; set; }
        [Required]

        public int CentroClinicoId { get; set; }
        [ForeignKey("CentroClinicoId")]
        public CentroClinico? CentrosClinicos { get; set; }
    }
}
