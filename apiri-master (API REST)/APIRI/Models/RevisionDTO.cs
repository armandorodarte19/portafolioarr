﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace APIRI.Models
{
    public class RevisionDTO
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public string UrlImagen { get; set; }

        
        public int EtapasVisualesId { get; set; }
        [ForeignKey("EtapasVisualesId")]
        public EtapasVisuales ?EtapasVisuales { get; set; }

        public int CasoMedicoId { get; set; }
        [ForeignKey("CasoMedicoId")]
        public CasoMedico ?CasoMedico { get; set; }

        public int TipoHeridaId { get; set; }
        [ForeignKey("TipoHeridaId")]
        public TipoHerida ?TipoHerida { get; set; }

        public int MedicoId { get; set; }
        [ForeignKey("MedicoId")]
        public Medico ?Medico { get; set; }

        public int FactoresSanacionId { get; set; }
        [ForeignKey("FactoresSanacionId")]
        public virtual FactoresSanacion ?FactoresSanacion { get; set; }

    }
}
