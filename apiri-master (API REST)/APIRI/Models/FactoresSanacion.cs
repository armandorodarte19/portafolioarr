﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
    public class FactoresSanacion
    {
        public int Id { get; set; } //FactoresId
        [Required]
        public bool Fumar { get; set; }
        [Required]
        public bool BebeAlcohol { get; set; }
        [Required]
        public int IMC { get; set; }
        [Required]
        public bool EnfermedadesCronicoDegenerativas { get; set; }
        [Required]
        public bool EnfermedadesGeneticas { get; set; }
        [Required]
        public bool Mayor60 { get; set; }
        [Required]
        public bool BuenaAlimentacion { get; set; }
        [Required]
        public bool DesordenesAlimenticios { get; set; }
        [Required]
        public bool RealizaEjercicio { get; set; }
        [Required]
        public bool MedicamentosNegativos { get; set; }
        [Required]
        public bool MedicamentosPositivos { get; set; }
        [Required]
        public bool Endocrinopatias { get; set; }
       
        [Required]
        public bool Estres { get; set; }
        [Required]
        public bool InsuficienciaVascular { get; set; }
        [Required]
        public bool Cicatriz { get; set; }
        [Required]
        public bool HeridaConPuntos { get; set; }
        [Required]
        public bool CicatrizConDeformidad { get; set; }


    }
}
