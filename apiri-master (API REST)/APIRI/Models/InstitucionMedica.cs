﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
    public class InstitucionMedica
    {

        public int Id { get; set; }
        [Required]
        public string NombreInstitucion { get; set; }
        
        public IEnumerable<CentroClinico> CentroClinicos { get; set; }
        



    }
}
