﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
    public class CasoMedico
    {
        public int Id { get; set; }
        [Required]
        public string NombreCasoMedico {get;set;}
        [Required]
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        
        
        public int TiempoSanacion    { get; set; }

        [Required]
        public bool CasoAbierto { get; set; }

        
        public int PacienteId { get; set; } 
        [ForeignKey("PacienteId")]
        public virtual Paciente ?Paciente { get; set; }

        public int CentroClinicoId { get; set; }
        [ForeignKey("CentroClinicoId")]
        public  virtual CentroClinico ?CentroClinico { get;set;}

       
        public CasoMedico()
        {
            CasoAbierto = true;
        }
       
    }
}
