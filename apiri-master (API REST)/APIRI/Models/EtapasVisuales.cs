﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
    public class EtapasVisuales
    {
        public int Id { get; set; }
        [Required]
        public string NombreFase { get; set; }
        
        public string Descripcion { get; set; }

    }
}
