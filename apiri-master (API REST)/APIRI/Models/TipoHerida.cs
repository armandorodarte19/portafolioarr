﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
   
    public class TipoHerida
    {
        
        public int Id { get; set; } //TipoHeridaId
        [Required]
        public string NombreHerida { get; set; }
       
        //Relacion
        
        [ForeignKey("GradoGravedadId")]
        public int GradoGravedadId { get; set; }
        public virtual GradoGravedad GradoGravedad { get; set; }
    }
}
