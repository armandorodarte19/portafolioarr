﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace APIRI.Models
{
    public class Paciente
    {
        public int Id { get; set; } //PacienteID
        public string NSS { get; set; }
        [Required]
        [MaxLength(18)]
        [MinLength(18)]

        public string CURP { get; set; }
        [Required]
        public string ApellidoPaterno { get; set; }
        public string ApellidoMaterno { get; set; }
        [Required]
        public string NombrePaciente { get; set; }
        [Required]

        public char Genero { get; set; }

        [Range(0, 120)]
        [Required]
        public int Edad { get; set; }





    }
}
