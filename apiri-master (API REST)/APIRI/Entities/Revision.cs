﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using APIRI.Models;
using Microsoft.AspNetCore.Http;

namespace APIRI.Entities
{
    public class Revision
    {
        public int Id { get; set; }
        public DateTime Fecha { get; set; }
        public IFormFile UrlImagen { get; set; }

        public int EtapasVisualesId { get; set; }
        [ForeignKey("EtapasVisualesId")]
        public EtapasVisuales ?EtapasVisuales { get; set; }
        public int FactoresSanacionId { get; set; }
        [ForeignKey("FactoresSanacionId")]
        public FactoresSanacion ?FactoresSanacion { get; set; }

        public int CasoMedicoId { get; set; }
        [ForeignKey("CasoMedicoId")]
        public CasoMedico ?CasoMedico { get; set; }


        public int TipoHeridaId { get; set; }
        [ForeignKey("TipoHeridaId")]
        public TipoHerida ?TipoHerida { get; set; }

        public int MedicoId { get; set; }
        [ForeignKey("MedicoId")]
        public Medico ?Medico { get; set; }
    }
}
