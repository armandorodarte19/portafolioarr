﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class GradoGravedadController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public GradoGravedadController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/GradoGravedad
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GradoGravedad>>> GetGradoGravedad()
        {
            return await _context.GradoGravedad.ToListAsync();
        }

        // GET: api/GradoGravedad/5
        [HttpGet("{id}")]
        public async Task<ActionResult<GradoGravedad>> GetGradoGravedad(int id)
        {
            var gradoGravedad = await _context.GradoGravedad.FindAsync(id);

            if (gradoGravedad == null)
            {
                return NotFound();
            }

            return gradoGravedad;
        }

        // PUT: api/GradoGravedad/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(Roles ="admin")]
        public async Task<IActionResult> PutGradoGravedad(int id, GradoGravedad gradoGravedad)
        {
            if (id != gradoGravedad.Id)
            {
                return BadRequest();
            }

            _context.Entry(gradoGravedad).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GradoGravedadExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/GradoGravedad
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]

        public async Task<ActionResult<GradoGravedad>> PostGradoGravedad(GradoGravedad gradoGravedad)
        {
            try
            {
                _context.GradoGravedad.Add(gradoGravedad);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetGradoGravedad", new { id = gradoGravedad.Id }, gradoGravedad);
            }catch(Exception e)
            {
                return BadRequest();
            }
        }

       

        private bool GradoGravedadExists(int id)
        {
            return _context.GradoGravedad.Any(e => e.Id == id);
        }
    }
}
