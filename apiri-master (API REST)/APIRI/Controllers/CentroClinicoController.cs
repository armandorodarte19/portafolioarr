﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CentroClinicoController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CentroClinicoController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CentroClinico
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CentroClinico>>> GetCentroClinico()
        {
            return await _context.CentroClinico.ToListAsync();
        }

        // GET: api/CentroClinico/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CentroClinico>> GetCentroClinico(int id)
        {
            var centroClinico = await _context.CentroClinico.FirstOrDefaultAsync(x => x.Id == id);

            if (centroClinico == null)
            {
                return NotFound();
            }

            return centroClinico;
        }
        // GET: api/CentroClinico/centros/
        //Obtiene los nombres de los centros
        [HttpGet("centros")]
        public async Task<ActionResult<CentroClinico>> GetCentroClinicoAdmn()
        {
            Console.WriteLine(User.Identity.Name);

            var centroClinico = await _context.CentroClinico.FirstOrDefaultAsync(x => User.Identity.Name.Contains(x.NombreCentroClinico));

            if (centroClinico == null)
            {
                return NotFound();
            }

            return centroClinico;
        }
        // PUT: api/CentroClinico/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.

        [HttpPut("{id}")]
        [Authorize(Roles = "admin")]
        public async Task<IActionResult> PutCentroClinico(int id, CentroClinico centroClinico)
        {
            if (id != centroClinico.Id)
            {
                return BadRequest();
            }

            _context.Entry(centroClinico).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CentroClinicoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CentroClinico
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        //[Authorize(Roles = "admin")]
        public async Task<ActionResult<CentroClinico>> PostCentroClinico(CentroClinico centroClinico)
        {
            try
            {
                _context.CentroClinico.Add(centroClinico);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetCentroClinico", new { id = centroClinico.Id }, centroClinico);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        /* DELETE: api/CentroClinico/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<CentroClinico>> DeleteCentroClinico(int id)
        {
            var centroClinico = await _context.CentroClinico.FindAsync(id);
            if (centroClinico == null)
            {
                return NotFound();
            }

            _context.CentroClinico.Remove(centroClinico);
            await _context.SaveChangesAsync();

            return centroClinico;
        }
        */

        private bool CentroClinicoExists(int id)
        {
            return _context.CentroClinico.Any(e => e.Id == id);
        }
    }
}
