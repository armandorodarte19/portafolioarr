﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class TipoHeridaController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public TipoHeridaController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/TipoHerida
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TipoHerida>>> GetTipoHerida()
        {
            return await _context.TipoHerida.Include(x => x.GradoGravedad).ToListAsync();
        }

        // GET: api/TipoHerida/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TipoHerida>> GetTipoHerida(int id)
        {
            var tipoHerida = await _context.TipoHerida.Include(x => x.GradoGravedad).FirstOrDefaultAsync(x => x.Id == id);

            if (tipoHerida == null)
            {
                return NotFound();
            }

            return tipoHerida;
        }

        //Metodo para obtener el tipo de herida mediante el nombre de la herida, el grado de gravedad y si esta o no infectada
        [HttpGet("byName")]
        public async Task<ActionResult<TipoHerida>> GetTipoHeridaByName(String nombreHerida, int gradoGravedad,bool infectada )
        {
            var tipoHerida = await _context.TipoHerida
                .Include(x => x.GradoGravedad)
                .Where(x => x.NombreHerida == nombreHerida)
                .Where(x=>x.GradoGravedad.Grado==gradoGravedad)
                .Where(x => x.GradoGravedad.Infectada == infectada)
                .FirstOrDefaultAsync();
            Console.WriteLine(nombreHerida + " "+gradoGravedad+" "+infectada);
            if (tipoHerida == null)
            {
                return NotFound();
            }

            return tipoHerida;
        }

        //Obtiene los nombres de las heridas
        [HttpGet("heridas")]
        public async Task<IEnumerable<String>> GetHeridas()
        {
            return await _context.TipoHerida.Select(s => s.NombreHerida).Distinct().ToListAsync();
        }

        // PUT: api/TipoHerida/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles ="admin")]
        public async Task<IActionResult> PutTipoHerida(int id, TipoHerida tipoHerida)
        {
            if (id != tipoHerida.Id)
            {
                return BadRequest();
            }

            _context.Entry(tipoHerida).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoHeridaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }



        // POST: api/TipoHerida
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.

        [HttpPost]
        // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<TipoHerida>> PostTipoHerida([FromBody] TipoHerida tipoHerida)
        {
            try
            {
                _context.TipoHerida.Add(tipoHerida);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetTipoHerida", new { id = tipoHerida.Id }, tipoHerida);
            }
            catch (Exception e )
            {
                return BadRequest();
            }
        }

        /* DELETE: api/TipoHerida/5
        [HttpDelete("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        public async Task<ActionResult<TipoHerida>> DeleteTipoHerida(int id)
        {
            var tipoHerida = await _context.TipoHerida.FindAsync(id);
            if (tipoHerida == null)
            {
                return NotFound();
            }

            _context.TipoHerida.Remove(tipoHerida);
            await _context.SaveChangesAsync();

            return tipoHerida;
        }
        
        */
        private bool TipoHeridaExists(int id)
        {
            return _context.TipoHerida.Any(e => e.Id == id);
        }
    }
}
