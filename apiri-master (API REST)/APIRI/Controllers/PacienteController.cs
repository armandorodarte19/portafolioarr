﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class PacienteController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public PacienteController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/Paciente
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Paciente>>> GetPacientesList(int countActual)
        {
            int count = 0;
            int limite = _context.Paciente.Count();
            Console.WriteLine(count);
            Console.WriteLine(limite);
            if (countActual >= limite)
            {
                return null;
            }
            count = countActual + 10;

            //Si el countActual no supero el limite pero el count con la suma de otros 10, el count toma el valor del limite
            if (count > limite)
            {

                count = limite;
            }

            var resp = await _context.Paciente


                .Take(count)
                .ToListAsync();
            var respF = resp.TakeLast(count - countActual).ToList();

            return respF;
        }


        [HttpGet("search/{searched}")]
        public async Task<ActionResult<IEnumerable<Paciente>>> GetPacientesSearch(String searched)
        {
            Console.WriteLine(searched);
            try
            {
                var pacientes = await _context.Paciente
               .Where(x => x.NSS.Contains(searched) || x.CURP.Contains(searched)|| (x.NombrePaciente+" "+x.ApellidoPaterno+" "+x.ApellidoMaterno).Contains(searched))
               .ToListAsync();

                if (pacientes == null)
                {
                    return NotFound();
                }

                return pacientes;
            }
            catch (Exception e)
            {
                
                return BadRequest();
            }
        }
        // GET: api/Paciente/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Paciente>> GetPaciente(int id)
        {
            var paciente = await _context.Paciente
                .FirstOrDefaultAsync(x => x.Id == id);

            if (paciente == null)
            {
                return NotFound();
            }

            return paciente;
        }

        // PUT: api/Paciente/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPaciente(int id, Paciente paciente)
        {
            if (id != paciente.Id)
            {
                return BadRequest();
            }

            _context.Entry(paciente).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PacienteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Paciente
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Paciente>> PostPaciente(Paciente paciente)
        {

            try
            {
                String vCurp = VerifyCURP(paciente.CURP);
                String vNSS = VerifyNSS(paciente.NSS);

                if (vCurp != "OK")
                {
                    return new ObjectResult(new { statusCode = 409, error = vCurp });
                }
                if (vNSS != "OK")
                {
                    return new ObjectResult(new { statusCode = 409, error = vNSS });
                }


                _context.Paciente.Add(paciente);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetPaciente", new { id = paciente.Id }, paciente);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Paciente/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Paciente>> DeletePaciente(int id)
        {
            try
            {
                var paciente = await _context.Paciente.FindAsync(id);
                if (paciente == null)
                {
                    return NotFound();
                }

                _context.Paciente.Remove(paciente);

                int existPaciente = _context.CasoMedico.Where(s => s.PacienteId == paciente.Id).Count();
                if (existPaciente == 0)
                {
                    await _context.SaveChangesAsync(true);
                    return Ok();
                }




                return StatusCode(409);
            }
            catch (Exception e)
            {
                return BadRequest();
            }


            //return paciente;
        }
        private String VerifyCURP(string CURP)
        {
            if (_context.Paciente.Any(x => x.CURP == CURP))
            {

                return $"CURP {CURP} esta realmente en uso";
            }
            Console.WriteLine(_context.Paciente.Any(x => x.CURP == CURP));
            return "OK";
        }
        private String VerifyNSS(string NSS)
        {
            if (_context.Paciente.Any(x => x.NSS == NSS))
            {
                return $"NSS {NSS} esta realmente en uso";
            }
            Console.WriteLine(_context.Paciente.Any(x => x.NSS == NSS));
            return "OK";
        }
        private bool PacienteExists(int id)
        {
            return _context.Paciente.Any(e => e.Id == id);
        }
    }
}
