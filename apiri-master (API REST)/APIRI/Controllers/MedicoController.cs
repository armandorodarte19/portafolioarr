﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MedicoController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public MedicoController(ApplicationDbContext context)
        {
            _context = context;
        }
        [Authorize(Roles = "admin")]
        // GET: api/Medico
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Medico>>> GetMedico()
        {
            var medicos = await _context.Medico
                 .Include(x => x.CentrosClinicos)
                 .Where(a => User.Identity.Name.Contains(a.CentrosClinicos.NombreCentroClinico))
                 .ToListAsync();

            if (medicos == null)
            {
                return NotFound();
            }
            return medicos;
        }

        // GET: api/Medico/5

        [HttpGet("{id}")]
        public async Task<ActionResult<Medico>> GetMedico(int id)
        {
            var medico = await _context.Medico
                //.Where(x => x.Correo == User.Identity.Name)
                .Include(x => x.CentrosClinicos)
                
                .FirstOrDefaultAsync(x => x.Id == id);

            if (medico == null)
            {
                return NotFound();
            }

            return medico;
        }

        [HttpGet("email/{email}")]
        public async Task<ActionResult<Medico>> GetMedicoEmail(String email)
        {
            var medico = await _context.Medico
                //.Where(x => x.Correo == User.Identity.Name)
                .Include(x => x.CentrosClinicos)
               
                .FirstOrDefaultAsync(x => x.Correo == email);

            if (medico == null)
            {
                return NotFound();
            }

            return medico;
        }

        // PUT: api/Medico/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = "admin")]
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMedico(int id, Medico medico)
        {
            if (id != medico.Id)
            {
                return BadRequest();
            }
            

            _context.Entry(medico).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MedicoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Medico
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [Authorize(Roles = "admin")]
        [HttpPost]
        public async Task<ActionResult<Medico>> PostMedico(Medico medico)
        {
            try
            {
                _context.Medico.Add(medico);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetMedico", new { id = medico.Id }, medico);
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // DELETE: api/Medico/5

        [HttpDelete("{id}")]
        public async Task<ActionResult<Medico>> DeleteMedico(int id)
        {
            var medico = await _context.Medico.FindAsync(id);
            if (medico == null)
            {
                return NotFound();
            }

            _context.Medico.Remove(medico);
            await _context.SaveChangesAsync();

            return medico;
        }
        
        private bool MedicoExists(int id)
        {
            return _context.Medico.Any(e => e.Id == id);
        }
    }
}
