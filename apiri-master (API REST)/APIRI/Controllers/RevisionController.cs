﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using APIRI.Entities;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using System.Net.Http.Headers;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class RevisionController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        private static string _path;
        public RevisionController(ApplicationDbContext context)
        {
            _context = context;
            _path = Directory.GetCurrentDirectory() + "\\Upload\\";
        }




        private Dictionary<string, string> GetMimeTypes()
        {
            return new Dictionary<string, string>
            {

                {".png", "image/png"},
                {".jpg", "image/jpeg"},
                {".jpeg", "image/jpeg"}
            };
        }


        //Get: api/revision/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RevisionDTO>> GetRevision(int id)
        {
            var revision = await _context.RevisionDTO
                .Include(x => x.Medico)
                .Include(x => x.CasoMedico)
                .Include(x => x.FactoresSanacion)
                .Include(x => x.TipoHerida)
                .Include(x => x.TipoHerida.GradoGravedad)
                .Include(x => x.EtapasVisuales)
                .FirstOrDefaultAsync(x => x.Id == id);

            if (revision == null)
            {
                return NotFound();
            }

            return revision;
        }

        // GET: api/Revision/caso/5
        [HttpGet("caso/{id}")]
        public async Task<ActionResult<IEnumerable<RevisionDTO>>> GetRevisionesCasos(int id)
        {
            var revisionDTO = await _context.RevisionDTO
                .Where(x => x.CasoMedicoId == id)
                .Include(x => x.Medico)
                .Include(x => x.CasoMedico)
                .Include(x => x.FactoresSanacion)
                .Include(x => x.TipoHerida)
                .Include(x => x.EtapasVisuales)
                .Include(x => x.TipoHerida.GradoGravedad)
                .ToListAsync();

            if (revisionDTO == null)
            {
                return NotFound();
            }

            return revisionDTO;
        }



        // PUT: api/RevisionDTOes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRevisionDTO(int id, RevisionDTO revisionDTO)
        {
            if (id != revisionDTO.Id)
            {
                return BadRequest();
            }

            _context.Entry(revisionDTO).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RevisionDTOExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


        // POST: api/RevisionDTOes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<RevisionDTO>> PostRevisionDTO(RevisionDTO Revision)
        {

            try
            {
                _context.RevisionDTO.Add(Revision);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetRevision", new { id = Revision.Id }, Revision);

            }
            catch (Exception e)
            {
                return BadRequest();
            }



        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<RevisionDTO>> Delete(int id)
        {
            try
            {
                var revisionDTO = await _context.RevisionDTO.FindAsync(id);
                if (revisionDTO == null)
                {
                    return NotFound();
                }
                String nombreImagen = "Upload/" + revisionDTO.UrlImagen;
                _context.RevisionDTO.Remove(revisionDTO);

                if (System.IO.File.Exists(nombreImagen))
                {
                    System.IO.File.Delete(nombreImagen);
                }
                await _context.SaveChangesAsync();

                return revisionDTO;
            }
            catch (Exception e)
            {
                return Conflict();
            }
        }

        [HttpDelete("deleteImagen")]
        public IActionResult DeleteImagen(String path)
        {
            String nombreImagen = "Upload/" + path;
            if (System.IO.File.Exists(nombreImagen))
            {
                System.IO.File.Delete(nombreImagen);
                return NoContent();
            }
            return NotFound();

        }

        [HttpGet("download/{nombreImage}")]
        public async Task<IActionResult> Get(String nombreImage)
        {
            var ruta = "Upload\\" + nombreImage;

            var image = System.IO.File.OpenRead(ruta); ;

            return File(image, "image/jpeg");
        }

        [HttpPost("upload")]
        public String UploadImageHerida(IFormFile files)
        {

            Console.WriteLine(files);
            try
            {
                if (files.Length > 0)
                {
                    if (!Directory.Exists(_path))
                    {
                        Directory.CreateDirectory(_path);
                    }

                    string[] nombrecompleto = files.FileName.Split(".");
                    Console.WriteLine(files.FileName);
                    string nombreImagen = nombrecompleto[0];
                    string ext = nombrecompleto[1];

                    using (FileStream fileStream = System.IO.File.Create(_path + nombreImagen + "." + ext))
                    {



                        files.CopyTo(fileStream);
                        fileStream.Flush();
                        String resp = nombreImagen + "." + ext;

                        return resp;
                    }


                }
                else
                {
                    return "failure";
                }
            }
            catch (Exception e)
            {

                return "failure";
            }

        }

        private bool RevisionDTOExists(int id)
        {
            return _context.RevisionDTO.Any(e => e.Id == id);
        }
    }
}
