﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]

    public class EtapasVisualesController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public EtapasVisualesController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<EtapasVisuales>>> GetEtapasVisuales()
        {
            return await _context.EtapasVisuales.ToListAsync();
        }
        // GET api/EtapasVisualesController/5
        [HttpGet("{id}")]
        public async Task<ActionResult<EtapasVisuales>> GetEtapaVisual(int id)
        {
            var etapa = await _context.EtapasVisuales.FirstOrDefaultAsync(x => x.Id == id);
            if(etapa== null)
            {
                return NotFound();
            }
            return etapa;
        }

    }
}
