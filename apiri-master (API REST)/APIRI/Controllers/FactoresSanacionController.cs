﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        
    public class FactoresSanacionController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public FactoresSanacionController(ApplicationDbContext context)
        {
            _context = context;
        }

        
        // GET: api/FactoresSanacion/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FactoresSanacion>> GetFactoresSanacion(int id)
        {
            var factoresSanacion = await _context.FactoresSanacion
                    .FirstOrDefaultAsync(x => x.Id == id);

            if (factoresSanacion == null)
            {
                return NotFound();
            }

            return factoresSanacion;
        }

        // PUT: api/FactoresSanacion/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFactoresSanacion(int id, FactoresSanacion factoresSanacion)
        {
            if (id != factoresSanacion.Id)
            {
                return BadRequest();
            }

            _context.Entry(factoresSanacion).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FactoresSanacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            
            return NoContent();
        }

        // POST: api/FactoresSanacion
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<FactoresSanacion>> PostFactoresSanacion(FactoresSanacion factoresSanacion)
        {
            try
            {
                _context.FactoresSanacion.Add(factoresSanacion);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetFactoresSanacion", new { id = factoresSanacion.Id }, factoresSanacion);
            }
            catch(Exception e)
            {
                return BadRequest();
            }
        }

        // DELETE: api/FactoresSanacion/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<FactoresSanacion>> DeleteFactoresSanacion(int id)
        {
            try
            {
                var factoresSanacion = await _context.FactoresSanacion.FindAsync(id);
                if (factoresSanacion == null)
                {
                    return NotFound();
                }

                _context.FactoresSanacion.Remove(factoresSanacion);
                await _context.SaveChangesAsync();

                return factoresSanacion;
            }catch(Exception e)
            {
                return BadRequest();
            }
        }

        private bool FactoresSanacionExists(int id)
        {
            return _context.FactoresSanacion.Any(e => e.Id == id);
        }
    }
}
