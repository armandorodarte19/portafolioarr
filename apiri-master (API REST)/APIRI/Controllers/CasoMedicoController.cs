﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using APIRI.Contexts;
using APIRI.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace APIRI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CasoMedicoController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CasoMedicoController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/CasoMedico
        public async Task<ActionResult<IEnumerable<CasoMedico>>> GetCasoMedicos(int countActual, int centroClinicodId)
        {


            int count = 0;
            int limite = _context.CasoMedico.Count();
            //Supero el limite osea ultimo contador
            if (countActual >= limite)
            {
                return null;
            }
            count = countActual + 10;

            //Si el countActual no supero el limite pero el count con la suma de otros 10, el count toma el valor del limite
            if (count > limite)
            {

                count = limite;
            }
            var resp = await _context.CasoMedico

                .Include(x => x.Paciente)

                .Include(c => c.CentroClinico)
                
                .Where(cc => cc.CentroClinicoId == centroClinicodId)
                .OrderByDescending(d => d.FechaInicio)

                .Take(count)

                .ToListAsync();
            //Filtra la respuesta para tomar solo los ultimos datos 
            var respF = resp.TakeLast(count - countActual).ToList();

            return respF;
            ;
        }

        [HttpGet("search/{searched}")]
        public async Task<ActionResult<IEnumerable<CasoMedico>>>  GetPacienteSearch(String searched)
        {

            try
            {
                var cMedico = await _context.CasoMedico
               .Where(x => x.NombreCasoMedico.Contains(searched) )
               .Include(x => x.Paciente)

                .Include(c => c.CentroClinico)

               .ToListAsync();

                if (cMedico == null)
                {
                    return NotFound();
                }

                return cMedico;
            }
            catch (Exception e)
            {
                return BadRequest();
            }
        }

        // GET: api/CasoMedico/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CasoMedico>> GetCasoMedico(int id)
        {
            var casoMedico = await _context.CasoMedico
                .FirstOrDefaultAsync(x=> x.Id == id);

            if (casoMedico == null)
            {
                return NotFound();
            }

            return casoMedico;
        }

        //Obtener casos de un paciente
        [HttpGet("paciente/{idPaciente}")]
        public async Task<ActionResult<IEnumerable<CasoMedico>>> GetCasosMedicosPaciente(int idPaciente,int centroClinicoId )
        {
            var casosMedicos = await _context.CasoMedico
                .Where(p=>p.PacienteId== idPaciente && p.CentroClinicoId == centroClinicoId)
                .Include(x => x.Paciente)
               
                .Include(c => c.CentroClinico)

                .ToListAsync();
            if(casosMedicos == null)
            {
                return NotFound();
            }
            return casosMedicos;
            
        }


        // PUT: api/CasoMedico/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCasoMedico(int id, CasoMedico casoMedico)
        {
            if (id != casoMedico.Id)
            {
                return BadRequest();
            }

            _context.Entry(casoMedico).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CasoMedicoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CasoMedico
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<CasoMedico>> PostCasoMedico(CasoMedico casoMedico)
        {


            try
            {
                _context.CasoMedico.Add(casoMedico);
                await _context.SaveChangesAsync();

                return CreatedAtAction("GetCasoMedico", new { id = casoMedico.Id }, casoMedico);
            }catch(Exception e)
            {
                return BadRequest();
            }
        }

        // DELETE: api/CasoMedico/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<bool>> DeleteCasoMedico(int id)
        {
            try
            {
                var casoMedico = await _context.CasoMedico.FindAsync(id);
                if (casoMedico == null)
                {
                    return NotFound();
                }

                var revisionCaso = await _context.RevisionDTO.Where(x => x.CasoMedicoId == id).CountAsync();
                if (revisionCaso == 0)
                {
                    _context.CasoMedico.Remove(casoMedico);
                    await _context.SaveChangesAsync();

                    return true;
                }
                return false;

            }catch(Exception e)
            {
                return false;
            }
        }

        private bool CasoMedicoExists(int id)
        {
            return _context.CasoMedico.Any(e => e.Id == id);
        }
    }
}
