﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

using APIRI.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace APIRI.Contexts
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

       
        public DbSet<GradoGravedad> GradoGravedad { get; set; }
        public DbSet<TipoHerida> TipoHerida { get; set; }
        public DbSet<FactoresSanacion> FactoresSanacion { get; set; }
        public DbSet<Medico> Medico { get; set; }
        public DbSet<Paciente> Paciente { get; set; }
        public DbSet<CentroClinico> CentroClinico { get; set; }
        public DbSet<CasoMedico> CasoMedico { get; set; }
        public DbSet<RevisionDTO> RevisionDTO { get; set; }
        public DbSet<EtapasVisuales> EtapasVisuales { get; set; }
        
        
    }
}
