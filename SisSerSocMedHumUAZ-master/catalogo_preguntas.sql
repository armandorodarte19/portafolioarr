USE bd_ssmh_uaz;
INSERT INTO `mensajeria_pregunta` (`id_pregunta`, `pregunta`, `respuesta`, `coordinador_id`,`id_tipo_fk_id_id`) VALUES
(1, '¿Cuáles son los requisitos para realizar el Servicio Social?','1 Constancia, 1 Kardex, 1 Tutor, Carta Compromiso.',1,2),
(2, '¿Qué debo hacer para realizar el Servicio Social en otra entidad federativa?', 'Contar con un promedio mayor o igual a 9.0',1,2),
(3, '¿Cuáles son las modalidades del Servicio Social?', 'Rotatorio y de plaza completa',1,3),
(4, '¿En que consiste el Servicio Social rotatorio?', 'Que vas a cambiar de lugar de clinica por lo menos una vez',1,3),
(5, '¿En qué instituciones puedo hacer el Servicio Social?', 'IMSS, ISSTE,Seguro Popular', 1,3),
(6, '\r\n¿Cuál es la diferencia entre plazas A,B o C?', 'Las plazas A son las de los mejores promedios, las plazas B los siguientes y las plazas C los de promedio mas bajo', 1,2),
(7, '¿Cuáles son mis derechos y obligaciones como MPSS?', 'Cumplir con las horas comprometidas, entregar los reportes mensuales con tiempo y llevar un seguimiento', 1,2),
(8, '¿Cuáles son las medidas disciplinarias que se podrán imponer al los MPSS?', 'Tener compromiso, puntualidad, honestidad y sobre todo amor... por la medicina', 1,7),
(9, '\r\n¿Cuales son los requisitios para titulación?', 'Cumplir con el promedio, cumplir con las horas, cumplir con los reportes y tambien ser bueno', 1,2),
(10, '¿Cuales son las opciones de titulación?', 'Tesis, Practicas Profesionales, EGEL, Promedio', 1,3),
(11, '\r\n¿Cómo me entregan la carta de donación de libros?', 'Tiene que estar completa, revisada y firmada por su tutor', 1,2),
(12, '¿Cuánto tiempo se tarda en ser expedido el título?', 'De 6 a 12 meses', 1, 2),
(13, '¿Cuál es la clave de la Universidad?', 'La clave es 123456789', 1,2),
(14, '¿Qué debo hacer al terminar mis trámites de titulación?', 'Esperar a que le den su titulo', 1,2),
(15, '\r\n¿Cómo reporto una situación de seguridad dentro de mi Servicio Social?', 'Reportarlo con su coordinador mediante un Reporte de Inconformidad', 1,1),
(16, '\r\n¿Tengo derecho a permiso para trámites escolares?', 'En el momento que estes dado de alta, lo tienes', 1,5),
(17, '¿Tengo derecho a asistir a cursos, congresos, etc.?', 'Si, pero tienen preferencia aquellos con mayor promedio', 1,5),
(18, '\r\n¿Cómo me puedo contactar con la coordinación de Servicio Social?', 'Puedes hacerlo mediante este chat, continuando la conversacion o enviando un nuevo mensaje', 1,7),
(19, '\r\n¿Pueden obligarme a cambiar de sede sin mi consentimiento?', 'Si, depende de tu comportamiento, joven', 1,4),
(20, '¿Debo asistir en días festivos a mi unidad o centro de salud?', 'No es obligatorio, pero es recomendable', 1,5),
(21, '¿Tengo derecho a seguridad social?', 'Como estudiante de la UAZ lo tienes', 1,5);

