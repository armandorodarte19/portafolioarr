from django.http import Http404
from django.shortcuts import render
from rest_framework import generics
from contactos_pasante.serializers import ContactoSerializer
from pasante.serializers import PasanteSerializer
from contactos_pasante.models import ContactosPasante
from pasante.models import Telefono
from pasante.models import Pasante
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.contrib.auth import authenticate
from twilio.rest import Client

from usuario.models import Usuario
import time
import threading
import random


class GetContactos(APIView):

    def get(self, request, idPasante):
        contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk_id=idPasante)
        contactos_directivo = ContactosPasante.objects.filter(id_pasante_fk_id=None)

        queryset = contactos_pasante | contactos_directivo

        if idPasante is not None:
            serializer = ContactoSerializer(queryset, many=True)
        return Response(serializer.data)

class InsertContacto(APIView):

    def post(self, request, format=None):
        serializer = ContactoSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EliminarContacto(APIView):

    def delete(self, request, idContacto, format=None):
        snippet = self.get_object(idContacto)
        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self, idContacto):
        try:
            return ContactosPasante.objects.get(id_contacto=idContacto)
        except ContactosPasante.DoesNotExist:
            raise Http404


class ActualizarContacto(APIView):

    def put(self, request, idContacto, format=None):
        snippet = self.get_object(idContacto)
        serializer = ContactoSerializer(snippet, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def get_object(self, idContacto):
        try:
            return ContactosPasante.objects.get(id_contacto=idContacto)
        except ContactosPasante.DoesNotExist:
            raise Http404

class VerificarUsuario(APIView):

    def post(self, request):
        matricula = request.data['matricula']
        password = request.data['password']

        if(matricula != None and password != None):
            user = authenticate(username=matricula, password=password)

        if user is not None:
            #print("Autenticado")
            #queryset = (Pasante.objects.get(id_usuario_fk_id__matricula=matricula))
            #serializer = PasanteSerializer(queryset)
            pasante = Pasante.objects.get(id_usuario_fk_id__matricula=matricula)
            telefono = Telefono.objects.get(id_pasante_fk=pasante.id_pasante, tipo='movil')
            print(telefono.numero)
            return Response(self.send_confirmation_code(telefono.numero), status=status.HTTP_200_OK) 
        else:
            return Response("ERROR 400", status=status.HTTP_400)

    def generate_code(self):
        return str(random.randrange(100000, 999999))

    def send_confirmation_code(self, to_number):
        verification_code = self.generate_code()
        send_sms('+52' + to_number, verification_code)
        return verification_code


class ActivarAlerta(APIView):
    hilos = {}

    def post(self, request):
        matricula = request.POST['matricula']
        pasante = Pasante.objects.get(id_usuario_fk_id__matricula=matricula)
        contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk_id=pasante.id_pasante)
        contactos_directivo = ContactosPasante.objects.filter(id_pasante_fk_id=None)

        contactos = contactos_pasante | contactos_directivo

        latitud = request.POST.get('latitud', '0')
        longitud = request.POST.get('longitud', '0')

        ActivarAlerta.hilos[matricula] = SMSThread(pasante, contactos, matricula, latitud, longitud)
        ActivarAlerta.hilos[matricula].start()

        return Response('ALARMA ACTIVADA')

    def get(self, request):
        matricula = request.GET['matricula']
        operacion = request.GET['operacion']
        if operacion == 'detener':
            ActivarAlerta.hilos[matricula].shutdown = True
            return Response('ALARMA DESACTIVADA')

        elif operacion == 'obtener':
            if matricula in ActivarAlerta.hilos and not ActivarAlerta.hilos[matricula].shutdown:
                return Response('ALARMA ACTIVA')
            else:
                return Response('ALARMA DESACTIVA')
        
        
class SMSThread(threading.Thread):  
    def __init__(self, pasante, contactos, matricula, latitud, longitud):  
        threading.Thread.__init__(self)  
        self.pasante = pasante
        self.contactos = contactos
        self.matricula = matricula
        self.shutdown = False  
        self.latitud = latitud
        self.longitud = longitud
  
    def run(self):  
        nombre = self.pasante.nombre + " " + self.pasante.apellido_paterno
        msj = "Hola soy " + nombre + " y me encuentro en peligro. "
        msj_location = "Mi posición es https://www.google.com.mx/maps/@" + self.latitud + "," + self.longitud + ",18z"
        start_time = time.time()
        MAX_WAIT = 50

        while not ActivarAlerta.hilos[self.matricula].shutdown and time.time() - start_time < MAX_WAIT:
            for contacto in self.contactos:
                send_sms('+52' + contacto.numero, (msj) if self.latitud == "0" and self.longitud == "0" else (msj + msj_location))
                time.sleep(7)

        del(ActivarAlerta.hilos[self.matricula])


def send_sms(to_number, body):
    account_sid = 'AC6bed0366496aefbd8a93c3e4789aebda'
    auth_token = '28619020e3bc90df8e8f249806c61d0f'
    twilio_number = "+12673824332"
    client = Client(account_sid, auth_token)
    try:
        client.api.messages.create(to_number,
                           from_=twilio_number,
                           body=body)
    except Exception as e:
        print('Invalid Number')
    

class InformacionPasante(APIView):

    def post(self, request):
        matricula = request.data['matricula']
        password = request.data['password']

        if(matricula != None and password != None):
            user = authenticate(username=matricula, password=password)

        if user is not None:

            queryset = (Pasante.objects.get(id_usuario_fk_id__matricula=matricula))
            serializer = PasanteSerializer(queryset)
            return Response(serializer.data, status=status.HTTP_200_OK)

class EnviarMensaje(APIView):
    def post(self, request):
        numero = request.data['numero']
        mensaje = request.data['mensaje']

        send_sms('+52' + numero, mensaje)

        return Response("Mensaje Enviado", status=status.HTTP_200_OK)
