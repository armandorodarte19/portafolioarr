from contactos_pasante.models import ContactosPasante
from rest_framework import serializers

class ContactoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ContactosPasante
        fields = ('id_contacto','nombre','numero','id_pasante_fk')