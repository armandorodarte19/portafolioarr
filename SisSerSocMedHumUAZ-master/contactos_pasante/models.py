from django.db import models
from pasante.models import Pasante

# Create your models here.

class ContactosPasante(models.Model):
    id_contacto = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    numero = models.CharField(max_length=15)
    id_pasante_fk = models.ForeignKey(Pasante, related_name="usuario", 
    	blank=True, null=True, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre