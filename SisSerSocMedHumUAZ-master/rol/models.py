from django.db import models

from clinica.models import Clinica
from pasante.models import Pasante, Generacion, Promocion



class Rol(models.Model):
    id_rol = models.AutoField(primary_key=True)
    identificador = models.CharField(max_length=6, null=False, unique=True)
    id_generacion_fk = models.ForeignKey(Generacion, null=False,
                                         on_delete=models.CASCADE)
    beca = models.CharField(max_length=250, null=False)


class Rotacion(models.Model):
    id_rotacion = models.AutoField(primary_key=True)
    fecha_inicio = models.DateField(null=False)
    fecha_fin = models.DateField(null=False)
    id_rol_fk = models.ForeignKey(Rol, null=False, on_delete=models.CASCADE)
    id_clinica_fk = models.ForeignKey(Clinica, null=False,
                                      on_delete=models.CASCADE)


class Tutor(models.Model):
    id_tutor = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50, null=False)
    apellido_paterno = models.CharField(max_length=50, null=False)
    apellido_materno = models.CharField(max_length=50, null=False)
    telefono = models.CharField(max_length=15, null=False)
    correo_electronico = models.CharField(unique=True, max_length=255)
    cede = models.CharField(max_length=255, null=False)


class AsignacionRol(models.Model):
    id_rotacion = models.ForeignKey(Rotacion, null=False,
                                    on_delete=models.CASCADE)
    id_pasante = models.ForeignKey(Pasante, null=False,
                                   on_delete=models.CASCADE)
    id_tutor = models.ForeignKey(Tutor, null=False, on_delete=models.CASCADE)
    comision = models.CharField(max_length=100, null=False)


class ConfiguracionAsignacion(models.Model):
    id_configuracion = models.AutoField(primary_key=True)
    inicio_promocion = models.DateField(null=False)
    final_promocion = models.DateField(null=False)
    acuerdo_asignacion = models.DateField(null=False)
    folio_count = models.IntegerField(null=False, default=0)

class FechaCalificacion(models.Model):
    id_fecha_calificacion = models.AutoField(primary_key=True)
    id_pasante = models.ForeignKey(Pasante, on_delete=models.CASCADE,)
    id_rotacion = models.ForeignKey(Rotacion, on_delete=models.CASCADE,)
    fecha_calif_inicio = models.DateField(null=False)
    fecha_calif_fin = models.DateField(null=False)
    calificado = models.BooleanField(default=False)