from django.conf.urls import url

from rol.views import *

urlpatterns = [
    url(r'^registrar_rol/$', CrudRol.registrar_rol, name="registrar_rol"),
    url(r'^modificar_rol/$', CrudRol.modificar_rol, name="modificar_rol"),
    url(r'^roles/$', CrudRol.mostrar_roles, name="roles"),
    url(r'^eliminar_rol/$', CrudRol.eliminar_rol, name="eliminar_rol"),
    url(r'^tutores/$', CrudTutor.mostrar_tutores, name="tutores"),
    url(r'^eliminar_tutor/$', CrudTutor.eliminar_tutor, name="eliminar_tutor"),
    url(r'^modificar_tutor/$', CrudTutor.modificar_tutor,
        name="modificar_tutor"),
    url(r'^registrar_tutor/$', CrudTutor.registrar_tutor,
        name="registrar_tutor"),
    url(r'^asignar_rol/$', AsignacionRolPasante.asignar_rol,
        name='asignar_rol'),
    url(r'^rol_pasante_json/$', AsignacionRolPasante.rol_pasante_json,
        name='rol_pasante_json'),
    url(r'^modificar_asignacion/$', AsignacionRolPasante.modificar_asignacion,
        name='modificar_asignacion'),
    url(r'^carta_asignacion/$', AsignacionRolPasante.carta_asignacion,
        name='carta_asignacion'),
    url(r'^clinicas_institucion/$', obtener_clinicas),
    url(r'^carta_asignacion_oficial/$',
        AsignacionRolPasante.carta_asignacion_oficial,
        name='carta_asignacion_oficial'),
    url(r'configuracion_asignacion', AsignacionRolPasante.configurar_asignacion,
        name='configuracion_asignacion'),
]
