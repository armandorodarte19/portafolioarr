import urllib
from datetime import timezone, timedelta, datetime

from io import StringIO

import pytz
import reportlab
from django.db import transaction
from django.http import HttpResponse, JsonResponse
import simplejson
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User

from django.utils.datetime_safe import datetime
from io import BytesIO

from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER, TA_RIGHT, TA_LEFT, TA_JUSTIFY
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Paragraph, Table, TableStyle, \
    Image
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import letter

from clinica.models import Clinica, Institucion
from pasante.models import Generacion, Pasante
from rol.models import Rol, Rotacion, Tutor, AsignacionRol, \
    ConfiguracionAsignacion, FechaCalificacion

# Create your views here.
from ssmh_uaz_project import settings


class HelperFechas:
    def get_limite_fecha_fin(generacion):
        if generacion.generacion == 'Agosto':
            fecha_fin = '31/07/' + str(int(generacion.anio) + 1)
        else:
            fecha_fin = '31/01/' + str(int(generacion.anio) + 1)

        return fecha_fin

    def get_limite_fecha_inicio(generacion):
        if generacion.generacion == 'Agosto':
            fecha_inicio = '01/08/' + str(generacion.anio)
        else:
            fecha_inicio = '01/02/' + str(generacion.anio)

        return fecha_inicio


class CrudRol:
    def get_roles_asignados():
        return Rol.objects.raw(
            """
              SELECT id_rol, beca, id_generacion_fk_id
                FROM rol_rol
                WHERE id_rol IN (
                  SELECT id_rol_fk_id
                  FROM rol_rotacion
                  WHERE id_rotacion IN (
                    SELECT id_rotacion_id
                    FROM rol_asignacionrol
                  )
                )
            """
        )

    @login_required
    def mostrar_roles(request):
        roles = Rol.objects.all()
        rotaciones = Rotacion.objects.all().order_by('fecha_inicio',
                                                     'fecha_fin')

        return render(request, 'lista_roles.html', {
            'roles': roles,
            'roles_asignados': CrudRol.get_roles_asignados(),
            'rotaciones': rotaciones,
            'info_message': None,
            'error_messages': None
        })

    @login_required
    @transaction.atomic
    def registrar_rol(request):
        clinicas = Clinica.objects.all().order_by('nombre_unidad')
        
        ahora = datetime.now()
        formato_fecha = "%d-%m-%Y"
        fecha_inicio_agosto = datetime.strptime("01-02-" + str(ahora.year),
                                          formato_fecha)
        fecha_fin_agosto = datetime.strptime("31-08-" + str(ahora.year),
                                          formato_fecha)
        obtener_fechas =  User.objects.get(
            id=request.user.id).date_joined.strftime(formato_fecha)
        fecha_user = datetime.strptime(obtener_fechas,
                                                   formato_fecha)
        
        #generacion = Generacion.objects.get(generacion = "Febrero",anio = "2018")
        fecha_user = datetime.today()
        anio_correspondiente = str(ahora.year)
        generacion_correspondiente = ""
        anio_correspondiente = ""
        if (fecha_user < fecha_inicio_agosto):
            generacion_correspondiente = "Febrero"
            anio_correspondiente = str(ahora.year)

        elif (fecha_user >= fecha_inicio_agosto and fecha_user < fecha_fin_agosto):
            generacion_correspondiente = "Agosto"
            anio_correspondiente = str(ahora.year)
        else:
            generacion_correspondiente = "Febrero"
            anio_correspondiente = str(ahora.year+1)    

        generacion = None
        try:
            generacion = Generacion.objects.get(generacion=generacion_correspondiente,
                                                                    anio = anio_correspondiente)
        except Generacion.DoesNotExist as e:
            generacion = Generacion.objects.create(generacion=generacion_correspondiente,
                                                anio=anio_correspondiente)
        
        instituciones = Institucion.objects.all()
        if request.method == 'POST':
            rol = Rol()
            rol.id_generacion_fk = generacion
            rol.beca = request.POST.get('beca')
            rol.identificador = crear_identificador_rol(generacion)
            rol.save()

            for key in request.POST.keys():
                if 'dateInicioRotacion' in key:
                    rotacion = Rotacion()
                    rotacionNumber = key.replace('dateInicioRotacion', '')
                    dateInicioKey = key
                    dateFinKey = 'dateFinRotacion' + rotacionNumber
                    clinicaKey = 'comboboxClinicasRotacion' + rotacionNumber
                    fecha_inicio = datetime.strptime(request.POST.get(
                        dateInicioKey), '%d/%m/%Y').strftime('%Y-%m-%d')
                    rotacion.fecha_inicio = fecha_inicio
                    fecha_fin = datetime.strptime(request.POST.get(
                        dateFinKey), '%d/%m/%Y').strftime('%Y-%m-%d')
                    rotacion.fecha_fin = fecha_fin
                    rotacion.id_clinica_fk_id = request.POST.get(clinicaKey)
                    rotacion.id_rol_fk_id = rol.id_rol
                    rotacion.save()

            return render(request, 'alta_rol.html', {
                'promocion': generacion.generacion + ' ' + generacion.anio,
                'clinicas': clinicas,
                'instituciones': instituciones,
                'limite_inicio': HelperFechas.get_limite_fecha_inicio(
                    generacion),
                'limite_fin': HelperFechas.get_limite_fecha_fin(generacion),
                'info_message': 'Rol "' + rol.identificador + '" creado con éxito',
                'error_messages': None
            })
        else:
            return render(request, 'alta_rol.html', {
                'promocion': generacion.generacion + ' ' + generacion.anio,
                'clinicas': clinicas,
                'instituciones': instituciones,
                'limite_inicio': HelperFechas.get_limite_fecha_inicio(
                    generacion),
                'limite_fin': HelperFechas.get_limite_fecha_fin(generacion),
                'info_message': None,
                'error_messages': None
            })

    @login_required
    def modificar_rol(request):
        pass

    @login_required
    def eliminar_rol(request):
        id_rol = request.POST.get('id_rol', None)

        if id_rol is not None:
            try:
                rol = Rol.objects.get(id_rol=id_rol)
                rol.delete()
                info_message = 'Rol eliminado correctamente'
                error_message = None
            except:
                info_message = None
                error_message = 'El rol no existe'
        else:
            info_message = None
            error_message = 'Error al eliminar el rol'

        roles = Rol.objects.all()
        rotaciones = Rotacion.objects.all()

        return render(request, 'lista_roles.html', {
            'roles': roles,
            'rotaciones': rotaciones,
            'roles_asignados': CrudRol.get_roles_asignados(),
            'info_message': info_message,
            'error_messages': error_message
        })


def obtener_clinicas(request):
    try:
        institucion = Institucion.objects.get(pk=request.GET['id_institucion'])
        print(institucion.nombre_institucion)
        clinicas = Clinica.objects.filter(
            id_institucion_fk=institucion.id_institucion)
        print(str(len(clinicas)))
        clinicas_json = []
        for clinica in clinicas:
            clinica_json = {'nombre': clinica.nombre_unidad,
                            'id': clinica.id_clinica}
            clinicas_json.append(clinica_json)
        data = simplejson.dumps(clinicas_json)
        return HttpResponse(data, content_type="application/json")
    except ValueError:
        return HttpResponse("/")


def crear_identificador_rol(generacion):
    identificador = ''
    identificador += generacion.generacion[0]
    
    identificador += '{:02d}'.format(int(generacion.anio) % 1000)
    lista_roles = Rol.objects.filter(identificador__startswith=identificador)
    num_roles = len(lista_roles)

    if num_roles == 0:
        identificador += '{:03d}'.format(1)
    else:
        ultimorol = lista_roles[num_roles - 1]

        num_rol = ultimorol.identificador[3] + ultimorol.identificador[4] + \
                  ultimorol.identificador[5]
        identificador += '{:03d}'.format(int(num_rol) + 1)

    return identificador


class CrudTutor:
    def get_tutores_asignados():
        return Tutor.objects.raw(
            """
            SELECT id_tutor, nombre, apellido_paterno, apellido_materno,
            telefono, correo_electronico, cede
            FROM rol_tutor
            WHERE id_tutor IN (
              SELECT id_tutor_id
              FROM rol_asignacionrol
            )
            """)

    @login_required
    def mostrar_tutores(request):
        tutores = Tutor.objects.all()

        print(request.GET.get('info_message'))

        return render(request, 'lista_tutores.html', {
            'tutores': tutores,
            'tutores_asignados': CrudTutor.get_tutores_asignados(),
            'info_message': request.GET.get('info_message'),
            'error_message': request.GET.get('error_message')
        })

    @login_required
    def registrar_tutor(request):
        if request.method == 'POST':
            tutor = Tutor()
            tutor.nombre = request.POST.get('nombre')
            tutor.apellido_paterno = request.POST.get('apPaterno')
            tutor.apellido_materno = request.POST.get('apMaterno')
            tutor.telefono = request.POST.get('telefono')
            tutor.correo_electronico = request.POST.get('email')
            tutor.cede = request.POST.get('sede')

            try:
                tutor.save()
                params = urllib.parse.urlencode({
                    'info_message': 'Tutor insertado correctamente'
                })
            except:
                params = urllib.parse.urlencode({
                    'error_message': 'Error al insertar tutor'
                })

            return redirect('/Rol/tutores/' + '?%s' % params)

        else:
            return render(request, 'alta_tutor.html', {
                'info_message': None,
                'error_messages': None,
                'url_form': '/Rol/registrar_tutor/'
            })

    @login_required
    def modificar_tutor(request):
        if request.method == 'GET':
            tutor = Tutor.objects.get(id_tutor=request.GET.get('id_tutor'))

            return render(request, 'alta_tutor.html', {
                'info_message': None,
                'error_messages': None,
                'url_form': '/Rol/modificar_tutor/',
                'nombre': tutor.nombre,
                'apPaterno': tutor.apellido_paterno,
                'apMaterno': tutor.apellido_materno,
                'telefono': tutor.telefono,
                'email': tutor.correo_electronico,
                'sede': tutor.cede,
                'id_tutor': tutor.id_tutor
            })
        else:
            tutor = Tutor.objects.get(id_tutor=request.POST.get('id_tutor'))
            tutor.nombre = request.POST.get('nombre')
            tutor.apellido_paterno = request.POST.get('apPaterno')
            tutor.apellido_materno = request.POST.get('apMaterno')
            tutor.telefono = request.POST.get('telefono')
            tutor.correo_electronico = request.POST.get('email')
            tutor.cede = request.POST.get('sede')

            try:
                tutor.save()
                params = urllib.parse.urlencode({
                    'info_message': 'Tutor actualizado correctamente'
                })
            except:
                params = urllib.parse.urlencode({
                    'error_message': 'Error al actualizar tutor'
                })

            return redirect('/Rol/tutores/' + '?%s' % params)

    @login_required
    def eliminar_tutor(request):
        id_tutor = request.POST.get('id_tutor', None)

        try:
            tutor = Tutor.objects.get(id_tutor=id_tutor)
            tutor.delete()
            params = urllib.parse.urlencode({
                'info_message': 'Tutor eliminado correctamente'
            })
        except:
            params = urllib.parse.urlencode({
                'error_message': 'Error al eliminar tutor'
            })

        return redirect('/Rol/tutores/' + '?%s' % params)


class AsignacionRolPasante:

    @login_required
    def asignar_rol(request):
        try:
            if request.method == 'POST':
                id_pasante = request.POST.get('id_pasante')
                print('JSON' + str(request.POST))
                for key in request.POST.keys():
                    if 'comision' in key:
                        asignacion_rol = AsignacionRol()
                        id_rotacion = key.replace('comision', '')
                        comision = request.POST.get(key)
                        id_tutor = request.POST.get('tutor' + id_rotacion)

                        asignacion_rol.id_pasante_id = id_pasante
                        asignacion_rol.id_rotacion_id = id_rotacion
                        asignacion_rol.id_tutor_id = id_tutor
                        asignacion_rol.comision = comision
                        asignacion_rol.save()
                        
                        pasante = Pasante.objects.get(id_pasante=id_pasante)
                        
                        #Asignar Fechas para habilitar la calificacion
                        rotacion = Rotacion.objects.get(id_rotacion=id_rotacion)
                        
                        meses = rotacion.fecha_fin - rotacion.fecha_inicio 
                        
                        if meses  >= timedelta(10*30) :
                            
                            fecha_mediados = meses / 2
                            print(pasante)
                            print(rotacion)
                            print(rotacion.fecha_inicio + fecha_mediados)
                            print(rotacion.fecha_inicio + fecha_mediados+timedelta(7))

                            FechaCalificacion.objects.create(
                                id_pasante=pasante, 
                                id_rotacion = rotacion,
                                fecha_calif_inicio = rotacion.fecha_inicio + fecha_mediados,
                                fecha_calif_fin = rotacion.fecha_inicio + fecha_mediados+timedelta(7)
                                )
                            print('*******************')
                        print('*******************')
                        FechaCalificacion.objects.create(
                            id_pasante=pasante, 
                            id_rotacion = rotacion,
                            fecha_calif_inicio = rotacion.fecha_fin - timedelta(14),
                            fecha_calif_fin = rotacion.fecha_fin - timedelta(7)
                        )
                        print('*******************')
                    
                        


            params = urllib.parse.urlencode(
                {'rol_info_message': 'Se asignó el rol correctamente'})
        except:
            params = urllib.parse.urlencode(
                {'rol_error_message': 'Error al asignar rol'})

        return redirect('/Coordinador' + '?%s' % params)

    @login_required
    def configurar_asignacion(request):
        if request.method == 'GET':
            configuracion = ConfiguracionAsignacion.objects.all()

            if len(configuracion) > 0:
                configuracion = configuracion[0]
            else:
                configuracion = ConfiguracionAsignacion()

            inicio_promocion = configuracion.inicio_promocion
            inicio_promocion = None if inicio_promocion is None else inicio_promocion.__str__()

            final_promocion = configuracion.final_promocion
            final_promocion = None if final_promocion is None else final_promocion.__str__()

            acuerdo_asignacion = configuracion.acuerdo_asignacion
            acuerdo_asignacion = None if acuerdo_asignacion is None else acuerdo_asignacion.__str__()

            params = {
                'inicio_promocion': inicio_promocion,
                'final_promocion': final_promocion,
                'acuerdo_asignacion': acuerdo_asignacion,
                'error_message': request.GET.get('error_message'),
                'info_message': request.GET.get('info_message')
            }

            if configuracion.id_configuracion is not None:
                params['id_configuracion'] = configuracion.id_configuracion

            return render(request, 'configuracion_asignacion.html', params)
        else:
            id_configuracion = request.POST.get('id_configuracion', None)
            id_configuracion = None if len(
                id_configuracion) == 0 else id_configuracion

            if id_configuracion is None:
                configuracion = ConfiguracionAsignacion()
                configuracion.folio_count = 0
            else:
                configuracion = ConfiguracionAsignacion.objects.get(
                    id_configuracion=id_configuracion)

            inicio_promocion = request.POST.get('inicio_promocion', None)
            inicio_promocion = datetime.strptime(inicio_promocion, '%d/%m/%Y') \
                .strftime('%Y-%m-%d')

            final_promocion = request.POST.get('final_promocion', None)
            final_promocion = datetime.strptime(final_promocion, '%d/%m/%Y') \
                .strftime('%Y-%m-%d')

            acuerdo_asignacion = request.POST.get('acuerdo_asignacion', None)
            acuerdo_asignacion = datetime.strptime(acuerdo_asignacion,
                                                   '%d/%m/%Y') \
                .strftime('%Y-%m-%d')

            configuracion.inicio_promocion = inicio_promocion
            configuracion.final_promocion = final_promocion
            configuracion.acuerdo_asignacion = acuerdo_asignacion

            try:
                configuracion.save()
                params = urllib.parse.urlencode({
                    'info_message': 'La configuración se estableció '
                                    'correctamente'
                })
                return redirect(
                    '/Rol/configuracion_asignacion/' + '?%s' % params)
            except Exception as e:
                params = urllib.parse.urlencode({
                    'error_message': 'Error al establecer configuración'
                })
                return redirect(
                    '/Rol/configuracion_asignacion/' + '?%s' % params)

    

    @transaction.atomic
    @login_required
    def modificar_asignacion(request):
        try:
            form_data = request.POST

            id_pasante = request.POST.get('id_pasante')
            id_rol = request.POST.get('id_rol')
            beca = request.POST.get('beca')
            beca_modificada = len(beca) > 0

            # Elimina las asignaciones actuales del pasante
            AsignacionRol.objects.filter(id_pasante=id_pasante).delete()

            # Crea un nuevo rol para asignarselo al pasante
            actual_rol = Rol.objects.get(id_rol=id_rol)
            nuevo_rol = Rol()
            nuevo_rol.id_generacion_fk_id = actual_rol.id_generacion_fk_id

            generacion = Generacion.objects.get(
                id_generacion=actual_rol.id_generacion_fk_id)
            nuevo_rol.identificador = crear_identificador_rol(generacion)

            if beca_modificada:
                nuevo_rol.beca = beca
            else:
                nuevo_rol.beca = actual_rol.beca

            nuevo_rol.save()
            nuevo_rol = Rol.objects.latest('id_rol')

            # Recorre los datos del formulario para crear y asignar las rotaciones
            for key in request.POST.keys():
                if 'dateInicio' in key:
                    id_rotacion = key.replace('dateInicio', '')
                    date_inicio_key = key
                    date_fin_key = 'dateFin' + id_rotacion
                    clinica_key = 'cboxClinicas' + id_rotacion
                    comision_key = 'txtComision' + id_rotacion
                    tutor_key = 'cboxTutores' + id_rotacion
                    fecha_inicio = form_data.get(date_inicio_key)
                    fecha_inicio = datetime.strptime(fecha_inicio, '%d/%m/%Y') \
                        .strftime('%Y-%m-%d')
                    print(
                        'rotacion ' + id_rotacion + ', inicio: ' + fecha_inicio)
                    fecha_fin = form_data.get(date_fin_key)
                    fecha_fin = datetime.strptime(fecha_fin, '%d/%m/%Y') \
                        .strftime('%Y-%m-%d')
                    rotacion = Rotacion()
                    rotacion.fecha_inicio = fecha_inicio
                    rotacion.fecha_fin = fecha_fin
                    rotacion.id_clinica_fk_id = form_data.get(clinica_key)
                    rotacion.id_rol_fk_id = nuevo_rol.id_rol
                    rotacion.save()
                    rotacion = Rotacion.objects.latest('id_rotacion')
                    asignacion_rol = AsignacionRol()
                    asignacion_rol.comision = form_data.get(comision_key)
                    asignacion_rol.id_tutor_id = form_data.get(tutor_key)
                    asignacion_rol.id_pasante_id = id_pasante
                    asignacion_rol.id_rotacion_id = rotacion.id_rotacion
                    asignacion_rol.save()

            params = urllib.parse.urlencode(
                {'rol_info_message': 'Se modificó la asignación con éxito'})

        except Exception as e:
            print(e)
            params = urllib.parse.urlencode(
                {'rol_error_message': 'Error al modificar la asignación'})

        return redirect('/Coordinador' + '?%s' % params)

    # Este método no está asociado a ninguna vista, es solo para generar el
    # JSON de la asignación de un pasante
    def get_rol_pasante(id_pasante):
        if id_pasante is None:
            return JsonResponse({
                'error': 'Se esperaba recibir "id_pasante"'
            }, status=400)

        try:
            pasante = Pasante.objects.get(id_pasante=id_pasante)
        except Pasante.DoesNotExist:
            pasante = None

        if pasante is None:
            return JsonResponse({
                'error': 'El pasante no existe'
            }, status=400)

        asignaciones = AsignacionRol.objects.filter(
            id_pasante=pasante).order_by('id_rotacion__fecha_inicio')
        asignaciones_json = []

        for asignacion in asignaciones:
            rotaciones = Rotacion.objects.filter(
                id_rotacion=asignacion.id_rotacion_id)

            limite_inicio = HelperFechas.get_limite_fecha_inicio(
                rotaciones[0].id_rol_fk.id_generacion_fk)
            limite_fin = HelperFechas.get_limite_fecha_fin(
                rotaciones[0].id_rol_fk.id_generacion_fk)

            if len(rotaciones) > 0:
                rotacion_json = {
                    'id_rol': rotaciones[0].id_rol_fk_id,
                    'id_rotacion': rotaciones[0].id_rotacion,
                    'id_clinica': rotaciones[0].id_clinica_fk_id,
                    'id_institucion': rotaciones[
                        0].id_clinica_fk.id_institucion_fk_id,
                    'nombre_unidad': rotaciones[0].id_clinica_fk.nombre_unidad,
                    'fecha_inicio': rotaciones[0].fecha_inicio,
                    'fecha_fin': rotaciones[0].fecha_fin,
                    'limite_inicio': limite_inicio,
                    'limite_fin': limite_fin
                }
            else:
                rotacion_json = {}

            asignaciones_json.append({
                'id_asignacion': asignacion.id,
                'id_pasante': pasante.id_pasante,
                'rotacion': rotacion_json,
                'id_tutor': asignacion.id_tutor_id,
                'nombre_tutor': asignacion.id_tutor.nombre + ' ' +
                                asignacion.id_tutor.apellido_paterno + ' ' +
                                asignacion.id_tutor.apellido_materno,
                'comision': asignacion.comision
            })

        beca = asignaciones[0].id_rotacion.id_rol_fk.beca
        asignaciones_json.append({'beca': beca})

        return asignaciones_json

    @login_required
    def rol_pasante_json(request):
        id_pasante = request.GET.get('id_pasante', None)

        return JsonResponse(AsignacionRolPasante.get_rol_pasante(id_pasante),
                            safe=False, status=200)

    def generate_tabla_asignacion(id_pasante, c_style):
        asignacion_rol = AsignacionRol.objects.filter(
            id_pasante=id_pasante).order_by('id_rotacion__fecha_inicio')

        institucion = asignacion_rol[0].id_rotacion.id_clinica_fk \
            .id_institucion_fk.nombre_institucion

        import locale
        locale.setlocale(locale.LC_TIME, 'es_MX.UTF-8')

        rotaciones = [Paragraph('%s (%s al %s)' % (
            asignacion.id_rotacion.id_clinica_fk.nombre_unidad,
            asignacion.id_rotacion.fecha_inicio.strftime('%d de %B de %Y'),
            asignacion.id_rotacion.fecha_fin.strftime('%d de %B de %Y')),
                                c_style) for asignacion in asignacion_rol]

        datos_asignacion = [
            ('INSTITUCIÓN', Paragraph(institucion, c_style)),
            # ('SEDE', '-----'),
            ('ROTACIONES', rotaciones),
            ('TIPO', 'Rotatorio'),
            ('ESTADO', 'Zacatecas')
        ]

        tabla_asignacion = Table(datos_asignacion, colWidths=[7 * cm, 7 * cm])

        tabla_asignacion.setStyle(TableStyle(
            [
                ('GRID', (0, 0), (-1, -1), 1, colors.black),
                ('FONTSIZE', (0, 0), (-1, -1), 10)
            ]
        ))

        return tabla_asignacion

    @login_required
    def carta_asignacion(request):
        id_pasante = request.GET.get('id_pasante')
        pasante = Pasante.objects.get(id_pasante=id_pasante)
        nombre_pasante = pasante.nombre + ' ' + pasante.apellido_paterno + \
                         ' ' + pasante.apellido_materno

        filename = 'AsignaciónInformativa_' + str(
            pasante.id_usuario_fk.username) + '.pdf'

        response = HttpResponse(content_type='application/pdf')
        response[
            'Content-Disposition'] = 'attachment;filename="' + filename + '"'
        buffer = BytesIO()
        doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=40,
                                leftMargin=40, topMargin=40, bottomMargin=18)

        story = []
        styles = getSampleStyleSheet()
        c_style = styles['BodyText']

        titulo_style = ParagraphStyle(
            'titulo_style',
            parent=styles['Heading2'],
            alignment=TA_CENTER
        )

        titulo = Paragraph('Plaza Asignada a ' + nombre_pasante, titulo_style)

        himage = settings.BASE_DIR + '/static/img/header_carta_informativa.png'
        header_image = Image(himage, 510, 111.8)

        story.append(header_image)

        for i in range(10):
            story.append(Paragraph('', c_style))

        story.append(titulo)

        story.append(Paragraph('', c_style))
        story.append(Paragraph('', c_style))

        tabla_asignacion = AsignacionRolPasante.generate_tabla_asignacion(
            id_pasante, c_style)

        story.append(tabla_asignacion)
        story.append(Paragraph('', c_style))
        story.append(Paragraph('', c_style))

        for i in range(30):
            story.append(Paragraph('', c_style))

        nota_style = ParagraphStyle(
            'nota_style',
            parent=c_style,
            alignment=TA_RIGHT
        )

        nota = 'Página informativa, sin validez oficial'

        story.append(Paragraph(nota, nota_style))

        try:
            doc.build(story)
            pdf = buffer.getvalue()
            response.write(pdf)
            buffer.close()
        except Exception as e:
            print(e)
        return response

    @login_required
    def carta_asignacion_oficial(request):
        id_pasante = request.GET.get('id_pasante')
        pasante = Pasante.objects.get(id_pasante=id_pasante)
        nombre_pasante = pasante.nombre + ' ' + pasante.apellido_paterno + \
                         ' ' + pasante.apellido_materno

        carta_data = AsignacionRolPasante.get_carta_oficial_automatic_data()

        filename = 'AsignaciónOficial_' + str(
            pasante.id_usuario_fk.username) + '.pdf'

        response = HttpResponse(content_type='application/pdf')
        response[
            'Content-Disposition'] = 'attachment;filename="' + filename + '"'
        buffer = BytesIO()
        doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=2.5 * cm,
                                leftMargin=2.5 * cm, topMargin=2.5 * cm,
                                bottomMargin=1 * cm)

        story = []
        styles = getSampleStyleSheet()

        c_style = ParagraphStyle(
            'c_style',
            parent=styles['BodyText'],
            alignment=TA_JUSTIFY,
        )

        p_style = ParagraphStyle(
            'p_style',
            parent=c_style,
            firstLineIndent=1 * cm,
        )

        titulo_style = ParagraphStyle(
            'titulo_style',
            parent=styles['Heading2'],
            alignment=TA_LEFT
        )

        right_style = ParagraphStyle(
            'right_style',
            parent=c_style,
            alignment=TA_RIGHT
        )

        center_style = ParagraphStyle(
            'center_style',
            parent=c_style,
            alignment=TA_CENTER
        )

        too_small_style = ParagraphStyle(
            'too_small_style',
            parent=c_style,
            alignment=TA_LEFT,
            fontSize=7,
            spaceAfter=0,
            spaceBefore=0
        )

        folio = Paragraph('Oficio: %d' % (carta_data['folio_count']),
                          right_style)
        story.append(folio)

        licenciatura = Paragraph('Licenciatura', right_style)
        story.append(licenciatura)

        titulo = Paragraph('MPSS. ' + nombre_pasante, titulo_style)
        story.append(titulo)

        presente = Paragraph('PRESENTE', titulo_style)
        story.append(presente)
        story.append(Paragraph('', c_style))

        parrafo_1 = Paragraph('A través de la presente hago de su '
                              'conocimiento que de acuerdo al evento de '
                              'asignación de plazas de Servicio Social '
                              'para la promoción de %s al %s, realizado el '
                              'dia %s a usted le fue asignada la '
                              'siguiente plaza:' % (
                                  carta_data['inicio_promocion'],
                                  carta_data['final_promocion'],
                                  carta_data['acuerdo_asignacion']), p_style)
        story.append(parrafo_1)

        for i in range(2):
            story.append(Paragraph('', c_style))

        tabla_asignacion = AsignacionRolPasante.generate_tabla_asignacion(
            id_pasante, c_style)
        story.append(tabla_asignacion)

        for i in range(2):
            story.append(Paragraph('', c_style))

        parrafo_2 = Paragraph('Por lo anterior y de acuerdo a lo '
                              'institucionalmente establecido, Usted debe de '
                              'realizar el séptimo año escolar de la '
                              'Licenciatura de Médico General dentro del '
                              'Servicio Social en la plaza que le fue '
                              'asignada en el evento público, a fin de que, '
                              'a la conclusión del periodo referido, le sea '
                              'reconocida su preparación por la Institución de '
                              'salud de adscripción y por ésta Unidad '
                              'Académica.', p_style)
        story.append(parrafo_2)
        story.append(Paragraph('', c_style))

        parrafo_3 = Paragraph('Así mismo esta plaza por ningún motivo podrá '
                              'ser transferida sin autorización y aprobación '
                              'previa por parte de la Unidad Académica.',
                              p_style)
        story.append(parrafo_3)
        story.append(Paragraph('', c_style))

        parrafo_4 = Paragraph('Hago de su conocimiento lo anterior, para el '
                              'cumplimiento respectivo.', c_style)
        story.append(parrafo_4)

        for i in range(4):
            story.append(Paragraph('', c_style))

        atentamente = Paragraph('Atentamente', center_style)
        story.append(atentamente)

        hombre = Paragraph('"El Hombre por el Hombre"', center_style)
        story.append(hombre)

        fecha = Paragraph('Zacatecas, Zac., a %s' % (carta_data['today']),
                          center_style)
        story.append(fecha)

        for i in range(6):
            story.append(Paragraph('', c_style))

        doctor = Paragraph('Dr. William Humberto Ortiz Briceño', center_style)
        story.append(doctor)

        responsable = Paragraph('Responsable del programa de Licenciatura',
                                center_style)
        story.append(responsable)

        responsable_2 = Paragraph('de Médico General de la UAMHyCS',
                                  center_style)
        story.append(responsable_2)
        story.append(Paragraph('', c_style))

        ccp_1 = Paragraph('C.c.p.: Institucion Sede', too_small_style)
        story.append(ccp_1)

        ccp_2 = Paragraph('Enseñanza Estatal de Secretaria de Salud',
                          too_small_style)
        story.append(ccp_2)

        ccp_3 = Paragraph('Servicios Escolares Medicina Humana',
                          too_small_style)
        story.append(ccp_3)

        ccp_4 = Paragraph('Archivo', too_small_style)
        story.append(ccp_4)

        try:
            doc.build(story)
            pdf = buffer.getvalue()
            response.write(pdf)
            buffer.close()
        except Exception as e:
            print(e)
        return response

    def get_carta_oficial_automatic_data():
        import locale
        locale.setlocale(locale.LC_TIME, 'es_MX.UTF-8')

        configuracion = ConfiguracionAsignacion.objects.all()
        config_data = {}

        tz = pytz.timezone('America/Monterrey')
        today = datetime.now(tz).strftime('%d de %B de %Y')

        if len(configuracion) == 0:

            config_data['inicio_promocion'] = today
            config_data['final_promocion'] = today
            config_data['acuerdo_asignacion'] = today
            config_data['folio_count'] = 0
        else:
            configuracion = configuracion[0]
            inicio = configuracion.inicio_promocion.strftime('%d de %B de %Y')
            fin = configuracion.final_promocion.strftime('%d de %B de %Y')
            acuerdo = configuracion.acuerdo_asignacion.strftime(
                '%d de %B de %Y')
            config_data['inicio_promocion'] = inicio
            config_data['final_promocion'] = fin
            config_data['acuerdo_asignacion'] = acuerdo
            config_data['folio_count'] = configuracion.folio_count + 1
            configuracion.folio_count = configuracion.folio_count + 1
            configuracion.save()

        config_data['today'] = today

        return config_data
