from django.db import models
from foro.models import Categorias
from usuario.models import Usuario

class Experto(Usuario):
    id_experto = models.IntegerField(primary_key=True)
    nombre = models.CharField(max_length=50)
    ap_materno = models.CharField(max_length=50)
    ap_paterno = models.CharField(max_length=50)
    teléfono = models.CharField(max_length=10)
    especialidad = models.CharField(max_length=60)
    activo = models.BooleanField(default=False)
    id_categoria = models.ForeignKey(Categorias, on_delete=models.CASCADE)
