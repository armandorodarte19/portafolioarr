from pasante.models import Pasante
from rest_framework import serializers

class PasanteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pasante
        fields = ('id_pasante','nombre','apellido_paterno','apellido_materno', 'rfc', 'status', 'foto', 'id_direccion_fk', 'id_datos_academicos_fk', 'id_usuario_fk')