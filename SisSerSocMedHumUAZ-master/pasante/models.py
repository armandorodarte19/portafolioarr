from django.db import models
from usuario.models import Usuario
from clinica.models import Clinica

class DireccionPasante(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    numero = models.CharField(max_length=6, null=True)
    calle = models.CharField(max_length=50)
    colonia = models.CharField(max_length=50)
    municipio = models.ForeignKey('coordinador.Municipio', on_delete=models.CASCADE,)
    estado = models.ForeignKey('coordinador.Estado', on_delete=models.CASCADE,)


class DatosAcademicos(models.Model):
    id_datos_academicos = models.AutoField(primary_key=True)
    promedio = models.FloatField(max_length=5)


class Pasante(models.Model):
    id_pasante = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido_paterno = models.CharField(max_length=50)
    apellido_materno = models.CharField(max_length=50)
    curp = models.CharField(max_length=18)
    rfc = models.CharField(max_length=13)
    status = models.CharField(max_length=18)
    foto = models.ImageField(upload_to="fotos_pasantes/", blank=True, null=True)
    id_direccion_fk = models.ForeignKey(DireccionPasante, on_delete=models.CASCADE,)
    id_datos_academicos_fk = models.ForeignKey(DatosAcademicos, on_delete=models.CASCADE,)
    id_usuario_fk = models.ForeignKey(Usuario, on_delete=models.CASCADE,)


class Telefono(models.Model):
    id_telefono = models.AutoField(primary_key=True)
    numero = models.CharField(max_length=15)
    tipo = models.CharField(max_length=10)
    id_pasante_fk = models.ForeignKey(Pasante, on_delete=models.CASCADE,)


class Generacion(models.Model):
    id_generacion = models.AutoField(primary_key=True)
    generacion = models.CharField(u'generacion', max_length=30)
    anio = models.CharField(u'año', max_length=4)

    def __str__(self):
        return self.generacion+' '+self.anio

class Promocion(models.Model):
    id_promocion = models.AutoField(primary_key=True)
    id_pasante_fk = models.ForeignKey(Pasante, on_delete=models.CASCADE,)
    id_generacion_fk = models.ForeignKey(Generacion, on_delete=models.CASCADE,)
        
class Rubro(models.Model):
    id_rubro = models.AutoField(primary_key=True)
    nombre_rubro = models.CharField(max_length=20)

    def __str__(self):
        return '{}'.format(self.nombre_rubro)

class CalificacionPasante(models.Model):
    id_calificacion_pasante = models.AutoField(primary_key=True)
    id_pasante_fk = models.ForeignKey(Pasante, on_delete=models.CASCADE,)
    id_clinica_fk = models.ForeignKey(Clinica, on_delete=models.CASCADE,)
    id_rubro_fk = models.ForeignKey(Rubro, on_delete=models.CASCADE,)
    calificacion = models.FloatField(max_length=20)

class ComentarioPasante(models.Model):
    id_comentario_pasante = models.AutoField(primary_key=True)
    id_pasante_fk = models.ForeignKey(Pasante, on_delete=models.CASCADE,)
    id_clinica_fk = models.ForeignKey(Clinica, on_delete=models.CASCADE,)
    id_rubro_fk = models.ForeignKey(Rubro, on_delete=models.CASCADE,)
    comentario = models.CharField(max_length=280)
    fecha = models.DateField(null=False)

################################################################################
'''
from django.db import models
from usuario.models import Usuario


class DireccionPasante(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    numero = models.CharField( max_length=6, null=True)
    calle = models.CharField(max_length=50)
    colonia = models.CharField(max_length=50)
    municipio = models.ForeignKey('coordinador.Municipio')
    estado = models.ForeignKey('coordinador.Estado')


class DatosAcademicos(models.Model):
    id_datos_academicos = models.AutoField(primary_key=True)
    promedio = models.FloatField(max_length=5)


class Generacion(models.Model):
    id_generacion = models.AutoField(primary_key=True)
    generacion = models.CharField(max_length=30)
    anio = models.CharField(u'año', max_length=4)
    id_pasante_fk = models.ForeignKey(Pasante)


class Pasante(models.Model):
    id_pasante = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido_paterno = models.CharField(max_length=50)
    apellido_materno = models.CharField(max_length=50)
    curp = models.CharField(max_length=18)
    rfc = models.CharField(max_length=13)
    status = models.CharField(max_length=18)
    foto = models.ImageField(upload_to="fotos_pasantes/", blank=True, null=True)
    id_direccion_fk = models.ForeignKey(DireccionPasante)
    id_datos_academicos_fk = models.ForeignKey(DatosAcademicos)
    id_usuario_fk = models.ForeignKey(Usuario)
    id_generacion_fk = models.ForeignKey(Generacion, null=True)


class Telefono(models.Model):
    id_telefono = models.AutoField(primary_key=True)
    numero = models.CharField(max_length=15)
    tipo = models.CharField(max_length=10)
    id_pasante_fk = models.ForeignKey(Pasante)

'''
################################################################################
'''
from django.db import models
from usuario.models import Usuario


class DireccionPasante(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    numero = models.CharField(u'numero', max_length=6, null=True)
    calle = models.CharField(u'calle', max_length=50)
    colonia = models.CharField(u'colonia', max_length=50)
    municipio = models.ForeignKey('coordinador.Municipio')
    estado = models.ForeignKey('coordinador.Estado')


class DatosAcademicos(models.Model):
    id_datos_academicos = models.AutoField(primary_key=True)
    promedio = models.FloatField(u'promedio', max_length=5)


class Generacion(models.Model):
    id_generacion = models.AutoField(primary_key=True)
    generacion = models.CharField(u'generacion', max_length=30)
    anio = models.CharField(u'año', max_length=4)


class Pasante(models.Model):
    id_pasante = models.AutoField(primary_key=True)
    nombre = models.CharField(u'nombre', max_length=50)
    apellido_paterno = models.CharField(u'apellido_paterno', max_length=50)
    apellido_materno = models.CharField(u'apellido_materno', max_length=50)
    curp = models.CharField(u'curp', max_length=18)
    rfc = models.CharField(u'rfc', max_length=13)
    status = models.CharField(u'status', max_length=18)
    foto = models.ImageField('Foto', upload_to="fotos_pasantes/", blank=True,
                             null=True)
    id_direccion_fk = models.ForeignKey(DireccionPasante)
    id_datos_academicos_fk = models.ForeignKey(DatosAcademicos)
    id_usuario_fk = models.ForeignKey(Usuario)
    id_generacion_fk = models.ForeignKey(Generacion, null=True)


class Telefono(models.Model):
    id_telefono = models.AutoField(primary_key=True)
    numero = models.CharField(u'numero', max_length=15)
    tipo = models.CharField(u'tipo', max_length=10)
    id_pasante_fk = models.ForeignKey(Pasante)

'''
