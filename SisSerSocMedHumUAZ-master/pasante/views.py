from django.shortcuts import render, redirect, render_to_response, \
    get_object_or_404
from pasante.forms import *
from pasante.models import *
from clinica.models import *
from rol.models import Rol, AsignacionRol
from django.contrib import messages
from coordinador.models import AsignacionClinicaPasante, fecha_registro
from clinica.models import DireccionClinica, Clinica
from django.core.exceptions import ObjectDoesNotExist
from rol.models import AsignacionRol, Rol, Rotacion, Tutor, FechaCalificacion
from coordinador.forms import *
from coordinador.models import Estado, Municipio
from coordinador.forms import FormInformacion
from coordinador.models import Coordinador
from contactos_pasante.models import ContactosPasante

from foro.models import Pregunta_Foro
from experto.models import Experto
from random import randint

from usuario.models import Usuario
from django.contrib.auth.hashers import *
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from coordinador.models import Estado, Municipio ,fecha_registro, ReporteInconformidad
import json
from coordinador.models import Informacion as InformacionModel
from coordinador.models import ReporteInconformidad,ComentariosInconformidad
from coordinador.models import Catalogo_Preguntas
from datetime import datetime, timedelta , date
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from notificacion_pregunta.views import NotificarRespuesta

def error_404(request):
    return render(request, 'error_404.html', status=404)

def error_500(request):
    return render(request, 'error_500.html', status=500)

def entre_fechas(request):
    current_user = request.user
    id_pasante = int(current_user.id)


    hoy = datetime.now().date()
    if Pasante.objects.filter(id_usuario_fk_id__exact=id_pasante).exists():
        pasante = Pasante.objects.get(id_usuario_fk_id__exact=id_pasante)

        asignaciones=AsignacionRol.objects.filter(id_pasante__exact=pasante.id_pasante)


        for asignacion in asignaciones:
            inicio = asignacion.id_rotacion.fecha_inicio
            fin = asignacion.id_rotacion.fecha_fin
            if hoy >= inicio and hoy <= fin:
                return True

    return False

def getPasantesAsignaciones(request):
    roles = Rol.objects.all()
    pasantes_con_asignaciones = set()
    for rol in roles:
        asignaciones = AsignacionRol.objects.filter(
            id_rotacion__id_rol_fk__id_rol=rol.id_rol).order_by(
            'id_rotacion__fecha_inicio')

        for asignacion in asignaciones:
            pasantes_con_asignaciones.add(asignacion.id_pasante)
    return pasantes_con_asignaciones


@login_required
def activar_alerta(request):
    usuario = Pasante.objects.filter(
        id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
            0].id_usuario)
    pasante_p = usuario[0] if len(usuario) > 0 else None
    return render(request, 'activar_alerta.html', { 'matricula': Usuario.objects.get(id_usuario=request.user.id).matricula,'pasantes_asignados': getPasantesAsignaciones(request),
                     'pasante_p': pasante_p })

class NotificacionExperto:
    def notificar_experto(self, id_experto):
        return Pregunta_Foro.objects.filter(id_categoria_fk_id=id_experto.id_categoria) & Pregunta_Foro.objects.filter(estado=1)

class Contactos():

    @login_required
    def borrar_contactos(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        if request.method == 'GET':
            id = request.GET['id']
            contacto = ContactosPasante.objects.get(id_contacto=id)
            contacto.delete()

        return render(request, 'activar_alerta.html',{'pasantes_asignados': getPasantesAsignaciones(request),
                         'pasante_p': pasante_p})

    @login_required
    def editar_contactos(request):
        id_contacto = request.GET['id']
        nombre = request.GET.get('nombre', None)
        numero = request.GET.get('numero', None)

        if request.method == 'GET' and nombre != None and numero != None:
            contacto = ContactosPasante.objects.get(id_contacto=id_contacto)

            contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk=contacto.id_pasante_fk)
            for c in contactos_pasante:
                if numero == c.numero and str(id_contacto) != str(c.id_contacto):
                    return HttpResponse("REPETIDO")

            contacto.nombre = nombre
            contacto.numero = numero
            contacto.save()
            return redirect('activar_alerta')

        if request.is_ajax():
          html = render_to_string('edit-contacts.html',
              {
                  'contacto': ContactosPasante.objects.get(id_contacto=id_contacto)
              }
          )
          return HttpResponse(html)

    @login_required
    def obtener_contactos(request):
        pasante = Pasante.objects.get(id_usuario_fk_id=request.user.id)
        contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk_id=pasante.id_pasante)
        html = render_to_string('get-contacts.html',
            {
                'contactos': contactos_pasante,
            }
        )
        return HttpResponse(html)

    @login_required
    def insertar_contactos(request):
        pasante = Pasante.objects.get(id_usuario_fk_id=request.user.id)
        nombre = request.GET.get('nombre', None)
        numero = request.GET.get('numero', None)

        if request.method == 'GET' and nombre != None and numero != None:

            contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk=pasante.id_pasante)
            for contacto in contactos_pasante:
                if numero == contacto.numero:
                    return HttpResponse("REPETIDO")

            contacto = ContactosPasante(nombre=nombre, numero=numero, id_pasante_fk=pasante)
            contacto.save()
            return HttpResponse("OK")


        if request.is_ajax():
            html = render_to_string('add-contacts.html',
                {
                    'contactos': ContactosPasante.objects.filter(id_pasante_fk_id=pasante.id_pasante)
                }
            )
            return HttpResponse(html)







@login_required
def inicio_pasante(request):
    # el valor de id_usuario_fk debe ser el id del pasante que se ingresó
    usuario = Pasante.objects.filter(
        id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
            0].id_usuario)
    pasante_p = usuario[0] if len(usuario) > 0 else None
    pasantes_con_asignaciones = getPasantesAsignaciones(request)
    #Codigo para la notificacion experto
    usuarioActual = Usuario.objects.get(user_ptr_id=request.user.id)
    contPregPendExp = -1
    if request.user.is_staff == 0:
        if usuarioActual.tipo_usuario == 'Experto':
            print('experto')
            experto = Experto.objects.get(id_usuario=usuarioActual.id)
            notificador = NotificacionExperto()
            preguntasE = notificador.notificar_experto(experto)
            contPregPendExp = len(preguntasE)
    else:
        contPregPendExp = -1

    #codigo para habilitar la clinica
    habilitar_clinica = False
    if pasante_p:
        fechas_calificacion = FechaCalificacion.objects.filter(id_pasante=usuario[0].id_pasante)

        hoy = datetime.today().date()
        #fecha harcodeada para pruebas
        #hoy = datetime(2018,7,2).date()
        fecha_calificacion = None
        for fecha in fechas_calificacion:
            if fecha.fecha_calif_inicio <= hoy and fecha.fecha_calif_fin >= hoy and not fecha.calificado and not habilitar_clinica:
                habilitar_clinica = True

            if not (fecha.fecha_calif_inicio <= hoy and fecha.fecha_calif_fin >= hoy) and not fecha.calificado and not fecha_calificacion:
                fecha_calificacion = fecha.fecha_calif_inicio

        #fecha_calificacion = FechaCalificacion.objects.filter(id_pasante=usuario[0].id_pasante,calificado=False)
        #if fecha_calificacion :
        #    fecha_calificacion = fecha_calificacion[0].fecha_calif_inicio
    if len(usuario) > 0:
        texto = NotificarRespuesta.notificar_respuesta(request)
        return render(request, 'inicio_pasante.html', {
            'tiene_datos': True,
            'activo': usuario,
            'pasantes_asignados': getPasantesAsignaciones(request),
            'entre_fechas': entre_fechas(request),
            'pasante_p': pasante_p,
            'habilitar_clinica':habilitar_clinica,
            'fecha_calificacion':fecha_calificacion,
            'contPregPendExp': contPregPendExp,
            'texto':texto
            })
    else:
        try:
          fecha=fecha_registro.objects.last()
        except Exception as e:
          fecha=0
          print (fecha)

        if fecha==None:
          print("entro")
          texto = NotificarRespuesta.notificar_respuesta(request)
          return render(request, 'inicio_pasante.html', {
              'tiene_datos': False,
              'activo': usuario,
              'entre_fechas': entre_fechas(request),
              'pasantes_asignados': pasantes_con_asignaciones,
              'pasante_p': pasante_p,
              'habilitar_clinica':habilitar_clinica,
              'contPregPendExp': contPregPendExp,
              'texto':texto,

          })

        else:
          print ("entro 2.0")
          print(fecha)
          texto = NotificarRespuesta.notificar_respuesta(request)

        return render(request, 'inicio_pasante.html', {
            'tiene_datos': False,
            'fecha_inicio':fecha.fecha_inicio,
            'fecha_fin':fecha.fecha_fin,
            'fecha_hoy':date.today(),
            'activo': usuario,
            'entre_fechas': entre_fechas(request),
            'pasantes_asignados': pasantes_con_asignaciones,
            'pasante_p': pasante_p,
            'habilitar_clinica':habilitar_clinica,
            'contPregPendExp': contPregPendExp,
            'texto':texto,

        })


class AltaPasante:
    @login_required
    def registrar_pasante(request):
        """función que realiza el registro de un pasante
        si todo es correcto devuelve un mensaje de el pasante se registró
        en caso contrario muestra los diferentes mensajes de error.
        """

        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        ahora = datetime.now()
        formato_fecha = "%d-%m-%Y"
        fecha_inicio_agosto = datetime.strptime("01-02-" + str(ahora.year),
                                          formato_fecha)
        fecha_fin_agosto = datetime.strptime("31-08-" + str(ahora.year),
                                          formato_fecha)
        datos_usuario = Usuario.objects.get(
            id_usuario=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        if request.method == 'POST':
            form = FormPasante(request.POST, request.FILES)
            estados = Estado.objects.all()

            if (len(usuario) > 0):

                telefonos_pasante = Telefono.objects.filter(
                    id_pasante_fk_id=usuario[0].id_pasante)
                datos_academicos_pasante = DatosAcademicos.objects.get(
                    pk=usuario[0].id_datos_academicos_fk_id)
                datos_usuario = Usuario.objects.get(
                    id_usuario=usuario[0].id_usuario_fk_id)
                direccion_pasante = DireccionPasante.objects.get(
                    pk=usuario[0].id_direccion_fk_id)
                est = direccion_pasante.estado.id_estado
                mun = direccion_pasante.municipio.id_municipio
                fijo = None
                movil = None
                for i in telefonos_pasante:
                    if (i.tipo == "fijo"):
                        fijo = i
                    elif (i.tipo == "movil"):
                        movil = i
                form = FormPasante(
                    initial=recarga_form_inicial_pasante(usuario[0],
                                                         direccion_pasante,
                                                         fijo, movil,
                                                         datos_academicos_pasante))
                return render(request, 'registrar_pasante.html',
                              {'form': form,
                               'funcion': 'Tus datos han sido registrados.',
                               'usuario': datos_usuario,
                               'boolean': True,
                               'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                               'estados': estados,
                               'mun': mun,
                               'est': est})
            else:
                if form.is_valid():
                    formFoto = FormPasante(request.POST, request.FILES)
                    id_estado = request.POST.get('estados')
                    id_municipio = request.POST.get('municipios')
                    print(id_estado, id_municipio)
                    # validación nombre
                    nombre = ""
                    if (es_alfabetica(form.data['nombre'])):
                        nombre = form.data['nombre']
                        if (nombre == ""):
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'nombre': 'El campo "Nombre" no debe ser vacío.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})
                    else:
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'nombre': '"Nombre" contiene números o caracteres especiales.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})

                    # validación apellido paterno
                    ap = ""
                    if (es_alfabetica(form.data['apellido_paterno'])):
                        ap = form.data['apellido_paterno']
                        if (ap == ""):
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'ap': 'El campo "Apellido Paterno" no debe ser vacío.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})
                    else:
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'ap': '"Apellido Paterno" contiene números o caracteres especiales.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    # validación apellido materno
                    am = ""
                    if (form.data['apellido_materno'] == ""):
                        pass
                    else:
                        if (es_alfabetica(form.data['apellido_materno'])):
                            am = form.data['apellido_materno']

                        else:
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'am': '"Apellido Materno" contiene números o caracteres especiales.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})

                    # validación curp

                    curp = form.data['curp']
                    if (curp == ""):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'curp': 'El campo "CURP" no debe ser vacío.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    if (Pasante.objects.filter(curp=curp).exists()):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'curp': 'La CURP ingresada ya se encuentra registrada.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    if (len(curp) < 18):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'curp': '"CURP" debe contener 18 Caracteres',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})

                    # validación rfc.
                    rfc = form.data['rfc']
                    if (rfc == ""):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'rfc': 'El campo "RFC" no debe ser vacío.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    if (Pasante.objects.filter(rfc=rfc).exists()):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'rfc': 'El RFC ingresado ya se encuentra registrado.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    if (len(rfc) < 10):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'rfc': '"RFC" debe contener como mínimo 10 Digitos, máximo 13',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    # Validar foto------------------------------------------------------

                    try:
                        if (request.FILES.get('foto') != None):
                            if (
                                    not request.FILES.get(
                                        'foto').name.lower().endswith(
                                        '.jpg')):
                                return render(request, 'registrar_pasante.html',
                                              {'form': form,
                                               'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                               'rfc': 'Debes de seleccionar una imagen jpg.',
                                               'usuario': datos_usuario,
                                               'estados': estados,
                                               'est': id_estado,
                                               'mun': id_municipio})
                            foto = form.files['foto']
                        else:
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'promedio_rango': 'El campo "Foto" no debe ser vacío.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})

                    except (KeyError):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'promedio_rango': 'El campo "Foto" no debe ser vacío.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    # Validación promedio.------------------------------------------------
                    promedio = form.data['promedio_final']
                    if (form.data['promedio_final'] != ""):

                        # if(promedio.isdigit() == True):
                        try:
                            promedio = form.data['promedio_final']
                            if (float(promedio) > 10 or float(promedio) < 0):
                                return render(request, 'registrar_pasante.html',
                                              {'form': form,
                                               'promedio_rango': '"Promedio" debe estar en un rango de 0 y 10.',
                                               'usuario': datos_usuario,
                                               'estados': estados,
                                               'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                               'est': id_estado,
                                               'mun': id_municipio})
                        except ValueError:
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'promedio_num': '"Promedio" no es numérico.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})

                    # validación número fijo.-------------------------------------------------
                    numero_fijo = form.data['número_fijo']
                    if (form.data['número_fijo'] != ""):
                        try:

                            numero_fijo = int(form.data['número_fijo'])
                            if (numero_fijo < 0):
                                return render(request, 'registrar_pasante.html',
                                              {'form': form,
                                               'numero_fijo': '"Número fijo" no debe contener valores negativos.',
                                               'usuario': datos_usuario,
                                               'estados': estados,
                                               'est': id_estado,
                                               'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                               'mun': id_municipio})

                            if (len(form.data['número_fijo']) < 7):
                                return render(request, 'registrar_pasante.html',
                                              {'form': form,
                                               'numero_fijo': '"Número fijo" debe contener por lo menos 7 Digitos.',
                                               'usuario': datos_usuario,
                                               'estados': estados,
                                               'est': id_estado,
                                               'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                               'mun': id_municipio})
                        except ValueError:
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'numero_fijo': '"Número fijo" no es numérico.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})
                    # validar número móvil--------------------------------------------------------
                    try:
                        if (form.data['número_móvil'] == ""):
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'numero_movil': 'El campo "Número móvil" no debe ser vacío.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})
                        numero_movil = int(form.data['número_móvil'])

                        if (numero_movil < 0):
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'numero_movil': '"Número móvil" no debe contener valores negativos.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})
                        if (len(form.data['número_móvil']) < 10):
                            return render(request, 'registrar_pasante.html',
                                          {'form': form,
                                           'numero_movil': '"Número móvil" debe contener por lo menos 10 Digitos.',
                                           'usuario': datos_usuario,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio})
                    except ValueError:
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'numero_movil': '"Número móvil" no es numérico.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})

                    # validación calle----------------------------------------------------
                    calle = form.data['calle']
                    if (calle == ""):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'estados_error': 'El campo "Calle" no debe ser vacío.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    numero_exterior = form.data['número_exterior']
                    # validación colonia-------------------------------------------------
                    colonia = form.data['colonia']
                    if (colonia == ""):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'estados_error': 'El campo "Colonia" no debe ser vacío.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})

                    if (id_estado == "vacio"):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'estados_error': 'Debes seleccionar un "Estado" de la lista.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})
                    if (id_municipio == "vacio"):
                        return render(request, 'registrar_pasante.html',
                                      {'form': form,
                                       'municipios_error': 'Debes seleccionar un "Municipio" de la lista.',
                                       'usuario': datos_usuario,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio})

                    # se guarda dirección del pasante
                    direccion_pasante = DireccionPasante(numero=numero_exterior,
                                                         calle=calle,
                                                         colonia=colonia,
                                                         municipio=Municipio.objects.get(
                                                             pk=id_municipio),
                                                         estado=Estado.objects.get(
                                                             pk=id_estado))
                    direccion_pasante.save()
                    # se guardan datos academicos
                    if (promedio == ""):
                        datos_academicos = DatosAcademicos(promedio=0)
                        datos_academicos.save()
                    else:
                        datos_academicos = DatosAcademicos(promedio=promedio)
                        datos_academicos.save()

                    pasante = Pasante(nombre=nombre, apellido_paterno=ap,
                                      apellido_materno=am, curp=curp,
                                      rfc=rfc, status='desactivado',
                                      id_direccion_fk=direccion_pasante,
                                      id_datos_academicos_fk=datos_academicos,
                                      id_usuario_fk=Usuario.objects.get(
                                          username=request.user.username),
                                      foto=foto)

                    # se guarda al pasante
                    pasante.save()
                    # se guarda la generación
                    usuario = User.objects.get(id=request.user.id)

                    obtener_fechas = usuario.date_joined.strftime(formato_fecha)
                    fecha_user = datetime.strptime(obtener_fechas,
                                                   formato_fecha)
                    fecha_user = datetime.today()
                    generacion_correspondiente = ""
                    anio_correspondiente = ""
                    if (fecha_user < fecha_inicio_agosto):
                        generacion_correspondiente = "Febrero"
                        anio_correspondiente = str(ahora.year)

                    elif (fecha_user >= fecha_inicio_agosto and fecha_user < fecha_fin_agosto):
                        generacion_correspondiente = "Agosto"
                        anio_correspondiente = str(ahora.year)
                    else:
                        generacion_correspondiente = "Febrero"
                        anio_correspondiente = str(ahora.year+1)

                    generacion_asignada = None
                    try:
                        generacion_asignada = Generacion.objects.get(generacion=generacion_correspondiente,
                                                                    anio = anio_correspondiente)
                    except Generacion.DoesNotExist as e:
                        generacion_asignada = Generacion.objects.create(generacion=generacion_correspondiente,
                                                anio=anio_correspondiente)

                    Promocion.objects.create(id_pasante_fk = pasante,
                                             id_generacion_fk = generacion_asignada)
                    # se guardan teléfonos del pasante.
                    telefono1 = Telefono(numero=str(numero_fijo), tipo="fijo",
                                         id_pasante_fk=pasante)
                    telefono2 = Telefono(numero=str(numero_movil), tipo="movil",
                                         id_pasante_fk=pasante)
                    telefono1.save()
                    telefono2.save()
                    # de esta manera le mostramos que datos ingreso
                    # boolean: indica que se registro
                    usuario = Pasante.objects.filter(id_usuario_fk=
                                                     Usuario.objects.filter(
                                                         user_ptr_id=request.user.id)[
                                                         0].id_usuario)
                    telefonos_pasante = Telefono.objects.filter(
                        id_pasante_fk_id=usuario[0].id_pasante)
                    datos_academicos_pasante = DatosAcademicos.objects.get(
                        pk=usuario[0].id_datos_academicos_fk_id)
                    datos_usuario = Usuario.objects.get(
                        id_usuario=usuario[0].id_usuario_fk_id)
                    direccion_pasante = DireccionPasante.objects.get(
                        pk=usuario[0].id_direccion_fk_id)
                    est = direccion_pasante.estado.id_estado
                    mun = direccion_pasante.municipio.id_municipio
                    fijo = ""
                    movil = ""
                    for i in telefonos_pasante:
                        if (i.tipo == "fijo"):
                            fijo = i
                        elif (i.tipo == "movil"):
                            movil = i
                    form = FormPasante(
                        initial=recarga_form_inicial_pasante(usuario[0],
                                                             direccion_pasante,
                                                             fijo, movil,
                                                             datos_academicos_pasante))
                    return render(request, 'registrar_pasante.html',
                                  {'form': form,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'funcion': 'Tus datos han sido registrados.',
                                   'usuario': datos_usuario, 'boolean': True,
                                   'estados': estados, 'mun': mun, 'est': est})
                else:
                    return render(request, 'registrar_pasante.html',
                                  {'form': form, 'usuario': datos_usuario,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'estados': estados, })
        else:
            # verificar que si el pasante ya llenó sus datos
            values = Pasante.objects.filter(id_usuario_fk=
                                            Usuario.objects.filter(
                                                user_ptr_id=request.user.id)[
                                                0].id_usuario)
            estados = Estado.objects.all()

            if (len(values) > 0):

                return redirect('inicio_pasante')
            else:
                form = FormPasante()
                return render(request, 'registrar_pasante.html',
                              {'form': form, 'usuario': datos_usuario,
                               'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                               'estados': estados})


class MuestraNormatividadPasante:
    @login_required
    def mostrar_normatividad_pasante(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        info = InformacionModel.objects.all()
        if (len(info) > 0):
            return render(request, 'mostrar_informacion_normatividad.html',
                          {'info': info, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})
        else:
            return render(request, 'mostrar_informacion_normatividad.html',
                          {'no_info': 'No hay archivos disponibles.', 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})

    @login_required
    def buscar_tema(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        info = InformacionModel.objects.all()
        if (len(info) > 0):
            return render(request, 'buscar_normatividad.html',
                          {'info': info, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})
        else:
            return render(request, 'buscar_normatividad.html',
                          {'no_info': 'No hay archivos disponibles.', 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})


class VerNormatividadPasante:
    @login_required
    def ver_normatividad_pasante(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        info = InformacionModel.objects.all()
        if (len(info) > 0):
            return render(request, 'ver_informacion_normatividad.html',
                          {'info': info, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})
        else:
            return render(request, 'ver_informacion_normatividad.html',
                          {'no_info': 'No hay archivos disponibles.', 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})


from django.http import HttpResponse
import simplejson


def carga_municipios(request):
    try:
        estado = Estado.objects.get(pk=request.GET['id_estado'])

        municipios = Municipio.objects.filter(id_estado=estado.id_estado)

        municipios_json = []
        for municipio in municipios:
            municipio_json = {'nombre': municipio.nombre,
                              'id': municipio.id_municipio}
            municipios_json.append(municipio_json)
        data = simplejson.dumps(municipios_json)
        return HttpResponse(data, content_type="application/json")
    except ValueError:
        return HttpResponse("/")


def es_alfabetica(cadena):
    separar = cadena.split(" ")
    n = ""
    for i in separar:
        n = n + i.strip()
    if (n.isalpha() or n == ""):
        return True
    else:
        return False


def recarga_form_inicial_pasante(datos_usuario, direccion_pasante, fijo, movil,
                                 datos_academicos):
    return {
        'nombre': datos_usuario.nombre,
        'apellido_paterno': datos_usuario.apellido_paterno,
        'apellido_materno': datos_usuario.apellido_materno,
        'curp': datos_usuario.curp,
        'rfc': datos_usuario.rfc,
        # 'foto':datos_usuario.foto,
        'promedio_final': datos_academicos.promedio,
        'número_fijo': fijo.numero,
        'número_móvil': movil.numero,
        'calle': direccion_pasante.calle,
        'número_exterior': direccion_pasante.numero,
        'colonia': direccion_pasante.colonia}


def recarga_form_inicial_pasante2(datos_usuario, direccion_pasante, fijo, movil):
    """Función para recargar el form del pasante al modificar al utilizar la función modificar pasante."""
    return {
        'nombre': datos_usuario.nombre,
        'apellido_paterno': datos_usuario.apellido_paterno,
        'apellido_materno': datos_usuario.apellido_materno,
        'curp': datos_usuario.curp,
        'rfc': datos_usuario.rfc,
        'foto': datos_usuario.foto,
        #'promedio_final': datos_academicos.promedio,
        #'número_fijo': fijo.numero,
        'número_móvil': movil.numero,
        'calle': direccion_pasante.calle,
        'número_exterior': direccion_pasante.numero,
        'colonia': direccion_pasante.colonia,
    }


def recarga_form_post_pasante(request, pasante):
    """recarga el form en caso de que exista algún error en los campos"""
    return {
        'nombre': request.POST.get('nombre'),
        'apellido_paterno': request.POST.get('apellido_paterno'),
        'apellido_materno': request.POST.get('apellido_materno'),
        'curp': request.POST.get('curp'),
        'rfc': request.POST.get('rfc'),
        'foto': pasante.foto,
        # Validación promedio.
        'promedio_final': request.POST.get('promedio_final'),
        'número_fijo': request.POST.get('número_fijo'),
        'número_móvil': request.POST.get('número_móvil'),
        'calle': request.POST.get('calle'),
        'número_exterior': request.POST.get('número_exterior'),
        'colonia': request.POST.get('colonia'),
    }


class ModificaPerfil:
    """Función para modificar un Pasante. Se valida cada campo y en caso de que exista un error regresa un mensaje."""

    @login_required
    def modificar_perfil(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None

        estados = Estado.objects.all()
        """función que realiza la modificación de un Pasante
            si todo es correcto devuelve un mensaje de la modificación exito
            en caso contrario muestra los diferentes mensajes de error.
        """
        # se toman los datos del mostrar pasante , para diferenciar el Pasante seleccionado.
        # algo=request.POST.get('modificar')
        # print (algo)
        current_user = request.user
        id_pasante = int(current_user.id)
        try:
            # se obtiene el objeto del Pasante seleccionado.
            # pasante = get_object_or_404(Pasante, pk=id_pasante)
            pasante = Pasante.objects.get(id_usuario_fk=id_pasante)
            direccion_pasante = DireccionPasante.objects.get(
                pk=pasante.id_direccion_fk_id)

            telefonos_pasante = Telefono.objects.filter(
                id_pasante_fk_id=pasante.id_pasante)

            datos_usuario = Usuario.objects.get(
                id_usuario=pasante.id_usuario_fk_id)
            fijo = None
            movil = None

            for i in telefonos_pasante:
                if (i.tipo == "fijo"):
                    fijo = i
                elif (i.tipo == "movil"):
                    movil = i
        except Exception as e:
            print("error 1.0")
            estados = Estado.objects.all()
            return redirect("/Pasante/inicio/")
        est = direccion_pasante.estado.id_estado
        mun = direccion_pasante.municipio.id_municipio
        # se pasan los datos del Pasante seleccionado al form que generara la vista del form
        # si se hace una peticion post se ejecutara el codigo para guardar los datos
        if request.method == 'POST' or request.method == 'GET':
            form = FormPasante(initial=recarga_form_inicial_pasante2(pasante,
                                                                     direccion_pasante,
                                                                     fijo,
                                                                     movil))
            if request.POST.get('guardar') == "True":
                id_estado = request.POST.get('estados')
                id_municipio = request.POST.get('municipios')
                # validación nombre
                nombre = ""
                if (es_alfabetica(request.POST.get('nombre'))):
                    nombre = request.POST.get('nombre')

                    if (nombre == ""):
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))

                        return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'nombre': 'El campo "Nombre" no puede ser vacío.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'tiene_datos': 1})
                else:
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))


                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'nombre': '"Nombre" contiene números o caracteres especiales.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                # validación apellido paterno
                ap = ""
                if (es_alfabetica(request.POST.get('apellido_paterno'))):
                    ap = request.POST.get('apellido_paterno')
                    if (ap == ""):
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))

                        return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'ap': 'El campo "Apellido Paterno" no puede ser vacío.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                   'tiene_datos': 1})
                else:
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'ap': '"Apellido Paterno" contiene números o caracteres especiales.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                # validación apellido materno
                am = ""
                if (request.POST.get('apellido_materno') == ""):
                    pass
                else:
                    if (es_alfabetica(request.POST.get('apellido_materno'))):
                        am = request.POST.get('apellido_materno')
                    else:
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                        return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'am': '"Apellido Materno" contiene números o caracteres especiales.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                   'tiene_datos': 1})
                        # validación curp
                curp = request.POST.get('curp')
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))

                if (curp == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'curp': 'El campo "CURP" no debe ser vacío',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                # validación rfc.
                rfc = request.POST.get('rfc')
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))

                if (rfc == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'rfc': 'El campo "RFC" no debe ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})

                foto = None
                try:
                    if (request.FILES.get('foto') != None):
                        if (not request.FILES.get('foto').name.lower().endswith(
                                '.jpg')):
                            return render(request, 'modificar_perfil.html',
                                          {'form': form,
                                           'rfc': 'Debes de seleccionar una imagen jpg.',
                                           'estados': estados, 'est': id_estado,
                                           'mun': id_municipio,
                                           'pasante': pasante,
                                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                           'usuario': datos_usuario,
                                           'activo': pasante,
                                   'tiene_datos': 1})
                        foto = request.FILES.get('foto')
                    else:
                        pass

                except (KeyError):
                    pass
                # Validación promedio.
                #promedio = request.POST.get('promedio_final')
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))



                        # validar número móvil
                try:
                    numero_movil = request.POST.get('número_móvil')
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    if (numero_movil == ""):
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))

                        return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'numero_movil': 'El campo "Número móvil" no debe ser vacío.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                   'tiene_datos': 1})

                    if (int(numero_movil) < 0):
                        return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'numero_movil': '"Número móvil" no debe contener valores negativos.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                   'tiene_datos': 1})
                    if (len(request.POST.get('número_móvil')) < 10):
                        return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'numero_movil': '"Número móvil" debe contener por lo menos 10 Digitos.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                   'tiene_datos': 1})
                except ValueError:
                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'numero_movil': '"Número móvil" no es númerico.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})

                calle = request.POST.get('calle')
                if (calle == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'numero_movil': 'El campo "Calle" no puede ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                numero_exterior = request.POST.get('número_exterior')
                if (numero_exterior == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'numero_movil': 'El campo "Número Exterior" no puede ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                colonia = request.POST.get('colonia')
                if (colonia == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'numero_movil': 'El campo "Colonia" no puede ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))

                if (id_estado == "vacio"):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'estados_error': 'Debes seleccionar un "Estado" de la lista.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                if (id_municipio == "vacio"):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))

                    return render(request, 'modificar_perfil.html',
                                  {'form': form,
                                   'municipios_error': 'Debes seleccionar un "Municipio" de la lista.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                   'usuario': datos_usuario,
                                   'activo': pasante,
                                   'tiene_datos': 1})
                direccion_pasante_modificar = DireccionPasante(
                    id_direccion=direccion_pasante.id_direccion,
                    numero=numero_exterior, calle=calle,
                    colonia=colonia,
                    municipio=Municipio.objects.get(pk=id_municipio),
                    estado=Estado.objects.get(pk=id_estado))
                direccion_pasante_modificar.save()
                # se guardan datos academicos

                if foto:
                    foto.name
                    usuarios = Usuario.objects.all()
                    usuario = get_object_or_404(usuarios, id_usuario=pasante.id_usuario_fk_id)
                    password = request.POST.get('contrasena')
                    password2 = request.POST.get('rcontrasena')
                    if password == "":
                        password = "nada"
                    if password == password2:
                        usuario.password=make_password(password)
                        usuario.contrasena=make_password(password)
                        print ("entro")
                        usuario.save()
                    else:
                        if password == "nada" and password2=="":
                            pass
                        else:

                            return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'contrasena_error': 'tus contraseñas deben de coincidir.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                       'tiene_datos': 1})


                    correo = request.POST.get('correo')
                    if correo=="":
                        correo=pasante.id_usuario_fk_id.correo_electronico



                    usuario.correo_electronico=correo
                    usuario.save()
                    Pasante.objects.get(
                        id_pasante=pasante.id_pasante).foto.delete(save=True)
                    pasante_modificar = Pasante(id_pasante=pasante.id_pasante,
                                                nombre=nombre,
                                                apellido_paterno=ap,
                                                apellido_materno=am,
                                                status=pasante.status,
                                                id_direccion_fk=direccion_pasante,
                                                id_asignacion_fk=pasante.id_asignacion_fk,
                                                id_datos_academicos_fk=pasante.id_datos_academicos_fk,
                                                id_usuario_fk_id=datos_usuario.id_usuario,
                                                foto=foto)
                    # se guarda al pasante
                    pasante_modificar.save()
                else:
                    usuarios = Usuario.objects.all()
                    usuario = get_object_or_404(usuarios, id_usuario=pasante.id_usuario_fk_id)
                    password = request.POST.get('contrasena')
                    password2 = request.POST.get('rcontrasena')
                    if password == "":
                        password= "nada"
                    if password == password2:
                        usuario.password=make_password(password)
                        usuario.contrasena=make_password(password)
                        print ("entro2.0")
                        usuario.save()
                    else:
                        if password == "nada":
                            pass
                        else:

                            return render(request, 'modificar_perfil.html',
                                      {'form': form,
                                       'contrasena_error': 'tus contraseñas deben de coincidir.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                                       'usuario': datos_usuario,
                                       'activo': pasante,
                                       'tiene_datos': 1})
                    correo = request.POST.get('correo')
                    if correo=="":
                        correo=pasante.id_usuario_fk_id.correo_electronico


                    usuario.correo_electronico=correo
                    usuario.save()
                    pasante_modificar = Pasante(id_pasante=pasante.id_pasante,
                                                nombre=nombre,
                                                apellido_paterno=ap,
                                                apellido_materno=am,
                                                curp=pasante.curp,
                                                rfc=pasante.rfc,
                                                status=pasante.status,
                                                id_direccion_fk=direccion_pasante,
                                                id_datos_academicos_fk=pasante.id_datos_academicos_fk,
                                                id_usuario_fk_id=datos_usuario.id_usuario,
                                                foto=pasante.foto)
                    # se guarda al pasante
                    pasante_modificar.save()
                # se guardan teléfonos del pasante.

                telefono2 = Telefono(
                    id_telefono=telefonos_pasante[1].id_telefono,
                    numero=str(numero_movil), tipo="movil",
                    id_pasante_fk=pasante)
                telefono2.save()
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))

                direccion_pasante = DireccionPasante.objects.get(
                    pk=pasante.id_direccion_fk_id)
                est = direccion_pasante.estado.id_estado
                mun = direccion_pasante.municipio.id_municipio
                return render(request, 'inicio_pasante.html',
                              {'funcion': 'El pasante con la matrícula ' + datos_usuario.matricula + ' ha sido modificado.',
                               'activo': [pasante],'tiene_datos': 1, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p})

            else:
                form = FormPasante(
                    initial=recarga_form_inicial_pasante2(pasante,
                                                          direccion_pasante,
                                                          fijo, movil))
            return render(request, 'modificar_perfil.html',
                          {'form': form, 'estados': estados, 'est': est,
                           'mun': mun, 'pasante': pasante,
                           'usuario': datos_usuario,
                           'activo': pasante,
                           'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,
                           'tiene_datos': 1})

import sys
class Reporte_Inconformidad:

    @login_required
    def generar_reporte_inconformidad(request):

        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None

        estados = Estado.objects.all()

        current_user = request.user
        id_pasante = int(current_user.id)
        pasante_id = id_pasante
        asignacion_rol = []
        roles = Rol.objects.all()

        hoy = datetime.now().date()
        pasante = Pasante.objects.get(id_usuario_fk_id__exact=pasante_id)

        asignaciones=AsignacionRol.objects.filter(id_pasante__exact=pasante.id_pasante)


        for asignacion in asignaciones:
            inicio = asignacion.id_rotacion.fecha_inicio
            fin = asignacion.id_rotacion.fecha_fin
            if hoy >= inicio and hoy <= fin:
                asignacion_rol.append(asignacion)
                print('1')



        telefono=Telefono.objects.filter(id_pasante_fk__exact=pasante.id_pasante)


        try:
            # se obtiene el objeto del Pasante seleccionado.
            # pasante = get_object_or_404(Pasante, pk=id_pasante)
            pasante = Pasante.objects.get(id_usuario_fk=id_pasante)

            direccion_pasante = DireccionPasante.objects.get(
                pk=pasante.id_direccion_fk_id)


           # datos_academicos_pasante = DatosAcademicos.objects.get(
            #    pk=pasante.id_datos_academicos_fk_id)

            datos_usuario = Usuario.objects.get(
                id_usuario=pasante.id_usuario_fk_id)

        except Exception as e:
            print("error 1.0")
            estados = Estado.objects.all()
            return redirect("/Pasante/inicio/")
        est = direccion_pasante.estado.id_estado
        mun = direccion_pasante.municipio.id_municipio
        # se pasan los datos del Pasante seleccionado al form que generara la vista del form
        # si se hace una peticion post se ejecutara el codigo para guardar los datos




        if request.method == 'POST':
            form = FormReporteInconformidad(request.POST)
            if form.is_valid():
                reporte_inconformidad=form.save(commit=False)
                reporte_inconformidad.id_pasante_fk = pasante
                reporte_inconformidad.id_clinica_fk = asignacion_rol[0].id_rotacion.id_clinica_fk
                reporte_inconformidad.fecha_reporte = hoy
                reporte_inconformidad.id_tipo_inconformidad_FK = form.cleaned_data['id_tipo']
                reporte_inconformidad.save()

            #messages.success(request, 'Reporte enviado correctamente')
            return redirect('inicio_pasante')
            #return render(request, 'inicio_pasante.html',
            #            {'funcion': 'Reporte Enviado Correctamente',
            #            'activo': [pasante],'tiene_datos': 1})

        else:
            form = FormReporteInconformidad()
            print(asignacion_rol)
            return render(request, 'reporte_inconformidad.html',{'usuario':pasante,'form':form,'asignacion':asignacion_rol[0],'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,'telefono':telefono[0]}
                           )

class reporte_pasante:
    def resuelve_inconformidad(request):
        usuario = Pasante.objects.filter( id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        reporte_in = ReporteInconformidad.objects.filter(id_pasante_fk=pasante_p.id_pasante)
        comentario_in = "nada"
        if request.method == 'POST':
            id_reporte = request.POST.get('id_reporte')
            reporte = ReporteInconformidad.objects.get(pk=id_reporte)
            reporte.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=5)
            reporte.save()
            return render(request, 'Muestra_reportes_inconformidad.html',
                          {'reporte': reporte_in, 'Solucionado': True, 'comentario_in': comentario_in, 'entre_fechas': entre_fechas(request),
            'pasantes_asignados': getPasantesAsignaciones(request),
                             'pasante_p': pasante_p, 'tiene_datos': True})
        else:
            return render(request, 'Muestra_reportes_inconformidad.html',
                          {'reporte': reporte_in, 'comentario_in': comentario_in, 'entre_fechas': entre_fechas(request),
            'pasantes_asignados': getPasantesAsignaciones(request),
                             'pasante_p': pasante_p, 'tiene_datos': True})
    def agrega_comentario_reporte(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        current_user = request.user
        id_pasante = int(current_user.id)
        pasante = Pasante.objects.get(id_usuario_fk=id_pasante)
        comentario_in="nada"
        if (request.method == 'POST'):
            comentario = request.POST.get('comment')
            id_rep= int(request.POST.get('id_ii'))
            if comentario == "":
                comentario_in="El campo del comentario no debe ser vacío"
                reporte_in = ReporteInconformidad.objects.filter(id_pasante_fk=pasante.id_pasante)
                return render(request,'Muestra_reportes_inconformidad.html',
                            {'reporte':reporte_in,'comentario_in':comentario_in, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p, 'tiene_datos': True})
            else:
                comentario_in="Se agregó exitosamente su comentario"
                queryset = ReporteInconformidad.objects.get(pk=id_rep)
                print(id_rep)
                comentario_save = ComentariosInconformidad (id_reporte=queryset, es_coordinador=0, comentario=comentario, fecha=date.today())
                comentario_save.save()
                reporte_in = ReporteInconformidad.objects.filter(id_pasante_fk=pasante.id_pasante)
                return render(request,'Muestra_reportes_inconformidad.html',
                            {'reporte':reporte_in,'comentario_in':comentario_in, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p, 'tiene_datos': True})


    def mostrar_reportes(request, id_estatus, id_tipo):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        current_user = request.user
        id_pasante = int(current_user.id)
        pasante = Pasante.objects.get(id_usuario_fk=id_pasante)

        if int(id_estatus) > 0 and int(id_estatus) < 6:
         filtrado = True
         reporte_in = ReporteInconformidad.objects.filter(id_pasante_fk=pasante.id_pasante, id_status_fk=id_estatus)
         return render(request,'Muestra_reportes_inconformidad.html',
                      {'reporte':reporte_in, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p, 'filtrado':filtrado, 'tiene_datos': True})

        elif int(id_tipo) > 0 and int(id_tipo) <= 7:
            clasificado = True
            reporte_in = ReporteInconformidad.objects.filter(id_tipo_inconformidad_FK=id_tipo, id_pasante_fk=pasante.id_pasante)
            return render(request, 'Muestra_reportes_inconformidad.html',
                          {'reporte': reporte_in, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p, 'clasificado':clasificado, 'tiene_datos': True})
        else:
          reporte_in = ReporteInconformidad.objects.filter(id_pasante_fk=pasante.id_pasante)
          return render(request,'Muestra_reportes_inconformidad.html',
                      {'reporte':reporte_in, 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p, 'tiene_datos': True})

'''class VerNormatividadPasante:
    @login_required
    def ver_normatividad_pasante(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
                0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        info = InformacionModel.objects.all()
        if (len(info) > 0):
            return render(request, 'ver_informacion_normatividad.html',
                          {'info': info,
                          'entre_fechas': entre_fechas(request),
                          'pasantes_asignados': getPasantesAsignaciones(request),
                          'pasante_p': pasante_p,})
        else:
            return render(request, 'ver_informacion_normatividad.html',
                          {'no_info': 'No hay archivos disponibles.', 'entre_fechas': entre_fechas(request),
              'pasantes_asignados': getPasantesAsignaciones(request),
                               'pasante_p': pasante_p,})'''
class VerCatalogoPreguntas:
    @login_required
    def ver_preguntas_frecuentes(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        preguntas = Catalogo_Preguntas.objects.all()
        if(len(preguntas) > 0):
            return render(request,'ver_preguntas_frecuentes.html',
                {'preguntas':preguntas,
                'entre_fechas':entre_fechas(request),
                'pasantes_asignados':getPasantesAsignaciones(request),
                'pasante_p':pasante_p,})
        else:
            return render(request, 'ver_informacion_normatividad.html',
                          {'no_info': 'No hay archivos disponibles.',
                           'entre_fechas': entre_fechas(request),
                           'pasantes_asignados': getPasantesAsignaciones(request),
                           'pasante_p': pasante_p,})

class comentarios_pasante:

    def get_comentarios_pasante(request, id_rubro_sel, id_clinica_sel):
      comentarios = ComentarioPasante.objects.filter(id_clinica_fk = id_clinica_sel, id_rubro_fk = id_rubro_sel)

      if len(comentarios) < 0: None

      rubro = Rubro.objects.get(id_rubro = id_rubro_sel)
      clinicaCom = Clinica.objects.get(id_clinica=id_clinica_sel)

      usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
      pasante_p = usuario[0] if len(usuario) > 0 else None

      if len(comentarios) < 0: None
      return render(request,'Muestra_comentarios_pasante.html',
                      {'comentarios':comentarios,
                      'rubro':rubro,
                      'clinicaCom':clinicaCom,
                      'tiene_datos': True,
                      'pasante_p': pasante_p,
                      'pasantes_asignados': getPasantesAsignaciones(request),
                      })

def subirImagenClinica(request):
    usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
    pasante_p = usuario[0] if len(usuario) > 0 else None
    current_user = request.user
    id_usuario = int(current_user.id)
    pasante = None
    if Pasante.objects.filter(id_usuario_fk_id__exact=id_usuario).exists():
      pasante = Pasante.objects.get(id_usuario_fk_id__exact=id_usuario)

    asignacionRol = AsignacionRol.objects.filter(id_pasante=pasante.id_pasante)[0]
    rotacion = Rotacion.objects.get(id_rotacion=asignacionRol.id_rotacion.id_rotacion)
    clinica = Clinica.objects.get(id_clinica=rotacion.id_clinica_fk.id_clinica)
    hoy = datetime.now().date()
    fecha_valida = rotacion.fecha_fin - timedelta(days=14)

    if clinica.docfile == None:
      if request.method == 'POST':
          form = DocumentForm(request.POST, request.FILES)
          if form.is_valid():
            clinica.docfile = request.FILES['docfile']
            clinica.save()
            mensaje = 'La imagen fue subida con exito'
          return render(request,
            'subir_imagen_clinica.html',
            { 'mensaje': mensaje,
            	'tiene_datos': True,
                'pasante_p': pasante_p,
                'pasantes_asignados': getPasantesAsignaciones(request),
                'activo': usuario})
      else:
        form = DocumentForm()
        mensaje = 'No se pudo actualizar tu imagen, revisa tu conexion a internet de subida'
        form = DocumentForm()
        return render(request,
        'subir_imagen_clinica.html',
        { 'form': form,
        'tiene_datos': True,
                      'pasante_p': pasante_p,
                      'pasantes_asignados': getPasantesAsignaciones(request),
                      'activo': usuario})
    else:
      mensaje = 'Esta clinica ya cuenta con una imagen, solo se puede actualizar dos semanas antes de terminar la rotacion'
      flag = True
      return render(request,
      'subir_imagen_clinica.html',
      { 'mensaje': mensaje, 'flag':flag,
      'tiene_datos': True,
                      'pasante_p': pasante_p,
                      'pasantes_asignados': getPasantesAsignaciones(request),
                      'activo': usuario})

def actualizarImagenClinica(request):
    usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
    pasante_p = usuario[0] if len(usuario) > 0 else None
    current_user = request.user
    id_usuario = int(current_user.id)
    pasante = None
    if Pasante.objects.filter(id_usuario_fk_id__exact=id_usuario).exists():
      pasante = Pasante.objects.get(id_usuario_fk_id__exact=id_usuario)

    asignacionRol = AsignacionRol.objects.filter(id_pasante=pasante.id_pasante)[0]
    rotacion = Rotacion.objects.get(id_rotacion=asignacionRol.id_rotacion.id_rotacion)
    clinica = Clinica.objects.get(id_clinica=rotacion.id_clinica_fk.id_clinica)
    hoy = datetime.now().date()
    fecha_fake = hoy - timedelta(days=75)
    fecha_valida = rotacion.fecha_fin - timedelta(days=14)
    if fecha_fake >= fecha_valida and fecha_fake <= rotacion.fecha_fin:
    #if hoy >= fecha_valida and hoy <= rotacion.fecha_fin:
      clinica.docfile = None
      if request.method == 'POST':
          form = DocumentForm(request.POST, request.FILES)
          if form.is_valid():

            clinica.docfile = request.FILES['docfile']
            clinica.save()
            mensaje = 'La imagen fue actualizada con exito'
          return render(request,
            'actualizar_imagen_clinica.html',
            { 'mensaje': mensaje,
            'tiene_datos': True,
                      'pasante_p': pasante_p,
                      'pasantes_asignados': getPasantesAsignaciones(request),
                      'activo': usuario})
      else:
        mensaje = 'No se pudo actualizar tu imagen, revisa tu conexion a internet'
        form = DocumentForm()
        return render(request,
        'actualizar_imagen_clinica.html',
        { 'form': form,
        'tiene_datos': True,
                      'pasante_p': pasante_p,
                      'pasantes_asignados': getPasantesAsignaciones(request),
                      'activo': usuario})
    else:
      mensaje = 'Las fechas para actualizar la imagen de la clinica son dos semanas antes de que termine la rotacion'
      return render(request,
      'actualizar_imagen_clinica.html',
      { 'mensaje': mensaje,
      'tiene_datos': True,
                      'pasante_p': pasante_p,
                      'pasantes_asignados': getPasantesAsignaciones(request),
                      'activo': usuario})

def mostrar_clinica_pasante(request,pk):
    usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
    pasante_p = usuario[0] if len(usuario) > 0 else None

    clinica=get_object_or_404(Clinica, id_clinica=pk)
    return render(request, "MostrarInfoClinicaPasante.html",{'clinica':clinica,
      'tiene_datos': True,
      'pasante_p': pasante_p,
      'pasantes_asignados': getPasantesAsignaciones(request),
      'activo': usuario,
      })

class CatalogoClinicas:

    @login_required
    def mostrar_clinicas(request):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        pasantes = Pasante.objects.all()

        clinicas = Clinica.objects.all()

        rubro=Rubro.objects.get(id_rubro=7)
        calificacion=CalificacionClinica.objects.filter(id_rubro_fk=rubro)
        print (rubro)

        if(len(calificacion) > 0):
            return render(request,'catalogo_clinica.html',
                {
                'calificacion': calificacion,
                'tiene_datos': True,
      			'pasante_p': pasante_p,
      			'pasantes_asignados': getPasantesAsignaciones(request),
      			'activo': usuario,
      			'clinicas':clinicas,
                },)

        else:
            return render(request, 'catalogo_clinica.html',
                          {'no_info': 'No hay clinicas calificadas.'})

class CalificarClinica():

    @login_required
    def calificar_clinica(request):
        pasante = Pasante.objects.get(id_usuario_fk_id = request.user.id)

        hoy = datetime.today().date()
        #hoy = datetime(2018,7,2).date()
        fechas_calificacion= FechaCalificacion.objects.filter(
                    id_pasante = pasante,
                    )
        if not fechas_calificacion:
            return redirect('inicio_pasante')

        tiene_fecha_valida = False
        for fecha in fechas_calificacion:
            tiene_fecha_valida = tiene_fecha_valida or (fecha.fecha_calif_inicio <= hoy and fecha.fecha_calif_fin >= hoy and not fecha.calificado)

        if not tiene_fecha_valida:
            return redirect('inicio_pasante')


        asignacion_rol = []
        fecha = datetime.now().date()
        comentario_seguridad_text = request.POST.get('comentario_seguridad')
        comentario_infraestructura_text =\
        request.POST.get('comentario_infraestructura')
        comentario_comunicacion_text =\
        request.POST.get('comentario_comunicacion')
        comentario_personal_text = request.POST.get('comentario_personal')
        comentario_comunidad_text = request.POST.get('comentario_comunidad')
        comentario_general_text = request.POST.get('comentario_general')
        asignaciones =\
        AsignacionRol.objects.filter(id_pasante__exact = pasante.id_pasante)
        #Este es para generar lo de reporte
        usuario = Pasante.objects.filter(
        id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[
            0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None

        cc = CalificarClinica()
        dictCal = {}
        alerta_error = ""
        alerta_error_cal = ""

        """Obtener la clinica del pasante"""
        asignacion_actual = None
        fecha_actual = datetime.now().date()

        asignaciones = AsignacionRol.objects.filter(id_pasante__exact=pasante.id_pasante)

        for asignacion in asignaciones:
            inicio = asignacion.id_rotacion.fecha_inicio
            fin = asignacion.id_rotacion.fecha_fin
            #si la fecha actual esta entre las fechas de la asignacion
            # qyuere decri qel pasante esta aasignacada a esa asginacion y pon ende a la cllinca
            if fecha_actual >= inicio and fecha_actual <= fin:
                asignacion_actual = asignacion.id_rotacion
                break

        clinica = asignacion_actual.id_clinica_fk
        print("Clinica: ", clinica.nombre_unidad)
        """Obtener la clinica del pasante"""

        # asignacion_rol = []
        # asignaciones =\
        # AsignacionRol.objects.filter(id_pasante__exact = pasante.id_pasante)
        # for asignacion in asignaciones:
        #     asignacion_rol.append(asignacion)
        # clinica = asignacion_rol[0].id_rotacion.id_clinica_fk

        habilitar_clinica = True
        if request.method == 'POST':
            form_seguridad = CalificaClinicaSeguridadForm(request.POST)
            cal_seg = request.POST.get('seguridadCalificacion')
            form_infraestructura = CalificaClinicaInfraestructuraForm(request.POST)
            cal_inf = request.POST.get('infrastructuraCalificacion')
            form_comunicacion = CalificaClinicaComunicacionForm(request.POST)
            cal_comunicacion = request.POST.get('comunicacionCalificacion')
            form_personal = CalificaClinicaPersonalForm(request.POST)
            cal_personal = request.POST.get('personalCalificacion')
            form_comunidad = CalificaClinicaComunidadForm(request.POST)
            cal_comunidad = request.POST.get('comunidadCalificacion')
            form_general = CalificaClinicaGeneralForm(request.POST)
            cal_general = request.POST.get('generalCalificacion')

            # print("************+ "+str((cal_seg=="")))

            if (len(comentario_seguridad_text)) == 0 or\
                (len(comentario_infraestructura_text)) == 0 or\
                (len(comentario_comunicacion_text)) == 0 or\
                (len(comentario_personal_text)) == 0 or \
                (len(comentario_comunidad_text)) == 0 or\
                (len(comentario_general_text)) == 0:

                alerta_error = 'Debes dejar un comentario en todos los rubros'

                if cal_seg == "" or\
                    cal_inf == "" or\
                    cal_comunicacion == "" or\
                    cal_personal == "" or \
                    cal_comunidad == "" or\
                    cal_general == "":

                    alerta_error_cal = "Debes calificar todos los rubros"

                    return render(request,'califica_clinica.html',
                        {'tiene_datos': True,
                        'activo': [pasante],
                        'alerta_error_cal':alerta_error_cal,
                        'alerta_error': alerta_error,
                        'form_seguridad':form_seguridad,
                        'form_infraestructura':form_infraestructura,
                        'form_comunicacion':form_comunicacion,
                        'form_personal':form_personal,
                        'form_comunidad': form_comunidad,
                        'form_general':form_general,
                        'pasantes_asignados': getPasantesAsignaciones(request),
                        'entre_fechas': entre_fechas(request),
                        'pasante_p':pasante_p,
                        'habilitar_clinica':habilitar_clinica,
                        'clinica':clinica
                        })
                else:
                    return render(request,'califica_clinica.html',
                        {'tiene_datos': True,
                        'activo': [pasante],
                        'clinica':clinica,
                        'alerta_error': alerta_error,
                        'form_seguridad':form_seguridad,
                        'form_infraestructura':form_infraestructura,
                        'form_comunicacion':form_comunicacion,
                        'form_personal':form_personal,
                        'form_comunidad': form_comunidad,
                        'form_general':form_general,
                        'pasantes_asignados': getPasantesAsignaciones(request),
                        'entre_fechas': entre_fechas(request),
                        'pasante_p':pasante_p,
                        'habilitar_clinica':habilitar_clinica})

            elif comentario_seguridad_text.isspace() or\
                comentario_infraestructura_text.isspace() or\
                comentario_comunicacion_text.isspace()  or\
                comentario_personal_text.isspace() or \
                comentario_comunidad_text.isspace() or\
                comentario_general_text.isspace():

                alerta_error = 'Debes dejar un comentario en todos los rubros'

                if cal_seg == "" or\
                    cal_inf == "" or\
                    cal_comunicacion == "" or\
                    cal_personal == "" or \
                    cal_comunidad == "" or\
                    cal_general == "":

                    alerta_error_cal = "Debes calificar todos los rubros"

                    return render(request,'califica_clinica.html',
                        {'tiene_datos': True,
                        'activo': [pasante],
                        'clinica':clinica,
                        'alerta_error_cal':alerta_error_cal,
                        'alerta_error': alerta_error,
                        'form_seguridad':form_seguridad,
                        'form_infraestructura':form_infraestructura,
                        'form_comunicacion':form_comunicacion,
                        'form_personal':form_personal,
                        'form_comunidad': form_comunidad,
                        'form_general':form_general,
                        'pasantes_asignados': getPasantesAsignaciones(request),
                        'entre_fechas': entre_fechas(request),
                        'pasante_p':pasante_p,
                        'habilitar_clinica':habilitar_clinica})
                else:
                    return render(request,'califica_clinica.html',
                        {'tiene_datos': True,
                        'activo': [pasante],
                        'clinica':clinica,
                        'alerta_error': alerta_error,
                        'form_seguridad':form_seguridad,
                        'form_infraestructura':form_infraestructura,
                        'form_comunicacion':form_comunicacion,
                        'form_personal':form_personal,
                        'form_comunidad': form_comunidad,
                        'form_general':form_general,
                        'pasantes_asignados': getPasantesAsignaciones(request),
                        'entre_fechas': entre_fechas(request),
                        'pasante_p':pasante_p,
                        'habilitar_clinica':habilitar_clinica})

            if form_seguridad.is_valid() and\
                form_infraestructura.is_valid() and\
                form_comunicacion.is_valid() and\
                form_personal.is_valid() and\
                form_comunidad.is_valid() and\
                form_general.is_valid():

                comentario_seguridad = form_seguridad.save(commit=False)
                comentario_infraestructura =\
                form_infraestructura.save(commit=False)
                comentario_comunicacion = form_comunicacion.save(commit=False)
                comentario_personal = form_personal.save(commit=False)
                comentario_comunidad = form_comunidad.save(commit=False)
                comentario_general = form_general.save(commit=False)

                #Guarda comentario Seguridad
                print("************************")
                print(fecha)
                rubro = Rubro.objects.get(nombre_rubro='Seguridad')
                comentario_seguridad.id_pasante_fk = pasante
                comentario_seguridad.id_clinica_fk = clinica
                comentario_seguridad.id_rubro_fk = rubro
                comentario_seguridad.comentario = comentario_seguridad_text
                comentario_seguridad.fecha = fecha
                comentario_seguridad.save()


                #Guardar calificacion seguridadcal
                CalificacionPasante.objects.create(
                id_pasante_fk=comentario_seguridad.id_pasante_fk,
                id_clinica_fk = comentario_seguridad.id_clinica_fk,
                id_rubro_fk = comentario_seguridad.id_rubro_fk,calificacion = cal_seg)


                dictCal[rubro] = int(cal_seg)

                #Guarda comentario Infraestructura
                rubro = Rubro.objects.get(nombre_rubro='Infraestructura')
                comentario_infraestructura.id_pasante_fk = pasante
                comentario_infraestructura.id_clinica_fk = clinica
                comentario_infraestructura.id_rubro_fk = rubro
                comentario_infraestructura.comentario =\
                comentario_infraestructura_text
                comentario_infraestructura.fecha = fecha
                comentario_infraestructura.save()

                #Guardar calificacion seguridadcal
                CalificacionPasante.objects.create(
                id_pasante_fk=comentario_infraestructura.id_pasante_fk,
                id_clinica_fk = comentario_infraestructura.id_clinica_fk,
                id_rubro_fk = comentario_infraestructura.id_rubro_fk,calificacion = cal_inf)


                dictCal[rubro] = int(cal_inf)

                #Guarda comentario Comunicacion
                rubro = Rubro.objects.get(nombre_rubro='Comunicacion')
                comentario_comunicacion.id_pasante_fk = pasante
                comentario_comunicacion.id_clinica_fk = clinica
                comentario_comunicacion.id_rubro_fk = rubro
                comentario_comunicacion.comentario =\
                comentario_comunicacion_text
                comentario_comunicacion.fecha = fecha
                comentario_comunicacion.save()

                #Guardar calificacion Comunicacion
                CalificacionPasante.objects.create(
                id_pasante_fk=comentario_comunicacion.id_pasante_fk,
                id_clinica_fk = comentario_comunicacion.id_clinica_fk,
                id_rubro_fk = comentario_comunicacion.id_rubro_fk,calificacion = cal_comunicacion
                )

                dictCal[rubro] = int(cal_comunicacion)

                #Guarda comentario Personal
                rubro = Rubro.objects.get(nombre_rubro='Personal')
                comentario_personal.id_pasante_fk = pasante
                comentario_personal.id_clinica_fk = clinica
                comentario_personal.id_rubro_fk = rubro
                comentario_personal.comentario = comentario_personal_text
                comentario_personal.fecha = fecha
                comentario_personal.save()

                #Guardar calificacion Comunicacion
                CalificacionPasante.objects.create(
                id_pasante_fk=comentario_personal.id_pasante_fk,
                id_clinica_fk = comentario_personal.id_clinica_fk,
                id_rubro_fk = comentario_personal.id_rubro_fk,calificacion = cal_personal
                )

                dictCal[rubro] = int(cal_personal)

                #Guarda comentario Comunidad
                rubro = Rubro.objects.get(nombre_rubro='Comunidad')
                comentario_comunidad.id_pasante_fk = pasante
                comentario_comunidad.id_clinica_fk = clinica
                comentario_comunidad.id_rubro_fk = rubro
                comentario_comunidad.comentario = comentario_comunidad_text
                comentario_comunidad.fecha = fecha
                comentario_comunidad.save()

                #Guardar calificacion Comunicacion
                CalificacionPasante.objects.create(
                id_pasante_fk=comentario_comunidad.id_pasante_fk,
                id_clinica_fk = comentario_comunidad.id_clinica_fk,
                id_rubro_fk = comentario_comunidad.id_rubro_fk,calificacion = cal_comunidad)

                dictCal[rubro] = int(cal_comunidad)

                #Guarda comentario General
                rubro = Rubro.objects.get(nombre_rubro='General')
                comentario_general.id_pasante_fk = pasante
                comentario_general.id_clinica_fk = clinica
                comentario_general.id_rubro_fk = rubro
                comentario_general.comentario = comentario_general_text
                comentario_general.fecha = fecha
                comentario_general.save()

                #Guardar calificacion Comunicacion
                CalificacionPasante.objects.create(
                id_pasante_fk=comentario_general.id_pasante_fk,
                id_clinica_fk = comentario_general.id_clinica_fk,
                id_rubro_fk = comentario_general.id_rubro_fk,calificacion = cal_general
                )

                dictCal[rubro] = int(cal_general)

                cc.asigna_calificacion(clinica, dictCal)

                #Asigna bandera de que ya se realizo la calificacion
                fechas_calificacion= FechaCalificacion.objects.filter(
                    id_pasante = pasante,
                    )
                hoy = datetime.today().date()
                #fecha harcodeada para pruebas
                #hoy = datetime(2018,11,23).date()
                for fecha in fechas_calificacion:
                    if fecha.fecha_calif_inicio <= hoy and fecha.fecha_calif_fin >= hoy and not fecha.calificado:
                        fecha.calificado = True
                        fecha.save()
                        habilitar_clinica = False
                        break;

                alerta_exito = 'Calificación y comentarios realizados con éxito'
                return render(request,'MostrarInfoClinicaPasante.html',
                    {'tiene_datos': True,
                    'activo': [pasante],
                    'clinica':clinica,
                    'alerta_exito':alerta_exito,
                    'form_seguridad':form_seguridad,
                    'form_infraestructura':form_infraestructura,
                    'form_comunicacion':form_comunicacion,
                    'form_personal':form_personal,
                    'form_comunidad': form_comunidad,
                    'form_general':form_general,
                    'pasantes_asignados': getPasantesAsignaciones(request),
                    'entre_fechas': entre_fechas(request),
                    'pasante_p':pasante_p,
                    'habilitar_clinica':habilitar_clinica})

            else:
                alerta_error_cal = "Debes calificar todos los rubros"
                return render(request,'califica_clinica.html',
                    {'tiene_datos': True,
                    'activo': [pasante],
                    'clinica':clinica,
                    'alerta_error_cal':alerta_error_cal,
                    'form_seguridad':form_seguridad,
                    'form_infraestructura':form_infraestructura,
                    'form_comunicacion':form_comunicacion,
                    'form_personal':form_personal,
                    'form_comunidad': form_comunidad,
                    'form_general':form_general,
                    'pasantes_asignados': getPasantesAsignaciones(request),
                    'entre_fechas': entre_fechas(request),
                    'pasante_p':pasante_p,
                    'habilitar_clinica':habilitar_clinica})

        else:
            form_seguridad = CalificaClinicaSeguridadForm()
            form_infraestructura = CalificaClinicaInfraestructuraForm()
            form_comunicacion = CalificaClinicaComunicacionForm()
            form_personal = CalificaClinicaPersonalForm()
            form_comunidad = CalificaClinicaComunidadForm()
            form_general = CalificaClinicaGeneralForm()

        #print ("Nomnre unidad:", clinica.nombre_unidad)
        return render(request, 'califica_clinica.html',
            {'tiene_datos': True,
             'activo': [pasante],
             'clinica':clinica,
            'form_seguridad':form_seguridad,
            'form_infraestructura':form_infraestructura,
            'form_comunicacion':form_comunicacion,
            'form_personal':form_personal,
            'form_comunidad': form_comunidad,
            'form_general':form_general,
            'pasantes_asignados': getPasantesAsignaciones(request),
            'entre_fechas': entre_fechas(request),
            'pasante_p': pasante_p,
            'habilitar_clinica':habilitar_clinica})


    def asigna_calificacion(self, id_clinica, calificaciones):
        cc = CalificarClinica()
        #Hace una lista de id's de rubros
        rubros = []
        rubros = calificaciones.keys()

        #Hace una lista de los valores numéricos de las calificaciones
        calif_rubros = []
        calif_rubros = calificaciones.values()

        calif_items = calificaciones.items()

        #genera nuevo promedio general
        nuevo_promedio_gen = cc.promediar(calif_rubros)

        #Encuentra el id del rubro Promedio General
        all_rubros = Rubro.objects.all()
        id_prom_gen = 0
        for ar in all_rubros:
            if ar.nombre_rubro == 'Promedio General':
                id_prom_gen = ar

        #Tratará de obtener el promedio general de la clinica si no existe
        #habrá una excepcion y guardará las nuevas calificaciones como actuales
        #Si existe continuará y promediará cada nueva calificación
        try:

            promedio_gen_h = CalificacionClinica.objects.get(
                id_clinica_fk=id_clinica,
                id_rubro_fk=id_prom_gen)
            p = promedio_gen_h.promedio #respalda promedio general historico


            for rubros, calif_rubros in calif_items:
                promedio_rubro = CalificacionClinica.objects.get(
                    id_clinica_fk=id_clinica, id_rubro_fk=rubros)

                nuevo_promedio_rubro = cc.promediar(
                    [calif_rubros, promedio_rubro.promedio])
                promedio_rubro.promedio = nuevo_promedio_rubro

                promedio_rubro.save()

            promedio_gen_h.promedio = cc.promediar([nuevo_promedio_gen, p])
            promedio_gen_h.save()

        except ObjectDoesNotExist:

            for rubros, calif_rubros in calif_items:
                nueva_calif_rubro = CalificacionClinica(
                    id_clinica_fk=id_clinica,
                    id_rubro_fk=rubros,
                    promedio=calif_rubros)

                nueva_calif_rubro.save()

            nueva_calif_gen = CalificacionClinica(
                id_clinica_fk=id_clinica,
                id_rubro_fk= id_prom_gen,
                promedio=nuevo_promedio_gen)

            nueva_calif_gen.save()

    #Recibe una lista de elementos númericos para promediar
    def promediar(self, lista_numeros):
        sumatoria = 0

        for valor in lista_numeros:
            sumatoria += valor

        promedio = 0

        promedio = sumatoria / len(lista_numeros)

        return round(promedio, 1)

class MuestraCalificacion:
    @login_required
    def mostrar_calificacion(request,pk):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0  else None
        pasantes = Pasante.objects.all()

        get_clinica = pk

        seguridad = CalificacionClinica.objects.all().filter(id_rubro_fk=1).filter(id_clinica_fk=get_clinica)
        infraestructura=CalificacionClinica.objects.all().filter(id_rubro_fk=2).filter(id_clinica_fk=get_clinica)
        comunicacion=CalificacionClinica.objects.all().filter(id_rubro_fk=3).filter(id_clinica_fk=get_clinica)
        personal=CalificacionClinica.objects.all().filter(id_rubro_fk=4).filter(id_clinica_fk=get_clinica)
        comunidad=CalificacionClinica.objects.all().filter(id_rubro_fk=5).filter(id_clinica_fk=get_clinica)
        general=CalificacionClinica.objects.all().filter(id_rubro_fk=6).filter(id_clinica_fk=get_clinica)
        promedio_general=CalificacionClinica.objects.all().filter(id_rubro_fk=7).filter(id_clinica_fk=get_clinica)



        if(len(promedio_general) > 0):
            return render(request,'mostrar_info_clinica.html',
                {
                'tiene_datos': True,
      			'pasante_p': pasante_p,
      			'pasantes_asignados': getPasantesAsignaciones(request),
      			'activo': usuario,
                'seguridad': seguridad,
                'infraestructura':infraestructura,
                'comunicacion':comunicacion,
                'personal':personal,
                'comunidad':comunidad,
                'general':general,
                'promedio_general':promedio_general})

        else:
            return render(request, 'mostrar_info_clinica.html',
                          {'no_info': 'No hay calificaciones que mostrar.'})

class Foro():
	def mandame_foro(request):
		return (request,'../foro/foro.html')
