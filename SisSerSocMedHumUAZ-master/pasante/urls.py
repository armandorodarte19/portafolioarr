from django.conf.urls import url
from pasante.views import *
from mensajeria.views import enviarMensaje
from django.conf.urls import handler404, handler500

urlpatterns = [
    url(r'^subir/$',subirImagenClinica , name="subir"),
    url(r'^actualizar/$',actualizarImagenClinica , name="actualizar"),
    url(r'^mensajeria/mensajes/$', enviarMensaje, name="chat"),
    url(r'^$', AltaPasante.registrar_pasante, name="registrar_pasante"),
    url(r'^alerta/$', activar_alerta, name="activar_alerta"),
    url(r'^alerta/obtener_contactos$', Contactos.obtener_contactos, name="obtener_contactos"),
    url(r'^alerta/insertar_contactos$', Contactos.insertar_contactos, name="insertar_contactos"),
    url(r'^alerta/editar_contactos$', Contactos.editar_contactos, name="editar_contactos"),
    url(r'^alerta/borrar_contactos$', Contactos.borrar_contactos, name="borrar_contactos"),
    url(r'^inicio/$', inicio_pasante, name="inicio_pasante"),
    url(r'^municipio/$', carga_municipios),
    url(r'^modificar_perfil/$', ModificaPerfil.modificar_perfil,name="modificar_pas"),
    url(r'^mostrar_informacion/$',MuestraNormatividadPasante.mostrar_normatividad_pasante, name="mostrar_normatividad"),
    url(r'^ver_informacion/$',VerNormatividadPasante.ver_normatividad_pasante, name="ver_normatividad"),
    url(r'^buscar_informacion/$', MuestraNormatividadPasante.buscar_tema, name="buscar_normatividad" ),
    url(r'^reporte_inconformidad/$', Reporte_Inconformidad.generar_reporte_inconformidad, name="reporte_in" ),
    url(r'^mostrar_reporte/(?P<id_estatus>\d+)/(?P<id_tipo>\d+)/$', reporte_pasante.mostrar_reportes, name="reporte_inconformidad_pasante"),
    url(r'^agregar_comentario/$', reporte_pasante.agrega_comentario_reporte, name="agregar_comentario" ),
    url(r'^resuelve_inconformidad/$', reporte_pasante.resuelve_inconformidad, name="resuelve_reporte_pasante"),
    url(r'^mensajeria/preguntas_frecuentes/$',VerCatalogoPreguntas.ver_preguntas_frecuentes,name="ver_preguntas_frecuentes"),
    url(r'^mostrar_comentarios_pasante/(?P<id_rubro_sel>\d+)/(?P<id_clinica_sel>\d+)/$', comentarios_pasante.get_comentarios_pasante, name="comentarios_clinica_pasante"),    
    url(r'^MostrarInfoClinicaPasante/(?P<pk>\d+)/$',mostrar_clinica_pasante, name="mostrar_clinica_pasante"),
    url(r'^clinicas/mostrar_clinicas_pasante/$',CatalogoClinicas.mostrar_clinicas, name="mostrar_clinicas_pasante"),
    url(r'^calificar_clinica/$', CalificarClinica.calificar_clinica, name="calificar_clinica"),
    url(r'^clinicas/mostrar_calificacion/(?P<pk>\d+)/$', MuestraCalificacion.mostrar_calificacion,name="mostrar_calificacion"),
    url(r'^foro/$',Foro.mandame_foro,name="mandame_foro"),
]

#handler404 = error_404
#handler500 = error_500
