from django import forms
from pasante.models import Pasante
from pasante.models import ComentarioPasante
from coordinador.models import Estado, Municipio, ReporteInconformidad, CatalogoTipoInconformidad

class DocumentForm(forms.Form):
    docfile = forms.FileField(
        label='Selecciona un archivo',
        help_text='ejemplo: imagen.jpg')

class FormReporteInconformidad(forms.ModelForm):
	descripcion_inconformidad = forms.CharField(label='Descripcion del Problema:',required=True, max_length=500, widget=forms.Textarea)
	id_tipo = forms.ModelChoiceField(label='Tipo de Inconformidad:', queryset=CatalogoTipoInconformidad.objects.all())
	class Meta:
		model = ReporteInconformidad
		fields = (
		'id_tipo','descripcion_inconformidad',)

class FormPasante(forms.ModelForm):

	nombre = forms.CharField(label='Nombre: *',required=False)
	apellido_paterno = forms.CharField(label='Apellido Paterno: *',required=False)
	apellido_materno = forms.CharField(label='Apellido Materno:',required=False)
	curp = forms.CharField(label='CURP: *',required=False,max_length=18)
	rfc = forms.CharField(label='RFC: *',required=False,max_length=13)
	promedio_final = forms.CharField(label='Promedio Final:',required=False,max_length=5)
	número_fijo = forms.CharField(label='Teléfono Fijo:',required=False, max_length=15)
	número_móvil = forms.CharField(label='Teléfono Móvil: *',required=False,max_length=15)
	calle = forms.CharField(label='Calle: *',required=False)
	número_exterior = forms.CharField(label='Número Exterior: *',required=False,max_length=6)
	colonia = forms.CharField(label='Colonia: *',required=False)
	foto = forms.FileField(label='Foto: * (La foto debe ser tamaño infantil, con rostro descubierto, serio y sin maquillaje.)',required=False)



	class Meta:
		model = Pasante
		fields = '__all__'
		exclude = ['status','id_telefono_fk','id_direccion_fk',
			'id_datos_academicos_fk','id_usuario_fk','id_asignacion_fk']

class CalificaClinicaSeguridadForm(forms.ModelForm):
	seguridadCalificacion = forms.IntegerField(label='Seguridad')
	seguridadCalificacion.widget.attrs.update({'class':'rating','data-max':'5','data-min':'1'})
	comentario_seguridad = forms.CharField(label='', required=False, max_length=280, widget=forms.Textarea(attrs={'name': 'comentario_seguridad', 'class': 'form-control', 'style':'height: 100px; resize:none;'}))
	class Meta:
	 	model = ComentarioPasante
	 	fields = '__all__'
	 	exclude = ['comentario', 'id_pasante_fk', 'id_clinica_fk', 'id_rubro_fk', 'fecha']

class CalificaClinicaInfraestructuraForm(forms.ModelForm):
	infrastructuraCalificacion = forms.IntegerField(label='Infraestructura')
	infrastructuraCalificacion.widget.attrs.update({'class':'rating','data-max':'5','data-min':'1'})
	comentario_infraestructura = forms.CharField(label='', required=False, max_length=280, widget=forms.Textarea(attrs={'name': 'comentario_infraestructura', 'class': 'form-control', 'style':'height: 100px; resize:none;'}))

	class Meta:
	 	model = ComentarioPasante
	 	fields = '__all__'
	 	exclude = ['comentario', 'id_pasante_fk', 'id_clinica_fk', 'id_rubro_fk', 'fecha']

class CalificaClinicaComunicacionForm(forms.ModelForm):
	comunicacionCalificacion = forms.IntegerField(label='Comunicación')
	comunicacionCalificacion.widget.attrs.update({'class':'rating','data-max':'5','data-min':'1'})
	comentario_comunicacion = forms.CharField(label='', required=False, max_length=280, widget=forms.Textarea(attrs={'name': 'comentario_comunicacion', 'class': 'form-control', 'style':'height: 100px; resize:none;'}))

	class Meta:
	 	model = ComentarioPasante
	 	fields = '__all__'
	 	exclude = ['comentario', 'id_pasante_fk', 'id_clinica_fk', 'id_rubro_fk', 'fecha']

class CalificaClinicaPersonalForm(forms.ModelForm):
	personalCalificacion = forms.IntegerField(label='Personal')
	personalCalificacion.widget.attrs.update({'class':'rating','data-max':'5','data-min':'1'})
	comentario_personal = forms.CharField(label='', required=False, max_length=280, widget=forms.Textarea(attrs={'name': 'comentario_personal', 'class': 'form-control', 'style':'height: 100px; resize:none;'}))

	class Meta:
	 	model = ComentarioPasante
	 	fields = '__all__'
	 	exclude = ['comentario', 'id_pasante_fk', 'id_clinica_fk', 'id_rubro_fk', 'fecha']

class CalificaClinicaComunidadForm(forms.ModelForm):
	comunidadCalificacion = forms.IntegerField(label='Comunidad')
	comunidadCalificacion.widget.attrs.update({'class':'rating','data-max':'5','data-min':'1'})
	comentario_comunidad = forms.CharField(label='', required=False, max_length=280, widget=forms.Textarea(attrs={'name': 'comentario_comunidad', 'class': 'form-control', 'style':'height: 100px; resize:none;'}))

	class Meta:
	 	model = ComentarioPasante
	 	fields = '__all__'
	 	exclude = ['comentario', 'id_pasante_fk', 'id_clinica_fk', 'id_rubro_fk', 'fecha']

class CalificaClinicaGeneralForm(forms.ModelForm):
	generalCalificacion = forms.IntegerField(label='General')
	generalCalificacion.widget.attrs.update({'class':'rating','data-max':'5','data-min':'1'})
	comentario_general = forms.CharField(label='', required=False, max_length=280, widget=forms.Textarea(attrs={'name': 'comentario_general', 'class': 'form-control', 'style':'height: 100px; resize:none;'}))

	class Meta:
	 	model = ComentarioPasante
	 	fields = '__all__'
	 	exclude = ['comentario', 'id_pasante_fk', 'id_clinica_fk', 'id_rubro_fk', 'fecha']
