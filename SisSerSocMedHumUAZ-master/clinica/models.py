from django.db import models


class DireccionClinica(models.Model):
    id_direccion = models.AutoField(primary_key=True)
    localidad = models.CharField(max_length=50)
    municipio = models.ForeignKey('coordinador.Municipio', on_delete=models.CASCADE,)
    estado = models.ForeignKey('coordinador.Estado', on_delete=models.CASCADE,)


class Institucion(models.Model):
    id_institucion = models.AutoField(primary_key=True)
    nombre_institucion = models.CharField(max_length=100)
    def __str__(self):
        return self.nombre_institucion

class Clinica(models.Model):
    id_clinica = models.AutoField(primary_key=True)
    nombre_unidad = models.CharField(max_length=100)
    plaza_fed = models.CharField(max_length=1)
    complemento = models.CharField(null=True, max_length=1)
    observacion = models.CharField(max_length=250)
    status = models.BooleanField(default=False)
    id_direccion_fk = models.ForeignKey(DireccionClinica, on_delete=models.CASCADE,)
    id_institucion_fk = models.ForeignKey(Institucion, on_delete=models.CASCADE,)
    docfile = models.FileField(upload_to="fotos_clinicas/", null=True)

class CalificacionClinica(models.Model):
    id_calificacion_clinica = models.AutoField(primary_key=True)
    id_clinica_fk = models.ForeignKey(Clinica, on_delete=models.CASCADE,)
    id_rubro_fk = models.ForeignKey('pasante.Rubro', on_delete=models.CASCADE,)
    promedio = models.FloatField(max_length=5)

'''
from django.db import models


class DireccionClinica(models.Model):
    id_direccion = models.AutoField(primary_key=True)

    localidad = models.CharField(u'localidad', max_length=50)
    municipio = models.ForeignKey('coordinador.Municipio')
    estado = models.ForeignKey('coordinador.Estado')


class Clinica(models.Model):
    id_clinica = models.AutoField(primary_key=True)

    nombre_unidad = models.CharField(u'nombre unidad', max_length=100)
    plaza_fed = models.CharField(u'plaza fed', max_length=1)
    complemento = models.CharField(u'complemento', null=True, max_length=1)
    beca = models.CharField(u'beca', max_length=250)
    observacion = models.CharField(u'observacion', max_length=250)
    status = models.BooleanField(default=False)

    id_direccion_fk = models.ForeignKey(DireccionClinica)
'''
