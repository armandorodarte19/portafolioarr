#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from clinica.models import Clinica, DireccionClinica, Institucion
from coordinador.models import Estado, Municipio

A = 'A'
B = 'B'
C = 'C'
D = 'D'

FED_CHOICES = (
    (A, 'A'),
    (B, 'B'),
    (C, 'C'),
    (D, 'D')
)



class FormClinica(forms.ModelForm):
    nombre_unidad = forms.CharField(label='Nombre Unidad:', required=True,
                                    max_length=50)

    institucion = forms.ModelChoiceField(label='Institución', required=True,
                                    queryset=Institucion.objects.all() )

    plaza_fed = forms.ChoiceField(label='Plaza Fed:', required=True,
                                  choices=FED_CHOICES)

    complemento = forms.ChoiceField(label='Complemento:', required=True,
                                    choices=FED_CHOICES)

    observación = forms.CharField(label='Observaciones:', max_length=250)

    localidad = forms.CharField(label='Localidad:', required=True,
                                max_length=25)

    class Meta:
        model = Clinica
        model_Direccion = DireccionClinica
        fields = (
            'nombre_unidad', 'institucion', 'plaza_fed', 'complemento',
            # 'beca',
            'observación', 'localidad')
