# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-12-04 19:32
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('clinica', '0001_initial'),
        ('coordinador', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='direccionclinica',
            name='estado',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coordinador.Estado'),
        ),
        migrations.AddField(
            model_name='direccionclinica',
            name='municipio',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='coordinador.Municipio'),
        ),
        migrations.AddField(
            model_name='clinica',
            name='id_direccion_fk',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinica.DireccionClinica'),
        ),
        migrations.AddField(
            model_name='clinica',
            name='id_institucion_fk',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinica.Institucion'),
        ),
        migrations.AddField(
            model_name='calificacionclinica',
            name='id_clinica_fk',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='clinica.Clinica'),
        ),
    ]
