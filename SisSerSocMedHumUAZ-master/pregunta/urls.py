from django.conf.urls import url
from pasante.views import *
from mensajeria.views import enviarMensaje
from pregunta.views import *
from django.conf.urls import handler404, handler500

urlpatterns = [

    url(r'^responder/(?P<id>[0-9]+)$', Respuesta.responder_pasante, name="responder_pasante" ),
    url(r'^eliminar_respuesta', Respuesta.eliminar_respuesta, name='eliminar_respuesta_post'),
    url(r'^calificar_respuesta', Respuesta.calificar_respuesta, name='calificar_respuesta'),
    url(r'^cerrar_pregunta', Pregunta.cerrar_pregunta, name="cerrar_pregunta" ),
    url(r'eliminar_pregunta$', Pregunta.eliminar_pregunta),
]

#handler404 = error_404
#handler500 = error_500
