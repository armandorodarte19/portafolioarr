from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from foro.models import *
from pregunta.forms import *
from pregunta.models import Respuesta_Foro, Voto_Respuesta_Foro
from django.utils import timezone
from usuario.models import Usuario
from pasante.models import Pasante
from pasante.views import getPasantesAsignaciones
from django.contrib.auth.decorators import login_required
from django.contrib import messages
from experto.models import Experto
from foro.views import NotificacionExperto

from django.core.exceptions import ObjectDoesNotExist


class Pregunta:

    @login_required
    def obtener_Pregunta(request,id_pre):
        cuestionamiento=Pregunta_Foro.objects.filter(id_pregunta=id_pre)
        pregunta = cuestionamiento[0]
        return pregunta

    @login_required
    def cerrar_pregunta(request):
        ide_pregunta=request.POST.get('pregunta',None)

        respuestas_list = Respuesta_Foro.objects.filter(id_pregunta_fk_id = ide_pregunta)
        numRespuestas = respuestas_list.count()
        if numRespuestas > 0:
            pregunta=Pregunta_Foro.objects.get(id_pregunta=ide_pregunta)
            pregunta.estado = 0;
            pregunta.save()
        else:
            messages.warning(request, 'No se puede cerrar una pregunta sin respuestas')

        return redirect(request.META['HTTP_REFERER'])


    @login_required
    def eliminar_pregunta(request):
        '''
        Elimina una pregunta del foro.
        Si quien la intenta eliminar pertenece al staff (coordinador o superusuario), esta se
        elimina sin más. Si es un pasante, este sólo puede eliminar preguntas que este haya hecho.
        Cualquier intento no autorizado resulta en un mensaje de error indicando que no se puede
        eliminar la pregunta.
        '''

        if request.method == 'POST':
            id = request.POST.get('id_pregunta')
            pregunta = Pregunta_Foro.objects.filter(id_pregunta=id)[0]

            usuario = Usuario.objects.filter(id_usuario=request.user.id)[0]

            if request.user.is_superuser or request.user.is_staff:
                pregunta.delete()
                messages.success(request, 'Pregunta eliminada con éxito.')
            elif usuario == pregunta.id_usuario_fk_id:
                pregunta.delete()
                messages.success(request, 'Pregunta eliminada con éxito.')
            else:
                # Alguien se las arregló para intentar eliminar una pregunta que no es suya.
                # ¿Cómo carajos llegó hasta ahí? .___.
                messages.warning(request, 'No puedes eliminar esta pregunta.')
                return HttpResponseRedirect("/foro")

            return HttpResponseRedirect("/foro")



class Respuesta:

    def get_template(request):

        if(request.user.is_staff == 0):
			#Es pasante
            return 'base.html'
        elif(request.user.is_staff == 1):
			#Es cordinador
            return 'base_coordinador.html'
        else:
			#Es experto
            return 'base.html'

    @login_required
    def responder_pasante(request,id):

        try:
            ide_pregunta=Pregunta_Foro.objects.get(pk=id).id_pregunta
        except ObjectDoesNotExist:
            messages.error(request, 'Esa pregunta no existe.')
            return HttpResponseRedirect('/foro')
        pregunta_mostrada = Pregunta.obtener_Pregunta(request,ide_pregunta)
        respuestas = Respuesta.obtener_respuesta(request,pregunta_mostrada)
        id_usuario = request.user.id
        votos = Voto_Respuesta_Foro.objects.all()
        template = Respuesta.get_template(request)
        contPregPendExp = 0
        usuario = Pasante.objects.filter(id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None

        usuarioActual = Usuario.objects.get(user_ptr_id=request.user.id)
        contPregPendExp = -1
        if request.user.is_staff == 0:
            if usuarioActual.tipo_usuario == 'Experto':
                experto = Experto.objects.get(id_usuario=usuarioActual.id)
                notificador = NotificacionExperto()
                preguntasE = notificador.notificar_experto(experto)
                contPregPendExp = len(preguntasE)

        tipo = usuarioActual.tipo_usuario

        if request.method =='POST' and 'respuesta' in request.POST:
            form = RespuestaForm(request.POST)
            if form.is_valid():
                texto = request.POST.get('respuesta',None)
                if (texto.isspace()==False and len(texto)>1):
                    respuesta = Respuesta_Foro()
                    ultima = Respuesta.get_last_id_respuesta()
                    respuesta.id_respuesta=ultima+1
                    respuesta.respuesta = request.POST.get('respuesta',None)
                    respuesta.fecha = timezone.now()
                    respuesta.numero_votos = 0
                    respuesta.id_pregunta_fk_id = pregunta_mostrada
                    respuesta.id_usuario_fk_id = Usuario.objects.filter(id_usuario=request.user.id)[0]
                    respuesta.save()
                    form = RespuestaForm()
                    return render(request, 'responder.html',{'form':form,'votos':votos,'pregunta':pregunta_mostrada,'respuestas':respuestas,'id_usuario':id_usuario,'template':template, 'contPregPendExp': contPregPendExp, 'tipo':tipo, 'pasante_p':pasante_p, "activo":usuario, 'tiene_datos': True, 'is_foro':True})
                else:
                    return render(request, 'responder.html',{'form':form,'votos':votos,'pregunta':pregunta_mostrada,'respuestas':respuestas,'id_usuario':id_usuario,'alerta':'No se escribio respuesta','template':template, 'contPregPendExp': contPregPendExp, 'tipo':tipo, 'pasante_p':pasante_p, "activo":usuario, 'tiene_datos': True, 'is_foro':True})
        else:
            form = RespuestaForm()
        return render(request, 'responder.html',{'form':form,'votos':votos,'pregunta':pregunta_mostrada,'respuestas':respuestas,'id_usuario':id_usuario,'template':template, 'contPregPendExp': contPregPendExp, 'tipo':tipo, 'pasante_p':pasante_p, "activo":usuario, 'tiene_datos': True, 'is_foro':True})

    def get_last_id_respuesta():
        todas = Respuesta_Foro.objects.all()
        if( len(todas)==0):
            return 0;
        return todas[len(todas)-1].id_respuesta

    @login_required
    def eliminar_respuesta(request):
        idr = request.POST.get('id_respuesta',None)
        respuesta= Respuesta_Foro.objects.filter(id_respuesta=idr)[0]
        pregunta_mostrada= Pregunta.obtener_Pregunta(request,respuesta.id_pregunta_fk_id.id_pregunta)
        respuestas = Respuesta.obtener_respuesta(request,pregunta_mostrada)
        respuesta.delete();
        form = RespuestaForm()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

    @login_required
    def obtener_respuesta(request,pregunta):
        respuestas_list = Respuesta_Foro.objects.filter(id_pregunta_fk_id = pregunta.id_pregunta)
        return respuestas_list

    @login_required
    def obtener_respuesta_u(request,idusuario):
        respuestas_list = Respuesta_Foro.objects.filter(id_usuario_fk_id = idusuario)
        return respuestas_list

    @login_required
    def calificar_respuesta(request):
        idr = request.POST.get('id_respuesta',None)
        r = Respuesta_Foro.objects.get(id_respuesta=idr)
        pregunta_mostrada= Pregunta.obtener_Pregunta(request,r.id_pregunta_fk_id.id_pregunta)
        respuestas = Respuesta.obtener_respuesta(request,pregunta_mostrada)
        r.numero_votos = (r.numero_votos + 1)
        r.save()

        todas = Voto_Respuesta_Foro.objects.all()
        if( len(todas)==0):
            p = 0;
        else:
            p = todas[len(todas)-1].id_voto

        vrf = Voto_Respuesta_Foro()
        vrf.id_voto = p+1
        vrf.id_respuesta_v = r
        vrf.id_usuario_fk_id =  Usuario.objects.filter(id_usuario=request.user.id)[0]
        vrf.save()
        form = RespuestaForm()

        #pregunta_mostrada= Pregunta.obtener_Pregunta(request,r.id_pregunta_fk_id.id_pregunta)
        #respuestas = Respuesta.obtener_respuesta(request,pregunta_mostrada)
        #r.numero_votos = (r.numero_votos + 1)
        #r.save()
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
