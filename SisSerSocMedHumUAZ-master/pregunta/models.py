from django.db import models
from foro.models import Categorias, Pregunta_Foro
from usuario.models import Usuario

class Respuesta_Foro(models.Model):
    id_respuesta = models.IntegerField(primary_key=True)
    fecha = models.DateTimeField(auto_now_add=True)
    respuesta = models.CharField(max_length=500)
    numero_votos = models.IntegerField()
    id_usuario_fk_id =models.ForeignKey(Usuario, null=False, on_delete=models.CASCADE)
    id_pregunta_fk_id =models.ForeignKey(Pregunta_Foro, null=False, on_delete=models.CASCADE)

class Voto_Respuesta_Foro(models.Model):
    id_voto = models.IntegerField(primary_key=True)
    id_respuesta_v = models.ForeignKey(Respuesta_Foro, on_delete=models.CASCADE)
    id_usuario_fk_id =models.ForeignKey(Usuario, null=False, on_delete=models.CASCADE)
