from django import forms
from pregunta.models import *

class RespuestaForm(forms.Form):
	respuesta = forms.CharField(
	required= False,
	#widget=forms.TextInput(attrs={'placeholder':'Escribe tu respuesta'}),
	widget=forms.Textarea(attrs={'placeholder':'Escribe tu respuesta','cols':'100','rows':'2','class':'form-control', 'style':'resize:None'}), max_length=100)