from channels.generic.websocket import AsyncWebsocketConsumer
import json
from channels.db import database_sync_to_async
from mensajeria.models import Mensaje
from coordinador.models import CatalogoTipoInconformidad
import dateutil.parser
from mensajeria.models import chatbot

from channels.generic.websocket import WebsocketConsumer
from asgiref.sync import sync_to_async, async_to_sync
from datetime import datetime
from coordinador.models import Coordinador
from usuario.models import Usuario
from chatterbot.conversation import Statement



class ChatConsumer(AsyncWebsocketConsumer):
    chatbot.read_only = True
    async def connect(self):
        self.room_name = self.scope['url_route']['kwargs']['user_matricula']
        self.grupo_coord_pasante = 'chat_%s' % self.room_name
        self.grupo_alertas= 'alertas'
        self.is_updated=False
        self.is_coord_updated=False
        self.bot_activo=True
        
        # Join room group
        await self.channel_layer.group_add(
            self.grupo_coord_pasante,
            self.channel_name
        )

        await self.channel_layer.group_add(
            self.grupo_alertas,
            self.channel_name
        )

        await self.accept()


    def get_mensaje(self,pk):
        mensaje=Mensaje.objects.get(pk=pk)
        return mensaje

    def actualizar_tema(self,mensaje,nombre_tema):
        mensaje.id_tipo_fk_id=CatalogoTipoInconformidad.objects.get(nombre=nombre_tema)  
        mensaje.save()
        self.is_updated=True
        print("tema actualizado")
        print(mensaje.id_tipo_fk_id)

    def actualizar_coordinador(self,mensaje,user):
        
        mensaje.id_coordinador_fk_id=Coordinador.objects.get(matricula= Usuario.objects.get(username=user))
        mensaje.save()
        self.is_coord_updated=True
        print("coordinador actualizado")
        print(mensaje.id_coordinador_fk_id)
        type(user)

    def actualizar_texto_fecha_mensaje(self,mensaje,texto, fecha):
        mensaje.texto=str(mensaje.texto)+ "\n" + str(texto)
        mensaje.fecha_final=dateutil.parser.parse(fecha)
        mensaje.save()
        
        print("mensaje actualizado")

    def get_mensajes_nuevos(self):
        mensajes_nuevos=Mensaje.objects.filter(leido=True).count()
        return mensajes_nuevos


    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message_only = text_data_json['message']
        
        user = text_data_json['user']
        pk_mensaje = text_data_json['pk_mensaje']
        tema_seleccionado = text_data_json['tema_seleccionado']
        tipo_user = text_data_json['tipo_user']
        #print(tipo_user)
        #print(type(tipo_user))
        myDate = datetime.now()
        fecha_ultimo_mensaje = myDate.strftime("%Y-%m-%d %H:%M:%S")
        message = str(user)+ ": " + fecha_ultimo_mensaje +  "\n" +str(message_only)  + "\n"
        mensaje = await database_sync_to_async(self.get_mensaje)(pk_mensaje)
        #await sync_to_async(ChatCon2.enviar)(self, "hola mundo" )
        #print("usuario")

        #print(self.scope["user"])

        if tipo_user=="pasante": #si viene de un pasante se actualiza el tema la primera vez
            if not self.is_updated:
                await database_sync_to_async(self.actualizar_tema)(mensaje,tema_seleccionado)
            await database_sync_to_async(self.actualizar_texto_fecha_mensaje)(mensaje,message,fecha_ultimo_mensaje)
            #await database_sync_to_async(self.get_mensajes_nuevos)()
            if self.bot_activo:
                respuesta_bot=str(chatbot.get_response(message_only))
                if respuesta_bot == message_only:
                    respuesta_bot = "Espera un momento, tu mensaje será enviado a un coordinador."
                elif respuesta_bot == "Espera un momento, tu mensaje será enviado a un coordinador.":
                    self.bot_activo=False
                    mensajes_nuevos=await database_sync_to_async(self.get_mensajes_nuevos)()
                    '''await self.channel_layer.group_send(
                    self.grupo_alertas,
                        {
                            'type': 'chat_message',
                            'message': user,
                            'tipo':'alerta',
                            'mensajes_nuevos':mensajes_nuevos
                        }
                    )'''
                    
                message_bot = 'BOT: ' +fecha_ultimo_mensaje +  "\n" + str(respuesta_bot) + "\n"
                message = message +"\n"+ str(message_bot) + "\n"
            #else:
                #await database_sync_to_async(self.actualizar_texto_fecha_mensaje)(mensaje,message,fecha_ultimo_mensaje)
        else:
            if not self.is_coord_updated:
                await database_sync_to_async(self.actualizar_coordinador)(mensaje,self.scope["user"])            
            await database_sync_to_async(self.actualizar_texto_fecha_mensaje)(mensaje,message,fecha_ultimo_mensaje)
        


        # Send message to room group
        await self.channel_layer.group_send(
            self.grupo_coord_pasante,
            {
                'type': 'chat_message',
                'message': message,
                'tipo':'chat',
                'mensajes_nuevos':"0"
            }
        )


    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        tipo = event['tipo']
        mensajes_nuevos= event['mensajes_nuevos']

        # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message,
            'tipo': tipo,
            'mensajes_nuevos':mensajes_nuevos
        }))
    #Arreglar cuando un usuario se desconecta
    async def disconnect(self, close_code):
        # Leave room group

        await self.channel_layer.group_send(
            self.grupo_coord_pasante,
            {
                'type': 'chat_message',
                'message': 'El usuario se ha desconectado',
                'tipo':'chat',
                'mensajes_nuevos':"0"
            }
        )

        await self.channel_layer.group_discard(
            self.grupo_coord_pasante,
            self.channel_name
        )

        await self.channel_layer.group_discard(
            self.grupo_alertas,
            self.channel_name
        )


