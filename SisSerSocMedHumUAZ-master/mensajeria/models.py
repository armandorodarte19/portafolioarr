from django.db import models
from clinica.models import Clinica
from pasante.models import Pasante,Generacion,Promocion
from django.contrib.auth.models import User
from django.urls import reverse
from coordinador.models import CatalogoTipoInconformidad
from coordinador.models import Coordinador
from collections import OrderedDict


'''class Tema(models.Model):
    nombre_tema= models.CharField(null=True, max_length=50)

    def __str__(self):
        return (str(self.nombre_tema))'''



class Pregunta(models.Model):
    id_pregunta = models.AutoField(primary_key=True)
    pregunta= models.CharField(null=True, max_length=150, unique=True)
    respuesta = models.CharField(null=True, max_length=150)
    id_tipo_fk_id= models.ForeignKey(CatalogoTipoInconformidad, on_delete=models.CASCADE,verbose_name='Tema')#Nuevo
    coordinador= models.ForeignKey(Coordinador, on_delete=models.CASCADE)#Nuevo


class Mensaje(models.Model):
    id_mensaje = models.AutoField(primary_key=True)
    fecha_inicio= models.DateTimeField()
    fecha_final= models.DateTimeField()
    id_tipo_fk_id = models.ForeignKey(CatalogoTipoInconformidad, on_delete=models.CASCADE)
    texto= models.TextField(default=" ")
    id_pasante_fk_id= models.ForeignKey(Pasante, on_delete=models.CASCADE)
    id_coordinador_fk_id=models.ForeignKey(Coordinador, on_delete=models.CASCADE)
    id_clinica_fk_id=models.ForeignKey(Clinica, on_delete=models.CASCADE)
    promocion = models.ForeignKey(Promocion, on_delete=models.CASCADE)#Nuevo
    leido= models.BooleanField(default=1)


class conversacion(models.Model):
    id_mensaje = models.AutoField(primary_key=True)
    fecha_inicio= models.DateTimeField()
    fecha_final= models.DateTimeField()
    id_tipo_fk_id = models.ForeignKey(CatalogoTipoInconformidad, on_delete=models.CASCADE)
    importante = models.BooleanField(default=1)
    id_pasante_fk_id= models.ForeignKey(Pasante, on_delete=models.CASCADE)
    id_coordinador_fk_id=models.ForeignKey(Coordinador, on_delete=models.CASCADE)
    id_clinica_fk_id=models.ForeignKey(Clinica, on_delete=models.CASCADE)
    promocion = models.ForeignKey(Promocion, on_delete=models.CASCADE)#Nuevo
    leido= models.BooleanField(default=1)

class mensaje_Conversacion(models.Model):
    id_mensaje = models.AutoField(primary_key=True)
    id_conversacion_fk_id = models.ForeignKey(conversacion,on_delete=models.CASCADE)
    mensaje=models.TextField(default=" ")
    resaltado=models.BooleanField(default=False)

from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import Trainer
from chatterbot.conversation import Statement, Response
from mensajeria.views import Entrenador



chatbot = ChatBot(
    "faqsBot",

    storage_adapter='chatterbot.storage.SQLStorageAdapter',
    database='./faqsBotDB.sqlite3',
    input_adapter="chatterbot.input.VariableInputTypeAdapter",
    output_adapter="chatterbot.output.OutputAdapter",
    output_format="text",
    #trainer='chatterbot.trainers.ChatterBotCorpusTrainer',
    trainer= 'mensajeria.views.Entrenador',
    filters=["chatterbot.filters.RepetitiveResponseFilter"],


    logic_adapters=[

        {
            "import_path": "chatterbot.logic.BestMatch",
            "statement_comparison_function": "chatterbot.comparisons.levenshtein_distance",
            "response_selection_method": "chatterbot.response_selection.get_most_frequent_response"
        },
        {
            'import_path': 'chatterbot.logic.LowConfidenceAdapter',
            'threshold': 0.65,
            "response_selection_method": "chatterbot.response_selection.get_most_frequent_response",
            'default_response': 'Espera un momento, tu mensaje será enviado a un coordinador.'
        },

    ],

    preprocessors=[
        'chatterbot.preprocessors.clean_whitespace',

    ],

    read_only = False,
)

preguntasFreq = [" ",]
no_repetidos = []
conversaciones = []

def entrenar_bot():
    preguntas= Pregunta.objects.all()
    for p in preguntas:
        conversaciones.append(p.pregunta)
        conversaciones.append(p.respuesta)

    no_repetidos = list(OrderedDict.fromkeys(conversaciones))
    chatbot.train(no_repetidos)
    print (no_repetidos)
