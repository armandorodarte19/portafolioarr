# chat/routing.py
from django.conf.urls import url

from . import consumers

websocket_urlpatterns = [
	#7789
	#url(r'^ws/chat/alertas/$', consumers.ChatCon2),
    url(r'^ws/chat/(?P<user_matricula>[^/]+)/$', consumers.ChatConsumer),
]