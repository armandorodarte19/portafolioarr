from django.shortcuts import render, redirect
from django.utils.safestring import mark_safe
import json
from mensajeria.models import Mensaje, conversacion, mensaje_Conversacion
from pasante.models import Pasante,Generacion,Promocion
from usuario.models import Usuario
from clinica.models import Clinica
from coordinador.models import CatalogoTipoInconformidad
from rol.models import AsignacionRol
from coordinador.models import Coordinador
from chatterbot.trainers import Trainer
from chatterbot.conversation import Statement, Response
from chatterbot import utils
import sys


from django.contrib.auth.models import User
from datetime import datetime, timedelta , date
import re
importante = False

class Entrenador(Trainer):
    
    def train(self, conversation):
        """
        Train the chat bot based on the provided list of
        statements that represents a single conversation.
        """
        previous_statement_text = None#ahora es la pregunta
        #esPregunta = True
        for conversation_count, text in enumerate(conversation):
            if self.show_training_progress:
                utils.print_progress_bar(
                    'Entrenador',
                    conversation_count + 1, len(conversation)
                )
            #if esPregunta:
            statement = self.get_or_create(text)#genera la pregunta
            #   esPregunta = False
            #else:
            #    response = self.get_or_create(text)
            #    esPregunta = True

            if previous_statement_text and conversation_count % 2 == 1:
                statement.add_response(
                    Response(previous_statement_text)
                )
            #Aqui esta el error, hay que intentar agregar las preguntas en impares y las respuestas
            #en pares
            #genera la repuesta
            #if conversation_count % 2 == 1:
            previous_statement_text = statement.text                
            self.storage.update(statement)
            #else:
            #    previous_statement_text = None
            #    self.storage.update(statement)

def enviarMensaje(request):

    pk_mensaje= None
    is_coordinador=0
    mensaje=""
    #tipo_user = Usuario.tipo_usuario
    #nombreChat=Pasante.objects.get(id_usuario_fk= Usuario.objects.get(matricula=request.user.username)) 
    coordinador = Coordinador.objects.get(matricula= Usuario.objects.get(username='11111111'))
    #nombre_conexion=request.POST.get('matricula_pasante')
    if request.user.is_staff: #si es coordindor
        nombre_conexion = request.POST.get('matricula_pasante') #obtiene la matricula del pasante

        # si hay pk_mensaje es un coordinador y se envia el pk por contexto para actualizar la tabla en la siguiente funcion
        pk_mensaje=request.POST.get('pk_mensaje')
        mensaje=Mensaje.objects.get(id_mensaje=(pk_mensaje))
        #mensaje=Mensaje.objects.get(id_mensaje=(request.POST.get()))
        mensaje= str(mensaje.texto) + "\n"

        is_coordinador=1
    else:
        nombre_conexion=request.user.username
        print(request.POST.get('matricula_pasante'))
           #si es pasante se utiliza su matricula para la conexion
        pasante=Pasante.objects.get(id_usuario_fk= Usuario.objects.get(matricula=request.user.username))
    	#si es pasante se guarda el mensaje para que el coordinador lo pueda ver luego
        myDate = datetime.now()
        f_inicial = myDate.strftime("%Y-%m-%d %H:%M:%S")
        asignacion= AsignacionRol.objects.get(id_pasante=pasante)
        
        clinica = asignacion.id_rotacion.id_clinica_fk
        promocion1=Promocion.objects.get(id_pasante_fk=pasante.id_pasante)
        tema_mensaje= CatalogoTipoInconformidad.objects.get(id_tipo=1)  #escoger el tema por defecto
        m1= Mensaje.objects.create(fecha_inicio=f_inicial, fecha_final=f_inicial, id_tipo_fk_id=tema_mensaje, id_pasante_fk_id=pasante,
            id_coordinador_fk_id= coordinador, id_clinica_fk_id=clinica,promocion=promocion1, leido=False, texto="" )
        
        #pk_mensaje=request.POST.get('pk_mensaje')
        
        pk_mensaje=m1.pk
        #mensaje=Mensaje.objects.get(id_mensaje=(pk_mensaje))
        #mensaje= str(mensaje.texto) + "\n"
        print("mensaje pk ...................")
        print(pk_mensaje)



    obtenerTemas=CatalogoTipoInconformidad.objects.all()
    
    return render(request, 'mensajeria/mensajeria.html', {'temas':obtenerTemas,'pk_mensaje':pk_mensaje, 'texto_mensaje':mensaje,
        'is_coordinador':is_coordinador, 'usuario': mark_safe(json.dumps(nombre_conexion))
    })


def registrarDatos(request):
    if (request.method=='POST'):
        esImportante=request.POST.get("es_importante","0")
        varImportante = False
        if esImportante == str(1):
            varImportante = True
        print(varImportante)
        pk_mensaje=request.POST.get('pk_mensaje')
        mensaje=Mensaje.objects.get(id_mensaje=(pk_mensaje))
        mensaje.leido=True
        mensaje.save()
        ultima_conver= conversacion.objects.latest('id_mensaje')
        if mensaje.fecha_inicio != ultima_conver.fecha_inicio:
	        m2= conversacion.objects.create(fecha_inicio=mensaje.fecha_inicio, fecha_final=mensaje.fecha_final, id_tipo_fk_id=mensaje.id_tipo_fk_id, importante=varImportante, id_pasante_fk_id=mensaje.id_pasante_fk_id,
	            id_coordinador_fk_id=mensaje.id_coordinador_fk_id, id_clinica_fk_id=mensaje.id_clinica_fk_id,promocion=mensaje.promocion, leido=True )
	        print(str(mensaje.texto))
	        mensajitos = str(mensaje.texto)
	        men =  re.split('\n',mensajitos)
	        indi=2
	        while indi<len(men):
	        	m3= mensaje_Conversacion.objects.create(id_conversacion_fk_id=conversacion.objects.latest('id_mensaje'),mensaje=men[indi])
	        	indi= indi + 3

    return redirect("/")

    


