from django.db import models
from django.shortcuts import render, redirect
from usuario.models import Usuario
from foro.models import Pregunta_Foro, Tags_Foro, Categorias
from pregunta.models import Respuesta_Foro
from foro.forms import FormPreguntas, FormTag
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from pasante.models import Pasante
from experto.models import Experto
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.views.decorators.http import require_POST
from django.urls import reverse
from pasante.views import getPasantesAsignaciones

class NotificacionExperto:
    def notificar_experto(self, id_experto):
        return Pregunta_Foro.objects.filter(id_categoria_fk_id=id_experto.id_categoria) & Pregunta_Foro.objects.filter(estado=1)

class VerTodasPreguntas:
    @login_required
    def obtenerTodasLasPreguntas(request):
        usuario = Pasante.objects.filter(id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None

        preguntas = Pregunta_Foro.objects.all().order_by('-fecha', 'id_pregunta')
        preguntas2 = Categorias.objects.all()
        search_term=''
        usuarioActual = Usuario.objects.get(user_ptr_id=request.user.id)
        contPregPendExp = -1
        if request.user.is_staff == 0:

            if usuarioActual.tipo_usuario == 'Experto':
                experto = Experto.objects.get(id_usuario=usuarioActual.id)
                notificador = NotificacionExperto()
                preguntasE = notificador.notificar_experto(experto)
                contPregPendExp = len(preguntasE)
                #preguntas = Pregunta_Foro.objects.filter(id_categoria_fk_id=experto.id_categoria)
        else:
            contPregPendExp = -1

        if request.user.is_staff:
            tipo_usuario = 'base_coordinador.html'
        else:
            tipo_usuario = 'base.html'
        return render(request, "foro.html", {'preguntas': preguntas, 'tipo_usuario':tipo_usuario,'preguntas2': preguntas2, 'contPregPendExp': contPregPendExp,"usuarioActual":usuarioActual, "activo":usuario, 'tiene_datos': True, 'is_foro':True, 'pasante_p':pasante_p, 'pasantes_asignados': getPasantesAsignaciones(request)})

class HacerPregunta:
    @login_required
    def enviar_pregunta(request):
        ya_iba = 0
        usuario = Pasante.objects.filter(id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        contPregPendExp = -1

        if request.user.is_staff:
            tipo_usuario = 'base_coordinador.html'
        else:
            tipo_usuario = 'base.html'


        if request.method == 'POST':

            formP = FormPreguntas(request.POST)
            formT = [FormTag(request.POST, prefix=str(x)) for x in range(5)]


            if 'cancel' in request.POST:
                preguntas = Pregunta_Foro.objects.all().order_by('-fecha', 'id_pregunta')
                preguntas2 = Categorias.objects.all()
                if request.user.is_staff:
                    tipo_usuario = 'base_coordinador.html'
                else:
                    tipo_usuario = 'base.html'
                return render(request, "foro.html", {'preguntas': preguntas, 'tipo_usuario':tipo_usuario,'preguntas2': preguntas2, 'contPregPendExp': contPregPendExp, "activo":usuario, 'tiene_datos': True, 'is_foro':True,'pasante_p':pasante_p, 'pasantes_asignados': getPasantesAsignaciones(request)})

            else:
                if formP.is_valid():
                    pregunta = formP.save(commit=False)
                    usuario_actual = Usuario.objects.get(id_usuario=request.user.id)
                    pregunta.id_usuario_fk_id = usuario_actual
                    pregunta.estado = 1
                    pregunta.notificada = 0
                    pregunta.save()

                    for tag in formT:
                        if tag.is_valid():
                            nombre = tag.cleaned_data['tag']
                            if (nombre.strip(" ")!=""):
                                try:
                                    este_tag = Tags_Foro.objects.get(tag=nombre)
                                except ObjectDoesNotExist:
                                    este_tag = Tags_Foro(tag=nombre)
                                    este_tag.save()
                                pregunta.tags.add(este_tag)
                    return redirect('foro')
                else:
                    ya_iba = 1
                    return render(request, "HacerPregunta.html", {'formP': formP,'formT': formT, 'tipo_usuario':tipo_usuario, 'ya_iba':ya_iba, 'contPregPendExp': contPregPendExp, "activo":usuario, 'tiene_datos': True, 'is_foro':True, 'pasante_p':pasante_p, 'pasantes_asignados': getPasantesAsignaciones(request)})

        else:
            formP = FormPreguntas()
            formT =  [FormTag(prefix=str(x)) for x in range(5)]

        return render(request, "HacerPregunta.html", {'formP': formP,'formT': formT, 'tipo_usuario':tipo_usuario, 'ya_iba':ya_iba, 'contPregPendExp': contPregPendExp, "activo":usuario, 'tiene_datos': True, 'is_foro':True, 'pasante_p':pasante_p, 'pasantes_asignados': getPasantesAsignaciones(request)})

class Preguntas:

    @login_required
    def obtenerPreguntasSinRespuesta(request):

        preguntas_sin_respuesta_categoria=''
        contPregPendExp = -1
        preguntas_sin_respuesta= Pregunta_Foro.objects.filter(respuesta_foro__isnull=True)
        usuarioActual = Usuario.objects.get(user_ptr_id=request.user.id)
        usuario = Pasante.objects.filter(id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        if request.user.is_staff == 0:

            if usuarioActual.tipo_usuario == 'Experto':

                #Filtro para obtener la categoria del experto

                experto = Experto.objects.get(id_usuario=usuarioActual.id)

                #Filtro para obtener preguntas dada la categoria del experto

                preguntas_sin_respuesta_categoria=Pregunta_Foro.objects.filter(respuesta_foro__isnull=True).filter(id_categoria_fk_id=experto.id_categoria)
                tipo_usuario = 'base.html'
                experto = Experto.objects.get(id_usuario=usuarioActual.id)
                notificador = NotificacionExperto()
                preguntasE = notificador.notificar_experto(experto)
                contPregPendExp = len(preguntasE)
                return render(request, "foro.html", {'preguntas': preguntas_sin_respuesta_categoria, 'tipo_usuario':tipo_usuario, 'contPregPendExp': contPregPendExp})
            else:
                tipo_usuario = 'base.html'

        elif request.user.is_staff == 1:
            tipo_usuario = 'base_coordinador.html'



        return render(request, "foro.html", {'preguntas': preguntas_sin_respuesta, 'tipo_usuario':tipo_usuario, 'contPregPendExp': contPregPendExp, "activo":usuario, 'tiene_datos': True, 'is_foro':True, 'pasante_p':pasante_p})



    @login_required
    def obtener_preguntas_sr_pasante(request):
        preguntas_sin_respuesta=[]
        contPregPendExp = -1
        preguntas = Pregunta_Foro.objects.filter(id_usuario_fk_id = request.user.id)

        usuario = Pasante.objects.filter(id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        for p in preguntas:
            respuestas=Respuesta_Foro.objects.filter(id_pregunta_fk_id=p.id_pregunta)
            if len(respuestas)==0:
                preguntas_sin_respuesta.append(p)
        return render(request, "preguntas_sr_pasante.html",{'preguntas':preguntas_sin_respuesta, 'contPregPendExp': contPregPendExp, "activo":usuario, 'tiene_datos': True, 'is_foro':True, 'pasante_p':pasante_p})

class buscador:

    @login_required
    def buscarPregunta(request):

        usuario = Pasante.objects.filter(id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        usuarioActual = Usuario.objects.get(user_ptr_id=request.user.id)
        if request.POST:
            preguntas2 =Categorias.objects.all()
            menu = request.POST.get('menu')
            dato = request.POST.get('dato')
            contPregPendExp = -1

            tags = dato.split()


            lista_preguntas=[]

            if dato:
                lista_tags =[]
                for tag in tags:
                    x = Tags_Foro.objects.filter(tag=tag)
                    if x:
                        lista_tags.append(x[0])


                for tag in lista_tags:
                    resultados = Pregunta_Foro.objects.filter(id_categoria_fk_id=menu, tags=tag)
                    lista_preguntas.extend(resultados)

            else:
                lista_preguntas = Pregunta_Foro.objects.filter(id_categoria_fk_id=menu)

            if request.user.is_staff:
                tipo_usuario = 'base_coordinador.html'
            else:
                if usuarioActual.tipo_usuario == 'Experto':
                    experto = Experto.objects.get(id_usuario=usuarioActual.id)
                    notificador = NotificacionExperto()
                    preguntasE = notificador.notificar_experto(experto)
                    contPregPendExp = len(preguntasE)
                tipo_usuario = 'base.html'
            post_data = {'menu': menu, 'dato':dato}
            return render(request, "foro.html", {'preguntas': lista_preguntas, 'tipo_usuario':tipo_usuario, 'preguntas2':preguntas2, 'contPregPendExp': contPregPendExp, "activo":usuario, 'tiene_datos': True, 'is_foro':True, 'pasante_p':pasante_p})
        else:
            return HttpResponseRedirect('/foro')
