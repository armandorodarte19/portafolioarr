from django.db import models
from usuario.models import Usuario

class Categorias(models.Model):
    id_categoria = models.AutoField(primary_key=True)
    nombre =models.CharField(max_length=50)

    def __str__(self):
        return self.nombre

class Tags_Foro(models.Model):
    id_tag = models.AutoField(primary_key=True)
    tag=models.CharField(max_length=100)

    def __str__(self):
        return self.tag

class Pregunta_Foro(models.Model):
    id_pregunta = models.AutoField(primary_key=True)
    pregunta = models.TextField(default=" ")
    fecha = models.DateTimeField(auto_now_add=True)
    estado = models.IntegerField()
    notificada = models.IntegerField()
    tags = models.ManyToManyField(Tags_Foro, blank=True)
    id_categoria_fk_id = models.ForeignKey(Categorias, on_delete=models.CASCADE)
    id_usuario_fk_id = models.ForeignKey(Usuario, on_delete=models.CASCADE)

