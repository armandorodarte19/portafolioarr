from django.conf.urls import url, include
from foro.views import Preguntas, buscador, HacerPregunta, VerTodasPreguntas

urlpatterns = [
	url(r'^$', VerTodasPreguntas.obtenerTodasLasPreguntas, name="foro"),
	url(r'^nueva_pregunta/$', HacerPregunta.enviar_pregunta, name="nueva_pregunta"),
	url(r'^preguntas_sin_respuesta/$', Preguntas.obtenerPreguntasSinRespuesta, name="preguntas_sin_respuesta"),
	url(r'^preguntas_sr_pasante/$', Preguntas.obtener_preguntas_sr_pasante, name = "obtener_preguntas_sr_pasante"),
	url(r'^buscador$', buscador.buscarPregunta, name="buscador"),
]
