# -*- coding:utf-8 -*-
from django import forms
from foro.models import Pregunta_Foro, Categorias, Tags_Foro
from usuario.models import Usuario
from django.forms.formsets import BaseFormSet

class FormPreguntas(forms.ModelForm):
    class Meta:
        model = Pregunta_Foro
        fields = (
            'pregunta',
            'id_categoria_fk_id',
        )

        exclude = (
            'id_usuario_fk_id',
            'estado',
            'notificada',
        )

class FormTag(forms.ModelForm):
    tag = forms.CharField(required=False)
    class Meta:
        model = Tags_Foro
        fields = (
            'tag',
            )   