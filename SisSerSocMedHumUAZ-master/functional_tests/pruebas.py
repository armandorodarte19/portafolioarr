from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
import time

def mostrar_pasantes(url, matricula,password):
    driver = webdriver.Firefox()

    driver.get(url)
    driver.find_element_by_id("username").clear()
    driver.find_element_by_id("username").send_keys(matricula)
    driver.find_element_by_id("password").clear()
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_id("login-submit").click()
    driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/a').click()
    driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/ul/li[1]/a').click()
    
    time.sleep(5)
    Select(driver.find_element_by_name("select")).select_by_index(1)
    driver.implicitly_wait(3)
    orden = Select(driver.find_element_by_xpath('//*[@id="intro"]/div/div/div[1]/div[4]/h6/select'))
    opciones = orden.options
    driver.execute_script("window.scrollTo(250, document.body.scrollHeight);")
    for i in range(1,len(opciones)):
        orden.select_by_index(i)


    time.sleep(5)


    
    #driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/a').click()
    #driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/ul/li[1]/a').click()
    registros_pasantes = driver.find_element_by_id("mytable")
    registros_pasantes = registros_pasantes.find_elements_by_tag_name("tr")
    registros_pasantes[1].find_element_by_tag_name("input").click()
    driver.find_element_by_xpath('//*[@id="botonMultiples"]/button[1]').click()
    time.sleep(5)


    
    #driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/a').click()
    #driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/ul/li[1]/a').click()
    time.sleep(5)
    driver.execute_script("window.scrollTo(250, document.body.scrollHeight);")
    registros_pasantes = driver.find_element_by_id("mytable")
    registros_pasantes = registros_pasantes.find_elements_by_tag_name("tr")
    registros_pasantes[1].find_element_by_tag_name("input").click()
    driver.find_element_by_xpath('//*[@id="botonMultiples"]/button[2]').click()
    time.sleep(5)

    
    #driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/a').click()
    #driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/ul/li[1]/a').click()
    time.sleep(5)
    driver.execute_script("window.scrollTo(250, document.body.scrollHeight);")
    registros_pasantes = driver.find_element_by_id("mytable")
    registros_pasantes = registros_pasantes.find_elements_by_tag_name("tr")
    registros_pasantes[1].find_element_by_tag_name("input").click()
    driver.find_element_by_id('btnGenerarCartas').click()
    driver.close()

def agregar_contacto(url, matricula,password):
    driver = webdriver.Firefox()

    driver.get(url)
    driver.find_element_by_id("username").clear()
    driver.find_element_by_id("username").send_keys(matricula)
    driver.find_element_by_id("password").clear()
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_id("login-submit").click()
    driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/a').click()
    driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/ul/li[4]/a').click()
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="agregar_button"]').click()
    
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="agregar_contacto"]/div/div[1]/input').send_keys("Antonio")
    driver.find_element_by_xpath('//*[@id="agregar_contacto"]/div/div[2]/input').send_keys("1234567890")
    driver.find_element_by_xpath('//*[@id="agregar_contacto"]/button').click()
    
    
    time.sleep(5)
    driver.find_element_by_xpath('//*[@id="tabla_contactos"]/table/tbody/tr/td[3]/a[1]').click()
    time.sleep(3)
    driver.find_element_by_xpath('//*[@id="editar_contacto"]/div/div[2]/input').clear()
    driver.find_element_by_xpath('//*[@id="editar_contacto"]/div/div[2]/input').send_keys("1234567999")
    driver.find_element_by_xpath('//*[@id="editar_contacto"]/button').click()
    #driver.find_element_by_xpath('//*[@id="tabla_contactos"]/table/tbody/tr/td[3]/a[2]').click()
    time.sleep(3)
    driver.close()
    
def busqueda_avanzada_pasantes(url,matricula,password):
    driver = webdriver.Firefox()

    driver.get(url)
    driver.find_element_by_id("username").clear()
    driver.find_element_by_id("username").send_keys(matricula)
    driver.find_element_by_id("password").clear()
    driver.find_element_by_id("password").send_keys(password)
    driver.find_element_by_id("login-submit").click()
    driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/a').click()
    driver.find_element_by_xpath('//*[@id="wrapper"]/div/div[3]/div[2]/ul/li[2]/ul/li[2]/a').click()
    driver.find_element_by_xpath('//*[@id="intro"]/div/div[1]/div/button[1]').click()
    
    time.sleep(3)
    driver.find_element_by_xpath('//*[@id="anio_filter_1"]').send_keys("2018")
    driver.find_element_by_xpath('//*[@id="exampleModalLong"]/div[2]/div/div[3]/button[2]').click()
    time.sleep(3)
    driver.close()

if __name__ == '__main__':
    url = "http://0.0.0.0:8000/"
    matricula = "00000000"
    password = "00000000"
    mostrar_pasantes(url,matricula,password)
    agregar_contacto(url,matricula,password)
    busqueda_avanzada_pasantes(url,matricula,password)
