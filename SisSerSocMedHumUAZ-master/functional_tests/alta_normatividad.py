# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re, sys, os, time

driver = webdriver.Firefox()
tiempo_visualizacion = 3
nuevo_coordinador_keys = '00000001'
nombre_new_coordinador = 'Diego 1'
nombre_archivo1 = 'Manual de Servicio Social'
nombre_archivo2 = 'Plazas SSZ'
nombre_archivo3 = 'Plazas IMSS'

#Metodo para finalizar el driver
def finalizar_driver():
    driver.quit()

def coordinador_login():
	#coordinador_credenciales = 34150139
	coordinador_credenciales = '00000000'
	#driver.get('http://148.217.200.108:8000')
	driver.get('http://0.0.0.0:8000/')
	username = driver.find_element_by_id('username')
	username.send_keys(coordinador_credenciales)
	password = driver.find_element_by_id('password')
	password.send_keys(coordinador_credenciales)
	driver.find_element_by_id('login-submit').click()

	print ('Se ha ingresado correctamente al sistema')

def alta_normatividad():
	#coordinador_login()
	"""Alta informacion"""
	#Se guarda un pdf
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_id('id_nombre_archivo').send_keys(nombre_archivo1)
	driver.find_element_by_id('id_descripcion').send_keys('Manual de Servicio Social: Encontraras la normativa y reglamentos')
	driver.find_element_by_id('id_ruta_archivo').send_keys("/home/mublan/Documentos/ManualdeServicioSocial.pdf")
	driver.find_element_by_css_selector('button.btn').click()

	print('Se ha subido correctamente el archivo '+nombre_archivo1)

	#Se guarda un pdf
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_id('id_nombre_archivo').send_keys(nombre_archivo2)
	driver.find_element_by_id('id_descripcion').send_keys('Catalogo de plazas de los SSZ')	
	driver.find_element_by_id('id_ruta_archivo').send_keys("/home/mublan/Documentos/PlazasSSZ.pdf")
	driver.find_element_by_css_selector('button.btn').click()

	print('Se ha subido correctamente el archivo '+nombre_archivo2)

	#Se guarda un pdf	
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	nombre_archivo = driver.find_element_by_id('id_nombre_archivo').send_keys(nombre_archivo3)
	descripcion = driver.find_element_by_id('id_descripcion').send_keys('Catalogo de plazas del IMSS')	
	ruta_archivo = driver.find_element_by_id('id_ruta_archivo').send_keys("/home/mublan/Documentos/PlazasIMSS.pdf")
	guardar_alta_info = driver.find_element_by_css_selector('button.btn').click()

	print('Se ha subido correctamente el archivo '+nombre_archivo3)
	driver.refresh()

def coordinador_logout():
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(10) > a').click()
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li.dropdown.open > ul > li:nth-child(4) > a').click()

	print ('Se ha cerrado correctamente la sesion')
	
coordinador_login()
alta_normatividad()
coordinador_logout()
finalizar_driver()