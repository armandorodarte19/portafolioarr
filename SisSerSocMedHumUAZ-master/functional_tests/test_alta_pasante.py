# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

driver = webdriver.Firefox()
tiempo_visualizacion = 3
datosPasantes = {'nombres' : ['Cesar', 'Erika', 'Carlos', 'Pedro', 'Jorge', 'Humberto', 'Patricia', 'Gloria'], 
				 'paternos' : ['Jiménez', 'Baez', 'Esquivel', 'Pérez', 'Barraza', 'Vásquez', 'Castañón', 'Tizcareño'],
				 'maternos': ['Guerrero', 'Rodríguez', 'Almaraz', 'Saucedo', 'Riveros', 'Maldonado', 'Chávez', 'González'],
				 'curps': ['NHYU4158769MHRDJN48', 'BHTE416987BGFDER56', 'NHYR143258VFRESD09', 'RERS099878OIUIJU01', 'UYTY478587LOLIUI00', 'IKUI012458YYTYUI10', 'EREW343545JUJYHG00', 'MKJH876765UYUYTR03'],
				 'rfcs': ['NHYU415876', 'BHTE416987', 'NHYR143258', 'RERS099878', 'UYTY478587', 'IKUI012458', 'EREW343545', 'MKJH876765'],
				 'proms_finals': ['9','9','8','7','10','7','10','8'],
				 'moviles': ['1548695321', '4859674563', '4859632147', '1258930157', '0218964753', '1027896542','1529756384', '1578903654'],
				 'calles': ['Allá', 'Aquí', 'lejos', 'cerca', 'Al lado', 'Al otro lado', 'En frente', 'Atras'],
				 'nums_exteriores': ['1', '10', '30', '2', '9', '11', '7', '5'],
				 'colonias': ['Interior', 'Exterior', 'tienda', 'bosque', 'montaña', 'ciudad', 'escuela', 'playa']
				}

matriculas = ['99999990', '99999991', '99999992', '99999993', '99999994', '99999995', '99999911', '99999912']

def pasante_login(credenciales):
	pasante_credenciales = credenciales
	driver.get('http://0.0.0.0:8000/')
	username = driver.find_element_by_id('username')
	username.send_keys(pasante_credenciales)
	password = driver.find_element_by_id('password')
	password.send_keys(pasante_credenciales)
	driver.find_element_by_id('login-submit').click()
	
def pasante_registro(nombre, ap_paterno, ap_materno, curp, rfc, promedio_final, movil, calle, num_exterior, colonia):
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(2) > a').click()
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li.dropdown.open > ul > li > a').click()

	time.sleep(1)
	driver.find_element_by_id('id_nombre').clear()
	driver.find_element_by_id('id_nombre').send_keys(nombre)
	time.sleep(1)
	driver.find_element_by_id('id_apellido_paterno').clear()
	driver.find_element_by_id('id_apellido_paterno').send_keys(ap_paterno)
	time.sleep(1)
	driver.find_element_by_id('id_apellido_materno').clear()
	driver.find_element_by_id('id_apellido_materno').send_keys(ap_materno)
	time.sleep(1)
	driver.find_element_by_id('id_curp').clear()
	driver.find_element_by_id('id_curp').send_keys(curp)
	time.sleep(1)
	driver.find_element_by_id('id_rfc').clear()
	driver.find_element_by_id('id_rfc').send_keys(rfc)
	time.sleep(1)
	driver.find_element_by_id('id_foto').clear()
	driver.find_element_by_id('id_foto').send_keys('/home/esteban/Downloads/30653272_1705889172783417_1270270679832330240_n.jpg')
	time.sleep(1)
	driver.find_element_by_id('id_promedio_final').clear()
	driver.find_element_by_id('id_promedio_final').send_keys(promedio_final)
	time.sleep(1)
	driver.find_element_by_id('id_número_móvil').clear()
	driver.find_element_by_id('id_número_móvil').send_keys(movil)
	time.sleep(1)
	driver.find_element_by_id('id_calle').clear()
	driver.find_element_by_id('id_calle').send_keys(calle)
	time.sleep(1)
	driver.find_element_by_id('id_número_exterior').clear()
	driver.find_element_by_id('id_número_exterior').send_keys(num_exterior)
	time.sleep(1)
	driver.find_element_by_id('id_colonia').clear()
	driver.find_element_by_id('id_colonia').send_keys(colonia)
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="estados"]/option[33]').click()
	time.sleep(1)
	driver.find_element_by_xpath('//*[@id="municipios"]/option[57]').click()	
	time.sleep(1)
	driver.find_element_by_css_selector('#intro > div > div > div.col-lg-12 > form > button').click()
	
def pasante_logout():
	driver.find_element_by_css_selector('.dropdown-toggle').click()
	driver.find_element_by_css_selector('.dropdown-menu > li:nth-child(2) > a:nth-child(1)').click()

#Metodo para finalizar el driver
def finalizar_driver():
    driver.quit()

for x in range(0, 8):
		pasante_login(matriculas[x])
		pasante_registro(datosPasantes['nombres'][x], datosPasantes['paternos'][x], datosPasantes['maternos'][x], 
		datosPasantes['curps'][x], datosPasantes['rfcs'][x], datosPasantes['proms_finals'][x], datosPasantes['moviles'][x],
		datosPasantes['calles'][x], datosPasantes['nums_exteriores'][x], datosPasantes['colonias'][x])
		pasante_logout()
finalizar_driver()