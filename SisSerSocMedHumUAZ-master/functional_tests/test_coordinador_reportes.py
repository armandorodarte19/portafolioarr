# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import re, sys, os, time

driver = webdriver.Firefox()
tiempo_visualizacion = 3
nuevo_coordinador_keys = '00000001'
nombre_new_coordinador = 'Diego 1'
nombre_archivo1 = 'Manual de Servicio Social'
nombre_archivo2 = 'Plazas SSZ'
nombre_archivo3 = 'Plazas IMSS'

#Metodo para finalizar el driver
def finalizar_driver():
    driver.quit()

def coordinador_login():
	#coordinador_credenciales = 34150139
	coordinador_credenciales = '00000000'
	#driver.get('http://148.217.200.108:8000')
	driver.get('http://0.0.0.0:8000/')
	username = driver.find_element_by_id('username')
	username.send_keys(coordinador_credenciales)
	password = driver.find_element_by_id('password')
	password.send_keys(coordinador_credenciales)
	driver.find_element_by_id('login-submit').click()

	print ('Se ha ingresado correctamente al sistema')

def coordinador_nuevo():
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(10) > a').click()
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li.dropdown.open > ul > li:nth-child(2) > a').click()	
	matricula = driver.find_element_by_id('id_matricula').send_keys(nuevo_coordinador_keys)
	contrasenia = driver.find_element_by_id('id_password').send_keys(nuevo_coordinador_keys)
	confirmar_contrasenia = driver.find_element_by_id('id_confirmar_password').send_keys(nuevo_coordinador_keys)
	correo_principal = driver.find_element_by_id('id_correo_electronico').send_keys('a1@mail.com')
	correo_alternativo = driver.find_element_by_id('id_correo_institucional').send_keys('a1@uaz.edu.mx')
	nombre = driver.find_element_by_id('id_nombre').send_keys(nombre_new_coordinador)
	ap_paterno = driver.find_element_by_id('id_apellido_paterno').send_keys('Blanco 1')
	ap_materno = driver.find_element_by_id('id_apellido_materno').send_keys('Murillo 1')
	driver.find_element_by_css_selector('button.btn').click()

	print ('Se ha creado correctamente el coordinador '+nombre_new_coordinador)

def coordinador_modificar():
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(10) > a').click()
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li.dropdown.open > ul > li:nth-child(1) > a').click()

	#print ('Los coordinadores no se pueden modificar mediante la interfaz')	

def coordinador_eliminar():
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(10) > a').click()
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li.dropdown.open > ul > li:nth-child(3) > a').click()	
	tabla = driver.find_element_by_id('mytable')
	tb_body = tabla.find_element_by_tag_name('tbody')
	filas = tb_body.find_elements_by_tag_name('tr')

	for fila in filas:
		celdas = fila.find_elements_by_tag_name('td')
		if nombre_new_coordinador in u'' + celdas[1].text:
			celdas[4].find_element_by_css_selector('button.btn').click()
			driver.find_element_by_css_selector('#mytable > tbody > tr > td:nth-child(6) > div > ul > li > a').click()
			print ('Se ha eliminado correctamente al coordinador '+nombre_new_coordinador)
		else:
			print ('No existe el coordinador '+nombre_new_coordinador)

def coordinador_logout():
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(10) > a').click()
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li.dropdown.open > ul > li:nth-child(4) > a').click()

	print ('Se ha cerrado correctamente la sesion')

def atrasado_etiqueta():
	#coordinador_login()
	driver.find_element_by_css_selector('.notificaton-btn2').click()
	
	"""No Visto"""
	driver.find_element_by_css_selector('.btn-negro').click()
	#driver.find_element_by_css_selector('.searchable > tr:nth-child(1) > td:nth-child(9) > div:nth-child(1) > button:nth-child(1)').click()

	"""Visto"""
	driver.find_element_by_css_selector('a.btn:nth-child(2)').click()

	"""Seguimiento"""
	driver.find_element_by_css_selector('a.btn:nth-child(3)').click()

	"""Atrasado"""
	driver.find_element_by_css_selector('a.btn:nth-child(4)').click()
	
	"""Resuelto"""
	driver.find_element_by_css_selector('a.btn:nth-child(5)').click()

	"""Todos"""
	driver.find_element_by_css_selector('a.btn:nth-child(6)').click()
	
def pendiente_etiqueta():
	#coordinador_login()
	driver.find_element_by_css_selector('.notificaton-btn').click()
	
	"""No Visto"""
	driver.find_element_by_css_selector('.btn-negro').click()
	#driver.find_element_by_css_selector('.searchable > tr:nth-child(1) > td:nth-child(9) > div:nth-child(1) > button:nth-child(1)').click()

	"""Visto"""
	driver.find_element_by_css_selector('a.btn:nth-child(2)').click()

	"""Seguimiento"""
	driver.find_element_by_css_selector('a.btn:nth-child(3)').click()

	"""Atrasado"""
	driver.find_element_by_css_selector('a.btn:nth-child(4)').click()
	
	"""Resuelto"""
	driver.find_element_by_css_selector('a.btn:nth-child(5)').click()

	"""Todos"""
	driver.find_element_by_css_selector('a.btn:nth-child(6)').click()
def reportes():
	#coordinador_login()
	driver.find_element_by_css_selector('li.dropdown:nth-child(6) > a:nth-child(1) > strong:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(6) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	
	"""No Visto"""
	driver.find_element_by_css_selector('div.col-lg-6:nth-child(1) > div:nth-child(3) > h6:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(7)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(8)').click()

	"""Visto"""
	driver.find_element_by_css_selector('a.btn:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(7)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(8)').click()

	"""Seguimiento"""
	driver.find_element_by_css_selector('a.btn:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(7)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(8)').click()

	"""Atrasado"""
	driver.find_element_by_css_selector('a.btn:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(7)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(8)').click()
	
	"""Resuelto"""
	driver.find_element_by_css_selector('a.btn:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(7)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(8)').click()

	"""Todos"""
	driver.find_element_by_css_selector('a.btn:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(2)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(3)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(4)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(5)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(6)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(7)').click()
	driver.find_element_by_css_selector('#intro > div > div > div:nth-child(1) > div:nth-child(4) > h6 > select > option:nth-child(8)').click()

def alta_normatividad():
	#coordinador_login()
	"""Alta informacion"""
	#Se guarda un pdf
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_id('id_nombre_archivo').send_keys(nombre_archivo1)
	driver.find_element_by_id('id_descripcion').send_keys('Manual de Servicio Social: Encontraras la normativa y reglamentos')
	driver.find_element_by_id('id_ruta_archivo').send_keys("/home/mublan/Documentos/ManualdeServicioSocial.pdf")
	driver.find_element_by_css_selector('button.btn').click()

	print('Se ha subido correctamente el archivo '+nombre_archivo1)

	#Se guarda un pdf
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_id('id_nombre_archivo').send_keys(nombre_archivo2)
	driver.find_element_by_id('id_descripcion').send_keys('Catalogo de plazas de los SSZ')	
	driver.find_element_by_id('id_ruta_archivo').send_keys("/home/mublan/Documentos/PlazasSSZ.pdf")
	driver.find_element_by_css_selector('button.btn').click()

	print('Se ha subido correctamente el archivo '+nombre_archivo2)

	#Se guarda un pdf	
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	nombre_archivo = driver.find_element_by_id('id_nombre_archivo').send_keys(nombre_archivo3)
	descripcion = driver.find_element_by_id('id_descripcion').send_keys('Catalogo de plazas del IMSS')	
	ruta_archivo = driver.find_element_by_id('id_ruta_archivo').send_keys("/home/mublan/Documentos/PlazasIMSS.pdf")
	guardar_alta_info = driver.find_element_by_css_selector('button.btn').click()

	print('Se ha subido correctamente el archivo '+nombre_archivo3)
	driver.refresh()
	#cancelar_alta_info = driver.find_element_by_css_selector('a.btn:nth-child(6)').click()
def mostrar_normatividad():
	"""Mostrar informacion"""
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)').click()
	
	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h4:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('#collapse1 > a:nth-child(1)').click()
	time.sleep(tiempo_visualizacion)
	print('Se ha visualizado correctamente el archivo '+nombre_archivo1)
	driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL+"w")
	driver.switch_to.window(driver.window_handles[0])
	
	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(2) > th:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h4:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('#collapse2 > a:nth-child(1)').click()
	time.sleep(tiempo_visualizacion)
	print('Se ha visualizado correctamente el archivo '+nombre_archivo2)
	driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL+"w")
	driver.switch_to.window(driver.window_handles[0])

	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(3) > th:nth-child(1) > div:nth-child(1) > div:nth-child(1) > h4:nth-child(1) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('#collapse3 > a:nth-child(1)').click()
	time.sleep(tiempo_visualizacion)
	print('Se ha visualizado correctamente el archivo '+nombre_archivo3)
	driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL+"w")
	#driver.switch_to.window('Servicio Social Medicina Humana UAZ')
	driver.switch_to.window(driver.window_handles[0])
	#btn_eliminar = drive.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(2) > button:nth-child(1)')
def eliminar_normatividad():

	os.system("gnome-terminal -e "+"'python3.6 test_eliminar_normatividad.py'")
	# #Chrome
	# #/html/body/div/section/div/div/div[1]/table/tbody/tr/th[2]/button
	# driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	# driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)').click()
	
	# driver.find_element_by_xpath('//*[@id="table"]/tbody/tr[1]/th[2]/button').click()
	# alert = driver.switch_to_alert()
	# alert.accept()
	# print('Se ha eliminado correctamente el archivo '+nombre_archivo1)
	# driver.refresh()
	# driver.find_element_by_xpath('//*[@id="table"]/tbody/tr[2]/th[2]/button').click()
	# alert = driver.switch_to_alert()
	# alert.accept()
	# print('Se ha eliminado correctamente el archivo '+nombre_archivo2)
	# driver.refresh()
	# driver.find_element_by_xpath('/html/body/div/section/div/div/div[1]/table/tbody/tr/th[2]/button').click()
	# alert = driver.switch_to_alert()
	# alert.accept()
	# print('Se ha eliminado correctamente el archivo '+nombre_archivo3)
	# driver.refresh()

	# -----------------------------------------------
	# tabla = driver.find_element_by_id('table')
	# tb_body = tabla.find_element_by_tag_name('tbody')
	# filas = tb_body.find_elements_by_tag_name('tr')

	# for fila in filas:
	# 	celdas = fila.find_elements_by_tag_name('th')
	# 	if nombre_archivo1 or nombre_archivo2 or nombre_archivo3 in u'' + celdas[0].text:
	# 		celdas[1].find_element_by_tag_name('button').click()
	# 		time.sleep(1)
	# 		alert = driver.switch_to_alert()
	# 		alert.accept()
	# 		print('Se ha eliminado correctamente el archivo ')
	# 	else:
	# 		print ('No existe el archivo a eliminar ')
	
	# driver.find_element_by_css_selector('#table > tbody > tr:nth-child(4) > th:nth-child(2) > button').click()
	# driver.send_keys(Keys.RETURN)
	# print('Se ha eliminado correctamente el archivo 4')
	
	# driver.find_element_by_css_selector('#table > tbody > tr:nth-child(5) > th:nth-child(2) > button').click()
	# driver.send_keys(Keys.RETURN)
	# print('Se ha eliminado correctamente el archivo 5')
	
	# driver.find_element_by_css_selector('#table > tbody > tr:nth-child(6) > th:nth-child(2) > button').click()
	# driver.send_keys(Keys.RETURN)
	# print('Se ha eliminado correctamente el archivo 6')


coordinador_login()
coordinador_nuevo()
coordinador_modificar()
coordinador_eliminar()
#atrasado_etiqueta()
#pendiente_etiqueta()
reportes()
alta_normatividad()
mostrar_normatividad()
eliminar_normatividad()
time.sleep(7)
print('Se han eliminado correctamente los archivos '+nombre_archivo1+", "+nombre_archivo2+" y "+nombre_archivo3)
# eliminar_normatividad()
# eliminar_normatividad()
# eliminar_normatividad()

coordinador_logout()
finalizar_driver()