# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import re
import sys

driver = webdriver.Chrome()
nombre_archivo1 = 'Manual de Servicio Social'
nombre_archivo2 = 'Plazas SSZ'
nombre_archivo3 = 'Plazas IMSS'

#Metodo para finalizar el driver
def finalizar_driver():
    driver.quit()

def coordinador_login():
	#coordinador_credenciales = 34150139
	coordinador_credenciales = '00000000'
	#driver.get('http://148.217.200.108:8000')
	driver.get('http://0.0.0.0:8000/')
	username = driver.find_element_by_id('username')
	username.send_keys(coordinador_credenciales)
	password = driver.find_element_by_id('password')
	password.send_keys(coordinador_credenciales)
	driver.find_element_by_id('login-submit').click()

	print ('Se ha ingresado correctamente al sistema')

def eliminar_normatividad():
	#Chrome
	#/html/body/div/section/div/div/div[1]/table/tbody/tr/th[2]/button
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(5) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)').click()
	
	driver.find_element_by_xpath('//*[@id="table"]/tbody/tr[1]/th[2]/button').click()
	alert = driver.switch_to_alert()
	alert.accept()
	print('Se ha eliminado correctamente el archivo '+nombre_archivo1)
	driver.refresh()
	driver.find_element_by_xpath('//*[@id="table"]/tbody/tr[2]/th[2]/button').click()
	alert = driver.switch_to_alert()
	alert.accept()
	print('Se ha eliminado correctamente el archivo '+nombre_archivo2)
	driver.refresh()
	driver.find_element_by_xpath('/html/body/div/section/div/div/div[1]/table/tbody/tr/th[2]/button').click()
	alert = driver.switch_to_alert()
	alert.accept()
	print('Se ha eliminado correctamente el archivo '+nombre_archivo3)
	driver.refresh()

coordinador_login()
eliminar_normatividad()
coordinador_logout()
finalizar_driver()