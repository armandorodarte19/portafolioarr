# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import re
import sys
import os

driver = webdriver.Firefox()
tiempo_visualizacion = 3

def pasante_login():
	pasante_credenciales = '99999999'
	driver.get('http://0.0.0.0:8000/')
	username = driver.find_element_by_id('username')
	username.send_keys(pasante_credenciales)
	password = driver.find_element_by_id('password')
	password.send_keys(pasante_credenciales)
	driver.find_element_by_id('login-submit').click()
	
def pasante_modificar():
	driver.find_element_by_css_selector('li.dropdown:nth-child(7) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(7) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	time.sleep(1)
	driver.find_element_by_id('id_nombre').clear()
	driver.find_element_by_id('id_nombre').send_keys('Misael Alonso')
	time.sleep(1)
	driver.find_element_by_id('id_apellido_paterno').clear()
	driver.find_element_by_id('id_apellido_paterno').send_keys('Rodriguez')
	time.sleep(1)
	driver.find_element_by_id('id_apellido_materno').clear()
	driver.find_element_by_id('id_apellido_materno').send_keys('Gutierrez')
	time.sleep(1)
	driver.find_element_by_id('id_número_móvil').clear()
	driver.find_element_by_id('id_número_móvil').send_keys('4999999999')
	time.sleep(1)
	driver.find_element_by_id('id_calle').clear()
	driver.find_element_by_id('id_calle').send_keys('Calle Morelos')
	time.sleep(1)
	driver.find_element_by_id('correo').clear()
	driver.find_element_by_id('correo').send_keys('misael@hotmail.com')
	time.sleep(1)
	#driver.find_element_by_css_selector('button.btn').click()

def pasante_carta_de_asignacion():
	#driver.get('http://148.217.200.108:8000/Rol/carta_asignacion?id_pasante=1')
	#driver.get('http://148.217.200.108:8000/Rol/carta_asignacion?id_pasante=1').send_keys(u'\ue006')
	driver.find_element_by_css_selector('.active > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(7) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(7) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)').click()
	driver.refresh()

def alerta():
	driver.find_element_by_css_selector('#wrapper > div > div.container.navigation > div.collapse.navbar-collapse.navbar-right.navbar-main-collapse > ul > li:nth-child(5) > a').click()
	#driver.find_element_by_css_selector('.nav > li:nth-child(5) > a:nth-child(1)').click()
	driver.find_element_by_id('agregar_button').click()
	driver.find_element_by_css_selector('div.form-group:nth-child(1) > input:nth-child(2)').send_keys('Juan Perez')
	driver.find_element_by_css_selector('div.form-group:nth-child(2) > input:nth-child(2)').send_keys('4929229458')
	driver.find_element_by_css_selector('button.btn').click()

	driver.find_element_by_id('agregar_button').click()
	driver.find_element_by_css_selector('div.form-group:nth-child(1) > input:nth-child(2)').send_keys('Sergio Ramos')
	driver.find_element_by_css_selector('div.form-group:nth-child(2) > input:nth-child(2)').send_keys('4921229758')
	driver.find_element_by_css_selector('button.btn').click()

	#Activar
	driver.find_element_by_id('btn-panico').click()
	time.sleep(6)
	#Desactivar
	driver.find_element_by_id('btn-panico').click()
	time.sleep(2)
	#Modificar
	# driver.find_element_by_css_selector('#tabla_contactos > table > tbody > tr:nth-child(1) > td:nth-child(4) > a.btn.btn-primary.btn-edit').click()
	# driver.find_element_by_name('nombre').clear()
	# driver.find_element_by_name('nombre').send_keys('Juan Perez Moya Santos')
	# driver.find_element_by_css_selector('button.btn:nth-child(3)').click()
	
	#Activar
	driver.find_element_by_id('btn-panico').click()
	time.sleep(18)

	pasante_login()

	driver.find_element_by_css_selector('.nav > li:nth-child(5) > a:nth-child(1) > strong:nth-child(1) > font:nth-child(1)').click()
	driver.find_element_by_css_selector('.table > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(4) > a:nth-child(2)').click()
	alert = driver.switch_to_alert()
	time.sleep(2)
	alert.accept()

	driver.find_element_by_css_selector('.table > tbody:nth-child(2) > tr:nth-child(2) > td:nth-child(4) > a:nth-child(2)').click()
	alert2 = driver.switch_to_alert()
	time.sleep(2)
	alert2.accept()

def normatividad():
	#os.system("gnome-terminal -e "+"'python3.6 alta_normatividad.py'")
	driver.find_element_by_css_selector('li.dropdown:nth-child(4) > a:nth-child(1)').click()
	#Mostrar Normatividad
	#driver.find_element_by_css_selector('li.dropdown:nth-child(4) > ul:nth-child(2) > li:nth-child(1)').click()

	#Ver normatividad
	driver.find_element_by_css_selector('li.dropdown:nth-child(4) > ul:nth-child(2) > li:nth-child(2) > a:nth-child(1)').click()
	
	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(2)').click()
	time.sleep(tiempo_visualizacion + 1)
	#print('Se ha visualizado correctamente el archivo ')
	#driver.find_element_by_tag_name('body').send_keys(Keys.CONTROL+"w")
	driver.switch_to.window(driver.window_handles[0])

	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(2) > th:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(2)').click()
	time.sleep(tiempo_visualizacion + 3)
	driver.switch_to.window(driver.window_handles[0])

	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(3) > th:nth-child(1) > div:nth-child(1) > div:nth-child(1) > button:nth-child(2)').click()
	time.sleep(tiempo_visualizacion + 1)
	driver.switch_to.window(driver.window_handles[0])	

	#Buscar tema en normatividad
	driver.find_element_by_css_selector('li.dropdown:nth-child(4) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(4) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)').click()
	
	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(1) > th:nth-child(2) > button:nth-child(1)').click()
	manual = driver.find_element_by_id('search-input')
	manual.send_keys('SUBPROGRAMA DE ATENCIÓN MÉDICA')
	time.sleep(tiempo_visualizacion)

	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(2) > th:nth-child(2) > button:nth-child(1)').click()
	plazas_ssz = driver.find_element_by_id('search-input')
	plazas_ssz.send_keys('LORETO')
	time.sleep(tiempo_visualizacion)
	
	driver.find_element_by_css_selector('#table > tbody:nth-child(1) > tr:nth-child(3) > th:nth-child(2) > button:nth-child(1)').click()
	plazasIMSS = driver.find_element_by_id('search-input')
	plazasIMSS.send_keys('GUADALUPE')
	time.sleep(tiempo_visualizacion)

# def modificar_perfil():
# 	driver.get('http://148.217.200.108:8000/Pasante/modificar_perfil/')

def reportes():
	driver.find_element_by_css_selector('li.dropdown:nth-child(2) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(2) > ul:nth-child(2) > li:nth-child(1) > a:nth-child(1)').click()
	
	"""No Visto"""
	driver.find_element_by_css_selector('a.btn:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(7)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(8)').click()

	"""Visto"""
	driver.find_element_by_css_selector('a.btn:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(7)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(8)').click()
	"""Seguimiento"""
	driver.find_element_by_css_selector('a.btn:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(7)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(8)').click()
	"""Atrasado"""
	driver.find_element_by_css_selector('a.btn:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(7)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(8)').click()
	"""Resuelto"""
	driver.find_element_by_css_selector('a.btn:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(7)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(8)').click()
	"""Todos"""
	driver.find_element_by_css_selector('a.btn:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(2)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(3)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(4)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(5)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(6)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(7)').click()
	driver.find_element_by_css_selector('div.container:nth-child(5) > h6:nth-child(1) > select:nth-child(1) > option:nth-child(8)').click()

def pasante_logout():
	driver.find_element_by_css_selector('li.dropdown:nth-child(7) > a:nth-child(1)').click()
	driver.find_element_by_css_selector('li.dropdown:nth-child(7) > ul:nth-child(2) > li:nth-child(3) > a:nth-child(1)').click()

#Metodo para finalizar el driver
def finalizar_driver():
    driver.quit()

pasante_login()
pasante_modificar()
#pasante_carta_de_asignacion()
alerta()
reportes()
normatividad()
pasante_logout()
finalizar_driver()