from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException

import unittest

import time

class NewVisitorTest(unittest.TestCase):  

    def setUp(self):  
        self.browser = webdriver.Firefox()
        self.browser.get('http://0.0.0.0:8000')

    def tearDown(self):  
        self.browser.quit()

    """

    def test_can_create_rotation(self):
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('ROLES').click()
        self.browser.find_element_by_link_text('Alta Rol').click()

        Select(self.browser.find_element_by_class_name('combobox.comboboxinstitucion')).select_by_visible_text('IMSS')

        self.browser.find_element_by_id('btnAgregarRotacion').click()

        self.browser.find_element_by_name('dateInicioRotacion1').send_keys('09/08/2018')
        self.browser.find_element_by_name('dateFinRotacion1').send_keys('18/08/2018')

        #Select(self.browser.find_element_by_xpath('//*[@id="combo_clinicas"]')).select_by_visible_text('756')
        
        self.browser.find_element_by_id("btnGuardarRol").click()

        time.sleep(5)

    """
    #@unittest.skip("Prueba pasada")
    def test_can_create_tutor(self):
        nombre = "Roberto"
        appat = "Quintero"
        apmat = "Ruiz"
        tel = "1254897632"
        email = "rq_123d@correo.com"
        sede = "UAZ"

        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('ROLES').click()
        self.browser.find_element_by_link_text('Alta Tutor').click()

        self.browser.find_element_by_id('nombre').send_keys(nombre)
        self.browser.find_element_by_id('apPaterno').send_keys(appat)
        self.browser.find_element_by_id('apMaterno').send_keys(apmat)
        self.browser.find_element_by_id('telefono').send_keys(tel)
        self.browser.find_element_by_id('email').send_keys(email)
        self.browser.find_element_by_id('sede').send_keys(sede)

        self.browser.find_element_by_id("btnGuardarTutor").click()

        time.sleep(5)

    #@unittest.skip("Prueba pasada")
    def test_delete_tutor(self):
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('ROLES').click()
        self.browser.find_element_by_link_text('Catálogo de Tutores').click()

        tabla = self.browser.find_element_by_tag_name('table')

        rows = tabla.find_elements_by_tag_name('tr')
        for r in rows :
            celdas = r.find_elements_by_tag_name('td')
            for c in celdas :
                if c.text == "Roberto" :
                    celdas[6].find_element_by_class_name('btn.btn-danger').click()
                    break

        self.browser.find_element_by_id("btnConfirmaEliminacionTutor").click()

        time.sleep(8)

    #@unittest.skip("Prueba pasada")
    def test_modify_tutor(self):
        nombre = "Carlos"
        appat = "Sosa"
        apmat = "Pérez"
        tel = "1254897632"
        email = "hdu_123d@correo.com"
        sede = "UAZ"

        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('ROLES').click()
        self.browser.find_element_by_link_text('Alta Tutor').click()

        self.browser.find_element_by_id('nombre').send_keys(nombre)
        self.browser.find_element_by_id('apPaterno').send_keys(appat)
        self.browser.find_element_by_id('apMaterno').send_keys(apmat)
        self.browser.find_element_by_id('telefono').send_keys(tel)
        self.browser.find_element_by_id('email').send_keys(email)
        self.browser.find_element_by_id('sede').send_keys(sede)

        self.browser.find_element_by_id("btnGuardarTutor").click()

        time.sleep(3)
        
        tabla = self.browser.find_element_by_tag_name('table')

        rows = tabla.find_elements_by_tag_name('tr')
        for r in rows :
            celdas = r.find_elements_by_tag_name('td')
            for c in celdas :
                if c.text == "Carlos" :
                    boton = celdas[6].find_element_by_class_name('btn.btn-warning')
                    boton.click()
                    time. sleep(4)
                    break
                

        #time.sleep(10)

        nombre2 = "Enrique"
        email2 = "hdydfb_h@ufud.com"

        nom = self.browser.find_element_by_id('nombre')
        nom.clear()
        nom.send_keys(nombre2)

        em = self.browser.find_element_by_id('email')
        em.clear()
        em.send_keys(email2)

        self.browser.find_element_by_id("btnGuardarTutor").click()

        time.sleep(8)

    """"

    def test_carta_asignacion(self):
        fecha_inicio = "11/04/2018"
        fecha_fin = "12/04/2018"
        fecha_asignacion = "10/04/2018"
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('ROLES').click()
        self.browser.find_element_by_partial_link_text('Configuración').click()

        fin = self.browser.find_element_by_id('dateFinPromo')
        fin.click()
        fin.send_keys

        asig = self.browser.find_element_by_name('acuerdo_asignacion')
        asig.send_keys(fecha_asignacion)

        inicio = self.browser.find_element_by_id('dateInicioPromo')
        inicio.send_keys(fecha_inicio)

        self.browser.find_element_by_id("btnGuardarConfig").click()

        time.sleep(10)

    """
    # @unittest.skip("Prueba pasada")
    # def test_find_rol(self):
    #     self.inicio_sesion("00000000", "00000000")

    #     time.sleep(3)

    #     self.browser.find_element_by_link_text('ROLES').click()
    #     self.browser.find_element_by_partial_link_text('Catálogo de Roles').click()

    #     time.sleep(1)

    #     self.browser.find_element_by_id('filterRoles').send_keys("A18002")
    #     #rol = self.browser.find_element_by_class_name('panel-heading')
    #     #r2 = rol.find_element_by_class_name('panel-title')
    #     #r2.find_element_by_link_text('A18002').click()
    #     self.browser.find_element_by_link_text('A18002').click()
        
    #     time.sleep(8)


    # @unittest.skip("Prueba pendiente")
    # def test_delete_rol(self):
    #     self.inicio_sesion("00000000", "00000000")

    #     self.browser.find_element_by_link_text('ROLES').click()
    #     self.browser.find_element_by_partial_link_text('Catálogo de Roles').click()

    #     time.sleep(1)

    #     self.browser.find_element_by_id('filterRoles').send_keys("A18002")
    #     #rol = self.browser.find_element_by_class_name('panel-heading')
    #     #r2 = rol.find_element_by_class_name('panel-title')
    #     #r2.find_element_by_link_text('A18002').click()
    #     self.browser.find_element_by_class_name('btn.btn-danger').click()

    #     time.sleep(3)

    #     self.browser.find_element_by_id("btnConfirmaEliminacionRol").click()

    #     time.sleep(8)

    #@unittest.skip("Prueba pasada")
    def test_order_clinica(self):
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        time.sleep(3)

        self.crea_clinica('IMSS','Otra', 'A', 'C', 'Ob 1', 'Uno', 'Colima', 'Manzanillo')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        self.crea_clinica('ISSSTE','Not Found', 'D', 'A', 'Peligrus', 'Dos', 'Puebla', 'Amozoc')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        self.crea_clinica('SEDENA','Mist', 'C', 'B', 'Nuevita', 'Tres', 'Yucatán', 'Baca')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        self.crea_clinica('SSZ','Yee', 'B', 'D', 'Lost', 'Cuatro', 'Sinaloa', 'Choix')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_link_text('Buscar Clínica').click()

        Select(self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/h6/select')).select_by_visible_text('Nombre')
        time.sleep(2)
        Select(self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/h6/select')).select_by_visible_text('Plaza')
        time.sleep(2)
        Select(self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/h6/select')).select_by_visible_text('Complemento')
        time.sleep(2)
        Select(self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/h6/select')).select_by_visible_text('Status')
        time.sleep(2)
        Select(self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/h6/select')).select_by_visible_text('Estado')
        time.sleep(2)

    #@unittest.skip("Prueba pasada")
    def test_find_clinica(self):
        self.inicio_sesion("00000000", "00000000")
        
        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_link_text('Alta Clínica').click()
        
        time.sleep(2)

        self.crea_clinica('IMSS','Otra', 'A', 'C', 'Ob 1', 'Uno', 'Colima', 'Manzanillo')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        self.crea_clinica('ISSSTE','Not Found', 'D', 'A', 'Peligrus', 'Dos', 'Puebla', 'Amozoc')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        self.crea_clinica('SEDENA','Mist', 'C', 'B', 'Nuevita', 'Tres', 'Yucatán', 'Baca')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        self.crea_clinica('SSZ','Yee', 'B', 'D', 'Lost', 'Cuatro', 'Sinaloa', 'Choix')
        time.sleep(2)

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Buscar Clínica por Campos').click()
        
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        time.sleep(5)
        self.browser.find_element_by_id('unidad_filter-1').send_keys('Mist')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()
        
        time.sleep(1)

        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        Select(self.browser.find_element_by_id('plaza_federal_filter-1')).select_by_visible_text('C')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()

        time.sleep(1)

        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        Select(self.browser.find_element_by_id('complemento_filter-1')).select_by_visible_text('A')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()

        time.sleep(1)

        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        Select(self.browser.find_element_by_id('institucion_filter-1')).select_by_visible_text('SEDENA')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()

        time.sleep(1)

        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        Select(self.browser.find_element_by_id('status_filter-1')).select_by_visible_text('Desactivado')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()

        time.sleep(1)

        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        self.browser.find_element_by_id('unidad_filter-1').send_keys('Dos')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()

        time.sleep(1)

        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/button[1]').click()
        Select(self.browser.find_element_by_id('estados1')).select_by_visible_text('Sinaloa')
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[2]/div[2]/div/div[3]/button[2]').click()

        time.sleep(3)


    #@unittest.skip("Prueba pasada")
    def test_create_clinica(self):
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Alta Clínica').click()

        time.sleep(2)

        self.crea_clinica('IMSS','nueva', 'A', 'D', 'X', 'Lejos', 'Zacatecas', 'Guadalupe')

        time.sleep(8)

    #@unittest.skip("Prueba pasada")
    def test_create_reporte_clinica_pdf(self):
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Buscar Clínica').click()

        time.sleep(2)

        self.browser.find_element_by_id('btnModalReporte').click()
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[1]/div[2]/div/div[2]/table/tbody/tr/td[1]/form/button').click()
        time.sleep(5)
          
    #@unittest.skip("Prueba pasada")
    def test_create_reporte_clinica_xls(self):
        self.inicio_sesion("00000000", "00000000")

        self.browser.find_element_by_link_text('CLÍNICAS').click()
        self.browser.find_element_by_partial_link_text('Buscar Clínica').click()

        time.sleep(2)

        self.browser.find_element_by_id('btnModalReporte').click()
        self.browser.find_element_by_xpath('/html/body/div/section/div/div/div[1]/div[3]/div[1]/div[2]/div/div[2]/table/tbody/tr/td[2]/form/button').click()
        time.sleep(8) 
        

    def crea_clinica(self, inst, unidad, plaza, comp, observ, local, estado, mun):
        Select(self.browser.find_element_by_id('id_institucion')).select_by_visible_text(inst)
        self.browser.find_element_by_id('id_nombre_unidad').send_keys(unidad)
        Select(self.browser.find_element_by_id('id_plaza_fed')).select_by_visible_text(plaza)
        Select(self.browser.find_element_by_id('id_complemento')).select_by_visible_text(comp)
        self.browser.find_element_by_id('id_observación').send_keys(observ)
        self.browser.find_element_by_id('id_localidad').send_keys(local)
        Select(self.browser.find_element_by_id('estados')).select_by_visible_text(estado)
        time.sleep(3)
        Select(self.browser.find_element_by_id('municipios')).select_by_visible_text(mun)

        self.browser.find_element_by_class_name('btn.btn-primary.btn-lg').click()

    def inicio_sesion(self, usr, pwd):
        self.browser.find_element_by_id("username").send_keys(usr)
        self.browser.find_element_by_id("password").send_keys(pwd)

        self.browser.find_element_by_id("login-submit").click()


    def is_element_present(self, how, what):
        try:
            self.browser.find_element(by=how, value=what)
        except NoSuchElementException:
            return False
        return True

if __name__ == '__main__':  
    unittest.main(warnings='ignore')