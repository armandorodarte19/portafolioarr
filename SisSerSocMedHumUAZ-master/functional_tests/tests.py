from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
from selenium.common.exceptions import WebDriverException

MAX_WAIT = 10

class GenerarReporteTest(LiveServerTestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()

    def tearDown(self):
        self.browser.quit()


    def esperar_cargamiento_de_pagina(self, row_text):
        start_time = time.time()
        while True:
            try:
                table = self.browser.find_element_by_id('id_reporte')
                rows = table.find_elements_by_tag_name('tr')
                self.assertIn(row_text, [row.text for row in rows])
                return
            except (AssertionError, WebDriverException) as e:
                if time.time() - start_time > MAX_WAIT:
                    raise e
                time.sleep(0.5)

    def test_verificar_diferentes_usuarios_comiencen_sus_listas_de_diferentes_urls(self):
        #Feature: Crear un nuevo reporte de inconformidad
        #Como usuario pasante del sistema
        #quiero crear un nuevo reporte de inconformidad
        #para poder reportar sucesos ocurridos en la clínica en donde me encuentro

        #Scenario: Crear nuevo reporte de inconformidad
        #Dado a que inicio sesion como “31123452” con la contraseña “asdf”
        self.browser.get(self.live_server_url)
        inputbox_usuario = self.browser.find_element_by_id('username')
        self.assertEqual(
            inputbox_usuario.get_attribute('placeholder'),
            'Matrícula'
        )
        inputbox_usuario.send_keys('31123452')

        inputbox_contra = self.browser.find_element_by_id('password')
        self.assertEqual(
            inputbox_contra.get_attribute('placeholder'),
            'Contraseña'
        )
        inputbox_contra.send_keys('asdf')
        inputbox_contra.send_keys(Keys.ENTER)
        #doy click en el botón menú y selecciono la opción “Generar reporte de inconformidad”
        btn_menu = self.browser.find_elements_by_xpath("/html/body/div/div/div[3]/div[2]/ul/li[4]/a")
        btn_menu.send_keys(Keys.ENTER)

        btn_reporte = self.browser.find_elements_by_xpath("/html/body/div/div/div[3]/div[2]/ul/li[4]/ul/li[3]/a")
        btn_reporte.send_keys(Keys.ENTER)

        #entonces puedo seleccionar el tipo de reporte como “Seguridad”
        btn_tipo = self.browser.find_elements_by_xpath("//*[@id='id_id_tipo']")
        btn_tipo.send_keys('s')
        btn_tipo.send_keys(Keys.ENTER)
        #e ingreso el texto “Ayer se suscitó un asalto en la clínica en donde me encuentro porque la chapa de la puerta principal se encuentra dañada y la puerta no cierra completamente” en el campo de Reporte de inconformidad
        btn_descripcion = self.browser.find_elements_by_xpath("//*[@id='id_id_tipo']")
        btn_descripcion.send_keys('Ayer se suscitó un asalto en la clínica en donde me encuentro porque la chapa de la puerta principal se encuentra dañada y la puerta no cierra completamente')
        btn_descripcion.send_keys(Keys.ENTER)
        #Entonces doy click en Enviar reporte y verifico que se haya enviado correctamente mediante un mensaje que diga “Reporte Enviado”
        btn_enviar = self.browser.find_elements_by_xpath("/html/body/div/section/div/div/div[1]/form/button")
        btn_enviar.send_keys(Keys.ENTER)
        self.assertIn('Servicio Social Medicina Humana UAZ', self.browser.title)

        #y por último, cierro mi navegador
        tearDown()
