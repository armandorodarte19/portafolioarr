"""ssmh_uaz URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from contactos_pasante import  views



urlpatterns = [
    url(r'^chat/', include('mensajeria.urls')),
    url(r'^', include('usuario.urls')),
    url(r'^admin/', admin.site.urls),
    url(r'^Pasante/',include('pasante.urls')),
    url(r'^Coordinador/',include('coordinador.urls')),
    #url(r'^SuperUsuario/',include('SuperUsuario.urls')),
    url(r'^Rol/',include('rol.urls')),
    url(r'^foro/',include('foro.urls')),
    url(r'^Pregunta/',include('pregunta.urls')),

    url(r'^api/contacto/obtenerContactos/(?P<idPasante>.+)/$', views.GetContactos.as_view(), name='obtener_contactos'),
    url(r'^api/contacto/insertarContacto', views.InsertContacto.as_view(), name='insertar_contacto'),
    url(r'^api/contacto/eliminarContacto/(?P<idContacto>[0-9]+)/$', views.EliminarContacto.as_view(), name='eliminar_contacto'),
    url(r'^api/contacto/actualizarContacto/(?P<idContacto>[0-9]+)/$', views.ActualizarContacto.as_view(), name='actualizar_contacto'),
    url(r'^api/usuario/verificarUsuario/$', views.VerificarUsuario.as_view(), name='verificar_usuario'),
    url(r'^api/usuario/activaralerta$', views.ActivarAlerta.as_view(), name='alerta'),
    url(r'^api/usuario/informacionPasante/$', views.InformacionPasante.as_view()),
    url(r'^api/usuario/enviarMensaje/$', views.EnviarMensaje.as_view(), name='Enviar Mensaje'),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
