-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 31-05-2018 a las 12:04:29
-- Versión del servidor: 5.5.46-0+deb8u1
-- Versión de PHP: 5.6.33-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `bd_ssmh_uaz`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
`id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
`id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
`id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add user', 2, 'add_user'),
(5, 'Can change user', 2, 'change_user'),
(6, 'Can delete user', 2, 'delete_user'),
(7, 'Can add permission', 3, 'add_permission'),
(8, 'Can change permission', 3, 'change_permission'),
(9, 'Can delete permission', 3, 'delete_permission'),
(10, 'Can add group', 4, 'add_group'),
(11, 'Can change group', 4, 'change_group'),
(12, 'Can delete group', 4, 'delete_group'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add clinica', 7, 'add_clinica'),
(20, 'Can change clinica', 7, 'change_clinica'),
(21, 'Can delete clinica', 7, 'delete_clinica'),
(22, 'Can add direccion clinica', 8, 'add_direccionclinica'),
(23, 'Can change direccion clinica', 8, 'change_direccionclinica'),
(24, 'Can delete direccion clinica', 8, 'delete_direccionclinica'),
(25, 'Can add institucion', 9, 'add_institucion'),
(26, 'Can change institucion', 9, 'change_institucion'),
(27, 'Can delete institucion', 9, 'delete_institucion'),
(28, 'Can add seguimiento', 10, 'add_seguimiento'),
(29, 'Can change seguimiento', 10, 'change_seguimiento'),
(30, 'Can delete seguimiento', 10, 'delete_seguimiento'),
(31, 'Can add comentarios inconformidad', 11, 'add_comentariosinconformidad'),
(32, 'Can change comentarios inconformidad', 11, 'change_comentariosinconformidad'),
(33, 'Can delete comentarios inconformidad', 11, 'delete_comentariosinconformidad'),
(34, 'Can add fecha_registro', 12, 'add_fecha_registro'),
(35, 'Can change fecha_registro', 12, 'change_fecha_registro'),
(36, 'Can delete fecha_registro', 12, 'delete_fecha_registro'),
(37, 'Can add coordinador', 13, 'add_coordinador'),
(38, 'Can change coordinador', 13, 'change_coordinador'),
(39, 'Can delete coordinador', 13, 'delete_coordinador'),
(40, 'Can add estado', 14, 'add_estado'),
(41, 'Can change estado', 14, 'change_estado'),
(42, 'Can delete estado', 14, 'delete_estado'),
(43, 'Can add catalogo tipo inconformidad', 15, 'add_catalogotipoinconformidad'),
(44, 'Can change catalogo tipo inconformidad', 15, 'change_catalogotipoinconformidad'),
(45, 'Can delete catalogo tipo inconformidad', 15, 'delete_catalogotipoinconformidad'),
(46, 'Can add reporte inconformidad', 16, 'add_reporteinconformidad'),
(47, 'Can change reporte inconformidad', 16, 'change_reporteinconformidad'),
(48, 'Can delete reporte inconformidad', 16, 'delete_reporteinconformidad'),
(49, 'Can add municipio', 17, 'add_municipio'),
(50, 'Can change municipio', 17, 'change_municipio'),
(51, 'Can delete municipio', 17, 'delete_municipio'),
(52, 'Can add catalogo estatus inconformidad', 18, 'add_catalogoestatusinconformidad'),
(53, 'Can change catalogo estatus inconformidad', 18, 'change_catalogoestatusinconformidad'),
(54, 'Can delete catalogo estatus inconformidad', 18, 'delete_catalogoestatusinconformidad'),
(55, 'Can add informacion', 19, 'add_informacion'),
(56, 'Can change informacion', 19, 'change_informacion'),
(57, 'Can delete informacion', 19, 'delete_informacion'),
(58, 'Can add asignacion clinica pasante', 20, 'add_asignacionclinicapasante'),
(59, 'Can change asignacion clinica pasante', 20, 'change_asignacionclinicapasante'),
(60, 'Can delete asignacion clinica pasante', 20, 'delete_asignacionclinicapasante'),
(61, 'Can add direccion pasante', 21, 'add_direccionpasante'),
(62, 'Can change direccion pasante', 21, 'change_direccionpasante'),
(63, 'Can delete direccion pasante', 21, 'delete_direccionpasante'),
(64, 'Can add promocion', 22, 'add_promocion'),
(65, 'Can change promocion', 22, 'change_promocion'),
(66, 'Can delete promocion', 22, 'delete_promocion'),
(67, 'Can add telefono', 23, 'add_telefono'),
(68, 'Can change telefono', 23, 'change_telefono'),
(69, 'Can delete telefono', 23, 'delete_telefono'),
(70, 'Can add pasante', 24, 'add_pasante'),
(71, 'Can change pasante', 24, 'change_pasante'),
(72, 'Can delete pasante', 24, 'delete_pasante'),
(73, 'Can add generacion', 25, 'add_generacion'),
(74, 'Can change generacion', 25, 'change_generacion'),
(75, 'Can delete generacion', 25, 'delete_generacion'),
(76, 'Can add datos academicos', 26, 'add_datosacademicos'),
(77, 'Can change datos academicos', 26, 'change_datosacademicos'),
(78, 'Can delete datos academicos', 26, 'delete_datosacademicos'),
(79, 'Can add user', 27, 'add_usuario'),
(80, 'Can change user', 27, 'change_usuario'),
(81, 'Can delete user', 27, 'delete_usuario'),
(82, 'Can add tutor', 28, 'add_tutor'),
(83, 'Can change tutor', 28, 'change_tutor'),
(84, 'Can delete tutor', 28, 'delete_tutor'),
(85, 'Can add rol', 29, 'add_rol'),
(86, 'Can change rol', 29, 'change_rol'),
(87, 'Can delete rol', 29, 'delete_rol'),
(88, 'Can add configuracion asignacion', 30, 'add_configuracionasignacion'),
(89, 'Can change configuracion asignacion', 30, 'change_configuracionasignacion'),
(90, 'Can delete configuracion asignacion', 30, 'delete_configuracionasignacion'),
(91, 'Can add asignacion rol', 31, 'add_asignacionrol'),
(92, 'Can change asignacion rol', 31, 'change_asignacionrol'),
(93, 'Can delete asignacion rol', 31, 'delete_asignacionrol'),
(94, 'Can add rotacion', 32, 'add_rotacion'),
(95, 'Can change rotacion', 32, 'change_rotacion'),
(96, 'Can delete rotacion', 32, 'delete_rotacion'),
(97, 'Can add contactos pasante', 33, 'add_contactospasante'),
(98, 'Can change contactos pasante', 33, 'change_contactospasante'),
(99, 'Can delete contactos pasante', 33, 'delete_contactospasante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
`id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$100000$cmeIKaqfhhVz$BWbV1+KgirOmaJ8PHqeQD+dgC033zclb+5iLSDh5yPE=', '2018-05-31 16:59:16', 1, '34150139', '', '', '', 1, 1, '2018-01-13 18:31:50'),
(2, 'pbkdf2_sha256$100000$Us2dXmhqgEbL$SBQ/UJExG0OtKX+kdFtyoN+wPnV84vw0nNCl0KHg05c=', '2018-05-31 16:57:42', 0, '34150140', '', '', '', 0, 1, '2018-01-13 18:33:16'),
(3, 'pbkdf2_sha256$100000$tHLmWoFbSpVq$23ihpgrekWP4YO9/RNkJI1v8EfqgV4drPM5tCpgrN7Q=', '2018-01-16 05:26:21', 0, '30111914', '', '', '', 0, 1, '2018-01-13 18:42:42'),
(4, 'pbkdf2_sha256$100000$DZur4VjRc4OJ$zKPvmxToldndBpQ0G98fBnFJn2tw0PimETHSGE+78v8=', '2018-01-16 01:44:46', 0, '31120131', '', '', '', 0, 1, '2018-01-13 22:08:37'),
(5, 'pbkdf2_sha256$100000$ABdZDEc3Y6co$GYDFJzdnRpLRNvO9wJ1aE+rsPFj48pHKnWCuLreDuaw=', '2018-01-16 06:40:30', 0, '31120612', '', '', '', 0, 1, '2018-01-14 02:17:15'),
(6, 'pbkdf2_sha256$100000$XPe9V4lC38Pq$qaMeCu7hK+Iqr4UL82S6HOqbaFrG4bv6p4bykJRblbE=', '2018-01-16 13:05:04', 0, '28903365', '', '', '', 0, 1, '2018-01-14 03:12:16'),
(7, 'pbkdf2_sha256$100000$M059nMO1xSlw$xQLQ1lMMv5c98MV67CV4bo5pIj1dIt/2RwqZFw68ijY=', '2018-01-14 19:59:23', 0, '30111121', '', '', '', 0, 1, '2018-01-14 19:59:19'),
(8, 'pbkdf2_sha256$100000$pyOh1dg0ERGJ$zjHYA3PwG304cpKRclCrbbrgwYeN7owTlkNYnLYaLMU=', '2018-01-16 03:04:37', 0, '31128560', '', '', '', 0, 1, '2018-01-14 20:11:57'),
(9, 'pbkdf2_sha256$100000$bITKkfWqPWQ8$ZFX7SgAdcm8t/mCKo9DyjINBXIe75/htw+g1bjULmEY=', '2018-01-16 15:09:32', 0, '26704474', '', '', '', 0, 1, '2018-01-14 20:49:54'),
(10, 'pbkdf2_sha256$100000$QUOzUQjVgj5L$RltCLEKJaYCRTCnzvkcuzRLd/SpOc3BsuW4p5tDv65A=', '2018-01-16 02:41:26', 0, '31120503', '', '', '', 0, 1, '2018-01-14 21:37:04'),
(11, 'pbkdf2_sha256$100000$2Q4dc8KWoISy$++l9x3rAxPRyZD7SmqHl0Q7SLcV/bZwuEjA5eArvm0g=', '2018-01-14 22:26:00', 0, '28902282', '', '', '', 0, 1, '2018-01-14 22:23:53'),
(12, 'pbkdf2_sha256$100000$zYFCRcMMd2jG$E2FgLP1C9zK24BDrUb7JfrpwltYDslkioZvucRcclmk=', '2018-01-15 00:01:05', 0, '21202738', '', '', '', 0, 1, '2018-01-14 22:30:53'),
(13, 'pbkdf2_sha256$100000$bX1f41GCdtya$0HUD/EdNnjsHKMFN88QCEN74u2nSOhSDLM0ahohiI8E=', '2018-01-15 00:04:37', 0, '29102685', '', '', '', 0, 1, '2018-01-15 00:04:16'),
(14, 'pbkdf2_sha256$100000$vgEP5rvw9FW5$EeNZltuQoGnfWuQ8itym3o/wcAnKi1A6cLbhtWNVOB0=', '2018-01-16 05:42:15', 0, '28903266', '', '', '', 0, 1, '2018-01-15 01:10:29'),
(15, 'pbkdf2_sha256$100000$DdRhJZnCe31s$rJOqur6+ATQaKHkoubSqIMEJThJ6tOWE8A2rVZdiiD0=', '2018-01-15 02:11:19', 0, '31120218', '', '', '', 0, 1, '2018-01-15 02:10:58'),
(16, 'pbkdf2_sha256$100000$2kzKMLNEFFsj$mo40YRv3QMrCMCljS21jEw0m4yx9CIrreq9qcBTSEEM=', '2018-01-16 02:48:08', 0, '28902211', '', '', '', 0, 1, '2018-01-15 03:09:19'),
(17, 'pbkdf2_sha256$100000$vE1mPrgaeXAb$OKNv5/UoFJrJpGdbQ9BwFO7lwmCssaOo1Zgm1rkEtm0=', '2018-01-16 02:06:22', 0, '28901734', '', '', '', 0, 1, '2018-01-15 03:39:52'),
(18, 'pbkdf2_sha256$100000$dVNfUd0AiKeN$Odbik+rc8WkaxKfQdHsD64mD2g3erdJVU05WtKxJjKI=', '2018-01-16 00:27:44', 0, '25600462', '', '', '', 0, 1, '2018-01-15 04:13:59'),
(19, 'pbkdf2_sha256$100000$LCYEmK5nYdYM$Ws9O2fXPrcV7+P+lBH6HAEku8AL4ewbvtEvQdDc5dNM=', '2018-01-16 06:46:49', 0, '31120593', '', '', '', 0, 1, '2018-01-15 04:25:50'),
(20, 'pbkdf2_sha256$100000$L7w8509XIRk5$uN7MnswECQN9Unw+5zLC2yxTQVQveQExHVZvLfUDHps=', '2018-01-15 21:37:41', 0, '23400451', '', '', '', 0, 1, '2018-01-15 21:37:23'),
(21, 'pbkdf2_sha256$100000$4V6iL2MEqmdS$znX16P/b0QwrWWXeIpFlq74IAs6fNi2t1g8mrEIQ8fg=', '2018-01-15 22:33:38', 0, '31120581', '', '', '', 0, 1, '2018-01-15 22:33:30'),
(22, 'pbkdf2_sha256$100000$gxipmAOlDzEN$nMiV7F9xLBYVH2gswxhyRVK+lDTW9+ITPeRxKxdVgOY=', '2018-01-16 02:14:25', 0, '31120340', '', '', '', 0, 1, '2018-01-15 22:49:24'),
(23, 'pbkdf2_sha256$100000$9ut06LeukEOH$CAEt/GPR0PTzMLn1x+iUZbRXrl2wXBkWY8vwY9DkH+c=', '2018-01-16 03:53:11', 0, '27804491', '', '', '', 0, 1, '2018-01-15 22:57:55'),
(24, 'pbkdf2_sha256$100000$ka0NOO9Rnbjx$SWYwsqteb6W4gfa7jzvTc0UZ45B7KQRHbealiFFRSss=', '2018-01-15 23:06:01', 0, '30111146', '', '', '', 0, 1, '2018-01-15 23:05:39'),
(25, 'pbkdf2_sha256$100000$gRJ9O4wOaJeU$uY/olbBwQ6+gANWnaf5flvKWgyemIGjgpbVTZLJwKfM=', '2018-01-15 23:10:54', 0, '31128518', '', '', '', 0, 1, '2018-01-15 23:10:28'),
(26, 'pbkdf2_sha256$100000$tQvz4vqKQmWU$BfgwtR2RZqi2eUm189uCEwJ1iOpmKrmbKz0hNqR38/Q=', '2018-01-16 13:43:10', 0, '31122238', '', '', '', 0, 1, '2018-01-15 23:14:23'),
(27, 'pbkdf2_sha256$100000$aZ7p6tUUFPJF$TERiwEPMMcSJRqAzB0SLcQBlTpe2aMWSjYgk1pQT7gY=', '2018-01-16 03:09:18', 0, '20001309', '', '', '', 0, 1, '2018-01-15 23:18:40'),
(28, 'pbkdf2_sha256$100000$RSV1Ijq4SI3L$gA6YERGA3UhPa+GZ1xU6eH6GzkyMn4+Hx/eU6OXV5GQ=', '2018-01-16 01:18:14', 0, '31128622', '', '', '', 0, 1, '2018-01-15 23:23:20'),
(29, 'pbkdf2_sha256$100000$CKgI5IBSzPm0$N+NrZF1o11mr0l5od62LIFN99zMsLvQ9UO4oAogn7ak=', '2018-01-15 23:49:46', 0, '29106463', '', '', '', 0, 1, '2018-01-15 23:49:28'),
(30, 'pbkdf2_sha256$100000$0kV8pmF3l05M$brk/Y2Z28HeHxiZs3bTmLFrGs0imb2pxUdClX2ssgKM=', '2018-01-16 00:39:16', 0, '24501180', '', '', '', 0, 1, '2018-01-15 23:57:39'),
(31, 'pbkdf2_sha256$100000$LW9OEPPZSygy$e2TSF/ikzDJ5PuN2hgjrphaf1bbQzphvpKiuuR2nQw4=', '2018-01-16 00:48:58', 0, '27801147', '', '', '', 0, 1, '2018-01-15 23:58:47'),
(32, 'pbkdf2_sha256$100000$WkaIFlfeEVSy$ZTKX1Ar2tBuTB36NPpFmxUNO2hAkRcq82UvSzwfGOLk=', '2018-01-16 00:00:09', 0, '31120571', '', '', '', 0, 1, '2018-01-15 23:59:53'),
(33, 'pbkdf2_sha256$100000$GiV1fAg68KE1$rDhGnYclIvqEW3cq8ndGQD3QXcoCgbcm3erBOaXUZOY=', '2018-01-16 00:44:43', 0, '31120339', '', '', '', 0, 1, '2018-01-16 00:04:24'),
(34, 'pbkdf2_sha256$100000$73yxOTG14y0o$mH0ydYFH/ACHNoBofUx78HfottWMTloHzr0h2/QDfWM=', '2018-01-16 00:35:47', 0, '28901405', '', '', '', 0, 1, '2018-01-16 00:06:04'),
(35, 'pbkdf2_sha256$100000$mpxa2zKbbqar$mJh+etn6tL4pjTSRlnW4NB8esGHuINObVJEsGxGVWUg=', '2018-01-16 15:10:29', 0, '27803467', '', '', '', 0, 1, '2018-01-16 00:07:00'),
(36, 'pbkdf2_sha256$100000$61QFdgkDcUdo$4LCyfotljBmhr4PyMreIveEIq/kwzSMJo/Ay3KtlBxU=', '2018-01-16 00:14:14', 0, '31128569', '', '', '', 0, 1, '2018-01-16 00:08:50'),
(37, 'pbkdf2_sha256$100000$w7OjKEKJdjq1$zMxX/bhP7EmR2Lpwe+8dPKrS75xTnAriblR4zYZT6dA=', '2018-01-16 01:44:28', 0, '31128554', '', '', '', 0, 1, '2018-01-16 00:29:22'),
(38, 'pbkdf2_sha256$100000$A56IMb73z1j9$ajE1vajmcHIPoGSvh1UyVr+UJi9tolV3MC3izuu+aHk=', '2018-01-16 01:41:55', 0, '28903408', '', '', '', 0, 1, '2018-01-16 00:34:31'),
(39, 'pbkdf2_sha256$100000$Vkb3jL1kkBwV$9wAvkCmaA+2StM3tpTRrgTLrxjBdXqrTsIRhFQwNFuQ=', '2018-01-16 14:58:18', 0, '31120584', '', '', '', 0, 1, '2018-01-16 00:35:10'),
(40, 'pbkdf2_sha256$100000$VmNZJRxYLZKu$o61Zi3W38UiYyWwrb8EO4Iq1YdLB5Kh9DmAGtbyyItQ=', '2018-01-16 02:06:34', 0, '31128578', '', '', '', 0, 1, '2018-01-16 00:35:55'),
(41, 'pbkdf2_sha256$100000$A3VrNxNytAGf$toKMeQvWm62I6eR667VZYwE82htqNgZCwdZrqtXqiqg=', '2018-01-16 04:38:50', 0, '27803268', '', '', '', 0, 1, '2018-01-16 00:37:09'),
(42, 'pbkdf2_sha256$100000$GfOzK5hRfhrS$CmdKxm1naAKjCiYfpmnNLaNxKxrMhGmkFMYbw01FfO4=', '2018-01-16 02:58:36', 0, '29107068', '', '', '', 0, 1, '2018-01-16 00:37:10'),
(43, 'pbkdf2_sha256$100000$ZU6PLqopgsbw$AW/Id73C0cpBKxFfxyT0hiNZcrv7gUIMxxBZDQbInl8=', '2018-01-16 02:34:21', 0, '26701204', '', '', '', 0, 1, '2018-01-16 00:41:20'),
(44, 'pbkdf2_sha256$100000$I2bqnxkWRIvo$e6lqsl9pAQ3a6fKtHi3u4Syev1+XCLViQzlZFPKFLsk=', '2018-01-16 15:18:12', 0, '28903316', '', '', '', 0, 1, '2018-01-16 00:42:14'),
(45, 'pbkdf2_sha256$100000$wIwjfpbMuSpN$XO3zfspzoFIfVPWWj79c+u02sIio4geMj32+AMnGQPY=', '2018-01-16 01:25:05', 0, '25603244', '', '', '', 0, 1, '2018-01-16 00:43:15'),
(46, 'pbkdf2_sha256$100000$nCNIkTkYXOhI$uQDmdtM3e8FvISjAkPnh2ZwCfVOvhKOHNlLjBfd18EI=', '2018-01-16 00:43:50', 0, '30117052', '', '', '', 0, 1, '2018-01-16 00:43:32'),
(47, 'pbkdf2_sha256$100000$i485VKt7QnAo$XPBYDb2Vlp6nSBc15vzUfR5wtlJ2huFl4vp28Ky+y6I=', '2018-01-16 00:47:11', 0, '31128598', '', '', '', 0, 1, '2018-01-16 00:47:06'),
(48, 'pbkdf2_sha256$100000$wgJYXBduS7QF$g3KccpWHZp3v8+1s+4P4e/Nqd/H9grXn4/NZcUjy59M=', '2018-01-16 05:16:07', 0, '31128589', '', '', '', 0, 1, '2018-01-16 00:50:56'),
(49, 'pbkdf2_sha256$100000$AMdNQ7HKHE9y$Zq2VgqOuwMsXZsckYdH3dXIuoC4qs6x5cmnsV06mX9I=', '2018-01-16 00:52:11', 0, '27802319', '', '', '', 0, 1, '2018-01-16 00:51:51'),
(50, 'pbkdf2_sha256$100000$bijj8j3VYXfE$rlf1qBj1sWRemczwXfSWIog7d0A8hGjPmKV6/a4yFlY=', '2018-01-16 15:01:55', 0, '31128596', '', '', '', 0, 1, '2018-01-16 00:53:47'),
(51, 'pbkdf2_sha256$100000$UjFjhA2BRKug$io8+cYG/K0zbeTYafTnm6yeixXWWzqi0dANIzWabkco=', '2018-01-16 01:05:34', 0, '31128561', '', '', '', 0, 1, '2018-01-16 00:54:24'),
(52, 'pbkdf2_sha256$100000$sGVnAOC8gDaE$S4Ze/GuDL9dkb7NKqvt7v6xIPO05pG7TVcph+BM+ewM=', '2018-01-23 02:24:36', 0, '26700153', '', '', '', 0, 1, '2018-01-16 00:56:39'),
(53, 'pbkdf2_sha256$100000$AD4O5R0dlsEj$zVZELcQxFD7EKEitKXuZ7cVhJ62zksDqcEfhL9qB0dw=', '2018-01-16 02:41:15', 0, '27803439', '', '', '', 0, 1, '2018-01-16 00:59:31'),
(54, 'pbkdf2_sha256$100000$oAGkz2kAkfl4$HHirRdQjXvgrLGmhwJF4THRvcOvpHrggj2C2dgQAY4k=', '2018-01-16 00:59:45', 0, '30111414', '', '', '', 0, 1, '2018-01-16 00:59:32'),
(55, 'pbkdf2_sha256$100000$974l9OdnrV7U$PcV7iToUalCh2Eh2b5HXsZzhaiyoolOMQ6yig+is3go=', '2018-01-16 01:01:13', 0, '28903096', '', '', '', 0, 1, '2018-01-16 01:01:08'),
(56, 'pbkdf2_sha256$100000$kWQ1VeW8FpL5$bYJdy4QCcqsAP7Bgjm9Rj1v+Z/npQeU2TZsSQlioltw=', '2018-01-16 05:45:03', 0, '31128699', '', '', '', 0, 1, '2018-01-16 01:04:15'),
(57, 'pbkdf2_sha256$100000$W3rweGOHzUJX$NVrRAxbSUXJ7HEm+uKBVLcZ3R3v5cMtzpNInLFnjpcU=', '2018-01-16 01:30:20', 0, '28902514', '', '', '', 0, 1, '2018-01-16 01:06:46'),
(58, 'pbkdf2_sha256$100000$YhADXe9KuuzW$8y8vt3UWk3vRYZqPov8r+G0RIVPwAx0hKzBv+RAcEhE=', '2018-01-16 01:07:30', 0, '28903793', '', '', '', 0, 1, '2018-01-16 01:07:05'),
(59, 'pbkdf2_sha256$100000$TW37lBJhZNcx$qSDaDQ4zJGR9vHbaoxC50EVxndZhi7Va6u+1iEQFg9o=', '2018-01-16 12:21:48', 0, '31120572', '', '', '', 0, 1, '2018-01-16 01:07:27'),
(60, 'pbkdf2_sha256$100000$SUXMdr3jRh8O$Hhpo9P7wHKt6EkrQ4skB43VzT7+kGNYtBKhQxqmYjfo=', '2018-01-16 03:10:09', 0, '31120576', '', '', '', 0, 1, '2018-01-16 01:13:32'),
(61, 'pbkdf2_sha256$100000$9xhkuU8oNO7j$G59D5f5GbI/FfKgcbcHGS0ljoNAXy7ZFvKJT9G+Feu8=', '2018-01-16 01:13:51', 0, '28903247', '', '', '', 0, 1, '2018-01-16 01:13:36'),
(62, 'pbkdf2_sha256$100000$GToshPhRdLCn$+pbXKaucJBIhlW7Uek/9/U6k6o5dwkVXtHs04/279o0=', '2018-01-16 01:15:13', 0, '31128591', '', '', '', 0, 1, '2018-01-16 01:14:55'),
(63, 'pbkdf2_sha256$100000$NjsFKg2zBVOm$ftCr45z0mrRnjTC4tSDo93cvn++zfSkWT/68HgOBRxo=', '2018-01-16 01:15:55', 0, '31128565', '', '', '', 0, 1, '2018-01-16 01:15:44'),
(64, 'pbkdf2_sha256$100000$xzIekIzCuMJp$2PBY9eD+U+nkL3pUKZe7MC8xCCGeLY8Yvd9YwL82y0s=', '2018-01-16 02:17:10', 0, '31128594', '', '', '', 0, 1, '2018-01-16 01:16:01'),
(65, 'pbkdf2_sha256$100000$K5Tyygn8FbEP$z9nLbIwGSyf0YU/KlacFwU/2L21eX7dvUKqQ47rhS3g=', '2018-01-16 01:19:28', 0, '31128557', '', '', '', 0, 1, '2018-01-16 01:19:02'),
(66, 'pbkdf2_sha256$100000$oX3DmRwwyx1F$U5XJJnBJMFEKs1KG/7AFU8kJ3tzAHL6wq2ruvsnIiao=', '2018-01-16 01:19:22', 0, '31128588', '', '', '', 0, 1, '2018-01-16 01:19:15'),
(67, 'pbkdf2_sha256$100000$nafWyJQaXbXz$w7eWkuYJ9PoU1tCFBZQc1AW6ysiu9hRvw9xrdry61/c=', '2018-01-16 01:27:10', 0, '25600194', '', '', '', 0, 1, '2018-01-16 01:25:41'),
(68, 'pbkdf2_sha256$100000$oERgxoBsWZpa$EhpkyQ9wWv4WeLSElUIs2ZhFdmJGF/fGw5mhLxON6Zw=', '2018-01-16 01:31:43', 0, '31120652', '', '', '', 0, 1, '2018-01-16 01:31:32'),
(69, 'pbkdf2_sha256$100000$7lVuXLCSFTSa$7JpWifYmyqhpI6UGivD6HQ/PCvQcU8iav65QzmWM7hs=', '2018-01-22 02:49:14', 0, '31120610', '', '', '', 0, 1, '2018-01-16 01:31:55'),
(70, 'pbkdf2_sha256$100000$nySDwAV5aJr7$Qah2bIvNacDig6zT2m2iq07q56zdUuptu1WlWzTZgbg=', '2018-01-16 02:06:16', 0, '27801012', '', '', '', 0, 1, '2018-01-16 01:32:30'),
(71, 'pbkdf2_sha256$100000$X5YOFjTt7HbW$LC9FEItFOG1Gi2ARJCjNCtXtnPYxiSeTV1tqiWrX5XI=', '2018-01-16 04:55:03', 0, '31128563', '', '', '', 0, 1, '2018-01-16 01:37:18'),
(72, 'pbkdf2_sha256$100000$n0WkA8UchbQs$LbAt8sS87B9XAI/n1v0cBVAgYn0c+jSKmGyzUVQpBUg=', '2018-01-16 01:48:29', 0, '31120653', '', '', '', 0, 1, '2018-01-16 01:48:19'),
(73, 'pbkdf2_sha256$100000$9eEk4TU8M5Dg$V2fnxjEzHb5yu2fUPcjmBbGY16UZlp3M2Is241BhyZc=', '2018-01-16 01:50:48', 0, '31128592', '', '', '', 0, 1, '2018-01-16 01:50:28'),
(74, 'pbkdf2_sha256$100000$qFd121qu0Ovp$H1jKgW020//55Z4qbe2EymWtplDv8hN5slrScYL+RSE=', '2018-01-16 02:09:55', 0, '31120366', '', '', '', 0, 1, '2018-01-16 02:09:39'),
(75, 'pbkdf2_sha256$100000$wIfYPuKyV6rF$YKsqJ9cTFumcpQKw5bm25YI7KExQVe/Br7DqCuYCwFg=', '2018-01-16 02:28:49', 0, '28903274', '', '', '', 0, 1, '2018-01-16 02:28:31'),
(76, 'pbkdf2_sha256$100000$E4l049Ad78bF$98PTNgeqg4XMW1oqm60sl8RyNt2jg8Tq2SUjbfk5RHU=', '2018-01-16 02:31:47', 0, '26704157', '', '', '', 0, 1, '2018-01-16 02:31:18'),
(77, 'pbkdf2_sha256$100000$un5kBzoeWPxn$aVZ3gXxPno6allnlmmmDSuMtbZb2fd63F0Zle+OBbLI=', '2018-01-16 02:32:22', 0, '31120601', '', '', '', 0, 1, '2018-01-16 02:31:58'),
(78, 'pbkdf2_sha256$100000$UPvZfnKqyaQn$9LKkqhpranHkzQW7x8myspNYLN5XOkh0BHP59RN5LCo=', '2018-01-16 02:39:31', 0, '31128564', '', '', '', 0, 1, '2018-01-16 02:39:19'),
(79, 'pbkdf2_sha256$100000$GtnIqmTi0uCv$VfbiRm8PWeovs92Y6yiOzMMtxjad3ImwFxipOC8yWW0=', '2018-01-16 02:45:46', 0, '30117758', '', '', '', 0, 1, '2018-01-16 02:40:07'),
(80, 'pbkdf2_sha256$100000$bInM8DiYiaWv$jsq2virKAye0XtViIqFE2gyor+5yqKECjk2W1muv3yk=', '2018-01-16 02:59:25', 0, '30111379', '', '', '', 0, 1, '2018-01-16 02:59:17'),
(81, 'pbkdf2_sha256$100000$a76yU29XqdzR$zTmxIbHEDXNOeJJOM0cCn5Y1BJThY6SsVF2ZBUQOYlU=', '2018-01-16 03:04:09', 0, '31128517', '', '', '', 0, 1, '2018-01-16 03:03:45'),
(82, 'pbkdf2_sha256$100000$5nCEmfWfVjBD$o1pizP+zd+CWG7pcEUh5Pa9NJOw1H9OUH9dx6Bw4MKc=', '2018-01-16 03:15:20', 0, '30114945', '', '', '', 0, 1, '2018-01-16 03:14:56'),
(83, 'pbkdf2_sha256$100000$77MwHU7tpFch$wqi/6pM++bay5qLJC9StVNJFGfSnvPsfrGuSPzSotfw=', '2018-01-16 03:24:26', 0, '31128570', '', '', '', 0, 1, '2018-01-16 03:24:13'),
(84, 'pbkdf2_sha256$100000$1v25wERKJCTy$z1GA7IjeQbhwWvQIjLrd85Ju5dysW6tI3VnZ5DulG7I=', '2018-01-16 03:40:25', 0, '25600569', '', '', '', 0, 1, '2018-01-16 03:40:13'),
(85, 'pbkdf2_sha256$100000$0g0dCCktDwz2$9LVnG1a3/lLgXeQY+BHcb+cTcx961Z65sFt/UKFLBDI=', '2018-01-16 03:52:44', 0, '28903288', '', '', '', 0, 1, '2018-01-16 03:52:22'),
(86, 'pbkdf2_sha256$100000$QR0yFkSgpOXQ$CoaDUPnbPWgkfiaaAmje231sjDcgh+JESKSPJxJHo0c=', '2018-01-16 04:02:45', 0, '26704178', '', '', '', 0, 1, '2018-01-16 04:02:36'),
(87, 'pbkdf2_sha256$100000$NNYl5MlcNObR$iNSPKT93uPHCgXgW2UwpAbiR+ordnjy5QjWd/GIWm7o=', '2018-01-16 04:15:50', 0, '28902509', '', '', '', 0, 1, '2018-01-16 04:15:20'),
(88, 'pbkdf2_sha256$100000$A4f6f9uIeYAl$OwJBUTDFJr1Poat1TG0AUL9cK4pvnKa8r6y8buFefnc=', '2018-01-16 04:31:32', 0, '28904489', '', '', '', 0, 1, '2018-01-16 04:23:02'),
(89, 'pbkdf2_sha256$100000$tmkvkzOclLkU$lvaRzZYPX8R5k9HNehswl1aILtGHl6QotvifdhAilwg=', '2018-01-16 14:17:28', 0, '27801077', '', '', '', 0, 1, '2018-01-16 04:32:27'),
(90, 'pbkdf2_sha256$100000$YBmMeKWwalDx$a2qChuVFtbSLCn41aiJ39Eriqdchq5iNWSQKb8ldAME=', '2018-01-16 05:39:27', 0, '28902499', '', '', '', 0, 1, '2018-01-16 04:58:19'),
(91, 'pbkdf2_sha256$100000$WmQyToGs0DaZ$LWRUWlc8tDiMvclfBDEnEyOFic3LgynA+BgPKyo27G4=', '2018-01-16 05:07:40', 0, '31120341', '', '', '', 0, 1, '2018-01-16 05:06:54'),
(92, 'pbkdf2_sha256$100000$kCwQeHsTh8E0$mo7sQaI//lPm98eJEoTUjIjOv+YXiIETWYUjpqWH9tg=', '2018-01-16 05:16:46', 0, '24502065', '', '', '', 0, 1, '2018-01-16 05:16:23'),
(93, 'pbkdf2_sha256$100000$3AhbcCoMMcll$jmmzFwFHYfYoOXh9A32cZ2HYdHfad+eeTvKx7CvgiXc=', '2018-01-16 07:44:52', 0, '26702463', '', '', '', 0, 1, '2018-01-16 07:01:06'),
(94, 'pbkdf2_sha256$100000$djVtTbszOrOu$PmVq+cqK1qPk5YLCC5wtgkv2GDsExg8puSJs4GdfP3M=', '2018-01-16 07:47:42', 0, '29104742', '', '', '', 0, 1, '2018-01-16 07:44:32'),
(95, 'pbkdf2_sha256$100000$yqHSq7UffbHE$4bD3Zzedikfb/gYiQLCDw6Z0AA/Qg0ZYPGm5bInMxvA=', '2018-01-16 15:13:08', 0, '31128599', '', '', '', 0, 1, '2018-01-16 12:53:25'),
(96, 'pbkdf2_sha256$100000$KJlQ7XrD4knv$8LNUAYudC/Hnf4TkqV0rxrJC6bYED8P21z1ltVVfQ3k=', '2018-01-16 15:38:38', 0, '30111886', '', '', '', 0, 1, '2018-01-16 14:20:51'),
(97, 'pbkdf2_sha256$100000$sA3qmulD27Au$u9O0PHpugiUgOaD/JIuC+spjgTRkWNW0DmURYOoXoqQ=', '2018-01-22 16:05:54', 0, '34150161', '', '', '', 0, 1, '2018-01-22 16:05:41'),
(98, 'pbkdf2_sha256$100000$srC1rvu302s1$3XvPRHLZQFcN4jKF4Ug3R5VUvjkM+0CZIGYbCA/i7Hw=', '2018-04-18 13:18:43', 0, '31125138', '', '', '', 0, 1, '2018-01-29 19:04:47'),
(99, 'pbkdf2_sha256$100000$3h4zSULigWOv$NXrLXvnU+lMVeayIy1DL4Cyswxdy2knPOJwaOyXEMjo=', '2018-01-31 20:03:45', 0, '27800814', '', '', '', 0, 1, '2018-01-31 20:03:36'),
(100, 'pbkdf2_sha256$100000$tWG94hGm9f8B$fy4NQaOR6Hv/waCwnFr9TAFuS/e9/PEvXKQTiSwIh9Q=', '2018-01-31 20:05:34', 0, '29103147', '', '', '', 0, 1, '2018-01-31 20:05:15'),
(101, 'pbkdf2_sha256$100000$hgonRAAiHlXg$IBd5bNYSOBfuCem9r9DVCId+ey9EgMpxN7ctDtuL4H4=', NULL, 0, '27803347', '', '', '', 0, 1, '2018-01-31 20:05:22'),
(102, 'pbkdf2_sha256$100000$1KFVQudDQYUe$P9MZJ+v6RP9Ija/uEuvayCHpU+KUoJR0tEKdzlW+2S8=', '2018-01-31 20:09:06', 0, '30117409', '', '', '', 0, 1, '2018-01-31 20:08:57'),
(103, 'pbkdf2_sha256$100000$upGizNg0d4Cf$X5t8nDldaWG4sJq3qZXNvcEWgtD5SDsR6aWyT1w3fdg=', '2018-01-31 20:10:06', 0, '27803665', '', '', '', 0, 1, '2018-01-31 20:09:49'),
(104, 'pbkdf2_sha256$100000$cdgMw85HQPTT$qqBWSpWvyshKem8hLu3X9PzdgVPEXWrm77FYRFY6spY=', '2018-02-19 19:19:53', 0, '34150282', '', '', '', 0, 1, '2018-02-19 17:41:00'),
(105, 'pbkdf2_sha256$100000$JVb18QIALFfe$zPXV8gduY/LwVNNogd+nkwJwFgMIJ4CCTcbEi002iwI=', '2018-02-19 17:44:00', 0, '31123451', '', '', '', 0, 1, '2018-02-19 17:41:18'),
(106, 'pbkdf2_sha256$100000$sgPWWqmXgR2y$kIzclTeJ0q4p7vAkSlbQwlK1Zo2i7+lSUEFapJO6qRw=', '2018-03-23 23:13:40', 0, '37181633', '', '', '', 0, 1, '2018-03-23 23:11:57'),
(107, 'pbkdf2_sha256$100000$FmpObn2DKyUP$a+CSZYMkHLKD/CVyhF2i1EL6S/ewjkgEr14+uhkOlOg=', '2018-04-11 14:37:11', 0, '30114924', '', '', '', 0, 1, '2018-04-11 14:35:40');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinica_clinica`
--

CREATE TABLE IF NOT EXISTS `clinica_clinica` (
`id_clinica` int(11) NOT NULL,
  `nombre_unidad` varchar(100) NOT NULL,
  `plaza_fed` varchar(1) NOT NULL,
  `complemento` varchar(1) DEFAULT NULL,
  `observacion` varchar(250) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `id_direccion_fk_id` int(11) NOT NULL,
  `id_institucion_fk_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clinica_clinica`
--

INSERT INTO `clinica_clinica` (`id_clinica`, `nombre_unidad`, `plaza_fed`, `complemento`, `observacion`, `status`, `id_direccion_fk_id`, `id_institucion_fk_id`) VALUES
(1, 'UNEME Cronicas', 'D', 'D', 'UNEME', 1, 1, 2),
(2, 'CS Genaro Codina', 'C', 'C', 'Centro de Salud', 1, 2, 2),
(3, 'H.C. Calera', 'D', 'D', 'Hospital Comunitario', 1, 3, 2),
(4, 'CS Zoquite', 'B', 'B', 'Centro de Salud', 1, 4, 2),
(5, 'UNEME Urgencias', 'D', 'D', 'UNEME', 1, 5, 2),
(6, 'HC Trancoso', 'D', 'D', 'Hospital Comunitario', 1, 6, 2),
(7, 'CS El Lampotal', 'C', 'C', 'Centro de Salud', 1, 7, 2),
(8, 'CS Villa García', 'B', 'B', 'Centro de Salud', 1, 8, 2),
(9, 'UNEME de Hemodialisis', 'D', 'D', 'UNEME', 1, 9, 2),
(10, 'CS Col. Morelos', 'B', 'B', 'Centro de Salud', 1, 10, 2),
(11, 'HC Ojocaliente', 'D', 'D', 'Hospital Comunitario', 1, 11, 2),
(12, 'UNEME Crónicos Ojocaliente', 'D', 'D', 'UNEME', 1, 12, 2),
(13, 'CS San Jose de Castellanos', 'C', 'C', 'Centro de Salud', 1, 13, 2),
(14, 'CS Villa Hidalgo', 'B', 'B', 'Centro de Salud', 1, 14, 2),
(15, 'CS El Tule', 'C', 'C', 'Centro de Salud', 1, 15, 2),
(16, 'CS Loreto', 'C', 'C', 'Centro de Salud', 1, 16, 2),
(17, 'CS Ojo de Agua de la Palma', 'C', 'C', 'Centro de Salud', 1, 17, 2),
(18, 'CS Villa Gonzalez Ortega', 'B', 'B', 'Centro de Salud', 1, 18, 2),
(19, 'CS El Refugio', 'C', 'C', 'Centro de Salud', 1, 19, 2),
(20, 'UNEME Cisame', 'D', 'D', 'UNEME', 1, 20, 2),
(21, 'CS San Marcos', 'C', 'C', 'Centro de Salud', 1, 21, 2),
(22, 'CS Luis Moya', 'B', 'B', 'Centro de Salud', 1, 22, 2),
(23, 'CS Maravillas', 'C', 'C', 'Centro de Salud', 1, 23, 2),
(24, 'UNEME CISAME Fresnillo', 'D', 'D', 'UNEME', 1, 24, 2),
(25, 'CS Jimenez del Teul', 'C', 'C', 'Centro de Salud', 1, 25, 2),
(26, 'CS Agua Zarca', 'C', 'C', 'Centro de Salud', 1, 26, 2),
(27, 'HC Sombrerete', 'D', 'D', 'Hospital Comunitario', 1, 27, 2),
(28, 'CS Chalchihuites', 'B', 'B', 'Centro de Salud', 1, 28, 2),
(29, 'CS Sain Alto', 'C', 'C', 'Centro de Salud', 1, 29, 2),
(30, 'HC Valparaiso', 'D', 'D', 'Hospital Comunitario', 1, 30, 2),
(31, 'CS Rio Florido', 'C', 'C', 'Centro de Salud', 1, 31, 2),
(32, 'CS San MAteo', 'C', 'C', 'Centro de Salud', 1, 32, 2),
(33, 'CS Rancho Grande', 'C', 'C', 'Centro de Salud', 1, 33, 2),
(34, 'CS San Jose de Llanetes', 'C', 'C', 'Centro de Salud', 1, 34, 2),
(35, 'CS Villa Insurgentes', 'C', 'C', 'Centro de Salud', 1, 35, 2),
(36, 'CS Col. Gonzalez Ortega', 'C', 'C', 'Centro de Salud', 1, 36, 2),
(37, 'CS Santa Lucia', 'C', 'C', 'Centro de Salud', 1, 37, 2),
(38, 'CS Santa Rosa', 'C', 'C', 'Centro de Salud', 1, 38, 2),
(39, 'CS Trujillo', 'C', 'C', 'Centro de Salud', 1, 39, 2),
(40, 'CS Mendoza', 'C', 'C', 'Centro de Salud', 1, 40, 2),
(41, 'CS Plateros', 'B', 'B', 'Centro de Salud', 1, 41, 2),
(42, 'CS Estacion San Jose', 'B', 'B', 'Centro de Salud', 1, 42, 2),
(43, 'CS Miguel Hidalgo', 'C', 'C', 'Centro de Salud', 1, 43, 2),
(44, 'CS Las Norias', 'C', 'C', 'Centro de Salud', 1, 44, 2),
(45, 'UNEME Cronicas Rio Grande', 'D', 'D', 'UNEME', 1, 45, 2),
(46, 'HC Juan Aldama', 'D', 'D', 'Hospital Comunitario', 1, 46, 2),
(47, 'CS Emiliano Zapata (Morones)', 'C', 'C', 'Centro de Salud', 1, 47, 2),
(48, 'CS Pacheco', 'C', 'C', 'Centro de Salud', 1, 48, 2),
(49, 'CS Progreso', 'B', 'B', 'Centro de Salud', 1, 49, 2),
(50, 'CS Nieves', 'C', 'C', 'Centro de Salud', 1, 50, 2),
(51, 'CS Juan Aldama', 'B', 'B', 'Centro de Salud', 1, 51, 2),
(52, 'CS Apozol', 'B', 'B', 'Centro de Salud', 1, 52, 2),
(53, 'HC Juchipila', 'D', 'D', 'Hospital Comunitario', 1, 53, 2),
(54, 'CS Huanusco', 'B', 'B', 'Centro de Salud', 1, 54, 2),
(55, 'HC Jalpa', 'D', 'D', 'Hospital Comunitario', 1, 55, 2),
(56, 'HC Tabasco', 'D', 'D', 'Hospital Comunitario', 1, 56, 2),
(57, 'CS La Pitaya', 'C', 'C', 'Centro de Salud', 1, 57, 2),
(58, 'CS Cosalima', 'C', 'C', 'Centro de Salud', 1, 58, 2),
(59, 'CS Jerez', 'B', 'B', 'Centro de Salud', 1, 59, 2),
(60, 'HC Nochistlan', 'D', 'D', 'Hospital Comunitario', 1, 60, 2),
(61, 'CS Estacion Camacho', 'C', 'C', 'Centro de Salud', 1, 61, 2),
(62, 'CS El Rucio', 'C', 'C', 'Centro de Salud', 1, 62, 2),
(63, 'CS Mazapil', 'B', 'B', 'Centro de Salud', 1, 63, 2),
(64, 'CS Tanque Nuevo', 'C', 'C', 'Centro de Salud', 1, 64, 2),
(65, 'CS San Tiburcio', 'C', 'C', 'Centro de Salud', 1, 65, 2),
(66, 'aaaa', 'A', 'A', 'aa', 1, 66, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinica_direccionclinica`
--

CREATE TABLE IF NOT EXISTS `clinica_direccionclinica` (
`id_direccion` int(11) NOT NULL,
  `localidad` varchar(50) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `municipio_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clinica_direccionclinica`
--

INSERT INTO `clinica_direccionclinica` (`id_direccion`, `localidad`, `estado_id`, `municipio_id`) VALUES
(1, 'Zacatecas', 32, 2490),
(2, 'Genaro Codina', 32, 2446),
(3, 'Calera', 32, 2439),
(4, 'Zoquite', 32, 2451),
(5, 'Guadalupe', 32, 2451),
(6, 'Trancoso', 32, 2451),
(7, 'El lampotal', 32, 2484),
(8, 'Villa Garcia', 32, 2486),
(9, 'Guadalupe', 32, 2490),
(10, 'Morelos', 32, 2466),
(11, 'Ojocaliente', 32, 2470),
(12, 'Ojocaliente', 32, 2470),
(13, 'San Jose de Castellanos', 32, 2472),
(14, 'Villa Hidalgo', 32, 2488),
(15, 'El Tule', 32, 2450),
(16, 'Loreto', 32, 2458),
(17, 'Ojo de Agua de la Palma', 32, 2472),
(18, 'Villa Gonzalez', 32, 2487),
(19, 'El Refugio', 32, 2488),
(20, 'Calera', 32, 2439),
(21, 'San Marcos', 32, 2458),
(22, 'Luis Moya', 32, 2459),
(23, 'Maravillas', 32, 2469),
(24, 'Fresnillo', 32, 2444),
(25, 'Jimenez del Teul', 32, 2455),
(26, 'Agua Zarca', 32, 2476),
(27, 'Sombrerete', 32, 2476),
(28, 'Chalchihuites', 32, 2443),
(29, 'Sain Alto', 32, 2474),
(30, 'Valparaiso', 32, 2483),
(31, 'Rio Florido', 32, 2444),
(32, 'San Mateo', 32, 2483),
(33, 'Rancho Grande', 32, 2444),
(34, 'San Jose de Llanetes', 32, 2483),
(35, 'Villa Insurgentes', 32, 2476),
(36, 'Colonia Gonzalez Ortega', 32, 2476),
(37, 'Santa Lucia', 32, 2444),
(38, 'Santa Rosa', 32, 2444),
(39, 'Trujillo', 32, 2444),
(40, 'Mendoza', 32, 2443),
(41, 'Plateros', 32, 2444),
(42, 'Estacion San Jose', 32, 2444),
(43, 'Miguel Hidalgo', 32, 2448),
(44, 'Las Norias', 32, 2448),
(45, 'Rio Grande', 32, 2473),
(46, 'Juan Aldama', 32, 2456),
(47, 'Emiliano Zapata', 32, 2474),
(48, 'Pacheco', 32, 2448),
(49, 'Progreso', 32, 2473),
(50, 'Nieves', 32, 2448),
(51, 'Juan Aldama', 32, 2456),
(52, 'Apozol', 32, 2435),
(53, 'Juchipila', 32, 2457),
(54, 'Huanusco', 32, 2452),
(55, 'Jalpa', 32, 2453),
(56, 'Tabasco', 32, 2478),
(57, 'La Pitaya', 32, 2453),
(58, 'Cosalima', 32, 2478),
(59, 'Jerez', 32, 2454),
(60, 'Nochistlan', 32, 2468),
(61, 'Estacion Camacho', 32, 2460),
(62, 'El Rucio', 32, 2485),
(63, 'Mazapil', 32, 2460),
(64, 'Tanque Nuevo', 32, 2475),
(65, 'San Tiburcio', 32, 2460),
(66, 'aa', 17, 913);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clinica_institucion`
--

CREATE TABLE IF NOT EXISTS `clinica_institucion` (
`id_institucion` int(11) NOT NULL,
  `nombre_institucion` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clinica_institucion`
--

INSERT INTO `clinica_institucion` (`id_institucion`, `nombre_institucion`) VALUES
(1, 'IMSS'),
(2, 'SSZ'),
(3, 'ISSSTE'),
(4, 'SEDENA'),
(5, 'Universidad'),
(6, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contactos_pasante_contactospasante`
--

CREATE TABLE IF NOT EXISTS `contactos_pasante_contactospasante` (
`id_contacto` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `numero` varchar(15) NOT NULL,
  `id_pasante_fk_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contactos_pasante_contactospasante`
--

INSERT INTO `contactos_pasante_contactospasante` (`id_contacto`, `nombre`, `numero`, `id_pasante_fk_id`) VALUES
(1, 'HILARIO VILLA TORRES', '4924932831', 15),
(2, 'ROSARIO HERNANDEZ', '4921319800', 20),
(3, 'Oscar Meza', '4921257149', NULL),
(4, 'Mi Chapis', '4929972665', 60),
(5, 'Rosy Cel Nuevo', '4929490038', 60),
(6, 'Citla 2', '4921376267', 60),
(7, 'Moy', '4921030749', 94),
(8, 'Estibaliz', '4921072057', 94),
(9, 'Oscar', '4921030837', 94),
(20, 'Gerardo', '4928692161', 1),
(21, 'Fernando', '4922045384', 1),
(22, 'Alberto Garcia', '4931422670', 1),
(23, 'Ricardo Frausto Luján', '4928704660', 21),
(24, 'Marina Rodríguez Pérez', '4922439984', 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_asignacionclinicapasante`
--

CREATE TABLE IF NOT EXISTS `coordinador_asignacionclinicapasante` (
`id_asignacion` int(11) NOT NULL,
  `comision` varchar(100) NOT NULL,
  `id_clinica_fk_id` int(11) DEFAULT NULL,
  `id_pasante_fk_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_catalogoestatusinconformidad`
--

CREATE TABLE IF NOT EXISTS `coordinador_catalogoestatusinconformidad` (
`id_estatus` int(11) NOT NULL,
  `nombre_estatus` varchar(15) NOT NULL,
  `color` varchar(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_catalogoestatusinconformidad`
--

INSERT INTO `coordinador_catalogoestatusinconformidad` (`id_estatus`, `nombre_estatus`, `color`) VALUES
(1, 'Negro', '000000'),
(2, 'Rojo', 'FF0000'),
(3, 'Amarillo', 'FFFF00'),
(4, 'Naranja', 'FFBB00'),
(5, 'Verde', '0000FF');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_catalogotipoinconformidad`
--

CREATE TABLE IF NOT EXISTS `coordinador_catalogotipoinconformidad` (
`id_tipo` int(11) NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_catalogotipoinconformidad`
--

INSERT INTO `coordinador_catalogotipoinconformidad` (`id_tipo`, `nombre`) VALUES
(1, 'Seguridad'),
(2, 'Administrativo'),
(3, 'Escolar'),
(4, 'Bajas'),
(5, 'Permisos'),
(6, 'Infraestructura'),
(7, 'Otra');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_comentariosinconformidad`
--

CREATE TABLE IF NOT EXISTS `coordinador_comentariosinconformidad` (
`id_comentario` int(11) NOT NULL,
  `es_coordinador` int(11) DEFAULT NULL,
  `comentario` varchar(350) NOT NULL,
  `fecha` date NOT NULL,
  `id_reporte_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_comentariosinconformidad`
--

INSERT INTO `coordinador_comentariosinconformidad` (`id_comentario`, `es_coordinador`, `comentario`, `fecha`, `id_reporte_id`) VALUES
(1, 0, 'Comentario Comentario', '2018-05-09', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_coordinador`
--

CREATE TABLE IF NOT EXISTS `coordinador_coordinador` (
`id_coordinador` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) DEFAULT NULL,
  `matricula_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_estado`
--

CREATE TABLE IF NOT EXISTS `coordinador_estado` (
`id_estado` int(11) NOT NULL,
  `clave` varchar(2) NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `abrev` varchar(16) NOT NULL,
  `activo` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_estado`
--

INSERT INTO `coordinador_estado` (`id_estado`, `clave`, `nombre`, `abrev`, `activo`) VALUES
(1, '01', 'Aguascalientes', 'Ags.', 1),
(2, '02', 'Baja California', 'BC', 1),
(3, '03', 'Baja California Sur', 'BCS', 1),
(4, '04', 'Campeche', 'Camp.', 1),
(5, '05', 'Coahuila de Zaragoza', 'Coah.', 1),
(6, '06', 'Colima', 'Col.', 1),
(7, '07', 'Chiapas', 'Chis.', 1),
(8, '08', 'Chihuahua', 'Chih.', 1),
(9, '09', 'Distrito Federal', 'DF', 1),
(10, '10', 'Durango', 'Dgo.', 1),
(11, '11', 'Guanajuato', 'Gto.', 1),
(12, '12', 'Guerrero', 'Gro.', 1),
(13, '13', 'Hidalgo', 'Hgo.', 1),
(14, '14', 'Jalisco', 'Jal.', 1),
(15, '15', 'México', 'Mex.', 1),
(16, '16', 'Michoacán de Ocampo', 'Mich.', 1),
(17, '17', 'Morelos', 'Mor.', 1),
(18, '18', 'Nayarit', 'Nay.', 1),
(19, '19', 'Nuevo León', 'NL', 1),
(20, '20', 'Oaxaca', 'Oax.', 1),
(21, '21', 'Puebla', 'Pue.', 1),
(22, '22', 'Querétaro', 'Qro.', 1),
(23, '23', 'Quintana Roo', 'Q. Roo', 1),
(24, '24', 'San Luis Potosí', 'SLP', 1),
(25, '25', 'Sinaloa', 'Sin.', 1),
(26, '26', 'Sonora', 'Son.', 1),
(27, '27', 'Tabasco', 'Tab.', 1),
(28, '28', 'Tamaulipas', 'Tamps.', 1),
(29, '29', 'Tlaxcala', 'Tlax.', 1),
(30, '30', 'Veracruz de Ignacio de la Llave', 'Ver.', 1),
(31, '31', 'Yucatán', 'Yuc.', 1),
(32, '32', 'Zacatecas', 'Zac.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_fecha_registro`
--

CREATE TABLE IF NOT EXISTS `coordinador_fecha_registro` (
`id_registro` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_fecha_registro`
--

INSERT INTO `coordinador_fecha_registro` (`id_registro`, `fecha_inicio`, `fecha_fin`) VALUES
(1, '2018-01-01', '2018-02-28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_informacion`
--

CREATE TABLE IF NOT EXISTS `coordinador_informacion` (
`id_informacion` int(11) NOT NULL,
  `nombre_archivo` varchar(100) NOT NULL,
  `descripcion` varchar(500) DEFAULT NULL,
  `ruta_archivo` varchar(100) DEFAULT NULL,
  `ruta_archivo_html` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_informacion`
--

INSERT INTO `coordinador_informacion` (`id_informacion`, `nombre_archivo`, `descripcion`, `ruta_archivo`, `ruta_archivo_html`) VALUES
(2, 'Manual de Servicio Social', 'Manual de Servicio Social: Encontraras la normativa y reglamentos', 'normatividad/56fae322-f579-4190-a5fc-2d93969a1c55.pdf', 'normatividad/56fae322-f579-4190-a5fc-2d93969a1c55.pdf.html'),
(5, 'Plazas IMSS', 'Catalogo de plazas del IMSS', 'normatividad/576ee3ff-f828-4a9d-b136-f2f10f14c6e0.pdf', 'normatividad/576ee3ff-f828-4a9d-b136-f2f10f14c6e0.pdf.html'),
(6, 'Plazas SSZ', 'Catalogo de plazas de los SSZ', 'normatividad/5baa54ee-6c48-4be7-ba3b-98c7b5a72e02.pdf', 'normatividad/5baa54ee-6c48-4be7-ba3b-98c7b5a72e02.pdf.html');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_municipio`
--

CREATE TABLE IF NOT EXISTS `coordinador_municipio` (
`id_municipio` int(11) NOT NULL,
  `clave` varchar(3) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `activo` int(11) NOT NULL,
  `id_estado_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2493 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_municipio`
--

INSERT INTO `coordinador_municipio` (`id_municipio`, `clave`, `nombre`, `activo`, `id_estado_id`) VALUES
(1, '001', 'Aguascalientes', 1, 1),
(2, '002', 'Asientos', 1, 1),
(3, '003', 'Calvillo', 1, 1),
(4, '004', 'Cosío', 1, 1),
(5, '005', 'Jesús María', 1, 1),
(6, '006', 'Pabellón de Arteaga', 1, 1),
(7, '007', 'Rincón de Romos', 1, 1),
(8, '008', 'San José de Gracia', 1, 1),
(9, '009', 'Tepezalá', 1, 1),
(10, '010', 'El Llano', 1, 1),
(11, '011', 'San Francisco de los Romo', 1, 1),
(12, '001', 'Ensenada', 1, 2),
(13, '002', 'Mexicali', 1, 2),
(14, '003', 'Tecate', 1, 2),
(15, '004', 'Tijuana', 1, 2),
(16, '005', 'Playas de Rosarito', 1, 2),
(17, '001', 'Comondú', 1, 3),
(18, '002', 'Mulegé', 1, 3),
(19, '003', 'La Paz', 1, 3),
(20, '008', 'Los Cabos', 1, 3),
(21, '009', 'Loreto', 1, 3),
(22, '001', 'Calkiní', 1, 4),
(23, '002', 'Campeche', 1, 4),
(24, '003', 'Carmen', 1, 4),
(25, '004', 'Champotón', 1, 4),
(26, '005', 'Hecelchakán', 1, 4),
(27, '006', 'Hopelchén', 1, 4),
(28, '007', 'Palizada', 1, 4),
(29, '008', 'Tenabo', 1, 4),
(30, '009', 'Escárcega', 1, 4),
(31, '010', 'Calakmul', 1, 4),
(32, '011', 'Candelaria', 1, 4),
(33, '001', 'Abasolo', 1, 5),
(34, '002', 'Acuña', 1, 5),
(35, '003', 'Allende', 1, 5),
(36, '004', 'Arteaga', 1, 5),
(37, '005', 'Candela', 1, 5),
(38, '006', 'Castaños', 1, 5),
(39, '007', 'Cuatro Ciénegas', 1, 5),
(40, '008', 'Escobedo', 1, 5),
(41, '009', 'Francisco I. Madero', 1, 5),
(42, '010', 'Frontera', 1, 5),
(43, '011', 'General Cepeda', 1, 5),
(44, '012', 'Guerrero', 1, 5),
(45, '013', 'Hidalgo', 1, 5),
(46, '014', 'Jiménez', 1, 5),
(47, '015', 'Juárez', 1, 5),
(48, '016', 'Lamadrid', 1, 5),
(49, '017', 'Matamoros', 1, 5),
(50, '018', 'Monclova', 1, 5),
(51, '019', 'Morelos', 1, 5),
(52, '020', 'Múzquiz', 1, 5),
(53, '021', 'Nadadores', 1, 5),
(54, '022', 'Nava', 1, 5),
(55, '023', 'Ocampo', 1, 5),
(56, '024', 'Parras', 1, 5),
(57, '025', 'Piedras Negras', 1, 5),
(58, '026', 'Progreso', 1, 5),
(59, '027', 'Ramos Arizpe', 1, 5),
(60, '028', 'Sabinas', 1, 5),
(61, '029', 'Sacramento', 1, 5),
(62, '030', 'Saltillo', 1, 5),
(63, '031', 'San Buenaventura', 1, 5),
(64, '032', 'San Juan de Sabinas', 1, 5),
(65, '033', 'San Pedro', 1, 5),
(66, '034', 'Sierra Mojada', 1, 5),
(67, '035', 'Torreón', 1, 5),
(68, '036', 'Viesca', 1, 5),
(69, '037', 'Villa Unión', 1, 5),
(70, '038', 'Zaragoza', 1, 5),
(71, '001', 'Armería', 1, 6),
(72, '002', 'Colima', 1, 6),
(73, '003', 'Comala', 1, 6),
(74, '004', 'Coquimatlán', 1, 6),
(75, '005', 'Cuauhtémoc', 1, 6),
(76, '006', 'Ixtlahuacán', 1, 6),
(77, '007', 'Manzanillo', 1, 6),
(78, '008', 'Minatitlán', 1, 6),
(79, '009', 'Tecomán', 1, 6),
(80, '010', 'Villa de Álvarez', 1, 6),
(81, '001', 'Acacoyagua', 1, 7),
(82, '002', 'Acala', 1, 7),
(83, '003', 'Acapetahua', 1, 7),
(84, '004', 'Altamirano', 1, 7),
(85, '005', 'Amatán', 1, 7),
(86, '006', 'Amatenango de la Frontera', 1, 7),
(87, '007', 'Amatenango del Valle', 1, 7),
(88, '008', 'Angel Albino Corzo', 1, 7),
(89, '009', 'Arriaga', 1, 7),
(90, '010', 'Bejucal de Ocampo', 1, 7),
(91, '011', 'Bella Vista', 1, 7),
(92, '012', 'Berriozábal', 1, 7),
(93, '013', 'Bochil', 1, 7),
(94, '014', 'El Bosque', 1, 7),
(95, '015', 'Cacahoatán', 1, 7),
(96, '016', 'Catazajá', 1, 7),
(97, '017', 'Cintalapa', 1, 7),
(98, '018', 'Coapilla', 1, 7),
(99, '019', 'Comitán de Domínguez', 1, 7),
(100, '020', 'La Concordia', 1, 7),
(101, '021', 'Copainalá', 1, 7),
(102, '022', 'Chalchihuitán', 1, 7),
(103, '023', 'Chamula', 1, 7),
(104, '024', 'Chanal', 1, 7),
(105, '025', 'Chapultenango', 1, 7),
(106, '026', 'Chenalhó', 1, 7),
(107, '027', 'Chiapa de Corzo', 1, 7),
(108, '028', 'Chiapilla', 1, 7),
(109, '029', 'Chicoasén', 1, 7),
(110, '030', 'Chicomuselo', 1, 7),
(111, '031', 'Chilón', 1, 7),
(112, '032', 'Escuintla', 1, 7),
(113, '033', 'Francisco León', 1, 7),
(114, '034', 'Frontera Comalapa', 1, 7),
(115, '035', 'Frontera Hidalgo', 1, 7),
(116, '036', 'La Grandeza', 1, 7),
(117, '037', 'Huehuetán', 1, 7),
(118, '038', 'Huixtán', 1, 7),
(119, '039', 'Huitiupán', 1, 7),
(120, '040', 'Huixtla', 1, 7),
(121, '041', 'La Independencia', 1, 7),
(122, '042', 'Ixhuatán', 1, 7),
(123, '043', 'Ixtacomitán', 1, 7),
(124, '044', 'Ixtapa', 1, 7),
(125, '045', 'Ixtapangajoya', 1, 7),
(126, '046', 'Jiquipilas', 1, 7),
(127, '047', 'Jitotol', 1, 7),
(128, '048', 'Juárez', 1, 7),
(129, '049', 'Larráinzar', 1, 7),
(130, '050', 'La Libertad', 1, 7),
(131, '051', 'Mapastepec', 1, 7),
(132, '052', 'Las Margaritas', 1, 7),
(133, '053', 'Mazapa de Madero', 1, 7),
(134, '054', 'Mazatán', 1, 7),
(135, '055', 'Metapa', 1, 7),
(136, '056', 'Mitontic', 1, 7),
(137, '057', 'Motozintla', 1, 7),
(138, '058', 'Nicolás Ruíz', 1, 7),
(139, '059', 'Ocosingo', 1, 7),
(140, '060', 'Ocotepec', 1, 7),
(141, '061', 'Ocozocoautla de Espinosa', 1, 7),
(142, '062', 'Ostuacán', 1, 7),
(143, '063', 'Osumacinta', 1, 7),
(144, '064', 'Oxchuc', 1, 7),
(145, '065', 'Palenque', 1, 7),
(146, '066', 'Pantelhó', 1, 7),
(147, '067', 'Pantepec', 1, 7),
(148, '068', 'Pichucalco', 1, 7),
(149, '069', 'Pijijiapan', 1, 7),
(150, '070', 'El Porvenir', 1, 7),
(151, '071', 'Villa Comaltitlán', 1, 7),
(152, '072', 'Pueblo Nuevo Solistahuacán', 1, 7),
(153, '073', 'Rayón', 1, 7),
(154, '074', 'Reforma', 1, 7),
(155, '075', 'Las Rosas', 1, 7),
(156, '076', 'Sabanilla', 1, 7),
(157, '077', 'Salto de Agua', 1, 7),
(158, '078', 'San Cristóbal de las Casas', 1, 7),
(159, '079', 'San Fernando', 1, 7),
(160, '080', 'Siltepec', 1, 7),
(161, '081', 'Simojovel', 1, 7),
(162, '082', 'Sitalá', 1, 7),
(163, '083', 'Socoltenango', 1, 7),
(164, '084', 'Solosuchiapa', 1, 7),
(165, '085', 'Soyaló', 1, 7),
(166, '086', 'Suchiapa', 1, 7),
(167, '087', 'Suchiate', 1, 7),
(168, '088', 'Sunuapa', 1, 7),
(169, '089', 'Tapachula', 1, 7),
(170, '090', 'Tapalapa', 1, 7),
(171, '091', 'Tapilula', 1, 7),
(172, '092', 'Tecpatán', 1, 7),
(173, '093', 'Tenejapa', 1, 7),
(174, '094', 'Teopisca', 1, 7),
(175, '096', 'Tila', 1, 7),
(176, '097', 'Tonalá', 1, 7),
(177, '098', 'Totolapa', 1, 7),
(178, '099', 'La Trinitaria', 1, 7),
(179, '100', 'Tumbalá', 1, 7),
(180, '101', 'Tuxtla Gutiérrez', 1, 7),
(181, '102', 'Tuxtla Chico', 1, 7),
(182, '103', 'Tuzantán', 1, 7),
(183, '104', 'Tzimol', 1, 7),
(184, '105', 'Unión Juárez', 1, 7),
(185, '106', 'Venustiano Carranza', 1, 7),
(186, '107', 'Villa Corzo', 1, 7),
(187, '108', 'Villaflores', 1, 7),
(188, '109', 'Yajalón', 1, 7),
(189, '110', 'San Lucas', 1, 7),
(190, '111', 'Zinacantán', 1, 7),
(191, '112', 'San Juan Cancuc', 1, 7),
(192, '113', 'Aldama', 1, 7),
(193, '114', 'Benemérito de las Américas', 1, 7),
(194, '115', 'Maravilla Tenejapa', 1, 7),
(195, '116', 'Marqués de Comillas', 1, 7),
(196, '117', 'Montecristo de Guerrero', 1, 7),
(197, '118', 'San Andrés Duraznal', 1, 7),
(198, '119', 'Santiago el Pinar', 1, 7),
(199, '001', 'Ahumada', 1, 8),
(200, '002', 'Aldama', 1, 8),
(201, '003', 'Allende', 1, 8),
(202, '004', 'Aquiles Serdán', 1, 8),
(203, '005', 'Ascensión', 1, 8),
(204, '006', 'Bachíniva', 1, 8),
(205, '007', 'Balleza', 1, 8),
(206, '008', 'Batopilas', 1, 8),
(207, '009', 'Bocoyna', 1, 8),
(208, '010', 'Buenaventura', 1, 8),
(209, '011', 'Camargo', 1, 8),
(210, '012', 'Carichí', 1, 8),
(211, '013', 'Casas Grandes', 1, 8),
(212, '014', 'Coronado', 1, 8),
(213, '015', 'Coyame del Sotol', 1, 8),
(214, '016', 'La Cruz', 1, 8),
(215, '017', 'Cuauhtémoc', 1, 8),
(216, '018', 'Cusihuiriachi', 1, 8),
(217, '019', 'Chihuahua', 1, 8),
(218, '020', 'Chínipas', 1, 8),
(219, '021', 'Delicias', 1, 8),
(220, '022', 'Dr. Belisario Domínguez', 1, 8),
(221, '023', 'Galeana', 1, 8),
(222, '024', 'Santa Isabel', 1, 8),
(223, '025', 'Gómez Farías', 1, 8),
(224, '026', 'Gran Morelos', 1, 8),
(225, '027', 'Guachochi', 1, 8),
(226, '028', 'Guadalupe', 1, 8),
(227, '029', 'Guadalupe y Calvo', 1, 8),
(228, '030', 'Guazapares', 1, 8),
(229, '031', 'Guerrero', 1, 8),
(230, '032', 'Hidalgo del Parral', 1, 8),
(231, '033', 'Huejotitán', 1, 8),
(232, '034', 'Ignacio Zaragoza', 1, 8),
(233, '035', 'Janos', 1, 8),
(234, '036', 'Jiménez', 1, 8),
(235, '037', 'Juárez', 1, 8),
(236, '038', 'Julimes', 1, 8),
(237, '039', 'López', 1, 8),
(238, '040', 'Madera', 1, 8),
(239, '041', 'Maguarichi', 1, 8),
(240, '042', 'Manuel Benavides', 1, 8),
(241, '043', 'Matachí', 1, 8),
(242, '044', 'Matamoros', 1, 8),
(243, '045', 'Meoqui', 1, 8),
(244, '046', 'Morelos', 1, 8),
(245, '047', 'Moris', 1, 8),
(246, '048', 'Namiquipa', 1, 8),
(247, '049', 'Nonoava', 1, 8),
(248, '050', 'Nuevo Casas Grandes', 1, 8),
(249, '051', 'Ocampo', 1, 8),
(250, '052', 'Ojinaga', 1, 8),
(251, '053', 'Praxedis G. Guerrero', 1, 8),
(252, '054', 'Riva Palacio', 1, 8),
(253, '055', 'Rosales', 1, 8),
(254, '056', 'Rosario', 1, 8),
(255, '057', 'San Francisco de Borja', 1, 8),
(256, '058', 'San Francisco de Conchos', 1, 8),
(257, '059', 'San Francisco del Oro', 1, 8),
(258, '060', 'Santa Bárbara', 1, 8),
(259, '061', 'Satevó', 1, 8),
(260, '062', 'Saucillo', 1, 8),
(261, '063', 'Temósachic', 1, 8),
(262, '064', 'El Tule', 1, 8),
(263, '065', 'Urique', 1, 8),
(264, '066', 'Uruachi', 1, 8),
(265, '067', 'Valle de Zaragoza', 1, 8),
(266, '002', 'Azcapotzalco', 1, 9),
(267, '003', 'Coyoacán', 1, 9),
(268, '004', 'Cuajimalpa de Morelos', 1, 9),
(269, '005', 'Gustavo A. Madero', 1, 9),
(270, '006', 'Iztacalco', 1, 9),
(271, '007', 'Iztapalapa', 1, 9),
(272, '008', 'La Magdalena Contreras', 1, 9),
(273, '009', 'Milpa Alta', 1, 9),
(274, '010', 'Álvaro Obregón', 1, 9),
(275, '011', 'Tláhuac', 1, 9),
(276, '012', 'Tlalpan', 1, 9),
(277, '013', 'Xochimilco', 1, 9),
(278, '014', 'Benito Juárez', 1, 9),
(279, '015', 'Cuauhtémoc', 1, 9),
(280, '016', 'Miguel Hidalgo', 1, 9),
(281, '017', 'Venustiano Carranza', 1, 9),
(282, '001', 'Canatlán', 1, 10),
(283, '002', 'Canelas', 1, 10),
(284, '003', 'Coneto de Comonfort', 1, 10),
(285, '004', 'Cuencamé', 1, 10),
(286, '005', 'Durango', 1, 10),
(287, '006', 'General Simón Bolívar', 1, 10),
(288, '007', 'Gómez Palacio', 1, 10),
(289, '008', 'Guadalupe Victoria', 1, 10),
(290, '009', 'Guanaceví', 1, 10),
(291, '010', 'Hidalgo', 1, 10),
(292, '011', 'Indé', 1, 10),
(293, '012', 'Lerdo', 1, 10),
(294, '013', 'Mapimí', 1, 10),
(295, '014', 'Mezquital', 1, 10),
(296, '015', 'Nazas', 1, 10),
(297, '016', 'Nombre de Dios', 1, 10),
(298, '017', 'Ocampo', 1, 10),
(299, '018', 'El Oro', 1, 10),
(300, '019', 'Otáez', 1, 10),
(301, '020', 'Pánuco de Coronado', 1, 10),
(302, '021', 'Peñón Blanco', 1, 10),
(303, '022', 'Poanas', 1, 10),
(304, '023', 'Pueblo Nuevo', 1, 10),
(305, '024', 'Rodeo', 1, 10),
(306, '025', 'San Bernardo', 1, 10),
(307, '026', 'San Dimas', 1, 10),
(308, '027', 'San Juan de Guadalupe', 1, 10),
(309, '028', 'San Juan del Río', 1, 10),
(310, '029', 'San Luis del Cordero', 1, 10),
(311, '030', 'San Pedro del Gallo', 1, 10),
(312, '031', 'Santa Clara', 1, 10),
(313, '032', 'Santiago Papasquiaro', 1, 10),
(314, '033', 'Súchil', 1, 10),
(315, '034', 'Tamazula', 1, 10),
(316, '035', 'Tepehuanes', 1, 10),
(317, '036', 'Tlahualilo', 1, 10),
(318, '037', 'Topia', 1, 10),
(319, '038', 'Vicente Guerrero', 1, 10),
(320, '039', 'Nuevo Ideal', 1, 10),
(321, '001', 'Abasolo', 1, 11),
(322, '002', 'Acámbaro', 1, 11),
(323, '003', 'San Miguel de Allende', 1, 11),
(324, '004', 'Apaseo el Alto', 1, 11),
(325, '005', 'Apaseo el Grande', 1, 11),
(326, '006', 'Atarjea', 1, 11),
(327, '007', 'Celaya', 1, 11),
(328, '008', 'Manuel Doblado', 1, 11),
(329, '009', 'Comonfort', 1, 11),
(330, '010', 'Coroneo', 1, 11),
(331, '011', 'Cortazar', 1, 11),
(332, '012', 'Cuerámaro', 1, 11),
(333, '013', 'Doctor Mora', 1, 11),
(334, '014', 'Dolores Hidalgo Cuna de la Independencia Nacional', 1, 11),
(335, '015', 'Guanajuato', 1, 11),
(336, '016', 'Huanímaro', 1, 11),
(337, '017', 'Irapuato', 1, 11),
(338, '018', 'Jaral del Progreso', 1, 11),
(339, '019', 'Jerécuaro', 1, 11),
(340, '020', 'León', 1, 11),
(341, '021', 'Moroleón', 1, 11),
(342, '022', 'Ocampo', 1, 11),
(343, '023', 'Pénjamo', 1, 11),
(344, '024', 'Pueblo Nuevo', 1, 11),
(345, '025', 'Purísima del Rincón', 1, 11),
(346, '026', 'Romita', 1, 11),
(347, '027', 'Salamanca', 1, 11),
(348, '028', 'Salvatierra', 1, 11),
(349, '029', 'San Diego de la Unión', 1, 11),
(350, '030', 'San Felipe', 1, 11),
(351, '031', 'San Francisco del Rincón', 1, 11),
(352, '032', 'San José Iturbide', 1, 11),
(353, '033', 'San Luis de la Paz', 1, 11),
(354, '034', 'Santa Catarina', 1, 11),
(355, '035', 'Santa Cruz de Juventino Rosas', 1, 11),
(356, '036', 'Santiago Maravatío', 1, 11),
(357, '037', 'Silao de la Victoria', 1, 11),
(358, '038', 'Tarandacuao', 1, 11),
(359, '039', 'Tarimoro', 1, 11),
(360, '040', 'Tierra Blanca', 1, 11),
(361, '041', 'Uriangato', 1, 11),
(362, '042', 'Valle de Santiago', 1, 11),
(363, '043', 'Victoria', 1, 11),
(364, '044', 'Villagrán', 1, 11),
(365, '045', 'Xichú', 1, 11),
(366, '046', 'Yuriria', 1, 11),
(367, '001', 'Acapulco de Juárez', 1, 12),
(368, '002', 'Ahuacuotzingo', 1, 12),
(369, '003', 'Ajuchitlán del Progreso', 1, 12),
(370, '004', 'Alcozauca de Guerrero', 1, 12),
(371, '005', 'Alpoyeca', 1, 12),
(372, '006', 'Apaxtla', 1, 12),
(373, '007', 'Arcelia', 1, 12),
(374, '008', 'Atenango del Río', 1, 12),
(375, '009', 'Atlamajalcingo del Monte', 1, 12),
(376, '010', 'Atlixtac', 1, 12),
(377, '011', 'Atoyac de Álvarez', 1, 12),
(378, '012', 'Ayutla de los Libres', 1, 12),
(379, '013', 'Azoyú', 1, 12),
(380, '014', 'Benito Juárez', 1, 12),
(381, '015', 'Buenavista de Cuéllar', 1, 12),
(382, '016', 'Coahuayutla de José María Izazaga', 1, 12),
(383, '017', 'Cocula', 1, 12),
(384, '018', 'Copala', 1, 12),
(385, '019', 'Copalillo', 1, 12),
(386, '020', 'Copanatoyac', 1, 12),
(387, '021', 'Coyuca de Benítez', 1, 12),
(388, '022', 'Coyuca de Catalán', 1, 12),
(389, '023', 'Cuajinicuilapa', 1, 12),
(390, '024', 'Cualác', 1, 12),
(391, '025', 'Cuautepec', 1, 12),
(392, '026', 'Cuetzala del Progreso', 1, 12),
(393, '027', 'Cutzamala de Pinzón', 1, 12),
(394, '028', 'Chilapa de Álvarez', 1, 12),
(395, '029', 'Chilpancingo de los Bravo', 1, 12),
(396, '030', 'Florencio Villarreal', 1, 12),
(397, '031', 'General Canuto A. Neri', 1, 12),
(398, '032', 'General Heliodoro Castillo', 1, 12),
(399, '033', 'Huamuxtitlán', 1, 12),
(400, '034', 'Huitzuco de los Figueroa', 1, 12),
(401, '035', 'Iguala de la Independencia', 1, 12),
(402, '036', 'Igualapa', 1, 12),
(403, '037', 'Ixcateopan de Cuauhtémoc', 1, 12),
(404, '038', 'Zihuatanejo de Azueta', 1, 12),
(405, '039', 'Juan R. Escudero', 1, 12),
(406, '040', 'Leonardo Bravo', 1, 12),
(407, '041', 'Malinaltepec', 1, 12),
(408, '042', 'Mártir de Cuilapan', 1, 12),
(409, '043', 'Metlatónoc', 1, 12),
(410, '044', 'Mochitlán', 1, 12),
(411, '045', 'Olinalá', 1, 12),
(412, '046', 'Ometepec', 1, 12),
(413, '047', 'Pedro Ascencio Alquisiras', 1, 12),
(414, '048', 'Petatlán', 1, 12),
(415, '049', 'Pilcaya', 1, 12),
(416, '050', 'Pungarabato', 1, 12),
(417, '051', 'Quechultenango', 1, 12),
(418, '052', 'San Luis Acatlán', 1, 12),
(419, '053', 'San Marcos', 1, 12),
(420, '054', 'San Miguel Totolapan', 1, 12),
(421, '055', 'Taxco de Alarcón', 1, 12),
(422, '056', 'Tecoanapa', 1, 12),
(423, '057', 'Técpan de Galeana', 1, 12),
(424, '058', 'Teloloapan', 1, 12),
(425, '059', 'Tepecoacuilco de Trujano', 1, 12),
(426, '060', 'Tetipac', 1, 12),
(427, '061', 'Tixtla de Guerrero', 1, 12),
(428, '062', 'Tlacoachistlahuaca', 1, 12),
(429, '063', 'Tlacoapa', 1, 12),
(430, '064', 'Tlalchapa', 1, 12),
(431, '065', 'Tlalixtaquilla de Maldonado', 1, 12),
(432, '066', 'Tlapa de Comonfort', 1, 12),
(433, '067', 'Tlapehuala', 1, 12),
(434, '068', 'La Unión de Isidoro Montes de Oca', 1, 12),
(435, '069', 'Xalpatláhuac', 1, 12),
(436, '070', 'Xochihuehuetlán', 1, 12),
(437, '071', 'Xochistlahuaca', 1, 12),
(438, '072', 'Zapotitlán Tablas', 1, 12),
(439, '073', 'Zirándaro', 1, 12),
(440, '074', 'Zitlala', 1, 12),
(441, '075', 'Eduardo Neri', 1, 12),
(442, '076', 'Acatepec', 1, 12),
(443, '077', 'Marquelia', 1, 12),
(444, '078', 'Cochoapa el Grande', 1, 12),
(445, '079', 'José Joaquín de Herrera', 1, 12),
(446, '080', 'Juchitán', 1, 12),
(447, '081', 'Iliatenco', 1, 12),
(448, '001', 'Acatlán', 1, 13),
(449, '002', 'Acaxochitlán', 1, 13),
(450, '003', 'Actopan', 1, 13),
(451, '004', 'Agua Blanca de Iturbide', 1, 13),
(452, '005', 'Ajacuba', 1, 13),
(453, '006', 'Alfajayucan', 1, 13),
(454, '007', 'Almoloya', 1, 13),
(455, '008', 'Apan', 1, 13),
(456, '009', 'El Arenal', 1, 13),
(457, '010', 'Atitalaquia', 1, 13),
(458, '011', 'Atlapexco', 1, 13),
(459, '012', 'Atotonilco el Grande', 1, 13),
(460, '013', 'Atotonilco de Tula', 1, 13),
(461, '014', 'Calnali', 1, 13),
(462, '015', 'Cardonal', 1, 13),
(463, '016', 'Cuautepec de Hinojosa', 1, 13),
(464, '017', 'Chapantongo', 1, 13),
(465, '018', 'Chapulhuacán', 1, 13),
(466, '019', 'Chilcuautla', 1, 13),
(467, '020', 'Eloxochitlán', 1, 13),
(468, '021', 'Emiliano Zapata', 1, 13),
(469, '022', 'Epazoyucan', 1, 13),
(470, '023', 'Francisco I. Madero', 1, 13),
(471, '024', 'Huasca de Ocampo', 1, 13),
(472, '025', 'Huautla', 1, 13),
(473, '026', 'Huazalingo', 1, 13),
(474, '027', 'Huehuetla', 1, 13),
(475, '028', 'Huejutla de Reyes', 1, 13),
(476, '029', 'Huichapan', 1, 13),
(477, '030', 'Ixmiquilpan', 1, 13),
(478, '031', 'Jacala de Ledezma', 1, 13),
(479, '032', 'Jaltocán', 1, 13),
(480, '033', 'Juárez Hidalgo', 1, 13),
(481, '034', 'Lolotla', 1, 13),
(482, '035', 'Metepec', 1, 13),
(483, '036', 'San Agustín Metzquititlán', 1, 13),
(484, '037', 'Metztitlán', 1, 13),
(485, '038', 'Mineral del Chico', 1, 13),
(486, '039', 'Mineral del Monte', 1, 13),
(487, '040', 'La Misión', 1, 13),
(488, '041', 'Mixquiahuala de Juárez', 1, 13),
(489, '042', 'Molango de Escamilla', 1, 13),
(490, '043', 'Nicolás Flores', 1, 13),
(491, '044', 'Nopala de Villagrán', 1, 13),
(492, '045', 'Omitlán de Juárez', 1, 13),
(493, '046', 'San Felipe Orizatlán', 1, 13),
(494, '047', 'Pacula', 1, 13),
(495, '048', 'Pachuca de Soto', 1, 13),
(496, '049', 'Pisaflores', 1, 13),
(497, '050', 'Progreso de Obregón', 1, 13),
(498, '051', 'Mineral de la Reforma', 1, 13),
(499, '052', 'San Agustín Tlaxiaca', 1, 13),
(500, '053', 'San Bartolo Tutotepec', 1, 13),
(501, '054', 'San Salvador', 1, 13),
(502, '055', 'Santiago de Anaya', 1, 13),
(503, '056', 'Santiago Tulantepec de Lugo Guerrero', 1, 13),
(504, '057', 'Singuilucan', 1, 13),
(505, '058', 'Tasquillo', 1, 13),
(506, '059', 'Tecozautla', 1, 13),
(507, '060', 'Tenango de Doria', 1, 13),
(508, '061', 'Tepeapulco', 1, 13),
(509, '062', 'Tepehuacán de Guerrero', 1, 13),
(510, '063', 'Tepeji del Río de Ocampo', 1, 13),
(511, '064', 'Tepetitlán', 1, 13),
(512, '065', 'Tetepango', 1, 13),
(513, '066', 'Villa de Tezontepec', 1, 13),
(514, '067', 'Tezontepec de Aldama', 1, 13),
(515, '068', 'Tianguistengo', 1, 13),
(516, '069', 'Tizayuca', 1, 13),
(517, '070', 'Tlahuelilpan', 1, 13),
(518, '071', 'Tlahuiltepa', 1, 13),
(519, '072', 'Tlanalapa', 1, 13),
(520, '073', 'Tlanchinol', 1, 13),
(521, '074', 'Tlaxcoapan', 1, 13),
(522, '075', 'Tolcayuca', 1, 13),
(523, '076', 'Tula de Allende', 1, 13),
(524, '077', 'Tulancingo de Bravo', 1, 13),
(525, '078', 'Xochiatipan', 1, 13),
(526, '079', 'Xochicoatlán', 1, 13),
(527, '080', 'Yahualica', 1, 13),
(528, '081', 'Zacualtipán de Ángeles', 1, 13),
(529, '082', 'Zapotlán de Juárez', 1, 13),
(530, '083', 'Zempoala', 1, 13),
(531, '084', 'Zimapán', 1, 13),
(532, '001', 'Acatic', 1, 14),
(533, '002', 'Acatlán de Juárez', 1, 14),
(534, '003', 'Ahualulco de Mercado', 1, 14),
(535, '004', 'Amacueca', 1, 14),
(536, '005', 'Amatitán', 1, 14),
(537, '006', 'Ameca', 1, 14),
(538, '007', 'San Juanito de Escobedo', 1, 14),
(539, '008', 'Arandas', 1, 14),
(540, '009', 'El Arenal', 1, 14),
(541, '010', 'Atemajac de Brizuela', 1, 14),
(542, '011', 'Atengo', 1, 14),
(543, '012', 'Atenguillo', 1, 14),
(544, '013', 'Atotonilco el Alto', 1, 14),
(545, '014', 'Atoyac', 1, 14),
(546, '015', 'Autlán de Navarro', 1, 14),
(547, '016', 'Ayotlán', 1, 14),
(548, '017', 'Ayutla', 1, 14),
(549, '018', 'La Barca', 1, 14),
(550, '019', 'Bolaños', 1, 14),
(551, '020', 'Cabo Corrientes', 1, 14),
(552, '021', 'Casimiro Castillo', 1, 14),
(553, '022', 'Cihuatlán', 1, 14),
(554, '023', 'Zapotlán el Grande', 1, 14),
(555, '024', 'Cocula', 1, 14),
(556, '025', 'Colotlán', 1, 14),
(557, '026', 'Concepción de Buenos Aires', 1, 14),
(558, '027', 'Cuautitlán de García Barragán', 1, 14),
(559, '028', 'Cuautla', 1, 14),
(560, '029', 'Cuquío', 1, 14),
(561, '030', 'Chapala', 1, 14),
(562, '031', 'Chimaltitán', 1, 14),
(563, '032', 'Chiquilistlán', 1, 14),
(564, '033', 'Degollado', 1, 14),
(565, '034', 'Ejutla', 1, 14),
(566, '035', 'Encarnación de Díaz', 1, 14),
(567, '036', 'Etzatlán', 1, 14),
(568, '037', 'El Grullo', 1, 14),
(569, '038', 'Guachinango', 1, 14),
(570, '039', 'Guadalajara', 1, 14),
(571, '040', 'Hostotipaquillo', 1, 14),
(572, '041', 'Huejúcar', 1, 14),
(573, '042', 'Huejuquilla el Alto', 1, 14),
(574, '043', 'La Huerta', 1, 14),
(575, '044', 'Ixtlahuacán de los Membrillos', 1, 14),
(576, '045', 'Ixtlahuacán del Río', 1, 14),
(577, '046', 'Jalostotitlán', 1, 14),
(578, '047', 'Jamay', 1, 14),
(579, '048', 'Jesús María', 1, 14),
(580, '049', 'Jilotlán de los Dolores', 1, 14),
(581, '050', 'Jocotepec', 1, 14),
(582, '051', 'Juanacatlán', 1, 14),
(583, '052', 'Juchitlán', 1, 14),
(584, '053', 'Lagos de Moreno', 1, 14),
(585, '054', 'El Limón', 1, 14),
(586, '055', 'Magdalena', 1, 14),
(587, '056', 'Santa María del Oro', 1, 14),
(588, '057', 'La Manzanilla de la Paz', 1, 14),
(589, '058', 'Mascota', 1, 14),
(590, '059', 'Mazamitla', 1, 14),
(591, '060', 'Mexticacán', 1, 14),
(592, '061', 'Mezquitic', 1, 14),
(593, '062', 'Mixtlán', 1, 14),
(594, '063', 'Ocotlán', 1, 14),
(595, '064', 'Ojuelos de Jalisco', 1, 14),
(596, '065', 'Pihuamo', 1, 14),
(597, '066', 'Poncitlán', 1, 14),
(598, '067', 'Puerto Vallarta', 1, 14),
(599, '068', 'Villa Purificación', 1, 14),
(600, '069', 'Quitupan', 1, 14),
(601, '070', 'El Salto', 1, 14),
(602, '071', 'San Cristóbal de la Barranca', 1, 14),
(603, '072', 'San Diego de Alejandría', 1, 14),
(604, '073', 'San Juan de los Lagos', 1, 14),
(605, '074', 'San Julián', 1, 14),
(606, '075', 'San Marcos', 1, 14),
(607, '076', 'San Martín de Bolaños', 1, 14),
(608, '077', 'San Martín Hidalgo', 1, 14),
(609, '078', 'San Miguel el Alto', 1, 14),
(610, '079', 'Gómez Farías', 1, 14),
(611, '080', 'San Sebastián del Oeste', 1, 14),
(612, '081', 'Santa María de los Ángeles', 1, 14),
(613, '082', 'Sayula', 1, 14),
(614, '083', 'Tala', 1, 14),
(615, '084', 'Talpa de Allende', 1, 14),
(616, '085', 'Tamazula de Gordiano', 1, 14),
(617, '086', 'Tapalpa', 1, 14),
(618, '087', 'Tecalitlán', 1, 14),
(619, '088', 'Tecolotlán', 1, 14),
(620, '089', 'Techaluta de Montenegro', 1, 14),
(621, '090', 'Tenamaxtlán', 1, 14),
(622, '091', 'Teocaltiche', 1, 14),
(623, '092', 'Teocuitatlán de Corona', 1, 14),
(624, '093', 'Tepatitlán de Morelos', 1, 14),
(625, '094', 'Tequila', 1, 14),
(626, '095', 'Teuchitlán', 1, 14),
(627, '096', 'Tizapán el Alto', 1, 14),
(628, '097', 'Tlajomulco de Zúñiga', 1, 14),
(629, '098', 'San Pedro Tlaquepaque', 1, 14),
(630, '099', 'Tolimán', 1, 14),
(631, '100', 'Tomatlán', 1, 14),
(632, '101', 'Tonalá', 1, 14),
(633, '102', 'Tonaya', 1, 14),
(634, '103', 'Tonila', 1, 14),
(635, '104', 'Totatiche', 1, 14),
(636, '105', 'Tototlán', 1, 14),
(637, '106', 'Tuxcacuesco', 1, 14),
(638, '107', 'Tuxcueca', 1, 14),
(639, '108', 'Tuxpan', 1, 14),
(640, '109', 'Unión de San Antonio', 1, 14),
(641, '110', 'Unión de Tula', 1, 14),
(642, '111', 'Valle de Guadalupe', 1, 14),
(643, '112', 'Valle de Juárez', 1, 14),
(644, '113', 'San Gabriel', 1, 14),
(645, '114', 'Villa Corona', 1, 14),
(646, '115', 'Villa Guerrero', 1, 14),
(647, '116', 'Villa Hidalgo', 1, 14),
(648, '117', 'Cañadas de Obregón', 1, 14),
(649, '118', 'Yahualica de González Gallo', 1, 14),
(650, '119', 'Zacoalco de Torres', 1, 14),
(651, '120', 'Zapopan', 1, 14),
(652, '121', 'Zapotiltic', 1, 14),
(653, '122', 'Zapotitlán de Vadillo', 1, 14),
(654, '123', 'Zapotlán del Rey', 1, 14),
(655, '124', 'Zapotlanejo', 1, 14),
(656, '125', 'San Ignacio Cerro Gordo', 1, 14),
(657, '001', 'Acambay de Ruíz Castañeda', 1, 15),
(658, '002', 'Acolman', 1, 15),
(659, '003', 'Aculco', 1, 15),
(660, '004', 'Almoloya de Alquisiras', 1, 15),
(661, '005', 'Almoloya de Juárez', 1, 15),
(662, '006', 'Almoloya del Río', 1, 15),
(663, '007', 'Amanalco', 1, 15),
(664, '008', 'Amatepec', 1, 15),
(665, '009', 'Amecameca', 1, 15),
(666, '010', 'Apaxco', 1, 15),
(667, '011', 'Atenco', 1, 15),
(668, '012', 'Atizapán', 1, 15),
(669, '013', 'Atizapán de Zaragoza', 1, 15),
(670, '014', 'Atlacomulco', 1, 15),
(671, '015', 'Atlautla', 1, 15),
(672, '016', 'Axapusco', 1, 15),
(673, '017', 'Ayapango', 1, 15),
(674, '018', 'Calimaya', 1, 15),
(675, '019', 'Capulhuac', 1, 15),
(676, '020', 'Coacalco de Berriozábal', 1, 15),
(677, '021', 'Coatepec Harinas', 1, 15),
(678, '022', 'Cocotitlán', 1, 15),
(679, '023', 'Coyotepec', 1, 15),
(680, '024', 'Cuautitlán', 1, 15),
(681, '025', 'Chalco', 1, 15),
(682, '026', 'Chapa de Mota', 1, 15),
(683, '027', 'Chapultepec', 1, 15),
(684, '028', 'Chiautla', 1, 15),
(685, '029', 'Chicoloapan', 1, 15),
(686, '030', 'Chiconcuac', 1, 15),
(687, '031', 'Chimalhuacán', 1, 15),
(688, '032', 'Donato Guerra', 1, 15),
(689, '033', 'Ecatepec de Morelos', 1, 15),
(690, '034', 'Ecatzingo', 1, 15),
(691, '035', 'Huehuetoca', 1, 15),
(692, '036', 'Hueypoxtla', 1, 15),
(693, '037', 'Huixquilucan', 1, 15),
(694, '038', 'Isidro Fabela', 1, 15),
(695, '039', 'Ixtapaluca', 1, 15),
(696, '040', 'Ixtapan de la Sal', 1, 15),
(697, '041', 'Ixtapan del Oro', 1, 15),
(698, '042', 'Ixtlahuaca', 1, 15),
(699, '043', 'Xalatlaco', 1, 15),
(700, '044', 'Jaltenco', 1, 15),
(701, '045', 'Jilotepec', 1, 15),
(702, '046', 'Jilotzingo', 1, 15),
(703, '047', 'Jiquipilco', 1, 15),
(704, '048', 'Jocotitlán', 1, 15),
(705, '049', 'Joquicingo', 1, 15),
(706, '050', 'Juchitepec', 1, 15),
(707, '051', 'Lerma', 1, 15),
(708, '052', 'Malinalco', 1, 15),
(709, '053', 'Melchor Ocampo', 1, 15),
(710, '054', 'Metepec', 1, 15),
(711, '055', 'Mexicaltzingo', 1, 15),
(712, '056', 'Morelos', 1, 15),
(713, '057', 'Naucalpan de Juárez', 1, 15),
(714, '058', 'Nezahualcóyotl', 1, 15),
(715, '059', 'Nextlalpan', 1, 15),
(716, '060', 'Nicolás Romero', 1, 15),
(717, '061', 'Nopaltepec', 1, 15),
(718, '062', 'Ocoyoacac', 1, 15),
(719, '063', 'Ocuilan', 1, 15),
(720, '064', 'El Oro', 1, 15),
(721, '065', 'Otumba', 1, 15),
(722, '066', 'Otzoloapan', 1, 15),
(723, '067', 'Otzolotepec', 1, 15),
(724, '068', 'Ozumba', 1, 15),
(725, '069', 'Papalotla', 1, 15),
(726, '070', 'La Paz', 1, 15),
(727, '071', 'Polotitlán', 1, 15),
(728, '072', 'Rayón', 1, 15),
(729, '073', 'San Antonio la Isla', 1, 15),
(730, '074', 'San Felipe del Progreso', 1, 15),
(731, '075', 'San Martín de las Pirámides', 1, 15),
(732, '076', 'San Mateo Atenco', 1, 15),
(733, '077', 'San Simón de Guerrero', 1, 15),
(734, '078', 'Santo Tomás', 1, 15),
(735, '079', 'Soyaniquilpan de Juárez', 1, 15),
(736, '080', 'Sultepec', 1, 15),
(737, '081', 'Tecámac', 1, 15),
(738, '082', 'Tejupilco', 1, 15),
(739, '083', 'Temamatla', 1, 15),
(740, '084', 'Temascalapa', 1, 15),
(741, '085', 'Temascalcingo', 1, 15),
(742, '086', 'Temascaltepec', 1, 15),
(743, '087', 'Temoaya', 1, 15),
(744, '088', 'Tenancingo', 1, 15),
(745, '089', 'Tenango del Aire', 1, 15),
(746, '090', 'Tenango del Valle', 1, 15),
(747, '091', 'Teoloyucan', 1, 15),
(748, '092', 'Teotihuacán', 1, 15),
(749, '093', 'Tepetlaoxtoc', 1, 15),
(750, '094', 'Tepetlixpa', 1, 15),
(751, '095', 'Tepotzotlán', 1, 15),
(752, '096', 'Tequixquiac', 1, 15),
(753, '097', 'Texcaltitlán', 1, 15),
(754, '098', 'Texcalyacac', 1, 15),
(755, '099', 'Texcoco', 1, 15),
(756, '100', 'Tezoyuca', 1, 15),
(757, '101', 'Tianguistenco', 1, 15),
(758, '102', 'Timilpan', 1, 15),
(759, '103', 'Tlalmanalco', 1, 15),
(760, '104', 'Tlalnepantla de Baz', 1, 15),
(761, '105', 'Tlatlaya', 1, 15),
(762, '106', 'Toluca', 1, 15),
(763, '107', 'Tonatico', 1, 15),
(764, '108', 'Tultepec', 1, 15),
(765, '109', 'Tultitlán', 1, 15),
(766, '110', 'Valle de Bravo', 1, 15),
(767, '111', 'Villa de Allende', 1, 15),
(768, '112', 'Villa del Carbón', 1, 15),
(769, '113', 'Villa Guerrero', 1, 15),
(770, '114', 'Villa Victoria', 1, 15),
(771, '115', 'Xonacatlán', 1, 15),
(772, '116', 'Zacazonapan', 1, 15),
(773, '117', 'Zacualpan', 1, 15),
(774, '118', 'Zinacantepec', 1, 15),
(775, '119', 'Zumpahuacán', 1, 15),
(776, '120', 'Zumpango', 1, 15),
(777, '121', 'Cuautitlán Izcalli', 1, 15),
(778, '122', 'Valle de Chalco Solidaridad', 1, 15),
(779, '123', 'Luvianos', 1, 15),
(780, '124', 'San José del Rincón', 1, 15),
(781, '125', 'Tonanitla', 1, 15),
(782, '001', 'Acuitzio', 1, 16),
(783, '002', 'Aguililla', 1, 16),
(784, '003', 'Álvaro Obregón', 1, 16),
(785, '004', 'Angamacutiro', 1, 16),
(786, '005', 'Angangueo', 1, 16),
(787, '006', 'Apatzingán', 1, 16),
(788, '007', 'Aporo', 1, 16),
(789, '008', 'Aquila', 1, 16),
(790, '009', 'Ario', 1, 16),
(791, '010', 'Arteaga', 1, 16),
(792, '011', 'Briseñas', 1, 16),
(793, '012', 'Buenavista', 1, 16),
(794, '013', 'Carácuaro', 1, 16),
(795, '014', 'Coahuayana', 1, 16),
(796, '015', 'Coalcomán de Vázquez Pallares', 1, 16),
(797, '016', 'Coeneo', 1, 16),
(798, '017', 'Contepec', 1, 16),
(799, '018', 'Copándaro', 1, 16),
(800, '019', 'Cotija', 1, 16),
(801, '020', 'Cuitzeo', 1, 16),
(802, '021', 'Charapan', 1, 16),
(803, '022', 'Charo', 1, 16),
(804, '023', 'Chavinda', 1, 16),
(805, '024', 'Cherán', 1, 16),
(806, '025', 'Chilchota', 1, 16),
(807, '026', 'Chinicuila', 1, 16),
(808, '027', 'Chucándiro', 1, 16),
(809, '028', 'Churintzio', 1, 16),
(810, '029', 'Churumuco', 1, 16),
(811, '030', 'Ecuandureo', 1, 16),
(812, '031', 'Epitacio Huerta', 1, 16),
(813, '032', 'Erongarícuaro', 1, 16),
(814, '033', 'Gabriel Zamora', 1, 16),
(815, '034', 'Hidalgo', 1, 16),
(816, '035', 'La Huacana', 1, 16),
(817, '036', 'Huandacareo', 1, 16),
(818, '037', 'Huaniqueo', 1, 16),
(819, '038', 'Huetamo', 1, 16),
(820, '039', 'Huiramba', 1, 16),
(821, '040', 'Indaparapeo', 1, 16),
(822, '041', 'Irimbo', 1, 16),
(823, '042', 'Ixtlán', 1, 16),
(824, '043', 'Jacona', 1, 16),
(825, '044', 'Jiménez', 1, 16),
(826, '045', 'Jiquilpan', 1, 16),
(827, '046', 'Juárez', 1, 16),
(828, '047', 'Jungapeo', 1, 16),
(829, '048', 'Lagunillas', 1, 16),
(830, '049', 'Madero', 1, 16),
(831, '050', 'Maravatío', 1, 16),
(832, '051', 'Marcos Castellanos', 1, 16),
(833, '052', 'Lázaro Cárdenas', 1, 16),
(834, '053', 'Morelia', 1, 16),
(835, '054', 'Morelos', 1, 16),
(836, '055', 'Múgica', 1, 16),
(837, '056', 'Nahuatzen', 1, 16),
(838, '057', 'Nocupétaro', 1, 16),
(839, '058', 'Nuevo Parangaricutiro', 1, 16),
(840, '059', 'Nuevo Urecho', 1, 16),
(841, '060', 'Numarán', 1, 16),
(842, '061', 'Ocampo', 1, 16),
(843, '062', 'Pajacuarán', 1, 16),
(844, '063', 'Panindícuaro', 1, 16),
(845, '064', 'Parácuaro', 1, 16),
(846, '065', 'Paracho', 1, 16),
(847, '066', 'Pátzcuaro', 1, 16),
(848, '067', 'Penjamillo', 1, 16),
(849, '068', 'Peribán', 1, 16),
(850, '069', 'La Piedad', 1, 16),
(851, '070', 'Purépero', 1, 16),
(852, '071', 'Puruándiro', 1, 16),
(853, '072', 'Queréndaro', 1, 16),
(854, '073', 'Quiroga', 1, 16),
(855, '074', 'Cojumatlán de Régules', 1, 16),
(856, '075', 'Los Reyes', 1, 16),
(857, '076', 'Sahuayo', 1, 16),
(858, '077', 'San Lucas', 1, 16),
(859, '078', 'Santa Ana Maya', 1, 16),
(860, '079', 'Salvador Escalante', 1, 16),
(861, '080', 'Senguio', 1, 16),
(862, '081', 'Susupuato', 1, 16),
(863, '082', 'Tacámbaro', 1, 16),
(864, '083', 'Tancítaro', 1, 16),
(865, '084', 'Tangamandapio', 1, 16),
(866, '085', 'Tangancícuaro', 1, 16),
(867, '086', 'Tanhuato', 1, 16),
(868, '087', 'Taretan', 1, 16),
(869, '088', 'Tarímbaro', 1, 16),
(870, '089', 'Tepalcatepec', 1, 16),
(871, '090', 'Tingambato', 1, 16),
(872, '091', 'Tingüindín', 1, 16),
(873, '092', 'Tiquicheo de Nicolás Romero', 1, 16),
(874, '093', 'Tlalpujahua', 1, 16),
(875, '094', 'Tlazazalca', 1, 16),
(876, '095', 'Tocumbo', 1, 16),
(877, '096', 'Tumbiscatío', 1, 16),
(878, '097', 'Turicato', 1, 16),
(879, '098', 'Tuxpan', 1, 16),
(880, '099', 'Tuzantla', 1, 16),
(881, '100', 'Tzintzuntzan', 1, 16),
(882, '101', 'Tzitzio', 1, 16),
(883, '102', 'Uruapan', 1, 16),
(884, '103', 'Venustiano Carranza', 1, 16),
(885, '104', 'Villamar', 1, 16),
(886, '105', 'Vista Hermosa', 1, 16),
(887, '106', 'Yurécuaro', 1, 16),
(888, '107', 'Zacapu', 1, 16),
(889, '108', 'Zamora', 1, 16),
(890, '109', 'Zináparo', 1, 16),
(891, '110', 'Zinapécuaro', 1, 16),
(892, '111', 'Ziracuaretiro', 1, 16),
(893, '112', 'Zitácuaro', 1, 16),
(894, '113', 'José Sixto Verduzco', 1, 16),
(895, '001', 'Amacuzac', 1, 17),
(896, '002', 'Atlatlahucan', 1, 17),
(897, '003', 'Axochiapan', 1, 17),
(898, '004', 'Ayala', 1, 17),
(899, '005', 'Coatlán del Río', 1, 17),
(900, '006', 'Cuautla', 1, 17),
(901, '007', 'Cuernavaca', 1, 17),
(902, '008', 'Emiliano Zapata', 1, 17),
(903, '009', 'Huitzilac', 1, 17),
(904, '010', 'Jantetelco', 1, 17),
(905, '011', 'Jiutepec', 1, 17),
(906, '012', 'Jojutla', 1, 17),
(907, '013', 'Jonacatepec', 1, 17),
(908, '014', 'Mazatepec', 1, 17),
(909, '015', 'Miacatlán', 1, 17),
(910, '016', 'Ocuituco', 1, 17),
(911, '017', 'Puente de Ixtla', 1, 17),
(912, '018', 'Temixco', 1, 17),
(913, '019', 'Tepalcingo', 1, 17),
(914, '020', 'Tepoztlán', 1, 17),
(915, '021', 'Tetecala', 1, 17),
(916, '022', 'Tetela del Volcán', 1, 17),
(917, '023', 'Tlalnepantla', 1, 17),
(918, '024', 'Tlaltizapán de Zapata', 1, 17),
(919, '025', 'Tlaquiltenango', 1, 17),
(920, '026', 'Tlayacapan', 1, 17),
(921, '027', 'Totolapan', 1, 17),
(922, '028', 'Xochitepec', 1, 17),
(923, '029', 'Yautepec', 1, 17),
(924, '030', 'Yecapixtla', 1, 17),
(925, '031', 'Zacatepec', 1, 17),
(926, '032', 'Zacualpan de Amilpas', 1, 17),
(927, '033', 'Temoac', 1, 17),
(928, '001', 'Acaponeta', 1, 18),
(929, '002', 'Ahuacatlán', 1, 18),
(930, '003', 'Amatlán de Cañas', 1, 18),
(931, '004', 'Compostela', 1, 18),
(932, '005', 'Huajicori', 1, 18),
(933, '006', 'Ixtlán del Río', 1, 18),
(934, '007', 'Jala', 1, 18),
(935, '008', 'Xalisco', 1, 18),
(936, '009', 'Del Nayar', 1, 18),
(937, '010', 'Rosamorada', 1, 18),
(938, '011', 'Ruíz', 1, 18),
(939, '012', 'San Blas', 1, 18),
(940, '013', 'San Pedro Lagunillas', 1, 18),
(941, '014', 'Santa María del Oro', 1, 18),
(942, '015', 'Santiago Ixcuintla', 1, 18),
(943, '016', 'Tecuala', 1, 18),
(944, '017', 'Tepic', 1, 18),
(945, '018', 'Tuxpan', 1, 18),
(946, '019', 'La Yesca', 1, 18),
(947, '020', 'Bahía de Banderas', 1, 18),
(948, '001', 'Abasolo', 1, 19),
(949, '002', 'Agualeguas', 1, 19),
(950, '003', 'Los Aldamas', 1, 19),
(951, '004', 'Allende', 1, 19),
(952, '005', 'Anáhuac', 1, 19),
(953, '006', 'Apodaca', 1, 19),
(954, '007', 'Aramberri', 1, 19),
(955, '008', 'Bustamante', 1, 19),
(956, '009', 'Cadereyta Jiménez', 1, 19),
(957, '010', 'El Carmen', 1, 19),
(958, '011', 'Cerralvo', 1, 19),
(959, '012', 'Ciénega de Flores', 1, 19),
(960, '013', 'China', 1, 19),
(961, '014', 'Doctor Arroyo', 1, 19),
(962, '015', 'Doctor Coss', 1, 19),
(963, '016', 'Doctor González', 1, 19),
(964, '017', 'Galeana', 1, 19),
(965, '018', 'García', 1, 19),
(966, '019', 'San Pedro Garza García', 1, 19),
(967, '020', 'General Bravo', 1, 19),
(968, '021', 'General Escobedo', 1, 19),
(969, '022', 'General Terán', 1, 19),
(970, '023', 'General Treviño', 1, 19),
(971, '024', 'General Zaragoza', 1, 19),
(972, '025', 'General Zuazua', 1, 19),
(973, '026', 'Guadalupe', 1, 19),
(974, '027', 'Los Herreras', 1, 19),
(975, '028', 'Higueras', 1, 19),
(976, '029', 'Hualahuises', 1, 19),
(977, '030', 'Iturbide', 1, 19),
(978, '031', 'Juárez', 1, 19),
(979, '032', 'Lampazos de Naranjo', 1, 19),
(980, '033', 'Linares', 1, 19),
(981, '034', 'Marín', 1, 19),
(982, '035', 'Melchor Ocampo', 1, 19),
(983, '036', 'Mier y Noriega', 1, 19),
(984, '037', 'Mina', 1, 19),
(985, '038', 'Montemorelos', 1, 19),
(986, '039', 'Monterrey', 1, 19),
(987, '040', 'Parás', 1, 19),
(988, '041', 'Pesquería', 1, 19),
(989, '042', 'Los Ramones', 1, 19),
(990, '043', 'Rayones', 1, 19),
(991, '044', 'Sabinas Hidalgo', 1, 19),
(992, '045', 'Salinas Victoria', 1, 19),
(993, '046', 'San Nicolás de los Garza', 1, 19),
(994, '047', 'Hidalgo', 1, 19),
(995, '048', 'Santa Catarina', 1, 19),
(996, '049', 'Santiago', 1, 19),
(997, '050', 'Vallecillo', 1, 19),
(998, '051', 'Villaldama', 1, 19),
(999, '001', 'Abejones', 1, 20),
(1000, '002', 'Acatlán de Pérez Figueroa', 1, 20),
(1001, '003', 'Asunción Cacalotepec', 1, 20),
(1002, '004', 'Asunción Cuyotepeji', 1, 20),
(1003, '005', 'Asunción Ixtaltepec', 1, 20),
(1004, '006', 'Asunción Nochixtlán', 1, 20),
(1005, '007', 'Asunción Ocotlán', 1, 20),
(1006, '008', 'Asunción Tlacolulita', 1, 20),
(1007, '009', 'Ayotzintepec', 1, 20),
(1008, '010', 'El Barrio de la Soledad', 1, 20),
(1009, '011', 'Calihualá', 1, 20),
(1010, '012', 'Candelaria Loxicha', 1, 20),
(1011, '013', 'Ciénega de Zimatlán', 1, 20),
(1012, '014', 'Ciudad Ixtepec', 1, 20),
(1013, '015', 'Coatecas Altas', 1, 20),
(1014, '016', 'Coicoyán de las Flores', 1, 20),
(1015, '017', 'La Compañía', 1, 20),
(1016, '018', 'Concepción Buenavista', 1, 20),
(1017, '019', 'Concepción Pápalo', 1, 20),
(1018, '020', 'Constancia del Rosario', 1, 20),
(1019, '021', 'Cosolapa', 1, 20),
(1020, '022', 'Cosoltepec', 1, 20),
(1021, '023', 'Cuilápam de Guerrero', 1, 20),
(1022, '024', 'Cuyamecalco Villa de Zaragoza', 1, 20),
(1023, '025', 'Chahuites', 1, 20),
(1024, '026', 'Chalcatongo de Hidalgo', 1, 20),
(1025, '027', 'Chiquihuitlán de Benito Juárez', 1, 20),
(1026, '028', 'Heroica Ciudad de Ejutla de Crespo', 1, 20),
(1027, '029', 'Eloxochitlán de Flores Magón', 1, 20),
(1028, '030', 'El Espinal', 1, 20),
(1029, '031', 'Tamazulápam del Espíritu Santo', 1, 20),
(1030, '032', 'Fresnillo de Trujano', 1, 20),
(1031, '033', 'Guadalupe Etla', 1, 20),
(1032, '034', 'Guadalupe de Ramírez', 1, 20),
(1033, '035', 'Guelatao de Juárez', 1, 20),
(1034, '036', 'Guevea de Humboldt', 1, 20),
(1035, '037', 'Mesones Hidalgo', 1, 20),
(1036, '038', 'Villa Hidalgo', 1, 20),
(1037, '039', 'Heroica Ciudad de Huajuapan de León', 1, 20),
(1038, '040', 'Huautepec', 1, 20),
(1039, '041', 'Huautla de Jiménez', 1, 20),
(1040, '042', 'Ixtlán de Juárez', 1, 20),
(1041, '043', 'Heroica Ciudad de Juchitán de Zaragoza', 1, 20),
(1042, '044', 'Loma Bonita', 1, 20),
(1043, '045', 'Magdalena Apasco', 1, 20),
(1044, '046', 'Magdalena Jaltepec', 1, 20),
(1045, '047', 'Santa Magdalena Jicotlán', 1, 20),
(1046, '048', 'Magdalena Mixtepec', 1, 20),
(1047, '049', 'Magdalena Ocotlán', 1, 20),
(1048, '050', 'Magdalena Peñasco', 1, 20),
(1049, '051', 'Magdalena Teitipac', 1, 20),
(1050, '052', 'Magdalena Tequisistlán', 1, 20),
(1051, '053', 'Magdalena Tlacotepec', 1, 20),
(1052, '054', 'Magdalena Zahuatlán', 1, 20),
(1053, '055', 'Mariscala de Juárez', 1, 20),
(1054, '056', 'Mártires de Tacubaya', 1, 20),
(1055, '057', 'Matías Romero Avendaño', 1, 20),
(1056, '058', 'Mazatlán Villa de Flores', 1, 20),
(1057, '059', 'Miahuatlán de Porfirio Díaz', 1, 20),
(1058, '060', 'Mixistlán de la Reforma', 1, 20),
(1059, '061', 'Monjas', 1, 20),
(1060, '062', 'Natividad', 1, 20),
(1061, '063', 'Nazareno Etla', 1, 20),
(1062, '064', 'Nejapa de Madero', 1, 20),
(1063, '065', 'Ixpantepec Nieves', 1, 20),
(1064, '066', 'Santiago Niltepec', 1, 20),
(1065, '067', 'Oaxaca de Juárez', 1, 20),
(1066, '068', 'Ocotlán de Morelos', 1, 20),
(1067, '069', 'La Pe', 1, 20),
(1068, '070', 'Pinotepa de Don Luis', 1, 20),
(1069, '071', 'Pluma Hidalgo', 1, 20),
(1070, '072', 'San José del Progreso', 1, 20),
(1071, '073', 'Putla Villa de Guerrero', 1, 20),
(1072, '074', 'Santa Catarina Quioquitani', 1, 20),
(1073, '075', 'Reforma de Pineda', 1, 20),
(1074, '076', 'La Reforma', 1, 20),
(1075, '077', 'Reyes Etla', 1, 20),
(1076, '078', 'Rojas de Cuauhtémoc', 1, 20),
(1077, '079', 'Salina Cruz', 1, 20),
(1078, '080', 'San Agustín Amatengo', 1, 20),
(1079, '081', 'San Agustín Atenango', 1, 20),
(1080, '082', 'San Agustín Chayuco', 1, 20),
(1081, '083', 'San Agustín de las Juntas', 1, 20),
(1082, '084', 'San Agustín Etla', 1, 20),
(1083, '085', 'San Agustín Loxicha', 1, 20),
(1084, '086', 'San Agustín Tlacotepec', 1, 20),
(1085, '087', 'San Agustín Yatareni', 1, 20),
(1086, '088', 'San Andrés Cabecera Nueva', 1, 20),
(1087, '089', 'San Andrés Dinicuiti', 1, 20),
(1088, '090', 'San Andrés Huaxpaltepec', 1, 20),
(1089, '091', 'San Andrés Huayápam', 1, 20),
(1090, '092', 'San Andrés Ixtlahuaca', 1, 20),
(1091, '093', 'San Andrés Lagunas', 1, 20),
(1092, '094', 'San Andrés Nuxiño', 1, 20),
(1093, '095', 'San Andrés Paxtlán', 1, 20),
(1094, '096', 'San Andrés Sinaxtla', 1, 20),
(1095, '097', 'San Andrés Solaga', 1, 20),
(1096, '098', 'San Andrés Teotilálpam', 1, 20),
(1097, '099', 'San Andrés Tepetlapa', 1, 20),
(1098, '100', 'San Andrés Yaá', 1, 20),
(1099, '101', 'San Andrés Zabache', 1, 20),
(1100, '102', 'San Andrés Zautla', 1, 20),
(1101, '103', 'San Antonino Castillo Velasco', 1, 20),
(1102, '104', 'San Antonino el Alto', 1, 20),
(1103, '105', 'San Antonino Monte Verde', 1, 20),
(1104, '106', 'San Antonio Acutla', 1, 20),
(1105, '107', 'San Antonio de la Cal', 1, 20),
(1106, '108', 'San Antonio Huitepec', 1, 20),
(1107, '109', 'San Antonio Nanahuatípam', 1, 20),
(1108, '110', 'San Antonio Sinicahua', 1, 20),
(1109, '111', 'San Antonio Tepetlapa', 1, 20),
(1110, '112', 'San Baltazar Chichicápam', 1, 20),
(1111, '113', 'San Baltazar Loxicha', 1, 20),
(1112, '114', 'San Baltazar Yatzachi el Bajo', 1, 20),
(1113, '115', 'San Bartolo Coyotepec', 1, 20),
(1114, '116', 'San Bartolomé Ayautla', 1, 20),
(1115, '117', 'San Bartolomé Loxicha', 1, 20),
(1116, '118', 'San Bartolomé Quialana', 1, 20),
(1117, '119', 'San Bartolomé Yucuañe', 1, 20),
(1118, '120', 'San Bartolomé Zoogocho', 1, 20),
(1119, '121', 'San Bartolo Soyaltepec', 1, 20),
(1120, '122', 'San Bartolo Yautepec', 1, 20),
(1121, '123', 'San Bernardo Mixtepec', 1, 20),
(1122, '124', 'San Blas Atempa', 1, 20),
(1123, '125', 'San Carlos Yautepec', 1, 20),
(1124, '126', 'San Cristóbal Amatlán', 1, 20),
(1125, '127', 'San Cristóbal Amoltepec', 1, 20),
(1126, '128', 'San Cristóbal Lachirioag', 1, 20),
(1127, '129', 'San Cristóbal Suchixtlahuaca', 1, 20),
(1128, '130', 'San Dionisio del Mar', 1, 20),
(1129, '131', 'San Dionisio Ocotepec', 1, 20),
(1130, '132', 'San Dionisio Ocotlán', 1, 20),
(1131, '133', 'San Esteban Atatlahuca', 1, 20),
(1132, '134', 'San Felipe Jalapa de Díaz', 1, 20),
(1133, '135', 'San Felipe Tejalápam', 1, 20),
(1134, '136', 'San Felipe Usila', 1, 20),
(1135, '137', 'San Francisco Cahuacuá', 1, 20),
(1136, '138', 'San Francisco Cajonos', 1, 20),
(1137, '139', 'San Francisco Chapulapa', 1, 20),
(1138, '140', 'San Francisco Chindúa', 1, 20),
(1139, '141', 'San Francisco del Mar', 1, 20),
(1140, '142', 'San Francisco Huehuetlán', 1, 20),
(1141, '143', 'San Francisco Ixhuatán', 1, 20),
(1142, '144', 'San Francisco Jaltepetongo', 1, 20),
(1143, '145', 'San Francisco Lachigoló', 1, 20),
(1144, '146', 'San Francisco Logueche', 1, 20),
(1145, '147', 'San Francisco Nuxaño', 1, 20),
(1146, '148', 'San Francisco Ozolotepec', 1, 20),
(1147, '149', 'San Francisco Sola', 1, 20),
(1148, '150', 'San Francisco Telixtlahuaca', 1, 20),
(1149, '151', 'San Francisco Teopan', 1, 20),
(1150, '152', 'San Francisco Tlapancingo', 1, 20),
(1151, '153', 'San Gabriel Mixtepec', 1, 20),
(1152, '154', 'San Ildefonso Amatlán', 1, 20),
(1153, '155', 'San Ildefonso Sola', 1, 20),
(1154, '156', 'San Ildefonso Villa Alta', 1, 20),
(1155, '157', 'San Jacinto Amilpas', 1, 20),
(1156, '158', 'San Jacinto Tlacotepec', 1, 20),
(1157, '159', 'San Jerónimo Coatlán', 1, 20),
(1158, '160', 'San Jerónimo Silacayoapilla', 1, 20),
(1159, '161', 'San Jerónimo Sosola', 1, 20),
(1160, '162', 'San Jerónimo Taviche', 1, 20),
(1161, '163', 'San Jerónimo Tecóatl', 1, 20),
(1162, '164', 'San Jorge Nuchita', 1, 20),
(1163, '165', 'San José Ayuquila', 1, 20),
(1164, '166', 'San José Chiltepec', 1, 20),
(1165, '167', 'San José del Peñasco', 1, 20),
(1166, '168', 'San José Estancia Grande', 1, 20),
(1167, '169', 'San José Independencia', 1, 20),
(1168, '170', 'San José Lachiguiri', 1, 20),
(1169, '171', 'San José Tenango', 1, 20),
(1170, '172', 'San Juan Achiutla', 1, 20),
(1171, '173', 'San Juan Atepec', 1, 20),
(1172, '174', 'Ánimas Trujano', 1, 20),
(1173, '175', 'San Juan Bautista Atatlahuca', 1, 20),
(1174, '176', 'San Juan Bautista Coixtlahuaca', 1, 20),
(1175, '177', 'San Juan Bautista Cuicatlán', 1, 20),
(1176, '178', 'San Juan Bautista Guelache', 1, 20),
(1177, '179', 'San Juan Bautista Jayacatlán', 1, 20),
(1178, '180', 'San Juan Bautista Lo de Soto', 1, 20),
(1179, '181', 'San Juan Bautista Suchitepec', 1, 20),
(1180, '182', 'San Juan Bautista Tlacoatzintepec', 1, 20),
(1181, '183', 'San Juan Bautista Tlachichilco', 1, 20),
(1182, '184', 'San Juan Bautista Tuxtepec', 1, 20),
(1183, '185', 'San Juan Cacahuatepec', 1, 20),
(1184, '186', 'San Juan Cieneguilla', 1, 20),
(1185, '187', 'San Juan Coatzóspam', 1, 20),
(1186, '188', 'San Juan Colorado', 1, 20),
(1187, '189', 'San Juan Comaltepec', 1, 20),
(1188, '190', 'San Juan Cotzocón', 1, 20),
(1189, '191', 'San Juan Chicomezúchil', 1, 20),
(1190, '192', 'San Juan Chilateca', 1, 20),
(1191, '193', 'San Juan del Estado', 1, 20),
(1192, '194', 'San Juan del Río', 1, 20),
(1193, '195', 'San Juan Diuxi', 1, 20),
(1194, '196', 'San Juan Evangelista Analco', 1, 20),
(1195, '197', 'San Juan Guelavía', 1, 20),
(1196, '198', 'San Juan Guichicovi', 1, 20),
(1197, '199', 'San Juan Ihualtepec', 1, 20),
(1198, '200', 'San Juan Juquila Mixes', 1, 20),
(1199, '201', 'San Juan Juquila Vijanos', 1, 20),
(1200, '202', 'San Juan Lachao', 1, 20),
(1201, '203', 'San Juan Lachigalla', 1, 20),
(1202, '204', 'San Juan Lajarcia', 1, 20),
(1203, '205', 'San Juan Lalana', 1, 20),
(1204, '206', 'San Juan de los Cués', 1, 20),
(1205, '207', 'San Juan Mazatlán', 1, 20),
(1206, '208', 'San Juan Mixtepec', 1, 20),
(1207, '209', 'San Juan Mixtepec', 1, 20),
(1208, '210', 'San Juan Ñumí', 1, 20),
(1209, '211', 'San Juan Ozolotepec', 1, 20),
(1210, '212', 'San Juan Petlapa', 1, 20),
(1211, '213', 'San Juan Quiahije', 1, 20),
(1212, '214', 'San Juan Quiotepec', 1, 20),
(1213, '215', 'San Juan Sayultepec', 1, 20),
(1214, '216', 'San Juan Tabaá', 1, 20),
(1215, '217', 'San Juan Tamazola', 1, 20),
(1216, '218', 'San Juan Teita', 1, 20),
(1217, '219', 'San Juan Teitipac', 1, 20),
(1218, '220', 'San Juan Tepeuxila', 1, 20),
(1219, '221', 'San Juan Teposcolula', 1, 20),
(1220, '222', 'San Juan Yaeé', 1, 20),
(1221, '223', 'San Juan Yatzona', 1, 20),
(1222, '224', 'San Juan Yucuita', 1, 20),
(1223, '225', 'San Lorenzo', 1, 20),
(1224, '226', 'San Lorenzo Albarradas', 1, 20),
(1225, '227', 'San Lorenzo Cacaotepec', 1, 20),
(1226, '228', 'San Lorenzo Cuaunecuiltitla', 1, 20),
(1227, '229', 'San Lorenzo Texmelúcan', 1, 20),
(1228, '230', 'San Lorenzo Victoria', 1, 20),
(1229, '231', 'San Lucas Camotlán', 1, 20),
(1230, '232', 'San Lucas Ojitlán', 1, 20),
(1231, '233', 'San Lucas Quiaviní', 1, 20),
(1232, '234', 'San Lucas Zoquiápam', 1, 20),
(1233, '235', 'San Luis Amatlán', 1, 20),
(1234, '236', 'San Marcial Ozolotepec', 1, 20),
(1235, '237', 'San Marcos Arteaga', 1, 20),
(1236, '238', 'San Martín de los Cansecos', 1, 20),
(1237, '239', 'San Martín Huamelúlpam', 1, 20),
(1238, '240', 'San Martín Itunyoso', 1, 20),
(1239, '241', 'San Martín Lachilá', 1, 20),
(1240, '242', 'San Martín Peras', 1, 20),
(1241, '243', 'San Martín Tilcajete', 1, 20),
(1242, '244', 'San Martín Toxpalan', 1, 20),
(1243, '245', 'San Martín Zacatepec', 1, 20),
(1244, '246', 'San Mateo Cajonos', 1, 20),
(1245, '247', 'Capulálpam de Méndez', 1, 20),
(1246, '248', 'San Mateo del Mar', 1, 20),
(1247, '249', 'San Mateo Yoloxochitlán', 1, 20),
(1248, '250', 'San Mateo Etlatongo', 1, 20),
(1249, '251', 'San Mateo Nejápam', 1, 20),
(1250, '252', 'San Mateo Peñasco', 1, 20),
(1251, '253', 'San Mateo Piñas', 1, 20),
(1252, '254', 'San Mateo Río Hondo', 1, 20),
(1253, '255', 'San Mateo Sindihui', 1, 20),
(1254, '256', 'San Mateo Tlapiltepec', 1, 20),
(1255, '257', 'San Melchor Betaza', 1, 20),
(1256, '258', 'San Miguel Achiutla', 1, 20),
(1257, '259', 'San Miguel Ahuehuetitlán', 1, 20),
(1258, '260', 'San Miguel Aloápam', 1, 20),
(1259, '261', 'San Miguel Amatitlán', 1, 20),
(1260, '262', 'San Miguel Amatlán', 1, 20),
(1261, '263', 'San Miguel Coatlán', 1, 20),
(1262, '264', 'San Miguel Chicahua', 1, 20),
(1263, '265', 'San Miguel Chimalapa', 1, 20),
(1264, '266', 'San Miguel del Puerto', 1, 20),
(1265, '267', 'San Miguel del Río', 1, 20),
(1266, '268', 'San Miguel Ejutla', 1, 20),
(1267, '269', 'San Miguel el Grande', 1, 20),
(1268, '270', 'San Miguel Huautla', 1, 20),
(1269, '271', 'San Miguel Mixtepec', 1, 20),
(1270, '272', 'San Miguel Panixtlahuaca', 1, 20),
(1271, '273', 'San Miguel Peras', 1, 20),
(1272, '274', 'San Miguel Piedras', 1, 20),
(1273, '275', 'San Miguel Quetzaltepec', 1, 20),
(1274, '276', 'San Miguel Santa Flor', 1, 20),
(1275, '277', 'Villa Sola de Vega', 1, 20),
(1276, '278', 'San Miguel Soyaltepec', 1, 20),
(1277, '279', 'San Miguel Suchixtepec', 1, 20),
(1278, '280', 'Villa Talea de Castro', 1, 20),
(1279, '281', 'San Miguel Tecomatlán', 1, 20),
(1280, '282', 'San Miguel Tenango', 1, 20),
(1281, '283', 'San Miguel Tequixtepec', 1, 20),
(1282, '284', 'San Miguel Tilquiápam', 1, 20),
(1283, '285', 'San Miguel Tlacamama', 1, 20),
(1284, '286', 'San Miguel Tlacotepec', 1, 20),
(1285, '287', 'San Miguel Tulancingo', 1, 20),
(1286, '288', 'San Miguel Yotao', 1, 20),
(1287, '289', 'San Nicolás', 1, 20),
(1288, '290', 'San Nicolás Hidalgo', 1, 20),
(1289, '291', 'San Pablo Coatlán', 1, 20),
(1290, '292', 'San Pablo Cuatro Venados', 1, 20),
(1291, '293', 'San Pablo Etla', 1, 20),
(1292, '294', 'San Pablo Huitzo', 1, 20),
(1293, '295', 'San Pablo Huixtepec', 1, 20),
(1294, '296', 'San Pablo Macuiltianguis', 1, 20),
(1295, '297', 'San Pablo Tijaltepec', 1, 20),
(1296, '298', 'San Pablo Villa de Mitla', 1, 20),
(1297, '299', 'San Pablo Yaganiza', 1, 20),
(1298, '300', 'San Pedro Amuzgos', 1, 20),
(1299, '301', 'San Pedro Apóstol', 1, 20),
(1300, '302', 'San Pedro Atoyac', 1, 20),
(1301, '303', 'San Pedro Cajonos', 1, 20),
(1302, '304', 'San Pedro Coxcaltepec Cántaros', 1, 20),
(1303, '305', 'San Pedro Comitancillo', 1, 20),
(1304, '306', 'San Pedro el Alto', 1, 20),
(1305, '307', 'San Pedro Huamelula', 1, 20),
(1306, '308', 'San Pedro Huilotepec', 1, 20),
(1307, '309', 'San Pedro Ixcatlán', 1, 20),
(1308, '310', 'San Pedro Ixtlahuaca', 1, 20),
(1309, '311', 'San Pedro Jaltepetongo', 1, 20),
(1310, '312', 'San Pedro Jicayán', 1, 20),
(1311, '313', 'San Pedro Jocotipac', 1, 20),
(1312, '314', 'San Pedro Juchatengo', 1, 20),
(1313, '315', 'San Pedro Mártir', 1, 20),
(1314, '316', 'San Pedro Mártir Quiechapa', 1, 20),
(1315, '317', 'San Pedro Mártir Yucuxaco', 1, 20),
(1316, '318', 'San Pedro Mixtepec', 1, 20),
(1317, '319', 'San Pedro Mixtepec', 1, 20),
(1318, '320', 'San Pedro Molinos', 1, 20),
(1319, '321', 'San Pedro Nopala', 1, 20),
(1320, '322', 'San Pedro Ocopetatillo', 1, 20),
(1321, '323', 'San Pedro Ocotepec', 1, 20),
(1322, '324', 'San Pedro Pochutla', 1, 20),
(1323, '325', 'San Pedro Quiatoni', 1, 20),
(1324, '326', 'San Pedro Sochiápam', 1, 20),
(1325, '327', 'San Pedro Tapanatepec', 1, 20),
(1326, '328', 'San Pedro Taviche', 1, 20),
(1327, '329', 'San Pedro Teozacoalco', 1, 20),
(1328, '330', 'San Pedro Teutila', 1, 20),
(1329, '331', 'San Pedro Tidaá', 1, 20),
(1330, '332', 'San Pedro Topiltepec', 1, 20),
(1331, '333', 'San Pedro Totolápam', 1, 20),
(1332, '334', 'Villa de Tututepec de Melchor Ocampo', 1, 20),
(1333, '335', 'San Pedro Yaneri', 1, 20),
(1334, '336', 'San Pedro Yólox', 1, 20),
(1335, '337', 'San Pedro y San Pablo Ayutla', 1, 20),
(1336, '338', 'Villa de Etla', 1, 20),
(1337, '339', 'San Pedro y San Pablo Teposcolula', 1, 20),
(1338, '340', 'San Pedro y San Pablo Tequixtepec', 1, 20),
(1339, '341', 'San Pedro Yucunama', 1, 20),
(1340, '342', 'San Raymundo Jalpan', 1, 20),
(1341, '343', 'San Sebastián Abasolo', 1, 20),
(1342, '344', 'San Sebastián Coatlán', 1, 20),
(1343, '345', 'San Sebastián Ixcapa', 1, 20),
(1344, '346', 'San Sebastián Nicananduta', 1, 20),
(1345, '347', 'San Sebastián Río Hondo', 1, 20),
(1346, '348', 'San Sebastián Tecomaxtlahuaca', 1, 20);
INSERT INTO `coordinador_municipio` (`id_municipio`, `clave`, `nombre`, `activo`, `id_estado_id`) VALUES
(1347, '349', 'San Sebastián Teitipac', 1, 20),
(1348, '350', 'San Sebastián Tutla', 1, 20),
(1349, '351', 'San Simón Almolongas', 1, 20),
(1350, '352', 'San Simón Zahuatlán', 1, 20),
(1351, '353', 'Santa Ana', 1, 20),
(1352, '354', 'Santa Ana Ateixtlahuaca', 1, 20),
(1353, '355', 'Santa Ana Cuauhtémoc', 1, 20),
(1354, '356', 'Santa Ana del Valle', 1, 20),
(1355, '357', 'Santa Ana Tavela', 1, 20),
(1356, '358', 'Santa Ana Tlapacoyan', 1, 20),
(1357, '359', 'Santa Ana Yareni', 1, 20),
(1358, '360', 'Santa Ana Zegache', 1, 20),
(1359, '361', 'Santa Catalina Quierí', 1, 20),
(1360, '362', 'Santa Catarina Cuixtla', 1, 20),
(1361, '363', 'Santa Catarina Ixtepeji', 1, 20),
(1362, '364', 'Santa Catarina Juquila', 1, 20),
(1363, '365', 'Santa Catarina Lachatao', 1, 20),
(1364, '366', 'Santa Catarina Loxicha', 1, 20),
(1365, '367', 'Santa Catarina Mechoacán', 1, 20),
(1366, '368', 'Santa Catarina Minas', 1, 20),
(1367, '369', 'Santa Catarina Quiané', 1, 20),
(1368, '370', 'Santa Catarina Tayata', 1, 20),
(1369, '371', 'Santa Catarina Ticuá', 1, 20),
(1370, '372', 'Santa Catarina Yosonotú', 1, 20),
(1371, '373', 'Santa Catarina Zapoquila', 1, 20),
(1372, '374', 'Santa Cruz Acatepec', 1, 20),
(1373, '375', 'Santa Cruz Amilpas', 1, 20),
(1374, '376', 'Santa Cruz de Bravo', 1, 20),
(1375, '377', 'Santa Cruz Itundujia', 1, 20),
(1376, '378', 'Santa Cruz Mixtepec', 1, 20),
(1377, '379', 'Santa Cruz Nundaco', 1, 20),
(1378, '380', 'Santa Cruz Papalutla', 1, 20),
(1379, '381', 'Santa Cruz Tacache de Mina', 1, 20),
(1380, '382', 'Santa Cruz Tacahua', 1, 20),
(1381, '383', 'Santa Cruz Tayata', 1, 20),
(1382, '384', 'Santa Cruz Xitla', 1, 20),
(1383, '385', 'Santa Cruz Xoxocotlán', 1, 20),
(1384, '386', 'Santa Cruz Zenzontepec', 1, 20),
(1385, '387', 'Santa Gertrudis', 1, 20),
(1386, '388', 'Santa Inés del Monte', 1, 20),
(1387, '389', 'Santa Inés Yatzeche', 1, 20),
(1388, '390', 'Santa Lucía del Camino', 1, 20),
(1389, '391', 'Santa Lucía Miahuatlán', 1, 20),
(1390, '392', 'Santa Lucía Monteverde', 1, 20),
(1391, '393', 'Santa Lucía Ocotlán', 1, 20),
(1392, '394', 'Santa María Alotepec', 1, 20),
(1393, '395', 'Santa María Apazco', 1, 20),
(1394, '396', 'Santa María la Asunción', 1, 20),
(1395, '397', 'Heroica Ciudad de Tlaxiaco', 1, 20),
(1396, '398', 'Ayoquezco de Aldama', 1, 20),
(1397, '399', 'Santa María Atzompa', 1, 20),
(1398, '400', 'Santa María Camotlán', 1, 20),
(1399, '401', 'Santa María Colotepec', 1, 20),
(1400, '402', 'Santa María Cortijo', 1, 20),
(1401, '403', 'Santa María Coyotepec', 1, 20),
(1402, '404', 'Santa María Chachoápam', 1, 20),
(1403, '405', 'Villa de Chilapa de Díaz', 1, 20),
(1404, '406', 'Santa María Chilchotla', 1, 20),
(1405, '407', 'Santa María Chimalapa', 1, 20),
(1406, '408', 'Santa María del Rosario', 1, 20),
(1407, '409', 'Santa María del Tule', 1, 20),
(1408, '410', 'Santa María Ecatepec', 1, 20),
(1409, '411', 'Santa María Guelacé', 1, 20),
(1410, '412', 'Santa María Guienagati', 1, 20),
(1411, '413', 'Santa María Huatulco', 1, 20),
(1412, '414', 'Santa María Huazolotitlán', 1, 20),
(1413, '415', 'Santa María Ipalapa', 1, 20),
(1414, '416', 'Santa María Ixcatlán', 1, 20),
(1415, '417', 'Santa María Jacatepec', 1, 20),
(1416, '418', 'Santa María Jalapa del Marqués', 1, 20),
(1417, '419', 'Santa María Jaltianguis', 1, 20),
(1418, '420', 'Santa María Lachixío', 1, 20),
(1419, '421', 'Santa María Mixtequilla', 1, 20),
(1420, '422', 'Santa María Nativitas', 1, 20),
(1421, '423', 'Santa María Nduayaco', 1, 20),
(1422, '424', 'Santa María Ozolotepec', 1, 20),
(1423, '425', 'Santa María Pápalo', 1, 20),
(1424, '426', 'Santa María Peñoles', 1, 20),
(1425, '427', 'Santa María Petapa', 1, 20),
(1426, '428', 'Santa María Quiegolani', 1, 20),
(1427, '429', 'Santa María Sola', 1, 20),
(1428, '430', 'Santa María Tataltepec', 1, 20),
(1429, '431', 'Santa María Tecomavaca', 1, 20),
(1430, '432', 'Santa María Temaxcalapa', 1, 20),
(1431, '433', 'Santa María Temaxcaltepec', 1, 20),
(1432, '434', 'Santa María Teopoxco', 1, 20),
(1433, '435', 'Santa María Tepantlali', 1, 20),
(1434, '436', 'Santa María Texcatitlán', 1, 20),
(1435, '437', 'Santa María Tlahuitoltepec', 1, 20),
(1436, '438', 'Santa María Tlalixtac', 1, 20),
(1437, '439', 'Santa María Tonameca', 1, 20),
(1438, '440', 'Santa María Totolapilla', 1, 20),
(1439, '441', 'Santa María Xadani', 1, 20),
(1440, '442', 'Santa María Yalina', 1, 20),
(1441, '443', 'Santa María Yavesía', 1, 20),
(1442, '444', 'Santa María Yolotepec', 1, 20),
(1443, '445', 'Santa María Yosoyúa', 1, 20),
(1444, '446', 'Santa María Yucuhiti', 1, 20),
(1445, '447', 'Santa María Zacatepec', 1, 20),
(1446, '448', 'Santa María Zaniza', 1, 20),
(1447, '449', 'Santa María Zoquitlán', 1, 20),
(1448, '450', 'Santiago Amoltepec', 1, 20),
(1449, '451', 'Santiago Apoala', 1, 20),
(1450, '452', 'Santiago Apóstol', 1, 20),
(1451, '453', 'Santiago Astata', 1, 20),
(1452, '454', 'Santiago Atitlán', 1, 20),
(1453, '455', 'Santiago Ayuquililla', 1, 20),
(1454, '456', 'Santiago Cacaloxtepec', 1, 20),
(1455, '457', 'Santiago Camotlán', 1, 20),
(1456, '458', 'Santiago Comaltepec', 1, 20),
(1457, '459', 'Santiago Chazumba', 1, 20),
(1458, '460', 'Santiago Choápam', 1, 20),
(1459, '461', 'Santiago del Río', 1, 20),
(1460, '462', 'Santiago Huajolotitlán', 1, 20),
(1461, '463', 'Santiago Huauclilla', 1, 20),
(1462, '464', 'Santiago Ihuitlán Plumas', 1, 20),
(1463, '465', 'Santiago Ixcuintepec', 1, 20),
(1464, '466', 'Santiago Ixtayutla', 1, 20),
(1465, '467', 'Santiago Jamiltepec', 1, 20),
(1466, '468', 'Santiago Jocotepec', 1, 20),
(1467, '469', 'Santiago Juxtlahuaca', 1, 20),
(1468, '470', 'Santiago Lachiguiri', 1, 20),
(1469, '471', 'Santiago Lalopa', 1, 20),
(1470, '472', 'Santiago Laollaga', 1, 20),
(1471, '473', 'Santiago Laxopa', 1, 20),
(1472, '474', 'Santiago Llano Grande', 1, 20),
(1473, '475', 'Santiago Matatlán', 1, 20),
(1474, '476', 'Santiago Miltepec', 1, 20),
(1475, '477', 'Santiago Minas', 1, 20),
(1476, '478', 'Santiago Nacaltepec', 1, 20),
(1477, '479', 'Santiago Nejapilla', 1, 20),
(1478, '480', 'Santiago Nundiche', 1, 20),
(1479, '481', 'Santiago Nuyoó', 1, 20),
(1480, '482', 'Santiago Pinotepa Nacional', 1, 20),
(1481, '483', 'Santiago Suchilquitongo', 1, 20),
(1482, '484', 'Santiago Tamazola', 1, 20),
(1483, '485', 'Santiago Tapextla', 1, 20),
(1484, '486', 'Villa Tejúpam de la Unión', 1, 20),
(1485, '487', 'Santiago Tenango', 1, 20),
(1486, '488', 'Santiago Tepetlapa', 1, 20),
(1487, '489', 'Santiago Tetepec', 1, 20),
(1488, '490', 'Santiago Texcalcingo', 1, 20),
(1489, '491', 'Santiago Textitlán', 1, 20),
(1490, '492', 'Santiago Tilantongo', 1, 20),
(1491, '493', 'Santiago Tillo', 1, 20),
(1492, '494', 'Santiago Tlazoyaltepec', 1, 20),
(1493, '495', 'Santiago Xanica', 1, 20),
(1494, '496', 'Santiago Xiacuí', 1, 20),
(1495, '497', 'Santiago Yaitepec', 1, 20),
(1496, '498', 'Santiago Yaveo', 1, 20),
(1497, '499', 'Santiago Yolomécatl', 1, 20),
(1498, '500', 'Santiago Yosondúa', 1, 20),
(1499, '501', 'Santiago Yucuyachi', 1, 20),
(1500, '502', 'Santiago Zacatepec', 1, 20),
(1501, '503', 'Santiago Zoochila', 1, 20),
(1502, '504', 'Nuevo Zoquiápam', 1, 20),
(1503, '505', 'Santo Domingo Ingenio', 1, 20),
(1504, '506', 'Santo Domingo Albarradas', 1, 20),
(1505, '507', 'Santo Domingo Armenta', 1, 20),
(1506, '508', 'Santo Domingo Chihuitán', 1, 20),
(1507, '509', 'Santo Domingo de Morelos', 1, 20),
(1508, '510', 'Santo Domingo Ixcatlán', 1, 20),
(1509, '511', 'Santo Domingo Nuxaá', 1, 20),
(1510, '512', 'Santo Domingo Ozolotepec', 1, 20),
(1511, '513', 'Santo Domingo Petapa', 1, 20),
(1512, '514', 'Santo Domingo Roayaga', 1, 20),
(1513, '515', 'Santo Domingo Tehuantepec', 1, 20),
(1514, '516', 'Santo Domingo Teojomulco', 1, 20),
(1515, '517', 'Santo Domingo Tepuxtepec', 1, 20),
(1516, '518', 'Santo Domingo Tlatayápam', 1, 20),
(1517, '519', 'Santo Domingo Tomaltepec', 1, 20),
(1518, '520', 'Santo Domingo Tonalá', 1, 20),
(1519, '521', 'Santo Domingo Tonaltepec', 1, 20),
(1520, '522', 'Santo Domingo Xagacía', 1, 20),
(1521, '523', 'Santo Domingo Yanhuitlán', 1, 20),
(1522, '524', 'Santo Domingo Yodohino', 1, 20),
(1523, '525', 'Santo Domingo Zanatepec', 1, 20),
(1524, '526', 'Santos Reyes Nopala', 1, 20),
(1525, '527', 'Santos Reyes Pápalo', 1, 20),
(1526, '528', 'Santos Reyes Tepejillo', 1, 20),
(1527, '529', 'Santos Reyes Yucuná', 1, 20),
(1528, '530', 'Santo Tomás Jalieza', 1, 20),
(1529, '531', 'Santo Tomás Mazaltepec', 1, 20),
(1530, '532', 'Santo Tomás Ocotepec', 1, 20),
(1531, '533', 'Santo Tomás Tamazulapan', 1, 20),
(1532, '534', 'San Vicente Coatlán', 1, 20),
(1533, '535', 'San Vicente Lachixío', 1, 20),
(1534, '536', 'San Vicente Nuñú', 1, 20),
(1535, '537', 'Silacayoápam', 1, 20),
(1536, '538', 'Sitio de Xitlapehua', 1, 20),
(1537, '539', 'Soledad Etla', 1, 20),
(1538, '540', 'Villa de Tamazulápam del Progreso', 1, 20),
(1539, '541', 'Tanetze de Zaragoza', 1, 20),
(1540, '542', 'Taniche', 1, 20),
(1541, '543', 'Tataltepec de Valdés', 1, 20),
(1542, '544', 'Teococuilco de Marcos Pérez', 1, 20),
(1543, '545', 'Teotitlán de Flores Magón', 1, 20),
(1544, '546', 'Teotitlán del Valle', 1, 20),
(1545, '547', 'Teotongo', 1, 20),
(1546, '548', 'Tepelmeme Villa de Morelos', 1, 20),
(1547, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1548, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1549, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1550, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1551, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1552, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1553, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1554, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1555, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1556, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1557, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1558, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1559, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1560, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1561, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1562, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1563, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1564, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1565, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1566, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1567, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1568, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1569, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1570, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1571, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1572, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1573, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1574, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1575, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1576, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1577, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1578, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1579, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1580, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1581, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1582, '549', 'Heroica Villa Tezoatlán de Segura y Luna, Cuna de ', 1, 20),
(1583, '550', 'San Jerónimo Tlacochahuaya', 1, 20),
(1584, '551', 'Tlacolula de Matamoros', 1, 20),
(1585, '552', 'Tlacotepec Plumas', 1, 20),
(1586, '553', 'Tlalixtac de Cabrera', 1, 20),
(1587, '554', 'Totontepec Villa de Morelos', 1, 20),
(1588, '555', 'Trinidad Zaachila', 1, 20),
(1589, '556', 'La Trinidad Vista Hermosa', 1, 20),
(1590, '557', 'Unión Hidalgo', 1, 20),
(1591, '558', 'Valerio Trujano', 1, 20),
(1592, '559', 'San Juan Bautista Valle Nacional', 1, 20),
(1593, '560', 'Villa Díaz Ordaz', 1, 20),
(1594, '561', 'Yaxe', 1, 20),
(1595, '562', 'Magdalena Yodocono de Porfirio Díaz', 1, 20),
(1596, '563', 'Yogana', 1, 20),
(1597, '564', 'Yutanduchi de Guerrero', 1, 20),
(1598, '565', 'Villa de Zaachila', 1, 20),
(1599, '566', 'San Mateo Yucutindoo', 1, 20),
(1600, '567', 'Zapotitlán Lagunas', 1, 20),
(1601, '568', 'Zapotitlán Palmas', 1, 20),
(1602, '569', 'Santa Inés de Zaragoza', 1, 20),
(1603, '570', 'Zimatlán de Álvarez', 1, 20),
(1604, '001', 'Acajete', 1, 21),
(1605, '002', 'Acateno', 1, 21),
(1606, '003', 'Acatlán', 1, 21),
(1607, '004', 'Acatzingo', 1, 21),
(1608, '005', 'Acteopan', 1, 21),
(1609, '006', 'Ahuacatlán', 1, 21),
(1610, '007', 'Ahuatlán', 1, 21),
(1611, '008', 'Ahuazotepec', 1, 21),
(1612, '009', 'Ahuehuetitla', 1, 21),
(1613, '010', 'Ajalpan', 1, 21),
(1614, '011', 'Albino Zertuche', 1, 21),
(1615, '012', 'Aljojuca', 1, 21),
(1616, '013', 'Altepexi', 1, 21),
(1617, '014', 'Amixtlán', 1, 21),
(1618, '015', 'Amozoc', 1, 21),
(1619, '016', 'Aquixtla', 1, 21),
(1620, '017', 'Atempan', 1, 21),
(1621, '018', 'Atexcal', 1, 21),
(1622, '019', 'Atlixco', 1, 21),
(1623, '020', 'Atoyatempan', 1, 21),
(1624, '021', 'Atzala', 1, 21),
(1625, '022', 'Atzitzihuacán', 1, 21),
(1626, '023', 'Atzitzintla', 1, 21),
(1627, '024', 'Axutla', 1, 21),
(1628, '025', 'Ayotoxco de Guerrero', 1, 21),
(1629, '026', 'Calpan', 1, 21),
(1630, '027', 'Caltepec', 1, 21),
(1631, '028', 'Camocuautla', 1, 21),
(1632, '029', 'Caxhuacan', 1, 21),
(1633, '030', 'Coatepec', 1, 21),
(1634, '031', 'Coatzingo', 1, 21),
(1635, '032', 'Cohetzala', 1, 21),
(1636, '033', 'Cohuecan', 1, 21),
(1637, '034', 'Coronango', 1, 21),
(1638, '035', 'Coxcatlán', 1, 21),
(1639, '036', 'Coyomeapan', 1, 21),
(1640, '037', 'Coyotepec', 1, 21),
(1641, '038', 'Cuapiaxtla de Madero', 1, 21),
(1642, '039', 'Cuautempan', 1, 21),
(1643, '040', 'Cuautinchán', 1, 21),
(1644, '041', 'Cuautlancingo', 1, 21),
(1645, '042', 'Cuayuca de Andrade', 1, 21),
(1646, '043', 'Cuetzalan del Progreso', 1, 21),
(1647, '044', 'Cuyoaco', 1, 21),
(1648, '045', 'Chalchicomula de Sesma', 1, 21),
(1649, '046', 'Chapulco', 1, 21),
(1650, '047', 'Chiautla', 1, 21),
(1651, '048', 'Chiautzingo', 1, 21),
(1652, '049', 'Chiconcuautla', 1, 21),
(1653, '050', 'Chichiquila', 1, 21),
(1654, '051', 'Chietla', 1, 21),
(1655, '052', 'Chigmecatitlán', 1, 21),
(1656, '053', 'Chignahuapan', 1, 21),
(1657, '054', 'Chignautla', 1, 21),
(1658, '055', 'Chila', 1, 21),
(1659, '056', 'Chila de la Sal', 1, 21),
(1660, '057', 'Honey', 1, 21),
(1661, '058', 'Chilchotla', 1, 21),
(1662, '059', 'Chinantla', 1, 21),
(1663, '060', 'Domingo Arenas', 1, 21),
(1664, '061', 'Eloxochitlán', 1, 21),
(1665, '062', 'Epatlán', 1, 21),
(1666, '063', 'Esperanza', 1, 21),
(1667, '064', 'Francisco Z. Mena', 1, 21),
(1668, '065', 'General Felipe Ángeles', 1, 21),
(1669, '066', 'Guadalupe', 1, 21),
(1670, '067', 'Guadalupe Victoria', 1, 21),
(1671, '068', 'Hermenegildo Galeana', 1, 21),
(1672, '069', 'Huaquechula', 1, 21),
(1673, '070', 'Huatlatlauca', 1, 21),
(1674, '071', 'Huauchinango', 1, 21),
(1675, '072', 'Huehuetla', 1, 21),
(1676, '073', 'Huehuetlán el Chico', 1, 21),
(1677, '074', 'Huejotzingo', 1, 21),
(1678, '075', 'Hueyapan', 1, 21),
(1679, '076', 'Hueytamalco', 1, 21),
(1680, '077', 'Hueytlalpan', 1, 21),
(1681, '078', 'Huitzilan de Serdán', 1, 21),
(1682, '079', 'Huitziltepec', 1, 21),
(1683, '080', 'Atlequizayan', 1, 21),
(1684, '081', 'Ixcamilpa de Guerrero', 1, 21),
(1685, '082', 'Ixcaquixtla', 1, 21),
(1686, '083', 'Ixtacamaxtitlán', 1, 21),
(1687, '084', 'Ixtepec', 1, 21),
(1688, '085', 'Izúcar de Matamoros', 1, 21),
(1689, '086', 'Jalpan', 1, 21),
(1690, '087', 'Jolalpan', 1, 21),
(1691, '088', 'Jonotla', 1, 21),
(1692, '089', 'Jopala', 1, 21),
(1693, '090', 'Juan C. Bonilla', 1, 21),
(1694, '091', 'Juan Galindo', 1, 21),
(1695, '092', 'Juan N. Méndez', 1, 21),
(1696, '093', 'Lafragua', 1, 21),
(1697, '094', 'Libres', 1, 21),
(1698, '095', 'La Magdalena Tlatlauquitepec', 1, 21),
(1699, '096', 'Mazapiltepec de Juárez', 1, 21),
(1700, '097', 'Mixtla', 1, 21),
(1701, '098', 'Molcaxac', 1, 21),
(1702, '099', 'Cañada Morelos', 1, 21),
(1703, '100', 'Naupan', 1, 21),
(1704, '101', 'Nauzontla', 1, 21),
(1705, '102', 'Nealtican', 1, 21),
(1706, '103', 'Nicolás Bravo', 1, 21),
(1707, '104', 'Nopalucan', 1, 21),
(1708, '105', 'Ocotepec', 1, 21),
(1709, '106', 'Ocoyucan', 1, 21),
(1710, '107', 'Olintla', 1, 21),
(1711, '108', 'Oriental', 1, 21),
(1712, '109', 'Pahuatlán', 1, 21),
(1713, '110', 'Palmar de Bravo', 1, 21),
(1714, '111', 'Pantepec', 1, 21),
(1715, '112', 'Petlalcingo', 1, 21),
(1716, '113', 'Piaxtla', 1, 21),
(1717, '114', 'Puebla', 1, 21),
(1718, '115', 'Quecholac', 1, 21),
(1719, '116', 'Quimixtlán', 1, 21),
(1720, '117', 'Rafael Lara Grajales', 1, 21),
(1721, '118', 'Los Reyes de Juárez', 1, 21),
(1722, '119', 'San Andrés Cholula', 1, 21),
(1723, '120', 'San Antonio Cañada', 1, 21),
(1724, '121', 'San Diego la Mesa Tochimiltzingo', 1, 21),
(1725, '122', 'San Felipe Teotlalcingo', 1, 21),
(1726, '123', 'San Felipe Tepatlán', 1, 21),
(1727, '124', 'San Gabriel Chilac', 1, 21),
(1728, '125', 'San Gregorio Atzompa', 1, 21),
(1729, '126', 'San Jerónimo Tecuanipan', 1, 21),
(1730, '127', 'San Jerónimo Xayacatlán', 1, 21),
(1731, '128', 'San José Chiapa', 1, 21),
(1732, '129', 'San José Miahuatlán', 1, 21),
(1733, '130', 'San Juan Atenco', 1, 21),
(1734, '131', 'San Juan Atzompa', 1, 21),
(1735, '132', 'San Martín Texmelucan', 1, 21),
(1736, '133', 'San Martín Totoltepec', 1, 21),
(1737, '134', 'San Matías Tlalancaleca', 1, 21),
(1738, '135', 'San Miguel Ixitlán', 1, 21),
(1739, '136', 'San Miguel Xoxtla', 1, 21),
(1740, '137', 'San Nicolás Buenos Aires', 1, 21),
(1741, '138', 'San Nicolás de los Ranchos', 1, 21),
(1742, '139', 'San Pablo Anicano', 1, 21),
(1743, '140', 'San Pedro Cholula', 1, 21),
(1744, '141', 'San Pedro Yeloixtlahuaca', 1, 21),
(1745, '142', 'San Salvador el Seco', 1, 21),
(1746, '143', 'San Salvador el Verde', 1, 21),
(1747, '144', 'San Salvador Huixcolotla', 1, 21),
(1748, '145', 'San Sebastián Tlacotepec', 1, 21),
(1749, '146', 'Santa Catarina Tlaltempan', 1, 21),
(1750, '147', 'Santa Inés Ahuatempan', 1, 21),
(1751, '148', 'Santa Isabel Cholula', 1, 21),
(1752, '149', 'Santiago Miahuatlán', 1, 21),
(1753, '150', 'Huehuetlán el Grande', 1, 21),
(1754, '151', 'Santo Tomás Hueyotlipan', 1, 21),
(1755, '152', 'Soltepec', 1, 21),
(1756, '153', 'Tecali de Herrera', 1, 21),
(1757, '154', 'Tecamachalco', 1, 21),
(1758, '155', 'Tecomatlán', 1, 21),
(1759, '156', 'Tehuacán', 1, 21),
(1760, '157', 'Tehuitzingo', 1, 21),
(1761, '158', 'Tenampulco', 1, 21),
(1762, '159', 'Teopantlán', 1, 21),
(1763, '160', 'Teotlalco', 1, 21),
(1764, '161', 'Tepanco de López', 1, 21),
(1765, '162', 'Tepango de Rodríguez', 1, 21),
(1766, '163', 'Tepatlaxco de Hidalgo', 1, 21),
(1767, '164', 'Tepeaca', 1, 21),
(1768, '165', 'Tepemaxalco', 1, 21),
(1769, '166', 'Tepeojuma', 1, 21),
(1770, '167', 'Tepetzintla', 1, 21),
(1771, '168', 'Tepexco', 1, 21),
(1772, '169', 'Tepexi de Rodríguez', 1, 21),
(1773, '170', 'Tepeyahualco', 1, 21),
(1774, '171', 'Tepeyahualco de Cuauhtémoc', 1, 21),
(1775, '172', 'Tetela de Ocampo', 1, 21),
(1776, '173', 'Teteles de Avila Castillo', 1, 21),
(1777, '174', 'Teziutlán', 1, 21),
(1778, '175', 'Tianguismanalco', 1, 21),
(1779, '176', 'Tilapa', 1, 21),
(1780, '177', 'Tlacotepec de Benito Juárez', 1, 21),
(1781, '178', 'Tlacuilotepec', 1, 21),
(1782, '179', 'Tlachichuca', 1, 21),
(1783, '180', 'Tlahuapan', 1, 21),
(1784, '181', 'Tlaltenango', 1, 21),
(1785, '182', 'Tlanepantla', 1, 21),
(1786, '183', 'Tlaola', 1, 21),
(1787, '184', 'Tlapacoya', 1, 21),
(1788, '185', 'Tlapanalá', 1, 21),
(1789, '186', 'Tlatlauquitepec', 1, 21),
(1790, '187', 'Tlaxco', 1, 21),
(1791, '188', 'Tochimilco', 1, 21),
(1792, '189', 'Tochtepec', 1, 21),
(1793, '190', 'Totoltepec de Guerrero', 1, 21),
(1794, '191', 'Tulcingo', 1, 21),
(1795, '192', 'Tuzamapan de Galeana', 1, 21),
(1796, '193', 'Tzicatlacoyan', 1, 21),
(1797, '194', 'Venustiano Carranza', 1, 21),
(1798, '195', 'Vicente Guerrero', 1, 21),
(1799, '196', 'Xayacatlán de Bravo', 1, 21),
(1800, '197', 'Xicotepec', 1, 21),
(1801, '198', 'Xicotlán', 1, 21),
(1802, '199', 'Xiutetelco', 1, 21),
(1803, '200', 'Xochiapulco', 1, 21),
(1804, '201', 'Xochiltepec', 1, 21),
(1805, '202', 'Xochitlán de Vicente Suárez', 1, 21),
(1806, '203', 'Xochitlán Todos Santos', 1, 21),
(1807, '204', 'Yaonáhuac', 1, 21),
(1808, '205', 'Yehualtepec', 1, 21),
(1809, '206', 'Zacapala', 1, 21),
(1810, '207', 'Zacapoaxtla', 1, 21),
(1811, '208', 'Zacatlán', 1, 21),
(1812, '209', 'Zapotitlán', 1, 21),
(1813, '210', 'Zapotitlán de Méndez', 1, 21),
(1814, '211', 'Zaragoza', 1, 21),
(1815, '212', 'Zautla', 1, 21),
(1816, '213', 'Zihuateutla', 1, 21),
(1817, '214', 'Zinacatepec', 1, 21),
(1818, '215', 'Zongozotla', 1, 21),
(1819, '216', 'Zoquiapan', 1, 21),
(1820, '217', 'Zoquitlán', 1, 21),
(1821, '001', 'Amealco de Bonfil', 1, 22),
(1822, '002', 'Pinal de Amoles', 1, 22),
(1823, '003', 'Arroyo Seco', 1, 22),
(1824, '004', 'Cadereyta de Montes', 1, 22),
(1825, '005', 'Colón', 1, 22),
(1826, '006', 'Corregidora', 1, 22),
(1827, '007', 'Ezequiel Montes', 1, 22),
(1828, '008', 'Huimilpan', 1, 22),
(1829, '009', 'Jalpan de Serra', 1, 22),
(1830, '010', 'Landa de Matamoros', 1, 22),
(1831, '011', 'El Marqués', 1, 22),
(1832, '012', 'Pedro Escobedo', 1, 22),
(1833, '013', 'Peñamiller', 1, 22),
(1834, '014', 'Querétaro', 1, 22),
(1835, '015', 'San Joaquín', 1, 22),
(1836, '016', 'San Juan del Río', 1, 22),
(1837, '017', 'Tequisquiapan', 1, 22),
(1838, '018', 'Tolimán', 1, 22),
(1839, '001', 'Cozumel', 1, 23),
(1840, '002', 'Felipe Carrillo Puerto', 1, 23),
(1841, '003', 'Isla Mujeres', 1, 23),
(1842, '004', 'Othón P. Blanco', 1, 23),
(1843, '005', 'Benito Juárez', 1, 23),
(1844, '006', 'José María Morelos', 1, 23),
(1845, '007', 'Lázaro Cárdenas', 1, 23),
(1846, '008', 'Solidaridad', 1, 23),
(1847, '009', 'Tulum', 1, 23),
(1848, '010', 'Bacalar', 1, 23),
(1849, '001', 'Ahualulco', 1, 24),
(1850, '002', 'Alaquines', 1, 24),
(1851, '003', 'Aquismón', 1, 24),
(1852, '004', 'Armadillo de los Infante', 1, 24),
(1853, '005', 'Cárdenas', 1, 24),
(1854, '006', 'Catorce', 1, 24),
(1855, '007', 'Cedral', 1, 24),
(1856, '008', 'Cerritos', 1, 24),
(1857, '009', 'Cerro de San Pedro', 1, 24),
(1858, '010', 'Ciudad del Maíz', 1, 24),
(1859, '011', 'Ciudad Fernández', 1, 24),
(1860, '012', 'Tancanhuitz', 1, 24),
(1861, '013', 'Ciudad Valles', 1, 24),
(1862, '014', 'Coxcatlán', 1, 24),
(1863, '015', 'Charcas', 1, 24),
(1864, '016', 'Ebano', 1, 24),
(1865, '017', 'Guadalcázar', 1, 24),
(1866, '018', 'Huehuetlán', 1, 24),
(1867, '019', 'Lagunillas', 1, 24),
(1868, '020', 'Matehuala', 1, 24),
(1869, '021', 'Mexquitic de Carmona', 1, 24),
(1870, '022', 'Moctezuma', 1, 24),
(1871, '023', 'Rayón', 1, 24),
(1872, '024', 'Rioverde', 1, 24),
(1873, '025', 'Salinas', 1, 24),
(1874, '026', 'San Antonio', 1, 24),
(1875, '027', 'San Ciro de Acosta', 1, 24),
(1876, '028', 'San Luis Potosí', 1, 24),
(1877, '029', 'San Martín Chalchicuautla', 1, 24),
(1878, '030', 'San Nicolás Tolentino', 1, 24),
(1879, '031', 'Santa Catarina', 1, 24),
(1880, '032', 'Santa María del Río', 1, 24),
(1881, '033', 'Santo Domingo', 1, 24),
(1882, '034', 'San Vicente Tancuayalab', 1, 24),
(1883, '035', 'Soledad de Graciano Sánchez', 1, 24),
(1884, '036', 'Tamasopo', 1, 24),
(1885, '037', 'Tamazunchale', 1, 24),
(1886, '038', 'Tampacán', 1, 24),
(1887, '039', 'Tampamolón Corona', 1, 24),
(1888, '040', 'Tamuín', 1, 24),
(1889, '041', 'Tanlajás', 1, 24),
(1890, '042', 'Tanquián de Escobedo', 1, 24),
(1891, '043', 'Tierra Nueva', 1, 24),
(1892, '044', 'Vanegas', 1, 24),
(1893, '045', 'Venado', 1, 24),
(1894, '046', 'Villa de Arriaga', 1, 24),
(1895, '047', 'Villa de Guadalupe', 1, 24),
(1896, '048', 'Villa de la Paz', 1, 24),
(1897, '049', 'Villa de Ramos', 1, 24),
(1898, '050', 'Villa de Reyes', 1, 24),
(1899, '051', 'Villa Hidalgo', 1, 24),
(1900, '052', 'Villa Juárez', 1, 24),
(1901, '053', 'Axtla de Terrazas', 1, 24),
(1902, '054', 'Xilitla', 1, 24),
(1903, '055', 'Zaragoza', 1, 24),
(1904, '056', 'Villa de Arista', 1, 24),
(1905, '057', 'Matlapa', 1, 24),
(1906, '058', 'El Naranjo', 1, 24),
(1907, '001', 'Ahome', 1, 25),
(1908, '002', 'Angostura', 1, 25),
(1909, '003', 'Badiraguato', 1, 25),
(1910, '004', 'Concordia', 1, 25),
(1911, '005', 'Cosalá', 1, 25),
(1912, '006', 'Culiacán', 1, 25),
(1913, '007', 'Choix', 1, 25),
(1914, '008', 'Elota', 1, 25),
(1915, '009', 'Escuinapa', 1, 25),
(1916, '010', 'El Fuerte', 1, 25),
(1917, '011', 'Guasave', 1, 25),
(1918, '012', 'Mazatlán', 1, 25),
(1919, '013', 'Mocorito', 1, 25),
(1920, '014', 'Rosario', 1, 25),
(1921, '015', 'Salvador Alvarado', 1, 25),
(1922, '016', 'San Ignacio', 1, 25),
(1923, '017', 'Sinaloa', 1, 25),
(1924, '018', 'Navolato', 1, 25),
(1925, '001', 'Aconchi', 1, 26),
(1926, '002', 'Agua Prieta', 1, 26),
(1927, '003', 'Alamos', 1, 26),
(1928, '004', 'Altar', 1, 26),
(1929, '005', 'Arivechi', 1, 26),
(1930, '006', 'Arizpe', 1, 26),
(1931, '007', 'Atil', 1, 26),
(1932, '008', 'Bacadéhuachi', 1, 26),
(1933, '009', 'Bacanora', 1, 26),
(1934, '010', 'Bacerac', 1, 26),
(1935, '011', 'Bacoachi', 1, 26),
(1936, '012', 'Bácum', 1, 26),
(1937, '013', 'Banámichi', 1, 26),
(1938, '014', 'Baviácora', 1, 26),
(1939, '015', 'Bavispe', 1, 26),
(1940, '016', 'Benjamín Hill', 1, 26),
(1941, '017', 'Caborca', 1, 26),
(1942, '018', 'Cajeme', 1, 26),
(1943, '019', 'Cananea', 1, 26),
(1944, '020', 'Carbó', 1, 26),
(1945, '021', 'La Colorada', 1, 26),
(1946, '022', 'Cucurpe', 1, 26),
(1947, '023', 'Cumpas', 1, 26),
(1948, '024', 'Divisaderos', 1, 26),
(1949, '025', 'Empalme', 1, 26),
(1950, '026', 'Etchojoa', 1, 26),
(1951, '027', 'Fronteras', 1, 26),
(1952, '028', 'Granados', 1, 26),
(1953, '029', 'Guaymas', 1, 26),
(1954, '030', 'Hermosillo', 1, 26),
(1955, '031', 'Huachinera', 1, 26),
(1956, '032', 'Huásabas', 1, 26),
(1957, '033', 'Huatabampo', 1, 26),
(1958, '034', 'Huépac', 1, 26),
(1959, '035', 'Imuris', 1, 26),
(1960, '036', 'Magdalena', 1, 26),
(1961, '037', 'Mazatán', 1, 26),
(1962, '038', 'Moctezuma', 1, 26),
(1963, '039', 'Naco', 1, 26),
(1964, '040', 'Nácori Chico', 1, 26),
(1965, '041', 'Nacozari de García', 1, 26),
(1966, '042', 'Navojoa', 1, 26),
(1967, '043', 'Nogales', 1, 26),
(1968, '044', 'Onavas', 1, 26),
(1969, '045', 'Opodepe', 1, 26),
(1970, '046', 'Oquitoa', 1, 26),
(1971, '047', 'Pitiquito', 1, 26),
(1972, '048', 'Puerto Peñasco', 1, 26),
(1973, '049', 'Quiriego', 1, 26),
(1974, '050', 'Rayón', 1, 26),
(1975, '051', 'Rosario', 1, 26),
(1976, '052', 'Sahuaripa', 1, 26),
(1977, '053', 'San Felipe de Jesús', 1, 26),
(1978, '054', 'San Javier', 1, 26),
(1979, '055', 'San Luis Río Colorado', 1, 26),
(1980, '056', 'San Miguel de Horcasitas', 1, 26),
(1981, '057', 'San Pedro de la Cueva', 1, 26),
(1982, '058', 'Santa Ana', 1, 26),
(1983, '059', 'Santa Cruz', 1, 26),
(1984, '060', 'Sáric', 1, 26),
(1985, '061', 'Soyopa', 1, 26),
(1986, '062', 'Suaqui Grande', 1, 26),
(1987, '063', 'Tepache', 1, 26),
(1988, '064', 'Trincheras', 1, 26),
(1989, '065', 'Tubutama', 1, 26),
(1990, '066', 'Ures', 1, 26),
(1991, '067', 'Villa Hidalgo', 1, 26),
(1992, '068', 'Villa Pesqueira', 1, 26),
(1993, '069', 'Yécora', 1, 26),
(1994, '070', 'General Plutarco Elías Calles', 1, 26),
(1995, '071', 'Benito Juárez', 1, 26),
(1996, '072', 'San Ignacio Río Muerto', 1, 26),
(1997, '001', 'Balancán', 1, 27),
(1998, '002', 'Cárdenas', 1, 27),
(1999, '003', 'Centla', 1, 27),
(2000, '004', 'Centro', 1, 27),
(2001, '005', 'Comalcalco', 1, 27),
(2002, '006', 'Cunduacán', 1, 27),
(2003, '007', 'Emiliano Zapata', 1, 27),
(2004, '008', 'Huimanguillo', 1, 27),
(2005, '009', 'Jalapa', 1, 27),
(2006, '010', 'Jalpa de Méndez', 1, 27),
(2007, '011', 'Jonuta', 1, 27),
(2008, '012', 'Macuspana', 1, 27),
(2009, '013', 'Nacajuca', 1, 27),
(2010, '014', 'Paraíso', 1, 27),
(2011, '015', 'Tacotalpa', 1, 27),
(2012, '016', 'Teapa', 1, 27),
(2013, '017', 'Tenosique', 1, 27),
(2014, '001', 'Abasolo', 1, 28),
(2015, '002', 'Aldama', 1, 28),
(2016, '003', 'Altamira', 1, 28),
(2017, '004', 'Antiguo Morelos', 1, 28),
(2018, '005', 'Burgos', 1, 28),
(2019, '006', 'Bustamante', 1, 28),
(2020, '007', 'Camargo', 1, 28),
(2021, '008', 'Casas', 1, 28),
(2022, '009', 'Ciudad Madero', 1, 28),
(2023, '010', 'Cruillas', 1, 28),
(2024, '011', 'Gómez Farías', 1, 28),
(2025, '012', 'González', 1, 28),
(2026, '013', 'Güémez', 1, 28),
(2027, '014', 'Guerrero', 1, 28),
(2028, '015', 'Gustavo Díaz Ordaz', 1, 28),
(2029, '016', 'Hidalgo', 1, 28),
(2030, '017', 'Jaumave', 1, 28),
(2031, '018', 'Jiménez', 1, 28),
(2032, '019', 'Llera', 1, 28),
(2033, '020', 'Mainero', 1, 28),
(2034, '021', 'El Mante', 1, 28),
(2035, '022', 'Matamoros', 1, 28),
(2036, '023', 'Méndez', 1, 28),
(2037, '024', 'Mier', 1, 28),
(2038, '025', 'Miguel Alemán', 1, 28),
(2039, '026', 'Miquihuana', 1, 28),
(2040, '027', 'Nuevo Laredo', 1, 28),
(2041, '028', 'Nuevo Morelos', 1, 28),
(2042, '029', 'Ocampo', 1, 28),
(2043, '030', 'Padilla', 1, 28),
(2044, '031', 'Palmillas', 1, 28),
(2045, '032', 'Reynosa', 1, 28),
(2046, '033', 'Río Bravo', 1, 28),
(2047, '034', 'San Carlos', 1, 28),
(2048, '035', 'San Fernando', 1, 28),
(2049, '036', 'San Nicolás', 1, 28),
(2050, '037', 'Soto la Marina', 1, 28),
(2051, '038', 'Tampico', 1, 28),
(2052, '039', 'Tula', 1, 28),
(2053, '040', 'Valle Hermoso', 1, 28),
(2054, '041', 'Victoria', 1, 28),
(2055, '042', 'Villagrán', 1, 28),
(2056, '043', 'Xicoténcatl', 1, 28),
(2057, '001', 'Amaxac de Guerrero', 1, 29),
(2058, '002', 'Apetatitlán de Antonio Carvajal', 1, 29),
(2059, '003', 'Atlangatepec', 1, 29),
(2060, '004', 'Atltzayanca', 1, 29),
(2061, '005', 'Apizaco', 1, 29),
(2062, '006', 'Calpulalpan', 1, 29),
(2063, '007', 'El Carmen Tequexquitla', 1, 29),
(2064, '008', 'Cuapiaxtla', 1, 29),
(2065, '009', 'Cuaxomulco', 1, 29),
(2066, '010', 'Chiautempan', 1, 29),
(2067, '011', 'Muñoz de Domingo Arenas', 1, 29),
(2068, '012', 'Españita', 1, 29),
(2069, '013', 'Huamantla', 1, 29),
(2070, '014', 'Hueyotlipan', 1, 29),
(2071, '015', 'Ixtacuixtla de Mariano Matamoros', 1, 29),
(2072, '016', 'Ixtenco', 1, 29),
(2073, '017', 'Mazatecochco de José María Morelos', 1, 29),
(2074, '018', 'Contla de Juan Cuamatzi', 1, 29),
(2075, '019', 'Tepetitla de Lardizábal', 1, 29),
(2076, '020', 'Sanctórum de Lázaro Cárdenas', 1, 29),
(2077, '021', 'Nanacamilpa de Mariano Arista', 1, 29),
(2078, '022', 'Acuamanala de Miguel Hidalgo', 1, 29),
(2079, '023', 'Natívitas', 1, 29),
(2080, '024', 'Panotla', 1, 29),
(2081, '025', 'San Pablo del Monte', 1, 29),
(2082, '026', 'Santa Cruz Tlaxcala', 1, 29),
(2083, '027', 'Tenancingo', 1, 29),
(2084, '028', 'Teolocholco', 1, 29),
(2085, '029', 'Tepeyanco', 1, 29),
(2086, '030', 'Terrenate', 1, 29),
(2087, '031', 'Tetla de la Solidaridad', 1, 29),
(2088, '032', 'Tetlatlahuca', 1, 29),
(2089, '033', 'Tlaxcala', 1, 29),
(2090, '034', 'Tlaxco', 1, 29),
(2091, '035', 'Tocatlán', 1, 29),
(2092, '036', 'Totolac', 1, 29),
(2093, '037', 'Ziltlaltépec de Trinidad Sánchez Santos', 1, 29),
(2094, '038', 'Tzompantepec', 1, 29),
(2095, '039', 'Xaloztoc', 1, 29),
(2096, '040', 'Xaltocan', 1, 29),
(2097, '041', 'Papalotla de Xicohténcatl', 1, 29),
(2098, '042', 'Xicohtzinco', 1, 29),
(2099, '043', 'Yauhquemehcan', 1, 29),
(2100, '044', 'Zacatelco', 1, 29),
(2101, '045', 'Benito Juárez', 1, 29),
(2102, '046', 'Emiliano Zapata', 1, 29),
(2103, '047', 'Lázaro Cárdenas', 1, 29),
(2104, '048', 'La Magdalena Tlaltelulco', 1, 29),
(2105, '049', 'San Damián Texóloc', 1, 29),
(2106, '050', 'San Francisco Tetlanohcan', 1, 29),
(2107, '051', 'San Jerónimo Zacualpan', 1, 29),
(2108, '052', 'San José Teacalco', 1, 29),
(2109, '053', 'San Juan Huactzinco', 1, 29),
(2110, '054', 'San Lorenzo Axocomanitla', 1, 29),
(2111, '055', 'San Lucas Tecopilco', 1, 29),
(2112, '056', 'Santa Ana Nopalucan', 1, 29),
(2113, '057', 'Santa Apolonia Teacalco', 1, 29),
(2114, '058', 'Santa Catarina Ayometla', 1, 29),
(2115, '059', 'Santa Cruz Quilehtla', 1, 29),
(2116, '060', 'Santa Isabel Xiloxoxtla', 1, 29),
(2117, '001', 'Acajete', 1, 30),
(2118, '002', 'Acatlán', 1, 30),
(2119, '003', 'Acayucan', 1, 30),
(2120, '004', 'Actopan', 1, 30),
(2121, '005', 'Acula', 1, 30),
(2122, '006', 'Acultzingo', 1, 30),
(2123, '007', 'Camarón de Tejeda', 1, 30),
(2124, '008', 'Alpatláhuac', 1, 30),
(2125, '009', 'Alto Lucero de Gutiérrez Barrios', 1, 30),
(2126, '010', 'Altotonga', 1, 30),
(2127, '011', 'Alvarado', 1, 30),
(2128, '012', 'Amatitlán', 1, 30),
(2129, '013', 'Naranjos Amatlán', 1, 30),
(2130, '014', 'Amatlán de los Reyes', 1, 30),
(2131, '015', 'Angel R. Cabada', 1, 30),
(2132, '016', 'La Antigua', 1, 30),
(2133, '017', 'Apazapan', 1, 30),
(2134, '018', 'Aquila', 1, 30),
(2135, '019', 'Astacinga', 1, 30),
(2136, '020', 'Atlahuilco', 1, 30),
(2137, '021', 'Atoyac', 1, 30),
(2138, '022', 'Atzacan', 1, 30),
(2139, '023', 'Atzalan', 1, 30),
(2140, '024', 'Tlaltetela', 1, 30),
(2141, '025', 'Ayahualulco', 1, 30),
(2142, '026', 'Banderilla', 1, 30),
(2143, '027', 'Benito Juárez', 1, 30),
(2144, '028', 'Boca del Río', 1, 30),
(2145, '029', 'Calcahualco', 1, 30),
(2146, '030', 'Camerino Z. Mendoza', 1, 30),
(2147, '031', 'Carrillo Puerto', 1, 30),
(2148, '032', 'Catemaco', 1, 30),
(2149, '033', 'Cazones de Herrera', 1, 30),
(2150, '034', 'Cerro Azul', 1, 30),
(2151, '035', 'Citlaltépetl', 1, 30),
(2152, '036', 'Coacoatzintla', 1, 30),
(2153, '037', 'Coahuitlán', 1, 30),
(2154, '038', 'Coatepec', 1, 30),
(2155, '039', 'Coatzacoalcos', 1, 30),
(2156, '040', 'Coatzintla', 1, 30),
(2157, '041', 'Coetzala', 1, 30),
(2158, '042', 'Colipa', 1, 30),
(2159, '043', 'Comapa', 1, 30),
(2160, '044', 'Córdoba', 1, 30),
(2161, '045', 'Cosamaloapan de Carpio', 1, 30),
(2162, '046', 'Cosautlán de Carvajal', 1, 30),
(2163, '047', 'Coscomatepec', 1, 30),
(2164, '048', 'Cosoleacaque', 1, 30),
(2165, '049', 'Cotaxtla', 1, 30),
(2166, '050', 'Coxquihui', 1, 30),
(2167, '051', 'Coyutla', 1, 30),
(2168, '052', 'Cuichapa', 1, 30),
(2169, '053', 'Cuitláhuac', 1, 30),
(2170, '054', 'Chacaltianguis', 1, 30),
(2171, '055', 'Chalma', 1, 30),
(2172, '056', 'Chiconamel', 1, 30),
(2173, '057', 'Chiconquiaco', 1, 30),
(2174, '058', 'Chicontepec', 1, 30),
(2175, '059', 'Chinameca', 1, 30),
(2176, '060', 'Chinampa de Gorostiza', 1, 30),
(2177, '061', 'Las Choapas', 1, 30),
(2178, '062', 'Chocamán', 1, 30),
(2179, '063', 'Chontla', 1, 30),
(2180, '064', 'Chumatlán', 1, 30),
(2181, '065', 'Emiliano Zapata', 1, 30),
(2182, '066', 'Espinal', 1, 30),
(2183, '067', 'Filomeno Mata', 1, 30),
(2184, '068', 'Fortín', 1, 30),
(2185, '069', 'Gutiérrez Zamora', 1, 30),
(2186, '070', 'Hidalgotitlán', 1, 30),
(2187, '071', 'Huatusco', 1, 30),
(2188, '072', 'Huayacocotla', 1, 30),
(2189, '073', 'Hueyapan de Ocampo', 1, 30),
(2190, '074', 'Huiloapan de Cuauhtémoc', 1, 30),
(2191, '075', 'Ignacio de la Llave', 1, 30),
(2192, '076', 'Ilamatlán', 1, 30),
(2193, '077', 'Isla', 1, 30),
(2194, '078', 'Ixcatepec', 1, 30),
(2195, '079', 'Ixhuacán de los Reyes', 1, 30),
(2196, '080', 'Ixhuatlán del Café', 1, 30),
(2197, '081', 'Ixhuatlancillo', 1, 30),
(2198, '082', 'Ixhuatlán del Sureste', 1, 30),
(2199, '083', 'Ixhuatlán de Madero', 1, 30),
(2200, '084', 'Ixmatlahuacan', 1, 30),
(2201, '085', 'Ixtaczoquitlán', 1, 30),
(2202, '086', 'Jalacingo', 1, 30),
(2203, '087', 'Xalapa', 1, 30),
(2204, '088', 'Jalcomulco', 1, 30),
(2205, '089', 'Jáltipan', 1, 30),
(2206, '090', 'Jamapa', 1, 30),
(2207, '091', 'Jesús Carranza', 1, 30),
(2208, '092', 'Xico', 1, 30),
(2209, '093', 'Jilotepec', 1, 30),
(2210, '094', 'Juan Rodríguez Clara', 1, 30),
(2211, '095', 'Juchique de Ferrer', 1, 30),
(2212, '096', 'Landero y Coss', 1, 30),
(2213, '097', 'Lerdo de Tejada', 1, 30),
(2214, '098', 'Magdalena', 1, 30),
(2215, '099', 'Maltrata', 1, 30),
(2216, '100', 'Manlio Fabio Altamirano', 1, 30),
(2217, '101', 'Mariano Escobedo', 1, 30),
(2218, '102', 'Martínez de la Torre', 1, 30),
(2219, '103', 'Mecatlán', 1, 30),
(2220, '104', 'Mecayapan', 1, 30),
(2221, '105', 'Medellín de Bravo', 1, 30),
(2222, '106', 'Miahuatlán', 1, 30),
(2223, '107', 'Las Minas', 1, 30),
(2224, '108', 'Minatitlán', 1, 30),
(2225, '109', 'Misantla', 1, 30),
(2226, '110', 'Mixtla de Altamirano', 1, 30),
(2227, '111', 'Moloacán', 1, 30),
(2228, '112', 'Naolinco', 1, 30),
(2229, '113', 'Naranjal', 1, 30),
(2230, '114', 'Nautla', 1, 30),
(2231, '115', 'Nogales', 1, 30),
(2232, '116', 'Oluta', 1, 30),
(2233, '117', 'Omealca', 1, 30),
(2234, '118', 'Orizaba', 1, 30),
(2235, '119', 'Otatitlán', 1, 30),
(2236, '120', 'Oteapan', 1, 30),
(2237, '121', 'Ozuluama de Mascareñas', 1, 30),
(2238, '122', 'Pajapan', 1, 30),
(2239, '123', 'Pánuco', 1, 30),
(2240, '124', 'Papantla', 1, 30),
(2241, '125', 'Paso del Macho', 1, 30),
(2242, '126', 'Paso de Ovejas', 1, 30),
(2243, '127', 'La Perla', 1, 30),
(2244, '128', 'Perote', 1, 30),
(2245, '129', 'Platón Sánchez', 1, 30),
(2246, '130', 'Playa Vicente', 1, 30),
(2247, '131', 'Poza Rica de Hidalgo', 1, 30),
(2248, '132', 'Las Vigas de Ramírez', 1, 30),
(2249, '133', 'Pueblo Viejo', 1, 30),
(2250, '134', 'Puente Nacional', 1, 30),
(2251, '135', 'Rafael Delgado', 1, 30),
(2252, '136', 'Rafael Lucio', 1, 30),
(2253, '137', 'Los Reyes', 1, 30),
(2254, '138', 'Río Blanco', 1, 30),
(2255, '139', 'Saltabarranca', 1, 30),
(2256, '140', 'San Andrés Tenejapan', 1, 30),
(2257, '141', 'San Andrés Tuxtla', 1, 30),
(2258, '142', 'San Juan Evangelista', 1, 30),
(2259, '143', 'Santiago Tuxtla', 1, 30),
(2260, '144', 'Sayula de Alemán', 1, 30),
(2261, '145', 'Soconusco', 1, 30),
(2262, '146', 'Sochiapa', 1, 30),
(2263, '147', 'Soledad Atzompa', 1, 30),
(2264, '148', 'Soledad de Doblado', 1, 30),
(2265, '149', 'Soteapan', 1, 30),
(2266, '150', 'Tamalín', 1, 30),
(2267, '151', 'Tamiahua', 1, 30),
(2268, '152', 'Tampico Alto', 1, 30),
(2269, '153', 'Tancoco', 1, 30),
(2270, '154', 'Tantima', 1, 30),
(2271, '155', 'Tantoyuca', 1, 30),
(2272, '156', 'Tatatila', 1, 30),
(2273, '157', 'Castillo de Teayo', 1, 30),
(2274, '158', 'Tecolutla', 1, 30),
(2275, '159', 'Tehuipango', 1, 30),
(2276, '160', 'Álamo Temapache', 1, 30),
(2277, '161', 'Tempoal', 1, 30),
(2278, '162', 'Tenampa', 1, 30),
(2279, '163', 'Tenochtitlán', 1, 30),
(2280, '164', 'Teocelo', 1, 30),
(2281, '165', 'Tepatlaxco', 1, 30),
(2282, '166', 'Tepetlán', 1, 30),
(2283, '167', 'Tepetzintla', 1, 30),
(2284, '168', 'Tequila', 1, 30),
(2285, '169', 'José Azueta', 1, 30),
(2286, '170', 'Texcatepec', 1, 30),
(2287, '171', 'Texhuacán', 1, 30),
(2288, '172', 'Texistepec', 1, 30),
(2289, '173', 'Tezonapa', 1, 30),
(2290, '174', 'Tierra Blanca', 1, 30),
(2291, '175', 'Tihuatlán', 1, 30),
(2292, '176', 'Tlacojalpan', 1, 30),
(2293, '177', 'Tlacolulan', 1, 30),
(2294, '178', 'Tlacotalpan', 1, 30),
(2295, '179', 'Tlacotepec de Mejía', 1, 30),
(2296, '180', 'Tlachichilco', 1, 30),
(2297, '181', 'Tlalixcoyan', 1, 30),
(2298, '182', 'Tlalnelhuayocan', 1, 30),
(2299, '183', 'Tlapacoyan', 1, 30),
(2300, '184', 'Tlaquilpa', 1, 30),
(2301, '185', 'Tlilapan', 1, 30),
(2302, '186', 'Tomatlán', 1, 30),
(2303, '187', 'Tonayán', 1, 30),
(2304, '188', 'Totutla', 1, 30),
(2305, '189', 'Tuxpan', 1, 30),
(2306, '190', 'Tuxtilla', 1, 30),
(2307, '191', 'Ursulo Galván', 1, 30),
(2308, '192', 'Vega de Alatorre', 1, 30),
(2309, '193', 'Veracruz', 1, 30),
(2310, '194', 'Villa Aldama', 1, 30),
(2311, '195', 'Xoxocotla', 1, 30),
(2312, '196', 'Yanga', 1, 30),
(2313, '197', 'Yecuatla', 1, 30),
(2314, '198', 'Zacualpan', 1, 30),
(2315, '199', 'Zaragoza', 1, 30),
(2316, '200', 'Zentla', 1, 30),
(2317, '201', 'Zongolica', 1, 30),
(2318, '202', 'Zontecomatlán de López y Fuentes', 1, 30),
(2319, '203', 'Zozocolco de Hidalgo', 1, 30),
(2320, '204', 'Agua Dulce', 1, 30),
(2321, '205', 'El Higo', 1, 30),
(2322, '206', 'Nanchital de Lázaro Cárdenas del Río', 1, 30),
(2323, '207', 'Tres Valles', 1, 30),
(2324, '208', 'Carlos A. Carrillo', 1, 30),
(2325, '209', 'Tatahuicapan de Juárez', 1, 30),
(2326, '210', 'Uxpanapa', 1, 30),
(2327, '211', 'San Rafael', 1, 30),
(2328, '212', 'Santiago Sochiapan', 1, 30),
(2329, '001', 'Abalá', 1, 31),
(2330, '002', 'Acanceh', 1, 31),
(2331, '003', 'Akil', 1, 31),
(2332, '004', 'Baca', 1, 31),
(2333, '005', 'Bokobá', 1, 31),
(2334, '006', 'Buctzotz', 1, 31),
(2335, '007', 'Cacalchén', 1, 31),
(2336, '008', 'Calotmul', 1, 31),
(2337, '009', 'Cansahcab', 1, 31),
(2338, '010', 'Cantamayec', 1, 31),
(2339, '011', 'Celestún', 1, 31),
(2340, '012', 'Cenotillo', 1, 31),
(2341, '013', 'Conkal', 1, 31),
(2342, '014', 'Cuncunul', 1, 31),
(2343, '015', 'Cuzamá', 1, 31),
(2344, '016', 'Chacsinkín', 1, 31),
(2345, '017', 'Chankom', 1, 31),
(2346, '018', 'Chapab', 1, 31),
(2347, '019', 'Chemax', 1, 31),
(2348, '020', 'Chicxulub Pueblo', 1, 31),
(2349, '021', 'Chichimilá', 1, 31),
(2350, '022', 'Chikindzonot', 1, 31),
(2351, '023', 'Chocholá', 1, 31),
(2352, '024', 'Chumayel', 1, 31),
(2353, '025', 'Dzán', 1, 31),
(2354, '026', 'Dzemul', 1, 31),
(2355, '027', 'Dzidzantún', 1, 31),
(2356, '028', 'Dzilam de Bravo', 1, 31),
(2357, '029', 'Dzilam González', 1, 31),
(2358, '030', 'Dzitás', 1, 31),
(2359, '031', 'Dzoncauich', 1, 31),
(2360, '032', 'Espita', 1, 31),
(2361, '033', 'Halachó', 1, 31),
(2362, '034', 'Hocabá', 1, 31),
(2363, '035', 'Hoctún', 1, 31),
(2364, '036', 'Homún', 1, 31),
(2365, '037', 'Huhí', 1, 31),
(2366, '038', 'Hunucmá', 1, 31),
(2367, '039', 'Ixil', 1, 31),
(2368, '040', 'Izamal', 1, 31),
(2369, '041', 'Kanasín', 1, 31),
(2370, '042', 'Kantunil', 1, 31),
(2371, '043', 'Kaua', 1, 31),
(2372, '044', 'Kinchil', 1, 31),
(2373, '045', 'Kopomá', 1, 31),
(2374, '046', 'Mama', 1, 31),
(2375, '047', 'Maní', 1, 31),
(2376, '048', 'Maxcanú', 1, 31),
(2377, '049', 'Mayapán', 1, 31),
(2378, '050', 'Mérida', 1, 31),
(2379, '051', 'Mocochá', 1, 31),
(2380, '052', 'Motul', 1, 31),
(2381, '053', 'Muna', 1, 31),
(2382, '054', 'Muxupip', 1, 31),
(2383, '055', 'Opichén', 1, 31),
(2384, '056', 'Oxkutzcab', 1, 31),
(2385, '057', 'Panabá', 1, 31),
(2386, '058', 'Peto', 1, 31),
(2387, '059', 'Progreso', 1, 31),
(2388, '060', 'Quintana Roo', 1, 31),
(2389, '061', 'Río Lagartos', 1, 31),
(2390, '062', 'Sacalum', 1, 31),
(2391, '063', 'Samahil', 1, 31),
(2392, '064', 'Sanahcat', 1, 31),
(2393, '065', 'San Felipe', 1, 31),
(2394, '066', 'Santa Elena', 1, 31),
(2395, '067', 'Seyé', 1, 31),
(2396, '068', 'Sinanché', 1, 31),
(2397, '069', 'Sotuta', 1, 31),
(2398, '070', 'Sucilá', 1, 31),
(2399, '071', 'Sudzal', 1, 31),
(2400, '072', 'Suma', 1, 31),
(2401, '073', 'Tahdziú', 1, 31),
(2402, '074', 'Tahmek', 1, 31),
(2403, '075', 'Teabo', 1, 31),
(2404, '076', 'Tecoh', 1, 31),
(2405, '077', 'Tekal de Venegas', 1, 31),
(2406, '078', 'Tekantó', 1, 31),
(2407, '079', 'Tekax', 1, 31),
(2408, '080', 'Tekit', 1, 31),
(2409, '081', 'Tekom', 1, 31),
(2410, '082', 'Telchac Pueblo', 1, 31),
(2411, '083', 'Telchac Puerto', 1, 31),
(2412, '084', 'Temax', 1, 31),
(2413, '085', 'Temozón', 1, 31),
(2414, '086', 'Tepakán', 1, 31),
(2415, '087', 'Tetiz', 1, 31),
(2416, '088', 'Teya', 1, 31),
(2417, '089', 'Ticul', 1, 31),
(2418, '090', 'Timucuy', 1, 31),
(2419, '091', 'Tinum', 1, 31),
(2420, '092', 'Tixcacalcupul', 1, 31),
(2421, '093', 'Tixkokob', 1, 31),
(2422, '094', 'Tixmehuac', 1, 31),
(2423, '095', 'Tixpéhual', 1, 31),
(2424, '096', 'Tizimín', 1, 31),
(2425, '097', 'Tunkás', 1, 31),
(2426, '098', 'Tzucacab', 1, 31),
(2427, '099', 'Uayma', 1, 31),
(2428, '100', 'Ucú', 1, 31),
(2429, '101', 'Umán', 1, 31),
(2430, '102', 'Valladolid', 1, 31),
(2431, '103', 'Xocchel', 1, 31),
(2432, '104', 'Yaxcabá', 1, 31),
(2433, '105', 'Yaxkukul', 1, 31),
(2434, '106', 'Yobaín', 1, 31),
(2435, '001', 'Apozol', 1, 32),
(2436, '002', 'Apulco', 1, 32),
(2437, '003', 'Atolinga', 1, 32),
(2438, '004', 'Benito Juárez', 1, 32),
(2439, '005', 'Calera', 1, 32),
(2440, '006', 'Cañitas de Felipe Pescador', 1, 32),
(2441, '007', 'Concepción del Oro', 1, 32),
(2442, '008', 'Cuauhtémoc', 1, 32),
(2443, '009', 'Chalchihuites', 1, 32),
(2444, '010', 'Fresnillo', 1, 32),
(2445, '011', 'Trinidad García de la Cadena', 1, 32),
(2446, '012', 'Genaro Codina', 1, 32),
(2447, '013', 'General Enrique Estrada', 1, 32),
(2448, '014', 'General Francisco R. Murguía', 1, 32),
(2449, '015', 'El Plateado de Joaquín Amaro', 1, 32),
(2450, '016', 'General Pánfilo Natera', 1, 32),
(2451, '017', 'Guadalupe', 1, 32),
(2452, '018', 'Huanusco', 1, 32),
(2453, '019', 'Jalpa', 1, 32),
(2454, '020', 'Jerez', 1, 32),
(2455, '021', 'Jiménez del Teul', 1, 32),
(2456, '022', 'Juan Aldama', 1, 32),
(2457, '023', 'Juchipila', 1, 32),
(2458, '024', 'Loreto', 1, 32),
(2459, '025', 'Luis Moya', 1, 32),
(2460, '026', 'Mazapil', 1, 32),
(2461, '027', 'Melchor Ocampo', 1, 32),
(2462, '028', 'Mezquital del Oro', 1, 32),
(2463, '029', 'Miguel Auza', 1, 32),
(2464, '030', 'Momax', 1, 32),
(2465, '031', 'Monte Escobedo', 1, 32),
(2466, '032', 'Morelos', 1, 32),
(2467, '033', 'Moyahua de Estrada', 1, 32),
(2468, '034', 'Nochistlán de Mejía', 1, 32),
(2469, '035', 'Noria de Ángeles', 1, 32),
(2470, '036', 'Ojocaliente', 1, 32),
(2471, '037', 'Pánuco', 1, 32),
(2472, '038', 'Pinos', 1, 32),
(2473, '039', 'Río Grande', 1, 32),
(2474, '040', 'Sain Alto', 1, 32),
(2475, '041', 'El Salvador', 1, 32),
(2476, '042', 'Sombrerete', 1, 32),
(2477, '043', 'Susticacán', 1, 32),
(2478, '044', 'Tabasco', 1, 32),
(2479, '045', 'Tepechitlán', 1, 32),
(2480, '046', 'Tepetongo', 1, 32),
(2481, '047', 'Teúl de González Ortega', 1, 32),
(2482, '048', 'Tlaltenango de Sánchez Román', 1, 32),
(2483, '049', 'Valparaíso', 1, 32),
(2484, '050', 'Vetagrande', 1, 32),
(2485, '051', 'Villa de Cos', 1, 32),
(2486, '052', 'Villa García', 1, 32),
(2487, '053', 'Villa González Ortega', 1, 32),
(2488, '054', 'Villa Hidalgo', 1, 32),
(2489, '055', 'Villanueva', 1, 32),
(2490, '056', 'Zacatecas', 1, 32),
(2491, '057', 'Trancoso', 1, 32),
(2492, '058', 'Santa María de la Paz', 1, 32);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_reporteinconformidad`
--

CREATE TABLE IF NOT EXISTS `coordinador_reporteinconformidad` (
`id_reporte` int(11) NOT NULL,
  `descripcion_estatus` varchar(200) DEFAULT NULL,
  `descripcion_inconformidad` varchar(500) NOT NULL,
  `fecha_reporte` date NOT NULL,
  `dia_rec` int(11) DEFAULT NULL,
  `freq_rec` int(11) DEFAULT NULL,
  `id_clinica_fk_id` int(11) NOT NULL,
  `id_pasante_fk_id` int(11) NOT NULL,
  `id_status_fk_id` int(11) NOT NULL,
  `id_tipo_inconformidad_FK_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `coordinador_reporteinconformidad`
--

INSERT INTO `coordinador_reporteinconformidad` (`id_reporte`, `descripcion_estatus`, `descripcion_inconformidad`, `fecha_reporte`, `dia_rec`, `freq_rec`, `id_clinica_fk_id`, `id_pasante_fk_id`, `id_status_fk_id`, `id_tipo_inconformidad_FK_id`) VALUES
(1, 'aqui ando', 'Problema con la clínica', '2018-02-19', 1, 1, 1, 1, 5, 1),
(2, 'Si', 'Esta feo', '2018-02-19', 1, 1, 1, 1, 4, 6),
(3, '', 'falta algo', '2018-02-19', NULL, NULL, 1, 96, 2, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coordinador_seguimiento`
--

CREATE TABLE IF NOT EXISTS `coordinador_seguimiento` (
`id` int(11) NOT NULL,
  `id_reporte_id` int(11) NOT NULL,
  `id_usuario_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
`id` int(11) NOT NULL,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
`id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(4, 'auth', 'group'),
(3, 'auth', 'permission'),
(2, 'auth', 'user'),
(7, 'clinica', 'clinica'),
(8, 'clinica', 'direccionclinica'),
(9, 'clinica', 'institucion'),
(33, 'contactos_pasante', 'contactospasante'),
(5, 'contenttypes', 'contenttype'),
(20, 'coordinador', 'asignacionclinicapasante'),
(18, 'coordinador', 'catalogoestatusinconformidad'),
(15, 'coordinador', 'catalogotipoinconformidad'),
(11, 'coordinador', 'comentariosinconformidad'),
(13, 'coordinador', 'coordinador'),
(14, 'coordinador', 'estado'),
(12, 'coordinador', 'fecha_registro'),
(19, 'coordinador', 'informacion'),
(17, 'coordinador', 'municipio'),
(16, 'coordinador', 'reporteinconformidad'),
(10, 'coordinador', 'seguimiento'),
(26, 'pasante', 'datosacademicos'),
(21, 'pasante', 'direccionpasante'),
(25, 'pasante', 'generacion'),
(24, 'pasante', 'pasante'),
(22, 'pasante', 'promocion'),
(23, 'pasante', 'telefono'),
(31, 'rol', 'asignacionrol'),
(30, 'rol', 'configuracionasignacion'),
(29, 'rol', 'rol'),
(32, 'rol', 'rotacion'),
(28, 'rol', 'tutor'),
(6, 'sessions', 'session'),
(27, 'usuario', 'usuario');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
`id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-05-10 02:34:11'),
(2, 'auth', '0001_initial', '2018-05-10 02:34:12'),
(3, 'admin', '0001_initial', '2018-05-10 02:34:13'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-05-10 02:34:13'),
(5, 'contenttypes', '0002_remove_content_type_name', '2018-05-10 02:34:13'),
(6, 'auth', '0002_alter_permission_name_max_length', '2018-05-10 02:34:13'),
(7, 'auth', '0003_alter_user_email_max_length', '2018-05-10 02:34:13'),
(8, 'auth', '0004_alter_user_username_opts', '2018-05-10 02:34:13'),
(9, 'auth', '0005_alter_user_last_login_null', '2018-05-10 02:34:13'),
(10, 'auth', '0006_require_contenttypes_0002', '2018-05-10 02:34:13'),
(11, 'auth', '0007_alter_validators_add_error_messages', '2018-05-10 02:34:13'),
(12, 'auth', '0008_alter_user_username_max_length', '2018-05-10 02:34:14'),
(13, 'auth', '0009_alter_user_last_name_max_length', '2018-05-10 02:34:14'),
(14, 'coordinador', '0001_initial', '2018-05-10 02:34:15'),
(15, 'clinica', '0001_initial', '2018-05-10 02:34:15'),
(16, 'clinica', '0002_auto_20180219_0553', '2018-05-10 02:34:16'),
(17, 'pasante', '0001_initial', '2018-05-10 02:34:16'),
(18, 'contactos_pasante', '0001_initial', '2018-05-10 02:34:17'),
(19, 'contactos_pasante', '0002_contactospasante_id_pasante_fk', '2018-05-10 02:34:17'),
(20, 'usuario', '0001_initial', '2018-05-10 02:34:17'),
(21, 'coordinador', '0002_auto_20180219_0553', '2018-05-10 02:34:20'),
(22, 'pasante', '0002_auto_20180219_0553', '2018-05-10 02:34:21'),
(23, 'pasante', '0003_auto_20180228_0441', '2018-05-10 02:34:22'),
(24, 'rol', '0001_initial', '2018-05-10 02:34:24'),
(25, 'sessions', '0001_initial', '2018-05-10 02:34:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('0ecpnaoybruvw9vry8naspadqyabb84v', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-05-24 02:45:04'),
('1mcfvwss089pmia83en0ivvhix4s1jnd', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-05 18:26:27'),
('1nt0q64ih6zstjpchywurml9awnb2pm3', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-05-25 22:40:07'),
('43o81zgmhmg8eqtcy9ag1rpz6avfj2pf', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-04 13:24:30'),
('9cb3i49j8ksj857k5a2871gcst8j3b3y', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-01 14:40:21'),
('at00r2ubzy1q0dyvx3mzsqdzvv0x8ugk', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-05-24 03:02:18'),
('ejobbnsp3dx48foy56mzof3gzy7ko5mk', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-02 16:47:43'),
('ek4psu6uuglvuf0c3yic2ucr4uyw43f7', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-06-10 03:55:21'),
('fgys2mc8cy7ouasqdp7tozflsm9llz8v', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-14 16:36:42'),
('gvjsfkhr3tin7c0wjx1f6062803goth7', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-06-01 14:15:46'),
('hx14480nxi7049lk45zl6mib1ot835ag', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-13 04:43:42'),
('iu4lx4rxg60ktxox1cut31rg78p91gxq', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-06-14 16:59:16'),
('peympncn0ip044eu6yex6ntshlm5qwq1', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-06-01 18:11:30'),
('phey3n8xgu6t1ynsxbyq1t92id56bzyg', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-06-04 14:54:22'),
('rq7ofrnts6ynclina1mlyzazid3tntl9', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-06-01 20:12:02'),
('t7jwvxnmyje2cesx6cniaimu7jli025f', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-05-24 02:42:33'),
('von7dsb6k0vzuuqah1q08ef425ir3lcy', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-05-28 23:27:06'),
('w4pfhrouhspapb309o7s9zkfcsk5keqr', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-05-24 16:58:39'),
('xzcw5m7pi7cvkuxgkvz11cfug5vf29hw', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-05-24 02:58:05'),
('zlitng50magui9mr5jaqh6p9xo89ymej', 'M2VlNDRhZmY3ZGVhOGY4ODU0Y2ZhNWFhY2Q1OGY4YzdiOGMyMGFlNDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiMzM1NmY5N2Q3ZGNmNTAzOWI2ZTZjZGM5N2Q3NWI0YzZjMzlkNjc0YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-05-24 16:29:29'),
('zqsj39v0stb4reqvl0b8qu5cwubwe9e3', 'MmVlZDdkOWEwYzhkNmUyOTQ0MmIxMDEwMTI2ODAwOWI3ZGM1Mzc4MDp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiZTFiMGY4Mjk3NTYxYTg0OWQ2NjM2NDdmMzQ0ODAxZDZmZDdjYjcxMSIsIl9hdXRoX3VzZXJfaWQiOiIyIn0=', '2018-05-25 04:53:37');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_datosacademicos`
--

CREATE TABLE IF NOT EXISTS `pasante_datosacademicos` (
`id_datos_academicos` int(11) NOT NULL,
  `promedio` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasante_datosacademicos`
--

INSERT INTO `pasante_datosacademicos` (`id_datos_academicos`, `promedio`) VALUES
(1, 10),
(2, 7.8),
(3, 7.86),
(4, 7.6),
(5, 9.14),
(6, 7.4),
(7, 7.82),
(8, 8.43),
(9, 7.75),
(10, 7.94),
(11, 7.15),
(12, 7.72),
(13, 8.09),
(14, 7.83),
(15, 8.14),
(16, 8.13),
(17, 7.4),
(18, 8),
(19, 7.29),
(20, 7.96),
(21, 8.01),
(22, 8.17),
(23, 9.18),
(24, 8.77),
(25, 7.92),
(26, 7.71),
(27, 8.16),
(28, 7.69),
(29, 8.38),
(30, 7.69),
(31, 8.74),
(32, 8.87),
(33, 8.56),
(34, 8.47),
(35, 7.97),
(36, 8.09),
(37, 7.92),
(38, 7.73),
(39, 8.1),
(40, 7.5),
(41, 7.45),
(42, 8.77),
(43, 8.01),
(44, 7.8),
(45, 8.85),
(46, 8.65),
(47, 8.56),
(48, 8.53),
(49, 8.3),
(50, 8.76),
(51, 8.23),
(52, 8.74),
(53, 8.34),
(54, 8.64),
(55, 7.8),
(56, 8.29),
(57, 7.47),
(58, 8.51),
(59, 7.69),
(60, 7.96),
(61, 8.39),
(62, 8.17),
(63, 8.47),
(64, 8.43),
(65, 8.05),
(66, 7.36),
(67, 7.79),
(68, 8.45),
(69, 8.38),
(70, 8.04),
(71, 7.94),
(72, 8.62),
(73, 8.56),
(74, 7.48),
(75, 8.57),
(76, 8.14),
(77, 8.04),
(78, 7.83),
(79, 8.71),
(80, 7.88),
(81, 8.04),
(82, 7.99),
(83, 8.32),
(84, 8.19),
(85, 7.79),
(86, 8.47),
(87, 8.4),
(88, 7.73),
(89, 7.99),
(90, 8.13),
(91, 7.75),
(92, 8.9),
(93, 8.64),
(94, 7.84),
(95, 8.3),
(96, 8.5),
(97, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_direccionpasante`
--

CREATE TABLE IF NOT EXISTS `pasante_direccionpasante` (
`id_direccion` int(11) NOT NULL,
  `numero` varchar(6) DEFAULT NULL,
  `calle` varchar(50) NOT NULL,
  `colonia` varchar(50) NOT NULL,
  `estado_id` int(11) NOT NULL,
  `municipio_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasante_direccionpasante`
--

INSERT INTO `pasante_direccionpasante` (`id_direccion`, `numero`, `calle`, `colonia`, `estado_id`, `municipio_id`) VALUES
(1, '18', 'Prueba', 'Prueba', 21, 1613),
(2, '905', 'PINOS', 'CORTEZ', 32, 2439),
(3, '37', 'privada cerro del grillo ', 'uaz', 32, 2451),
(4, '10', 'JOSE SOLEDAD TORRES ', 'TEPEYAC', 32, 2473),
(5, '5', 'PROGRESO', 'FRANCISCO I. MADERO', 32, 2483),
(6, '1', 'Ayuntamiento', 'Nueva Reforma', 32, 2454),
(7, '20', 'FCO JAVIER MINA', 'CENTRO', 32, 2444),
(8, '2', 'Camilo torres', 'Emiliano zapata', 32, 2444),
(9, '17', 'PRIVADA TEOFILO VILLEGAS', 'LAS PALMITAS', 32, 2468),
(10, '111', 'PRIVADA DE CALEROS', 'FLORES MAGON', 32, 2490),
(11, '17', 'felipe angeles', 'parga', 32, 2470),
(12, '103', 'Plazuela del Resurgimiento', 'Centro', 32, 2490),
(13, '138', 'Plan de Guadaluoe', 'Venustiano Carranza', 32, 2444),
(14, '130', 'Ramon López Velarde', 'Gonzalez orteha', 32, 2476),
(15, '321', 'Ponciano Arriaga', 'Benito Juarez', 32, 2490),
(16, '101', 'CONVENTO DE SANTA ISABEL ', 'VILLAS DEL MONASTERIO', 32, 2451),
(17, '19', 'Fuente De Versalles', 'Las Fuentes', 32, 2451),
(18, '27 A', 'CIRUELOS', 'ARBOLEDAS II', 32, 2451),
(19, '136', 'CERRO DEL GRILLO', 'colinas del padre primera seccion', 32, 2490),
(20, '109', 'Sierra Mojada', 'Privada Cerro de la Virgen', 32, 2490),
(21, '410', 'BENITO JUAREZ', 'CNOP', 32, 2490),
(22, '105', 'LOPEZ MATEOS', 'CENTRO', 32, 2447),
(23, '301', 'GUADALUPE VICTORIA ', 'LADRILLERA', 32, 2439),
(24, '54', 'concepcion', 'barrio de guadalupe', 32, 2489),
(25, '231', 'Regeneración', 'Sierra de Alica', 32, 2490),
(26, '217', 'CARLOS HINOJOSA', 'POPULAR CTM', 32, 2490),
(27, '219', 'Tomas Mendez Sosa', 'centro', 32, 2444),
(28, '102', 'ANDADOR DE LA PAZ', 'FOVISSSSTE', 32, 2451),
(29, '9', 'JAVIER SOLIS ', 'BONITO PUEBLO', 32, 2451),
(30, '34', 'CABORCA', 'Plutarco Elias Calles', 32, 2444),
(31, '173', 'INDEPENDENCIA', 'BARRIO DE AMECA', 32, 2483),
(32, '102', 'Miguel Hidalgo', 'Emiliano Zapata', 32, 2444),
(33, '301', 'SIERRA MAPIMI', 'COLINAS DEL PADRE SEGUNDA SECCION', 32, 2490),
(34, '8', 'PLAZA ROJA', 'EMILIANO ZAPATA ', 32, 2444),
(35, '132', 'RETORNO RUBI ', 'PRIVADA ESMERALDA COLINAS DEL PADRE ', 32, 2490),
(36, '126', 'fresnillo', 'industrial', 32, 2444),
(37, '8', 'Tecnologico', 'Tecnologica', 32, 2444),
(38, '#52', 'ANDADOR SOL ', 'CALIFORNIA II', 32, 2451),
(39, '40', 'LOPEZ VELARDE', 'ELECTRICISTAS', 32, 2444),
(40, '41', 'NOCHE BUENA ', 'RAMON LOPEZ VELARDE', 32, 2466),
(41, '208', 'LOMAS DEL ROBLE', 'LOMAS DE LA VIRGEN ', 32, 2490),
(42, '71', 'Del Panteon', 'Ramon Lopez Velarde', 32, 2439),
(43, '1', 'José Venturelli', 'Frac. Muralistas', 32, 2444),
(44, '305', 'DE LAS ROSAS', 'LAS FLORES', 32, 2444),
(45, '4', 'La Luz', 'Los Caracoles', 32, 2483),
(46, '8', 'RIO SONORA', 'SANTA GERTRUDIS', 32, 2482),
(47, '16', 'Privada Torre de David ', 'Fraccionamiento Las Torres', 32, 2451),
(48, '224', 'SAN LORENZO', 'INDUSTRIAL', 32, 2444),
(49, '58 A', 'Cadena', 'Centro', 32, 2468),
(50, '202pte', 'MORELOS', 'CENTRO', 32, 2439),
(51, '325', 'Veta de Aluminio ', 'Fraccionamiento Lomas de la Fortuna', 32, 2444),
(52, '7', 'SABINAS', 'INFONAVIT', 32, 2476),
(53, '401', 'PANFILO NATERA', 'FRANCISCO E. GARCIA', 32, 2490),
(54, '57', '17 de junio', 'Plan de ayala', 32, 2444),
(55, '209', 'HIDALGO', 'CENTRO', 32, 2482),
(56, '241', 'AVENIDA GAVILANES', 'GAVILANES', 32, 2451),
(57, '102', 'Francisco mujica', 'Barros Sierra', 32, 2490),
(58, '223', 'LEONA VICARIO', 'BALCONCITOS', 32, 2478),
(59, '40', 'hacienda de guadalupe ', 'el molino', 32, 2454),
(60, '104', 'Periférico norte ', 'Hacienda conde de Bernárdez ', 32, 2451),
(61, '28', 'FRATERNIDAD', 'DIVISION DEL NORTE', 32, 2451),
(62, '27A', 'NICOLAS BRAVO ', 'SAN JOSE DE CASTELLANOS', 32, 2472),
(63, '101', 'SIERRA MOJADA ', 'Colinas del Padre 4ta Sección, Privada Cerro de la', 32, 2490),
(64, '552', 'SIERRA NEVADA ', 'Colinas del padre ', 32, 2490),
(65, '8', 'Madero', 'Alameda', 32, 2487),
(66, '13-a', 'pascual orozco', 'ejidal', 32, 2451),
(67, '502', 'Luis Moya', 'Centro', 32, 2439),
(68, '6 C', 'DEL CARTERO', 'CENTRO', 32, 2444),
(69, '102', 'De la paz ', 'Industrial', 32, 2444),
(70, '401', 'Segunda del tanquecito ', 'Centro', 32, 2490),
(71, '225', 'VILLAS DEL VERGEL', 'FRACCIONAMIENTO VILLAS DE GUADALUPE', 32, 2451),
(72, '8', 'Ignacio Lopez Rayon ', 'Centro ', 32, 2444),
(73, '101', 'CJON EL ARBOLITO ', 'RANCHITO DE LA CRUZ', 32, 2483),
(74, '214', 'MALVAS', 'FELIPE ÁNGELES', 32, 2490),
(75, 'R17 ', 'PLAZA LOS ZOPILOTES ', 'GAVILANES', 32, 2451),
(76, '9', 'OBRERO MUNDIAL ', 'FRACC.VENUSTIANO CARRANZA', 32, 2444),
(77, '10', 'CUAHUTLA', 'CENTRO', 32, 2457),
(78, '4-D', 'ANDADOR ALUMINIO ', 'FOVISSSTE', 32, 2451),
(79, '115', 'República', 'Francisco I Madero', 32, 2483),
(80, '16', 'HACIENDA DE LA CONCEPCION', 'FRACCIONAMIENTO SANTA RITA', 32, 2451),
(81, '29', 'CIPRES', 'BOSQUES', 32, 2454),
(82, '5', 'TOLTECAS', 'MESOAMERICAS', 32, 2444),
(83, '209', 'Cristóbal de Oñate ', 'Lomas Bizantinas ', 32, 2490),
(84, '16-B', 'PAJES ', 'FRACCIONAMIENTO VALLE DEL CONDE ', 32, 2451),
(85, '105', 'PRIVADA DE PLUTON', 'LAS PALMAS', 32, 2490),
(86, '205', 'RAFAEL MORENO VALLE', 'DIAZ ORDAZ', 32, 2490),
(87, '11', 'Plomeros ', 'Tres cruces ', 32, 2490),
(88, '42', 'GUADALUPE VICTORIA', 'CARLOS HINOJOSA PETIT', 32, 2490),
(89, '1B', 'MIGUEL ALEMAN ', 'DULCE GRANDE', 24, 1897),
(90, '171', 'INDEPENDENCIA ', 'BARRIO DE AMECA', 32, 2483),
(91, '133', 'AGAPANTOS ', 'FRACC LAS ARBOLEDAS', 32, 2451),
(92, '30', 'Constitucion', 'Zapata', 4, 27),
(93, '613', 'Benito Juarez', 'C.N.O.P', 32, 2490),
(94, '76', 'Avenida 5 de Mayo ', 'Primera de Mayo', 32, 2451),
(95, '205', 'Sierra campana', 'Colinas del padre', 32, 2490),
(96, '10', 'Catalunya', 'Centro', 32, 2439),
(97, '122f', 'Vinateros', 'Tres Cruces', 32, 2490);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_generacion`
--

CREATE TABLE IF NOT EXISTS `pasante_generacion` (
`id_generacion` int(11) NOT NULL,
  `generacion` varchar(30) NOT NULL,
  `anio` varchar(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasante_generacion`
--

INSERT INTO `pasante_generacion` (`id_generacion`, `generacion`, `anio`) VALUES
(1, 'Febrero', '2018'),
(2, 'Agosto', '2018');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_pasante`
--

CREATE TABLE IF NOT EXISTS `pasante_pasante` (
`id_pasante` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `curp` varchar(18) NOT NULL,
  `rfc` varchar(13) NOT NULL,
  `status` varchar(18) NOT NULL,
  `foto` varchar(100) DEFAULT NULL,
  `id_datos_academicos_fk_id` int(11) NOT NULL,
  `id_direccion_fk_id` int(11) NOT NULL,
  `id_usuario_fk_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasante_pasante`
--

INSERT INTO `pasante_pasante` (`id_pasante`, `nombre`, `apellido_paterno`, `apellido_materno`, `curp`, `rfc`, `status`, `foto`, `id_datos_academicos_fk_id`, `id_direccion_fk_id`, `id_usuario_fk_id`) VALUES
(1, 'Gerardo', 'Gallardo', 'Cordero', 'PRPP180101PRUEBA18', 'PRPP180101', 'activo', 'fotos_pasantes/me.jpg', 1, 1, 2),
(2, 'LUISA ANAHI', 'FERNANDEZ', 'RODRIGUEZ', 'FERL920508MDGRDS08', 'FERL920508SD2', 'activo', 'fotos_pasantes/ANAHIFERNANDEZ.JPG', 2, 2, 3),
(3, 'daniel', 'de la puente', 'padilla', 'PUPD920318HZSNDN09', 'PUPD920318B76', 'activo', 'fotos_pasantes/5a56c0d756f79_uDLN2qm.jpg', 3, 3, 4),
(4, 'ROGELIO', 'RANGEL', 'SALDAÑA', 'RASR921206HZSNLG03', 'RASR9212062N1', 'activo', 'fotos_pasantes/FOTO_RR.jpg', 4, 4, 5),
(5, 'MARIA DEL ROSARIO', 'FLORES', 'HERRERA', 'FOHR930823MZSLRS07', 'FOHR930823U10', 'activo', 'fotos_pasantes/Foto_ID_d39XCJr.jpg', 5, 5, 8),
(6, 'Martín Eduardo', 'Escobedo', 'Serrano', 'EOSM920713HZSSRR00', 'EOSM9207133T8', 'activo', 'fotos_pasantes/Foto_RiQP3HO.jpg', 6, 6, 7),
(7, 'MIGUEL', 'RODARTE', 'ALVARADO', 'ROAM910813HZSDLG08', 'ROAM910813QU7', 'activo', 'fotos_pasantes/archivo097_QDAFPhw.jpg', 7, 7, 9),
(8, 'Jessica', 'Ortiz', 'Torres', 'OITJ930214MZSRRS05', 'OITJ9302144X8', 'activo', 'fotos_pasantes/20170928_073252.jpg', 8, 8, 6),
(9, 'NEYDITH CRISTINA', 'AGUAYO', 'ROMERO', 'AURN930422MZSGMY08', 'AURN9304226K9', 'activo', 'fotos_pasantes/ney.jpg', 9, 9, 10),
(10, 'ALFONSO', 'MURILLO', 'VALADEZ', 'MUVA930321HZSRLL09', 'MUVA930321QL5', 'activo', 'fotos_pasantes/FOTO_SS_OE8hRUa.jpg', 10, 10, 11),
(11, 'Raul', 'Flores', 'Guerrero', 'FOGR830614HASLRL06', 'FOGR8306148EA', 'activo', 'fotos_pasantes/FOTOGRAFIA.jpg', 11, 11, 12),
(12, 'Marcela', 'González', 'Gutiérrez', 'GOGM780727MSPNTR05', 'GOGM780727DT0', 'activo', 'fotos_pasantes/inf_foto.jpg', 12, 12, 13),
(13, 'Omar guadalupe', 'Dorado', 'Hernandez', 'DOHO930525HZSRRM09', 'DOHO9305254J1', 'activo', 'fotos_pasantes/20180104_224356.jpg', 13, 13, 14),
(14, 'Pedro Ernesto', 'Sierra', 'Ochoa', 'SIOP930807HZSRCD07', 'SIOP930807P77', 'activo', 'fotos_pasantes/20180112_105815.jpg', 14, 14, 15),
(15, 'Karen Raquel', 'Villa', 'López', 'VILK930213MZSLPR03', 'VILK9302135A0', 'activo', 'fotos_pasantes/Photo_Editor-20180114_023417.jpg', 15, 15, 16),
(16, 'MAYRA LIZETH', 'MARTINEZ', 'RODRIGUEZ', 'MARM930818MJCRDY03', 'MARM93081819A', 'activo', 'fotos_pasantes/SERV.jpg', 16, 16, 17),
(17, 'Humbelina', 'Guerrero', 'Jaime', 'GUJH900217MMCRMM09', 'GUJH900217G51', 'activo', 'fotos_pasantes/humbe2_f5NC5Q6.jpg', 17, 17, 18),
(18, 'Ilse Izamaly', 'Morales', 'Maldonado', 'MOMI930731MZSRLL07', 'MOMI930731LG7', 'activo', 'fotos_pasantes/IMAGEN_II.jpg', 18, 18, 19),
(19, 'RAFAEL', 'MARTINEZ', 'MURCIA', 'MAMR880320HZSRRF04', 'MAMR880320ST4', 'activo', 'fotos_pasantes/bco_y_negro.jpg', 19, 19, 20),
(20, 'Ana Cecilia ', 'Barragán ', 'Hernández', 'BAHA931122MOCRRN00', 'BAHA931122CE9', 'activo', 'fotos_pasantes/D_1_OqiWaXl.jpg', 20, 20, 21),
(21, 'JUANA MARIA', 'GALVAN', 'RODRIGUEZ', 'GARJ930624MZSLDN12', 'GARJ930624HL3', 'activo', 'fotos_pasantes/IMG_20180109_191915_K7SAbld.jpg', 21, 21, 22),
(22, 'JESUS', 'AGUILAR', 'BAÑUELOS', 'AUBJ931101HZSGXS00', 'aubj931101280', 'activo', 'fotos_pasantes/chuyy.jpg', 22, 22, 25),
(23, 'ITZAYANA', 'GUTIÉRREZ', 'GONZÁLEZ', 'GUGI931231MZSTNT15', 'GUGI9312319P8', 'activo', 'fotos_pasantes/DSC03617_V522W7w.jpg', 23, 23, 26),
(24, 'GABRIELA', 'VILLAGRANA', 'SALAS', 'VISG930826MZSLLB03', 'VISG930826779', 'activo', 'fotos_pasantes/26829241_1500284136753998_1871278231_o_rJjhkYi.jpg', 24, 24, 28),
(25, 'Ilze Nayeli Anahí', 'Esqueda', 'Davila', 'EUDI910212MZSSVL06', 'EUDI910212E52', 'activo', 'fotos_pasantes/IMG_20180109_200047_EBAA2Aw.jpg', 25, 25, 29),
(26, 'JANET JACQUELINE', 'LEON', 'FUENTES', 'LEFJ920103MJCNNN02', 'LEFJ920103AD1', 'activo', 'fotos_pasantes/26168459_10213700241772545_3566800120107821130_n_p42HbWB.jpg', 26, 26, 31),
(27, 'Daniel Misael ', 'Cid', 'Ruvalcaba', 'CIRD930305HZSDVN08', 'CIRD930305EW6', 'activo', 'fotos_pasantes/FOTO_SS_VBxFC6W.jpg', 27, 27, 32),
(28, 'ADRIANA CAROLINA', 'RAMIREZ ', 'SIFUENTES', 'RASA890127MZSMFD08', 'RASA890127122', 'activo', 'fotos_pasantes/26754423_10211277460154327_1237165722_n_6yZ2EWN.jpg', 28, 28, 30),
(29, 'LEYDA JANETH', 'GONZALEZ', 'RODRIGUEZ', 'GORL930519MZSNDY09', 'GORL9305193U3', 'activo', 'fotos_pasantes/IMG_1_2.jpg', 29, 29, 34),
(30, 'Rocio', 'Gallegos', 'Martínez', 'GAMR930109MZSLRC08', 'GAMR930109EQ7', 'activo', 'fotos_pasantes/RO_DMA3nb4.jpg', 30, 30, 33),
(31, 'EDITH', 'SARMIENTO', 'GALLEGOS', 'SAGE930315MZSRLD04', 'SAGE930315U96', 'activo', 'fotos_pasantes/ELIMINAR.jpg', 31, 31, 36),
(32, 'Ricardo ', 'Lugo', 'Leos', 'LULA880126HZSGSL07', 'LULR920811QG7', 'activo', 'fotos_pasantes/Scan0002.jpg', 32, 32, 35),
(33, 'CINTHIA NAYELY', 'CHAIREZ ', 'GUTIERREZ', 'CAGC930318MZSHTN02', 'CAGC930318NP5', 'activo', 'fotos_pasantes/FOTOMPSS_cZh6Q2F.jpg', 33, 33, 37),
(34, 'VALERIA ESTEFANIA ', 'QUINTERO', 'RODRIGUEZ', 'QURV930515MZSNDL02', 'QURV9305155S0', 'activo', 'fotos_pasantes/26804036_1934986249880069_697921806_n.jpg', 34, 34, 38),
(35, 'Ana Valeria ', 'Avila ', 'Bañuelos ', 'AIBA930816MZSVXN04', 'AIBA9308165Q7', 'activo', 'fotos_pasantes/SS.jpg', 35, 35, 39),
(36, 'dagoberto', 'morales', 'del real', 'MORD930908HZSRLG06', 'MORD930908RF2', 'activo', 'fotos_pasantes/14291793_10208524389205733_8721378056103457244_n_ASX9o0W.jpg', 36, 36, 44),
(37, 'Luz Maria', 'Campos', 'Jayme', 'CAJL920922MZSMYZ06', 'CAJL920922CA4', 'activo', 'fotos_pasantes/IMG_9640.JPG', 37, 37, 41),
(38, 'Sarai', 'Hernandez ', 'Ybarra', 'heys930127mzsrbr02', 'HEYS930127RR0', 'activo', 'fotos_pasantes/26941642_10159825401005164_892917720_o.jpg', 38, 38, 45),
(39, 'Rafael', 'Rodríguez', 'Sucunza', 'ROSR921107HZSDCF09', 'ROSR921107MR6', 'activo', 'fotos_pasantes/FOTO_lJyxyMz.jpg', 39, 39, 47),
(40, 'jorge luis ', 'medina ', 'ledezma', 'MELJ930219HZSDDR04', 'MELJ930219U82', 'activo', 'fotos_pasantes/20180112_102341_eDWVOLw.jpg', 40, 40, 48),
(41, 'MARÍA DE LOURDES', 'LUIS', 'CONTRERAS', 'LUCL880112MZSSNR00', 'LUCL8801124D8', 'activo', 'fotos_pasantes/MI_FOTO_GcsFhiY.jpg', 41, 41, 52),
(42, 'EMMANUEL', 'TREJO', 'URIBE', 'TEUE921025HZSRRM07', 'TEUE921025470', 'activo', 'fotos_pasantes/EMM.jpg', 42, 42, 40),
(43, 'Andrea', 'Herrera', 'Estrada', 'HEEA920208MZSRSN09', 'HEEA920208IL0', 'activo', 'fotos_pasantes/ANDREA.jpg', 43, 43, 53),
(44, 'SUSAN YAZMIN', 'GARCIA', 'DIAZ', 'GADS930326MZSRZS04', 'GADS930326TD0', 'activo', 'fotos_pasantes/26803550_1514757478574197_1300615369_n_InPkSfH.jpg', 44, 44, 55),
(45, 'MARTHA IVETH', 'RAMOS', 'MIER', 'RAMM930729MZSMRR03', 'RAMM9307293N5', 'activo', 'fotos_pasantes/20180110_121008_HDR-1-11.jpg', 45, 45, 50),
(46, 'Francisco Javier', 'Gaeta', 'Salinas', 'GASF930125HZSTLR02', 'GASF9301254N5', 'activo', 'fotos_pasantes/JAVI.jpg', 46, 46, 51),
(47, 'Pamela Cristina', 'Pereyra', 'Gutiérrez', 'PEGP930112MZSRTM06', 'PEGP930112R56', 'activo', 'fotos_pasantes/FOTO_INFANTIL_CRIS_yFg9k44.jpg', 47, 47, 57),
(48, 'EMILY MARITZA', 'CHABLE', 'COVARRUBIAS', 'CACE910526MVZHVM09', 'CACE910526JG2', 'activo', 'fotos_pasantes/26754039_1570758216325613_644324225_n_vw81YIw.jpg', 48, 48, 59),
(49, 'Israel', 'Tolentino', 'Pérez', 'TOPI930701HZSLRS02', 'TOPI930701E55', 'activo', 'fotos_pasantes/image-2018-01-15_1.jpg', 49, 49, 56),
(50, 'NANCY CAROLINA', 'HERNANDEZ', 'DE HARO ', 'HEHN921212MZSRRN02', 'HEHN9212128N7', 'activo', 'fotos_pasantes/Captura_BltOpUK.JPG', 50, 50, 63),
(51, 'Edgar Armando', 'de la Rosa', 'Huitron', 'ROHE921228HZSSTD05', 'ROHE9212282E5', 'activo', 'fotos_pasantes/26754725_1904031909649762_1439722077_n.jpg', 51, 51, 61),
(52, 'SONIA ESMERALDA', 'PEREZ', 'JIMENEZ', 'PEJS930405MZSRMN02', 'PEJS930405TWA', 'activo', 'fotos_pasantes/Foto_infantil.jpg', 52, 52, 64),
(53, 'VICTOR MAURILIO', 'SOSA', 'HERNANDEZ', 'SOHV920321HZSSRC02', 'SOHV920321H27', 'activo', 'fotos_pasantes/26793534_1935093076536053_1035537053_n.jpg', 53, 53, 49),
(54, 'Mirna Margarita', 'Martínez', 'Ruedas', 'MARM921113MZSRDR06', 'MARM921113QU6', 'activo', 'fotos_pasantes/IMG_20180110_135159_lXtXDCZ.jpg', 54, 54, 66),
(55, 'ABIGAIL BERENICE', 'GOMEZ', 'VALENZUELA', 'GOVA930328MZSMLB06', 'GOVA930328FR6', 'activo', 'fotos_pasantes/fotografia.jpg', 55, 55, 58),
(56, 'Paola Anahi', 'Cortez', 'Villarreal', 'COVP930427MZSRLL06', 'COVP930427QU8', 'activo', 'fotos_pasantes/image-2018-01-15.jpg', 56, 56, 65),
(57, 'Monserrat Berenice', 'Cardona', 'Roman', 'CARM900219MZSRMN08', 'CARM900219CF8', 'activo', 'fotos_pasantes/Alias_la_Monktzi_9SpZPnG.jpg', 57, 57, 67),
(58, 'DULCE RUBY', 'MONSIVAIS', 'HARO', 'MOHD930830MZSNRL07', 'MOHD930830KW2', 'activo', 'fotos_pasantes/26906405_1690201161038721_1350002121_o.jpg', 58, 58, 62),
(59, 'yesenia Elvira', 'Garcia', 'lopez', 'GALY921214MZSRPS06', 'GALY921214014', 'activo', 'fotos_pasantes/26909604_1617278871686640_216422338_o.jpg', 59, 59, 68),
(60, 'Juan', 'Espinosa', 'Vacio', 'EIVJ900619HZSSCN08', 'EIVJ900619RI3', 'activo', 'fotos_pasantes/IMG_20180109_181532733.jpg', 60, 60, 69),
(61, 'mariana ', 'espino', 'diaz ', 'EIDM920512MZSSZR04', 'EIDM9205124F3', 'activo', 'fotos_pasantes/19665641_1943596169257485_3489984321066796534_n.jpg', 61, 61, 70),
(62, 'CLAUDIO DAVID', 'GONZALEZ', 'ZAPATA', 'GOZC930129HSPNPL07', 'GOZC930129KD9', 'activo', 'fotos_pasantes/IMG_5526_0AQzBkb.JPG', 62, 62, 71),
(63, 'David', 'Román', 'Espitia', 'ROED930728HZSMSV04', 'ROED930728651', 'activo', 'fotos_pasantes/FOTO_B_Y_N_INFANTIL_DAVID_2.jpg', 63, 63, 72),
(64, 'BLANCA ELIZABETH', 'ORTEGA', 'CHAVEZ', 'OECB930709MZSRHL09', 'OECB930709K1A', 'activo', 'fotos_pasantes/FOTO_FORMAL.JPG', 64, 64, 73),
(65, 'Mariela', 'Mauricio', 'Rivera', 'MARM930308MZSRVR02', 'MARM9303088N7', 'activo', 'fotos_pasantes/IMG_0201.JPG', 65, 65, 74),
(66, 'Jose Ramses', 'Galvan ', 'de Avila', 'gaar830102hzslvm01', 'gaar8301028c4', 'activo', 'fotos_pasantes/5343_10200136078019380_1456642470_n.jpg', 66, 66, 27),
(67, 'GUADALUPE JOENI ', 'AGUILAR', 'VEGA', 'AUVG920224MZSGGD06', 'AUVG920224235', 'activo', 'fotos_pasantes/20170821_151359_K4P4j6G.jpg', 67, 67, 46),
(68, 'EDUARDO', 'MARQUEZ', 'ARMENTA', 'maae930504hzsrrd05', 'MAAE9305048D1', 'activo', 'fotos_pasantes/FOTO_BN1_JI5WUdN.jpg', 68, 68, 75),
(69, 'Edurdo Guadalupe', 'Valadez', 'Martinez ', 'vame900209HZSLRD05', 'vamE900209IP2', 'activo', 'fotos_pasantes/lalito_.jpg', 69, 69, 76),
(70, 'Artemisa', 'Fernández', 'Dávila', 'FEDA920731MZSRVR02', 'FEDA9207318M4', 'activo', 'fotos_pasantes/IMG_20180115_211127089.jpg', 70, 70, 77),
(71, 'FATIMA MONTSERRAT', 'SALAS ', 'ENCISO', 'SAEF910314MZSLNT02', 'SAEF910314F56', 'activo', 'fotos_pasantes/faty_ZpN8Uby.jpg', 71, 71, 43),
(72, 'Adriana', 'Gurrola', 'Acosta', 'GUAA930914MZSRCD06', 'GUAA930914I77', 'activo', 'fotos_pasantes/adr_UfkuL5o.jpg', 72, 72, 78),
(73, 'Gloria Evelia', 'Alvarez', 'Landa', 'AALG881219MJCLNL02', 'AALG881219557', 'activo', 'fotos_pasantes/26941481_2234367986588722_2106587235_o_1.jpg', 73, 73, 79),
(74, 'LUIS FELIPE', 'CASTAÑON', 'GOMEZ', 'CAGL910720HZSSMS05', 'CAGL9107204F2', 'activo', 'fotos_pasantes/LUISFELIPECG_trSuuyE.jpg', 74, 74, 80),
(75, 'ERICK ALEJANDRO', 'ACOSTA', 'CABRERA', 'AOCE930307HZSCBR02', 'AOCE930307277', 'activo', 'fotos_pasantes/26829579_1552751928142018_1025867433_o.jpg', 75, 75, 81),
(76, 'Saira Yuliet', 'Soto', 'Terrones', 'SOTS910630MZSTRR05', 'SOTS9106302L0', 'activo', 'fotos_pasantes/IMG_20180115_214746.JPG', 76, 76, 42),
(77, 'NATALIA', 'CABRERA', 'RUVALCABA', 'CARN930310MZSBVT05', 'CARN93031056A', 'activo', 'fotos_pasantes/NATALIA.jpg', 77, 77, 60),
(78, 'Adriana', 'Jauregui', 'Arambula', 'JAAA920825MZSRRD00', 'JAAA920825EC7', 'activo', 'fotos_pasantes/26754901_10214841482568123_1783384263_n.jpg', 78, 78, 82),
(79, 'Claudia del Rocío', 'Segovia', 'Lozano', 'SELC930524MZSGZL09', 'SELC930524X2', 'activo', 'fotos_pasantes/1516075524921919625541.jpg', 79, 79, 83),
(80, 'OMAR', 'JIMÉNEZ', 'NAVARRO', 'JINO900611HZSMVM02', 'JINO900611M91', 'activo', 'fotos_pasantes/2018-01-12_18-28_page_1_uUNYSUJ.jpg', 80, 80, 84),
(81, 'José Antonio', 'Escobedo', 'Márquez', 'EOMA921223HZSSRN00', 'EOMA921223UG2', 'activo', 'fotos_pasantes/26829337_10214612304801966_1900897555_o.jpg', 81, 81, 23),
(82, 'Mario Arturo', 'Luna', 'Garcia', 'LUGM910911HZSNRR09', 'LUGM9109114Y1', 'activo', 'fotos_pasantes/20180109_192352.jpg', 82, 82, 86),
(83, 'Christian Yadir ', 'Salas ', 'Martínez ', 'SAMC930706HASLRH02', 'SAMC9307069U3', 'activo', 'fotos_pasantes/IMG_5675_vWAHLFp.jpg', 83, 83, 88),
(84, 'JUAN ANTONIO ', 'GONZALEZ', 'RAMIREZ', 'GORJ920426HZSNMNOO', 'GORJ920426KG1', 'activo', 'fotos_pasantes/26854514_1762901893773498_908413276_o.jpg', 84, 84, 89),
(85, 'CAROLINA', 'AGUILERA', 'RAMIREZ', 'AURC930216MZSGMR00', 'AURC930216QD5', 'activo', 'fotos_pasantes/26972419_10211095520762157_934520728_o.jpg', 85, 85, 87),
(86, 'CRISTIAN EMMANEUL', 'BALTAZAR', 'JIMENEZ', 'BAJC930409HZSLMR09', 'BAJC930409ER3', 'activo', 'fotos_pasantes/CRIS2_57jJYFX.jpg', 86, 86, 90),
(87, 'Pablo alberto ', 'Dávila ', 'Oliva ', 'DAOP860619HZSVLB00', 'DAOP860619NR9', 'activo', 'fotos_pasantes/image_LkkYGIw.jpg', 87, 87, 92),
(88, 'Maritza', 'Enriquez', 'Adame', 'EIAM910507MZSNDR28', 'EIAM910507B80', 'activo', 'fotos_pasantes/WIN_20180110_17_48_01_Pro_k5V2tD3.jpg', 88, 88, 93),
(89, 'ALAN GASTON', 'MUÑOZ', 'MURILLO', 'MUMA930611HSPXRL05', 'MUMA9306112P2', 'activo', 'fotos_pasantes/26982510_1955700071125594_1204495575_o.jpg', 89, 89, 94),
(90, 'MIMBE SELENE', 'SANCHEZ', 'MIRAMONTES', 'SAMM930704MZSNRM03', 'SAMM930704PF2', 'activo', 'fotos_pasantes/mimbe_1_2.jpg', 90, 90, 95),
(91, 'KAREN MONTSERRAT', 'PEREZ', 'AGUIRRE', 'PEAK910928MZSRGR04', 'PEAK9109282T0', 'activo', 'fotos_pasantes/26755399_721924494676160_2144403896_n_rAYhunm.jpg', 91, 91, 96),
(92, 'Eden', 'Hazard', 'Belga', 'MECA960704HZSRRL00', 'MECA960704', 'desactivado', 'fotos_pasantes/20180116_185655.jpg', 92, 92, 97),
(93, 'Isela', 'Fraire', 'Alvarado', 'FAAI960312MZSRLS02', 'FAAI960312', 'desactivado', 'fotos_pasantes/22885835_1574234305973597_8252362338256675039_n.jpg', 93, 93, 98),
(94, 'Fabián Aarón', 'Venegas ', 'Basurto', 'VEBF920630HZSNSB01', 'VEBF920630RZ7', 'desactivado', 'fotos_pasantes/16402674_1304384786284441_1494547544405097109_o.jpg', 94, 94, 99),
(95, 'Rosa Elena', 'Ulloa', 'Laguna', 'UOLR920429MZSLGS06', 'UOLR920429U8', 'desactivado', 'fotos_pasantes/IMG_4344.JPG', 95, 95, 103),
(96, 'Lio', 'Messi', '', 'ROSJOIFJD219038OJF', 'OIHS094OIJS09', 'activo', 'fotos_pasantes/MESSI.jpg', 96, 96, 105),
(97, 'Johan Aleinikov', 'Longoria', 'Almaraz', 'LOAJ960830HZSNLH08', 'LOAJ960830', 'activo', 'fotos_pasantes/1.jpg', 97, 97, 104);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_promocion`
--

CREATE TABLE IF NOT EXISTS `pasante_promocion` (
`id_promocion` int(11) NOT NULL,
  `id_generacion_fk_id` int(11) NOT NULL,
  `id_pasante_fk_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasante_promocion`
--

INSERT INTO `pasante_promocion` (`id_promocion`, `id_generacion_fk_id`, `id_pasante_fk_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(7, 1, 7),
(8, 1, 8),
(9, 1, 9),
(10, 1, 10),
(11, 1, 11),
(12, 1, 12),
(13, 1, 13),
(14, 1, 14),
(15, 1, 15),
(16, 1, 16),
(17, 1, 17),
(18, 1, 18),
(19, 1, 19),
(20, 1, 20),
(21, 1, 21),
(22, 1, 22),
(23, 1, 23),
(24, 1, 24),
(25, 1, 25),
(26, 1, 26),
(27, 1, 27),
(28, 1, 28),
(29, 1, 29),
(30, 1, 30),
(31, 1, 31),
(32, 1, 32),
(33, 1, 33),
(34, 1, 34),
(35, 1, 35),
(36, 1, 36),
(37, 1, 37),
(38, 1, 38),
(39, 1, 39),
(40, 1, 40),
(41, 1, 41),
(42, 1, 42),
(43, 1, 43),
(44, 1, 44),
(45, 1, 45),
(46, 1, 46),
(47, 1, 47),
(48, 1, 48),
(49, 1, 49),
(50, 1, 50),
(51, 1, 51),
(52, 1, 52),
(53, 1, 53),
(54, 1, 54),
(55, 1, 55),
(56, 1, 56),
(57, 1, 57),
(58, 1, 58),
(59, 1, 59),
(60, 1, 60),
(61, 1, 61),
(62, 1, 62),
(63, 1, 63),
(64, 1, 64),
(65, 1, 65),
(66, 1, 66),
(67, 1, 67),
(68, 1, 68),
(69, 1, 69),
(70, 1, 70),
(71, 1, 71),
(72, 1, 72),
(73, 1, 73),
(74, 1, 74),
(75, 1, 75),
(76, 1, 76),
(77, 1, 77),
(78, 1, 78),
(79, 1, 79),
(80, 1, 80),
(81, 1, 81),
(82, 1, 82),
(83, 1, 83),
(84, 1, 84),
(85, 1, 85),
(86, 1, 86),
(87, 1, 87),
(88, 1, 88),
(89, 1, 89),
(90, 1, 90),
(91, 1, 91),
(92, 1, 92),
(93, 1, 93),
(94, 1, 94),
(95, 1, 95),
(96, 2, 96),
(97, 2, 97);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pasante_telefono`
--

CREATE TABLE IF NOT EXISTS `pasante_telefono` (
`id_telefono` int(11) NOT NULL,
  `numero` varchar(15) NOT NULL,
  `tipo` varchar(10) NOT NULL,
  `id_pasante_fk_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `pasante_telefono`
--

INSERT INTO `pasante_telefono` (`id_telefono`, `numero`, `tipo`, `id_pasante_fk_id`) VALUES
(1, '', 'fijo', 1),
(2, '4931600782', 'movil', 1),
(3, '', 'fijo', 2),
(4, '4781081412', 'movil', 2),
(5, '4579837267', 'fijo', 3),
(6, '4921067959', 'movil', 3),
(7, '4989820258', 'fijo', 4),
(8, '4921710774', 'movil', 4),
(9, '4579361269', 'fijo', 5),
(10, '4938791178', 'movil', 5),
(11, '9453702', 'fijo', 6),
(12, '4949428635', 'movil', 6),
(13, '4939320825', 'fijo', 7),
(14, '4494158568', 'movil', 7),
(15, '4931252380', 'fijo', 8),
(16, '4939599006', 'movil', 8),
(17, '3467132335', 'fijo', 9),
(18, '4925834749', 'movil', 9),
(19, '1547781', 'fijo', 10),
(20, '4921352719', 'movil', 10),
(21, '4581108735', 'fijo', 11),
(22, '4581108735', 'movil', 11),
(23, '', 'fijo', 12),
(24, '4441712308', 'movil', 12),
(25, '4939334732', 'fijo', 13),
(26, '4931106225', 'movil', 13),
(27, '4339834321', 'fijo', 14),
(28, '4927953459', 'movil', 14),
(29, '9246583', 'fijo', 15),
(30, '4922238395', 'movil', 15),
(31, '4928994759', 'fijo', 16),
(32, '4929972665', 'movil', 16),
(33, '', 'fijo', 17),
(34, '4921600794', 'movil', 17),
(35, '4929980953', 'fijo', 18),
(36, '4924934606', 'movil', 18),
(37, '9236170', 'fijo', 19),
(38, '4921620773', 'movil', 19),
(39, '4921567890', 'fijo', 20),
(40, '4921149140', 'movil', 20),
(41, '', 'fijo', 21),
(42, '4921325636', 'movil', 21),
(43, '4781002280', 'fijo', 22),
(44, '4781002280', 'movil', 22),
(45, '', 'fijo', 23),
(46, '4781087075', 'movil', 23),
(47, '4999478302', 'fijo', 24),
(48, '4991028430', 'movil', 24),
(49, '', 'fijo', 25),
(50, '4921435843', 'movil', 25),
(51, '4926905808', 'fijo', 26),
(52, '4921762857', 'movil', 26),
(53, '', 'fijo', 27),
(54, '4921603973', 'movil', 27),
(55, '', 'fijo', 28),
(56, '4941128660', 'movil', 28),
(57, '', 'fijo', 29),
(58, '4928926270', 'movil', 29),
(59, '9332353', 'fijo', 30),
(60, '4921032580', 'movil', 30),
(61, '4579362139', 'fijo', 31),
(62, '4571038395', 'movil', 31),
(63, '4931297904', 'fijo', 32),
(64, '4931297904', 'movil', 32),
(65, '7681959', 'fijo', 33),
(66, '4921299765', 'movil', 33),
(67, '9322477', 'fijo', 34),
(68, '4931133138', 'movil', 34),
(69, '4924913193', 'fijo', 35),
(70, '4921591011', 'movil', 35),
(71, '', 'fijo', 36),
(72, '4391424041', 'movil', 36),
(73, '4939324564', 'fijo', 37),
(74, '4929099871', 'movil', 37),
(75, '4929213728', 'fijo', 38),
(76, '4928926929', 'movil', 38),
(77, '', 'fijo', 39),
(78, '4939597347', 'movil', 39),
(79, '', 'fijo', 40),
(80, '4921283442', 'movil', 40),
(81, '', 'fijo', 41),
(82, '4921057371', 'movil', 41),
(83, '4931296606', 'fijo', 42),
(84, '4931610060', 'movil', 42),
(85, '444939353130', 'fijo', 43),
(86, '444931146326', 'movil', 43),
(87, '', 'fijo', 44),
(88, '4931380494', 'movil', 44),
(89, '4579362808', 'fijo', 45),
(90, '4921439890', 'movil', 45),
(91, '4371073580', 'fijo', 46),
(92, '4371073580', 'movil', 46),
(93, '', 'fijo', 47),
(94, '4921123394', 'movil', 47),
(95, '', 'fijo', 48),
(96, '4931097550', 'movil', 48),
(97, '3467131480', 'fijo', 49),
(98, '3461032741', 'movil', 49),
(99, '4781090122', 'fijo', 50),
(100, '4781022233', 'movil', 50),
(101, '8782022', 'fijo', 51),
(102, '4931066699', 'movil', 51),
(103, '4339350450', 'fijo', 52),
(104, '4331114997', 'movil', 52),
(105, '4921664249', 'fijo', 53),
(106, '4928704393', 'movil', 53),
(107, '8784568', 'fijo', 54),
(108, '4931245682', 'movil', 54),
(109, '4379541286', 'fijo', 55),
(110, '4921422735', 'movil', 55),
(111, '', 'fijo', 56),
(112, '4921395291', 'movil', 56),
(113, '4929240936', 'fijo', 57),
(114, '4921042077', 'movil', 57),
(115, '4639571445', 'fijo', 58),
(116, '4621031025', 'movil', 58),
(117, '', 'fijo', 59),
(118, '4941001401', 'movil', 59),
(119, '9231756', 'fijo', 60),
(120, '4921398496', 'movil', 60),
(121, '4921718540', 'fijo', 61),
(122, '4921404407', 'movil', 61),
(123, '4922011972', 'fijo', 62),
(124, '4922011972', 'movil', 62),
(125, '4927681910', 'fijo', 63),
(126, '4921139949', 'movil', 63),
(127, '9999999999', 'fijo', 64),
(128, '4921717265', 'movil', 64),
(129, '4921712015', 'fijo', 65),
(130, '4921712015', 'movil', 65),
(131, '4929234247', 'fijo', 66),
(132, '4921716599', 'movil', 66),
(133, '4789859657', 'fijo', 67),
(134, '4781005973', 'movil', 67),
(135, '9322381', 'fijo', 68),
(136, '4931387098', 'movil', 68),
(137, '4939323506', 'fijo', 69),
(138, '4931134697', 'movil', 69),
(139, '9241876', 'fijo', 70),
(140, '444927951793', 'movil', 70),
(141, '9220384', 'fijo', 71),
(142, '4921263171', 'movil', 71),
(143, '4939326874', 'fijo', 72),
(144, '4931118752', 'movil', 72),
(145, '4921275131', 'fijo', 73),
(146, '4921275131', 'movil', 73),
(147, '4576860276', 'fijo', 74),
(148, '4928930745', 'movil', 74),
(149, '1548292', 'fijo', 75),
(150, '4921294540', 'movil', 75),
(151, '4939321506', 'fijo', 76),
(152, '4931184092', 'movil', 76),
(153, '4679520985', 'fijo', 77),
(154, '4921598454', 'movil', 77),
(155, '', 'fijo', 78),
(156, '4921341697', 'movil', 78),
(157, '4579361192', 'fijo', 79),
(158, '4931241429', 'movil', 79),
(159, '4929234128', 'fijo', 80),
(160, '4921062601', 'movil', 80),
(161, '4949451394', 'fijo', 81),
(162, '4929490034', 'movil', 81),
(163, '4939592545', 'fijo', 82),
(164, '4939592545', 'movil', 82),
(165, '9224819', 'fijo', 83),
(166, '4921308462', 'movil', 83),
(167, '14929276842', 'fijo', 84),
(168, '4928702311', 'movil', 84),
(169, '1561083', 'fijo', 85),
(170, '4921157131', 'movil', 85),
(171, '4929255556', 'fijo', 86),
(172, '4921012357', 'movil', 86),
(173, '', 'fijo', 87),
(174, '4929271750', 'movil', 87),
(175, '', 'fijo', 88),
(176, '4921372940', 'movil', 88),
(177, '', 'fijo', 89),
(178, '4581107604', 'movil', 89),
(179, '4579362440', 'fijo', 90),
(180, '4571009813', 'movil', 90),
(181, '', 'fijo', 91),
(182, '4922023714', 'movil', 91),
(183, '4981123335', 'fijo', 92),
(184, '4981123335', 'movil', 92),
(185, '1544221', 'fijo', 93),
(186, '4922021785', 'movil', 93),
(187, '9231077', 'fijo', 94),
(188, '4921127573', 'movil', 94),
(189, '', 'fijo', 95),
(190, '4931375919', 'movil', 95),
(191, '', 'fijo', 96),
(192, '456291042424242', 'movil', 96),
(193, '', 'fijo', 97),
(194, '4981003050', 'movil', 97);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_asignacionrol`
--

CREATE TABLE IF NOT EXISTS `rol_asignacionrol` (
`id` int(11) NOT NULL,
  `comision` varchar(100) NOT NULL,
  `id_pasante_id` int(11) NOT NULL,
  `id_rotacion_id` int(11) NOT NULL,
  `id_tutor_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_asignacionrol`
--

INSERT INTO `rol_asignacionrol` (`id`, `comision`, `id_pasante_id`, `id_rotacion_id`, `id_tutor_id`) VALUES
(1, '', 1, 1, 1),
(2, '', 65, 1, 1),
(3, '20', 96, 1, 1),
(4, 'Hola', 97, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_configuracionasignacion`
--

CREATE TABLE IF NOT EXISTS `rol_configuracionasignacion` (
`id_configuracion` int(11) NOT NULL,
  `inicio_promocion` date NOT NULL,
  `final_promocion` date NOT NULL,
  `acuerdo_asignacion` date NOT NULL,
  `folio_count` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_configuracionasignacion`
--

INSERT INTO `rol_configuracionasignacion` (`id_configuracion`, `inicio_promocion`, `final_promocion`, `acuerdo_asignacion`, `folio_count`) VALUES
(1, '2018-02-01', '2019-01-31', '2018-01-16', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_rol`
--

CREATE TABLE IF NOT EXISTS `rol_rol` (
`id_rol` int(11) NOT NULL,
  `identificador` varchar(6) NOT NULL,
  `beca` varchar(250) NOT NULL,
  `id_generacion_fk_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_rol`
--

INSERT INTO `rol_rol` (`id_rol`, `identificador`, `beca`, `id_generacion_fk_id`) VALUES
(1, 'E18001', '1200', 1),
(4, 'F18001', '4000', 1),
(6, 'F18003', 'Sin beca', 1),
(7, 'F18004', 'Sin beca', 1),
(8, 'F18005', 'Sin beca', 1),
(9, 'F18006', '4000', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_rotacion`
--

CREATE TABLE IF NOT EXISTS `rol_rotacion` (
`id_rotacion` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `id_clinica_fk_id` int(11) NOT NULL,
  `id_rol_fk_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_rotacion`
--

INSERT INTO `rol_rotacion` (`id_rotacion`, `fecha_inicio`, `fecha_fin`, `id_clinica_fk_id`, `id_rol_fk_id`) VALUES
(1, '2018-02-01', '2018-03-31', 1, 1),
(2, '2018-02-01', '2018-04-30', 7, 4),
(3, '2018-05-01', '2019-01-31', 16, 4),
(5, '2018-02-01', '2018-05-23', 13, 6),
(6, '2018-05-24', '2019-01-31', 16, 6),
(7, '2018-02-23', '2018-02-24', 66, 7),
(8, '2018-02-01', '2018-02-28', 15, 8),
(9, '2018-02-01', '2018-02-28', 20, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_tutor`
--

CREATE TABLE IF NOT EXISTS `rol_tutor` (
`id_tutor` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `correo_electronico` varchar(255) NOT NULL,
  `cede` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_tutor`
--

INSERT INTO `rol_tutor` (`id_tutor`, `nombre`, `apellido_paterno`, `apellido_materno`, `telefono`, `correo_electronico`, `cede`) VALUES
(1, 'Sin Tutor', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_usuario`
--

CREATE TABLE IF NOT EXISTS `usuario_usuario` (
  `user_ptr_id` int(11) NOT NULL,
`id_usuario` int(11) NOT NULL,
  `matricula` varchar(8) NOT NULL,
  `contrasena` varchar(128) NOT NULL,
  `tipo_usuario` varchar(50) NOT NULL,
  `correo_electronico` varchar(250) NOT NULL,
  `correo_institucional` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `usuario_usuario`
--

INSERT INTO `usuario_usuario` (`user_ptr_id`, `id_usuario`, `matricula`, `contrasena`, `tipo_usuario`, `correo_electronico`, `correo_institucional`) VALUES
(1, 1, '34150139', 'pbkdf2_sha256$100000$owgxSN0e82KE$3jNNifNikXlI9OQcPmQvL2rSSYL5jBq/f6yji4c6nB4=', 'Pasante', 'porfirioads@gmail.com', ''),
(2, 2, '34150140', 'pbkdf2_sha256$100000$neKKID1hjR0X$09VIyXyxO91GiAWP5/OcC5PGcCpvLzPYwmlhksS5I/k=', 'Pasante', 'p34150140@gmail.com', ''),
(3, 3, '30111914', 'pbkdf2_sha256$100000$S03bL5hh7cjz$785byHf4MliZ6TFokXAfGF9K8Ga1+1GK8qefA4y9TiE=', 'Pasante', 'ositos_anahi@hotmail.es', ''),
(4, 4, '31120131', 'pbkdf2_sha256$100000$IGnIhlV9l3qd$WWY3EsZJ/uM0jAhnnVVq96Dq3O31sT9LZkMLhqxRJUg=', 'Pasante', 'nanielito9@gmail.com', 'nanielito9@gmail.com'),
(5, 5, '31120612', 'pbkdf2_sha256$100000$qzpjs89HXWXi$qpQ4wtgAmHic22JAbZtV20NUck90TtKGavzUn5E+16E=', 'Pasante', 'rojo_3210@hotmail.com', 'rojo0123adda@gmail.com'),
(6, 6, '28903365', 'pbkdf2_sha256$100000$FlQBX2XACSsH$doK+7bvt/PXgrUx9DD4yZ17ezX0C99gxEfh3iHY3uM8=', 'Pasante', 'aleinadssej4@gmail.com', 'macf_agro@hotmail.com'),
(7, 7, '30111121', 'pbkdf2_sha256$100000$JoayDo5OHBqI$eh2UeRkNs5wkUjkjhWtQwze6d88JyekS9fJ3a6qFteU=', 'Pasante', 'edygeneracionx@hotmail.com', 'cuando-hoy@hotmail.com'),
(8, 8, '31128560', 'pbkdf2_sha256$100000$jza2ZYvuVXRN$pUru+40n04RVMFRHK1NsDXrHEo21zPD89wIY4khP1SM=', 'Pasante', 'rosii_fh_9@outlook.com', ''),
(9, 9, '26704474', 'pbkdf2_sha256$100000$vlWp1E846xsB$b4K45o2YQFW00TcYPUBnYq6diwDMu7HpBrXgrBzS+yc=', 'Pasante', 'miguelrodarte03@hotmail.com', 'miguelrodarte03@hotmail.com'),
(10, 10, '31120503', 'pbkdf2_sha256$100000$Wz9p3HxUywkV$8SSiOdNg9pRm3WkP8VGP5agJyGSF/mMdgYgW6TENTuI=', 'Pasante', 'neyaguayo@gmail.com', 'ing14lopez@gmail.com'),
(11, 11, '28902282', 'pbkdf2_sha256$100000$6T7f1JKaJrwD$BzlBaJlpgvbmAxpjqrqqUoqhZ3ytSJfPsLylPBp1EZo=', 'Pasante', 'alfonso_enix21@hotmail.com', 'hanns-g93@hotmail.es'),
(12, 12, '21202738', 'pbkdf2_sha256$100000$HMOtWbA5FtDE$nwmraXTNSMnW7ekbiKYytCcnEuLECNh/GGEXsW4Khk4=', 'Pasante', 'floresguerreroraul8@gmail.com', ''),
(13, 13, '29102685', 'pbkdf2_sha256$100000$iap6LXVGO65P$9LD8jiCkE9XmTmQ1vtx1W3OX2xYW3yWLYmJXQllhXrc=', 'Pasante', 'marcegonzalezgtz@gmail.com', 'marce_gonzalez_g@hotmail.com'),
(14, 14, '28903266', 'pbkdf2_sha256$100000$j7ptguGKY2p4$Ua8Az+wkRi0pdDkveLGbG5yw/314WElhJI9B9SUfBRE=', 'Pasante', 'Omardoradohernandez@gmail.com', ''),
(15, 15, '31120218', 'pbkdf2_sha256$100000$jn80YVjmpwe0$DH4/dpASvA9UfOVHYEMQVQcvyk7jClxVXMIzTpWiDUI=', 'Pasante', 'Hummersut_85@hotmail.com', 'Br3nda0905@hotmail.com'),
(16, 16, '28902211', 'pbkdf2_sha256$100000$pAH4CHLWFv5e$w5rd+Vku50UfgdGDIAoiIgHnnZd/FVpl3tJ2nYklLvM=', 'Pasante', 'raquel13black@gmail.com', 'raquel_13black@hotmail.com'),
(17, 17, '28901734', 'pbkdf2_sha256$100000$2Q32UClhaYGh$+HMzIEA0eToCnrvnhXHYs+gW7fy8uOiAuYP9zREsH9w=', 'Pasante', 'maay.martz@gmail.com', 'my_2is@hotmail.com'),
(18, 18, '25600462', 'pbkdf2_sha256$100000$HeEtCFmn1xG6$1xwfeU94NFwrVRZwnfNIJg7XhR5bP+dv73qyi46NmQo=', 'Pasante', 'humbewita@gmail.com', ''),
(19, 19, '31120593', 'pbkdf2_sha256$100000$o2iKRqjbGcG4$i7XcUD4MBmE9DMVhpHejCPn/p2XxUcamxR9vMLwGSm8=', 'Pasante', 'yamasli73@hotmail.com', 'yamasli73@hotmail.com'),
(20, 20, '23400451', 'pbkdf2_sha256$100000$HpyXcBbZMehd$bideR9l/fIAodD6G8aQ7g9lk7JlCDqpIyUAWBAyHI7Y=', 'Pasante', 'drrafaelmurcia@hotmail.com', 'murciac4@hotmail.com'),
(21, 21, '31120581', 'pbkdf2_sha256$100000$aijljp1mAlFJ$omq+WFUgNQ3EoCSaCP7mw0SSYgsLFsas1luMmMkZ/9s=', 'Pasante', 'husher_tributo19@hotmail.com', 'barraganhac2@hotmail.com'),
(22, 22, '31120340', 'pbkdf2_sha256$100000$OBKEGZo0Z8U1$Wp/qlYsKGGPYgxbkem1beP+pcdEAi6MfPKaNTIgCeE8=', 'Pasante', 'janerodriguez08@gmail.com', 'jamarz@hotmail.com'),
(23, 23, '27804491', 'pbkdf2_sha256$100000$aLGaTdPhZ0fD$JLr1gQxRFd/bOrM4cew8v5E1q+YSM8KQsaiLKL1HKGw=', 'Pasante', 'chachin164_jaem@hotmail.com', 'tony164_jaem@icloud.com'),
(24, 24, '30111146', 'pbkdf2_sha256$100000$rTcf3a2g1ZVp$gr4TmakIm/CJ9lk4P62JC7q0KjhmriOsRqGrGJPQKtw=', 'Pasante', 'gil212012@hotmail.com', ''),
(25, 25, '31128518', 'pbkdf2_sha256$100000$9UUpGL7eqb6w$q2AW447SWSrzfe0AZAuE3+vX1Hm8EudXTsWbXOU4j10=', 'Pasante', 'jesusab1224@gmail.com', 'jj_aguilar@outlook.es'),
(26, 26, '31122238', 'pbkdf2_sha256$100000$7AiDC0Afvz5G$7sTksTJnGxPt+Cme+KPGSrNbmqVEdtn2LuJZZtDxJJ4=', 'Pasante', 'Itzall@hotmail.com', ''),
(27, 27, '20001309', 'pbkdf2_sha256$100000$MvzLwTDpo5rg$y1sl5/ebhMYd2loyvvnKiEZAgfY3kZKxfEJuATAVNf4=', 'Pasante', 'ramsesmed03@gmail.com', 'aliciaindrogs@gmail.com'),
(28, 28, '31128622', 'pbkdf2_sha256$100000$1jjbp64Nl8MN$/yS7wOgfdEEJNLYUXD9Tj4Cf7XeWaGb4ILl5nHmPuvM=', 'Pasante', 'princcess_9626@hotmail.com', 'gabi.villa@hotmail.com'),
(29, 29, '29106463', 'pbkdf2_sha256$100000$RIrG8cPcgi5z$kUFcaV0I1aoHL2rXK+S4NAwQ90bXiBY7RKgM3MYcdcM=', 'Pasante', 'anahiesqueda12@gmail.com', ''),
(30, 30, '24501180', 'pbkdf2_sha256$100000$bF2m8jvRrZo5$DrV4SZ2/Z6NqoNf5ZN6iBwvt+iUGJOBeEB0d/L6Irko=', 'Pasante', 'adry_kro@hotmail.com', ''),
(31, 31, '27801147', 'pbkdf2_sha256$100000$OnJnp7TwFk9K$wyO1Z5oCgVHM16qiaqm2RvVtuDmqq/T63l+vgxZWn0c=', 'Pasante', 'jackiie_0511@hotmail.com', ''),
(32, 32, '31120571', 'pbkdf2_sha256$100000$iCv4QyXkPjBg$a+EX6uWfr5iXASYeRNSIGrcjiQwCUGf5rkuSawkGE4A=', 'Pasante', 'cidruvda@gmail.com', ''),
(33, 33, '31120339', 'pbkdf2_sha256$100000$5h2DNTN6ciAu$tO07YSduJH5zZqA7piyHfqBc3AsQ0sw1iRTjB/2ygAA=', 'Pasante', 'rociogallegosm@hotmail.com', 'haamanalandya@gmail.com'),
(34, 34, '28901405', 'pbkdf2_sha256$100000$MuMxVoRNcl8M$W73CyzHt3tJlaBWM+zrarCgiRjUVCdV++0O6Vg5axK4=', 'Pasante', 'PANDA_LEYD@HOTMAIL.COM', ''),
(35, 35, '27803467', 'pbkdf2_sha256$100000$UrKHANdQJzxp$Q4GzhpylsIsLmjkuKaedgew6DTJRsLNmgvHU7tcrRoc=', 'Pasante', 'sandy_fry27@outlook.com', 'cecy048@hotmail.com'),
(36, 36, '31128569', 'pbkdf2_sha256$100000$E8jBUUwKIeo2$eupC6/lX3qCRR/N/+W1f13jxzS9vU0/WHv9Jyp2sTVc=', 'Pasante', 'sarmedith.15@hotmail.com', 'rosas_rojas_9389@hotmail.com'),
(37, 37, '31128554', 'pbkdf2_sha256$100000$8p7i1S6Vlrmn$KP4OZSUla4tgHX0JT5rP6tPmd0j6/F9KHtLNwhHsZHo=', 'Pasante', 'nalta_09@hotmail.com', 'popy_0894@hotmail.com'),
(38, 38, '28903408', 'pbkdf2_sha256$100000$x13uwwtMQBJE$C5TF7pFjv5y/En0Q+omIofHuj/q84FGUNnRlIRxJBIs=', 'Pasante', 'valeri_qr@hotmail.com', 'mari_rod2011@hotmail.com'),
(39, 39, '31120584', 'pbkdf2_sha256$100000$b7Gk7mwbqOHD$XdD8wik/rBRQhoWV6Gx4HTgTGGJpcCX6rhrNNzPxeGA=', 'Pasante', 'anavaleriaavila@outlook.com', ''),
(40, 40, '31128578', 'pbkdf2_sha256$100000$g2fXGGl3nYlv$ZyP/V0a9NV2W0xIz1sZZ9kJ40rvDsEJTReJ+lhhexlo=', 'Pasante', 'emmy_trejo@hotmail.com', 'emmy_trejo@hotmail.com'),
(41, 41, '27803268', 'pbkdf2_sha256$100000$2GBZkc5Fqohc$DqmvLeKN5716Mc3lsTt4Kl+N6ug3TFyw1FdSgBp+NhQ=', 'Pasante', 'lucha-lmcj@hotmail.com', 'lucy_cajayme@hotmail.com'),
(42, 42, '29107068', 'pbkdf2_sha256$100000$b9RtBjciC72I$Wvsy/VCNCl5zSkOlUPJmL4fbhg4ZO92VChwNKv+0HdI=', 'Pasante', 'yuliet_soto@yahoo.com.mx', ''),
(43, 43, '26701204', 'pbkdf2_sha256$100000$X21utyMfXLTv$zCv4aE+tdw7KlFtzU/qHcPgLdlbJ2SsvZ5iix4sGDEI=', 'Pasante', 'drafatimaenciso7@gmail.com', 'fatimocha77@gmail.com'),
(44, 44, '28903316', 'pbkdf2_sha256$100000$N2wnrdMvAPJt$/p2qHOkN7uzx/JG2fmmG44c/jHDwgz4jr03pA6iu4Ow=', 'Pasante', 'dagohigh@gmail.com', 'dagohigh@gmail.com'),
(45, 45, '25603244', 'pbkdf2_sha256$100000$5kuoAFcphXs9$IFK8GD/ItAFCHSZvIi5994Kr2d8pEljZvSiO4mENk3o=', 'Pasante', 'sarahdezybarra@gmail.com', 'apol_zahisar@hotmail.com'),
(46, 46, '30117052', 'pbkdf2_sha256$100000$RkVB5DvNnY56$KcyBFEUbi37Wi4cGLCFizmooAVrYyRtwywnFmeKh5sQ=', 'Pasante', 'Joeni.91@hotmail.com', ''),
(47, 47, '31128598', 'pbkdf2_sha256$100000$sCQBggIhfh4s$l6jCLIWJeo083QrHbU4+P4v5f1JxaybpuDBkLWOlMLo=', 'Pasante', 'raflenike7@gmail.com', ''),
(48, 48, '31128589', 'pbkdf2_sha256$100000$K6e5VWMeSOSN$rIkF8I0pOWxBdFWMckv77RO7Rg89XEBE7Qj3YyfCqOI=', 'Pasante', 'jorgeluis746@hotmail.com', 'jorgeluismedina746@gmail.com'),
(49, 49, '27802319', 'pbkdf2_sha256$100000$F56x8QIw1sae$KWh9lEwG9ZyV2q7fcL/LtgwTTNwTEZLeX5B4U1XekGc=', 'Pasante', 'rotciv_sosa@hotmail.com', 'valeri_qr@hotmail.com'),
(50, 50, '31128596', 'pbkdf2_sha256$100000$eFzidIOd3pxr$24f8MUyqdU94eSBwjWSx9SJku+tRjOxfQPJ9Q7zZIWY=', 'Pasante', 'marthitaramos290793@gmail.com', 'mar_ive29@hotmail.com'),
(51, 51, '31128561', 'pbkdf2_sha256$100000$1qkK4wpWQkrr$fPZylQ+2tmUF3bQH98dNECMRisQ301zaOlpG4ebHbO4=', 'Pasante', 'Javi_250193@hotmail.com', 'Javi_250193@hotmail.com'),
(52, 52, '26700153', 'pbkdf2_sha256$100000$bZ3jqkJO3lFa$k67qkdlxXzThtHfBt5NVqFJQOrzAk2PJN71ZPxOWQXQ=', 'Pasante', 'lourdesluis19@gmail.com', 'lourdesluis19@gmail.com'),
(53, 53, '27803439', 'pbkdf2_sha256$100000$uxXQfB4SFQOt$2ztkDL9SaLTHYwUdFUOu89Yq+Qd/YQmwHpxAtjSeuMI=', 'Pasante', 'andiheres605@gmail.com', 'andiheres605@gmail.com'),
(54, 54, '30111414', 'pbkdf2_sha256$100000$6SQzhmR4f0rT$MOcwdtmjsVmKQSHcbjb0gmTdzuSREwh92FKvxjJofPA=', 'Pasante', 'Omaar-mtz@outlook.es', ''),
(55, 55, '28903096', 'pbkdf2_sha256$100000$80mRQPD7J2wO$OGpK0/JeVzGrjD/Z7qFaLGUOEK7SWqnZBPpR5luffJk=', 'Pasante', 'susan.med26@gmail.com', 'susan_princes26@hotmail.com'),
(56, 56, '31128699', 'pbkdf2_sha256$100000$0q0dpTzUPS7K$pvbLVILbvrp6+k4IZWypmhE+W18pjTmcCTKOgueXyew=', 'Pasante', 'isratp07@gmail.com', 'isratp07@gmail.com'),
(57, 57, '28902514', 'pbkdf2_sha256$100000$Kdzg0DeGeBAt$3YZT031dvgxnlcitua/nk8moOp3haGJcEPuirlrNWLc=', 'Pasante', 'pamela.12.23@outlook.com', 'hectorpereyr@hotmail.com'),
(58, 58, '28903793', 'pbkdf2_sha256$100000$G7eitrMFpweV$TgIionaZZgtLHYM0/lyauiJS8Mr+gkUdmzw/0sRzHX0=', 'Pasante', 'abigail.gomez.val@gmail.com', ''),
(59, 59, '31120572', 'pbkdf2_sha256$100000$6FfFIiQ7Mbyy$oiu/CuRYPZzYauZBPmU5NojrH8NT9lMSW4H74am4Hys=', 'Pasante', 'milie_91mary@hotmail.com', ''),
(60, 60, '31120576', 'pbkdf2_sha256$100000$9C6pw3rVH1eK$PMRpAo6sIaa+5ucNX5viw5/7d7c3NQWAv8VtmVPmZcE=', 'Pasante', 'nataliaurit@gmail.com', 'Gerardocabrerapatron@hotmail.com'),
(61, 61, '28903247', 'pbkdf2_sha256$100000$FETXKJyInKS8$m7AQu0ELk+ikZAhQDxT713l3iSprI9KXXenh3klYCBQ=', 'Pasante', 'eddla_rosa@hotmail.com', ''),
(62, 62, '31128591', 'pbkdf2_sha256$100000$2hBbo0Uu1fht$g6HRqYDL6cw4LV74gMzRJBAtOE0qaKbwdAk7+ewvwuU=', 'Pasante', 'leche_06@hotmail.com', 'leche_06@hotmail.com'),
(63, 63, '31128565', 'pbkdf2_sha256$100000$lAmgssHBcsTN$NrDWsejDXP6okzUHSeByDAptBNuWqhqmjgAOR74X7QU=', 'Pasante', 'nchdh1234@hotmail.com', 'caro.hdz.h@hotmail.com'),
(64, 64, '31128594', 'pbkdf2_sha256$100000$HsnUhSMaOnZi$JMDJ7wID28HWfiT/vNtxo2IWQ2aOhYNdA+4qoLmbB6w=', 'Pasante', 'sonni_pej93@hotmail.com', 'sonni_pej93@hotmail.com'),
(65, 65, '31128557', 'pbkdf2_sha256$100000$UnD6gyDLD9Sc$meGS80D10Yuif0//w9SI/xOk9Pv29xRW494kx6hWIPI=', 'Pasante', 'pao_cz27@hotmail.com', 'pao_cz27@hotmail.com'),
(66, 66, '31128588', 'pbkdf2_sha256$100000$M6UnA4vXpWPL$FxqWgDb+TkyE8hW/z9JrkO+v8oNxG84u0MviRMyBdKU=', 'Pasante', 'mirna_3r@hotmail.com', ''),
(67, 67, '25600194', 'pbkdf2_sha256$100000$DdvnewJu1w2O$aGFPyKxLkTmlu8nGZ7Ny7yQN/BpPpzYWDKBerH7CtRo=', 'Pasante', 'monse_carom@hotmail.com', ''),
(68, 68, '31120652', 'pbkdf2_sha256$100000$V2VerybtqQH8$OKwzobtVz+pDuukRRVwzA33WSM9qRyLSmWpe8c1SN54=', 'Pasante', 'e_yesenia_14@hotmail.com', 'e.yezzi.14@gmail.com'),
(69, 69, '31120610', 'pbkdf2_sha256$100000$uaZu3QNHNsEA$TAMiFZC3TQWJlZUmZOBSJgEoT40ZLcoXwtuH7zbTr98=', 'Pasante', 'jev.espinosa@gmail.com', ''),
(70, 70, '27801012', 'pbkdf2_sha256$100000$Qa2c1nichrW4$6Y/TjEgoq82Tlk+ws5XzBYpLsBO0ULLYqTcHkUPEWE8=', 'Pasante', 'ma-ri-anaespinodiazmariana@hotmail.com', 'ma-ri-anaespinodiazmariana@hotmail.com'),
(71, 71, '31128563', 'pbkdf2_sha256$100000$AG7BCrerDSFE$qw+D15xNqyK9J2MKVW8lOHmdaK01oktxHzMjjAmgr8I=', 'Pasante', 'claudiozapatadr9@gmail.com', 'pollochikenss7@gmail.com'),
(72, 72, '31120653', 'pbkdf2_sha256$100000$tONPfBayva2s$+sgRr9jq8Du9lg1YxsG/uxstGZJWjspeUKpdrcz8G4Y=', 'Pasante', 'david_dx10@hotmail.com', 'david_dx10@hotmail.com'),
(73, 73, '31128592', 'pbkdf2_sha256$100000$WGSwvDj2Fc9H$xRKob+Xke9tiuzLfBGcHgAaQSZodJ1sQKyZ6xc6bsgM=', 'Pasante', 'blancaort09072@gmail.com', 'agetro0907@hotmail.com'),
(74, 74, '31120366', 'pbkdf2_sha256$100000$oESVA9ULtBEN$5VRmIaE0d1IRR5fsUO1AAnhw4JVoIk3TabBnSrAPQzw=', 'Pasante', 'nahomauri@hotmail.com', 'marv700403@hotmail.com'),
(75, 75, '28903274', 'pbkdf2_sha256$100000$pBsCBj42ytrI$I/H8VpXMKgVev7R2YUmaAoORCDHZkLla1sEunRhsH3g=', 'Pasante', 'mederi_ormuz@hotmail.com', 'demmikime@hotmail.com'),
(76, 76, '26704157', 'pbkdf2_sha256$100000$LJy6xVrElV7v$3QuMucGNy2uDcDgidN1SOKg2S7YWgjtn73Ym771Yq1c=', 'Pasante', 'lalo.rules.90@gmail.com', ''),
(77, 77, '31120601', 'pbkdf2_sha256$100000$OFI1UVE2i5bd$6qav8dCtNf/HWNNabYwuNppBCyWt4HkIE7uvDFeSqqI=', 'Pasante', 'Temifdz3192@gmail.com', 'Copiringo@hotmail.com'),
(78, 78, '31128564', 'pbkdf2_sha256$100000$25k6AttFEvPu$PkiwpAzVT75pfjx4w9yDlIointmQ51YVmWEZ7cL48BA=', 'Pasante', 'nana_adi14@hotmail.com', ''),
(79, 79, '30117758', 'pbkdf2_sha256$100000$mRb0mjIbqVM4$0HEwH0uLDbPmr91alvdo2aFkktAgNsfYOEk1GVRT9aI=', 'Pasante', 'gloriaa_1988@hotmail.com', 'gloriaa_1988@hotmail.com'),
(80, 80, '30111379', 'pbkdf2_sha256$100000$k6o328WFooKz$Bi7NFzo1lw8oacI7ML5/M3QXsZKUcGBl8ifOZBzjhyI=', 'Pasante', 'lufeliz18@hotmail.com', 'luisfelipecazgo@gmail.com'),
(81, 81, '31128517', 'pbkdf2_sha256$100000$hyQrfqbfQsi8$NSXOAccoKjagtTUiWm2kOfmLu/MNKgoZ6isNK+s3/9Q=', 'Pasante', 'eriaco93@hotmail.com', 'sanchezperez_teresa@yahoo.com'),
(82, 82, '30114945', 'pbkdf2_sha256$100000$aKcs4vxh0nHG$l/x5vgRVYn64+u6a48yw62s6mfP+SMBxsxv3hQGJDSM=', 'Pasante', 'adrijauregui.more@gmail.com', ''),
(83, 83, '31128570', 'pbkdf2_sha256$100000$50tNOc15ymKJ$JXkTS23B0N9/CaIFG6zE2vhkGUNfl1uanlBebxpesZQ=', 'Pasante', 'clau24_sl@live.com.mx', 'totorayas@gmail.com'),
(84, 84, '25600569', 'pbkdf2_sha256$100000$mRuCRx9MNomc$9NXih7JtUxVWlKwnwjwYmVJUDp1d/onCkWsR5N+Iiks=', 'Pasante', 'omarjimeneznavarro@yahoo.com', 'omaronwater@yahoo.com'),
(85, 85, '28903288', 'pbkdf2_sha256$100000$P9WlSkbGOKDT$7dxl7xHn1mRXxe5nnHpYt78n1NzkAOGDuy8yyla7b3Y=', 'Pasante', 'hannalu093@gmail.com', 'peter_marquez_92@hotmail.com'),
(86, 86, '26704178', 'pbkdf2_sha256$100000$XAmHfjCc79nq$aDzdR6E0p38dz3D5ij6AOomj8XvO70MtmdPFDaQvrmY=', 'Pasante', 'pelon_chaos@hotmail.com', 'pelonchaos33@gmail.com'),
(87, 87, '28902509', 'pbkdf2_sha256$100000$vSKweLOxXv9R$l4QSG4upKXjXxbtagjCINqFoI2ZdApMo6HNt/FfOaP8=', 'Pasante', 'aguilera_024@hotmail.com', 'karo_ztar1602@hotmail.com'),
(88, 88, '28904489', 'pbkdf2_sha256$100000$ETm83PNkcye1$gktHhHc+yxRVLFvupqqmpUaS7r4TTDae4PurVmfOy00=', 'Pasante', 'chris_ku21@hotmail.com', 'chirs.salas.ku@gmail.com'),
(89, 89, '27801077', 'pbkdf2_sha256$100000$d6AetVJvBJzd$NBZvYBtWmNIUuoj+nK+xn5Y3UD/jZp9O8VTXRWbuLn8=', 'Pasante', 'juanito_elmejor10@hotmail.com', 'vero_123barajas@hotmail.com'),
(90, 90, '28902499', 'pbkdf2_sha256$100000$7tRMDzRsoO6n$mcSBiPawaN2WFf7hWC2rPa3iIaqEt8c6fZh51rgcL7M=', 'Pasante', 'newcocomil_500@hotmail.com', 'newcocomil500@gmail.com'),
(91, 91, '31120341', 'pbkdf2_sha256$100000$PXzojrfNTBVG$jP1FN+RvOSWOLzEjqifWKm/qkr328skJdSTrF0anPOI=', 'Pasante', 'chavagar_666@hotmail.com', ''),
(92, 92, '24502065', 'pbkdf2_sha256$100000$6BakoN51nKXh$og+gB5dqD7mH5VRoRZUeSpHrvlEzzfMPUqpVxCmfh4Y=', 'Pasante', 'Pablin_156@hotmail.com', ''),
(93, 93, '26702463', 'pbkdf2_sha256$100000$FkW2alWw4Egb$rfkiun2NIcLaEQad76q8F9R/2Myse3ZRaMY7gGVTDVw=', 'Pasante', 'maritza07zac@hotmail.com', 'maritza07zac@hotmail.com'),
(94, 94, '29104742', 'pbkdf2_sha256$100000$02mGcOh2sk9b$ABNuvSZaPFIwQoJXnDBf4mhVbMtZl4nSZDdmxstJTDc=', 'Pasante', 'alan_55kaeto@hotmail.com', ''),
(95, 95, '31128599', 'pbkdf2_sha256$100000$lpCW1DiPILwi$F3B4KdXBx+9bhRq6/5LLx1RVLTLuVNfFzWp6ypwkcd8=', 'Pasante', 'mimbelobo123@hotmail.com', ''),
(96, 96, '30111886', 'pbkdf2_sha256$100000$Zn3R9y3dNfCd$Z+EihzOF31Hb5IudisIj3BiBTpx4uJS+miIaK50YnVQ=', 'Pasante', 'kamopeag@hotmail.com', 'kamopeag@gmail.com'),
(97, 97, '34150161', 'pbkdf2_sha256$100000$uahRfyWHd7kD$rErog5mog+zOcs5buowNK88k4HW9As78JnJcKdmDMGA=', 'Pasante', 'alan-anibal@hotmail.com', ''),
(98, 98, '31125138', 'pbkdf2_sha256$100000$YrVtTgRsRPos$Cdbz1HdpGedif4u68cWIIi99g+PgJ1JXrjtqolsbmlk=', 'Pasante', 'ise@gmail.com', ''),
(99, 99, '27800814', 'pbkdf2_sha256$100000$LpW3ajypX6uh$t+/5iiNZBg6+JvChweqXo8RWqMoLpSlW7B4JeKl0iIs=', 'Pasante', 'favb.tkd.2012@gmail.com', 'cristorey_6@hotmail.com'),
(100, 100, '29103147', 'pbkdf2_sha256$100000$rD5YP82iEQDX$5abSJDuRbKhs+niJBqbAf7sv+Ct8AeKn9RCinp5ZUdw=', 'Pasante', 'Md.fluac.91@gmail.com', 'M_fluac_91@hotmail.com'),
(101, 101, '27803347', 'pbkdf2_sha256$100000$wSiVkuPQDGM0$8C4pp/osELZF/WR2hiY5ytbQbOGyB1yplPDEUue8/jU=', 'Pasante', 'esrome79@gmail.com', 'escobedoguillermo79@gmail.com'),
(102, 102, '30117409', 'pbkdf2_sha256$100000$Mh0bswBykLqQ$W421RuhhB3J8RfieFNW4141RFmiX+dXolKOEnXfQyvQ=', 'Pasante', 'elodiaflores65@gmail.com', 'amora.ef@gmail.com'),
(103, 103, '27803665', 'pbkdf2_sha256$100000$0NlIJWu0nKVM$a8UhfH2X+OTw2z+NKPDHywLEFYdbjIcx0/cpVZyR53w=', 'Pasante', 'elenaulag25@gmail.com', 'luzanaulloa@gmail.com'),
(104, 104, '34150282', 'pbkdf2_sha256$100000$UbXs6t3Lk1io$gF69J8M/8e/BoB0ObjL2Vd+GjAkau7Y5zm3KMB+Csaw=', 'Pasante', 'johan_longoria13@hotmail.com', ''),
(105, 105, '31123451', 'pbkdf2_sha256$100000$A97dLkldXP48$nybCIT9vvF+rNJzhB/qxlzKgIolV9a1Q9IFuX/ns7L4=', 'Pasante', 'homar_karim10@hotmail.com', ''),
(106, 106, '37181633', 'pbkdf2_sha256$100000$Eil8l4qQz8aS$FHdwmud/4yFIjNuSkBcdigSnczqyhXu2jqY+5yG3+90=', 'Pasante', 'akcire.ramos12@gmail.com', 'todos_unidos12@outlook.com'),
(107, 107, '30114924', 'pbkdf2_sha256$100000$ygDJnN1CxYUA$W3G2wWOnCQLJ99sZAOE6BOT2DScJNdNPbWqJvbxAMjg=', 'Pasante', 'fontenaak@gmail.com', 'fontenaak@gmail.com');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`), ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indices de la tabla `auth_user`
--
ALTER TABLE `auth_user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`);

--
-- Indices de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`), ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`), ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indices de la tabla `clinica_clinica`
--
ALTER TABLE `clinica_clinica`
 ADD PRIMARY KEY (`id_clinica`), ADD KEY `clinica_clinica_id_direccion_fk_id_da262c2c_fk_clinica_d` (`id_direccion_fk_id`), ADD KEY `clinica_clinica_id_institucion_fk_id_3fdd207f_fk_clinica_i` (`id_institucion_fk_id`);

--
-- Indices de la tabla `clinica_direccionclinica`
--
ALTER TABLE `clinica_direccionclinica`
 ADD PRIMARY KEY (`id_direccion`), ADD KEY `clinica_direccioncli_estado_id_9582eccc_fk_coordinad` (`estado_id`), ADD KEY `clinica_direccioncli_municipio_id_3e6a532b_fk_coordinad` (`municipio_id`);

--
-- Indices de la tabla `clinica_institucion`
--
ALTER TABLE `clinica_institucion`
 ADD PRIMARY KEY (`id_institucion`);

--
-- Indices de la tabla `contactos_pasante_contactospasante`
--
ALTER TABLE `contactos_pasante_contactospasante`
 ADD PRIMARY KEY (`id_contacto`), ADD KEY `contactos_pasante_co_id_pasante_fk_id_41442e59_fk_pasante_p` (`id_pasante_fk_id`);

--
-- Indices de la tabla `coordinador_asignacionclinicapasante`
--
ALTER TABLE `coordinador_asignacionclinicapasante`
 ADD PRIMARY KEY (`id_asignacion`), ADD KEY `coordinador_asignaci_id_clinica_fk_id_b65a53a6_fk_clinica_c` (`id_clinica_fk_id`), ADD KEY `coordinador_asignaci_id_pasante_fk_id_3c152b9f_fk_pasante_p` (`id_pasante_fk_id`);

--
-- Indices de la tabla `coordinador_catalogoestatusinconformidad`
--
ALTER TABLE `coordinador_catalogoestatusinconformidad`
 ADD PRIMARY KEY (`id_estatus`);

--
-- Indices de la tabla `coordinador_catalogotipoinconformidad`
--
ALTER TABLE `coordinador_catalogotipoinconformidad`
 ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `coordinador_comentariosinconformidad`
--
ALTER TABLE `coordinador_comentariosinconformidad`
 ADD PRIMARY KEY (`id_comentario`), ADD KEY `coordinador_comentar_id_reporte_id_3ef5d9ad_fk_coordinad` (`id_reporte_id`);

--
-- Indices de la tabla `coordinador_coordinador`
--
ALTER TABLE `coordinador_coordinador`
 ADD PRIMARY KEY (`id_coordinador`), ADD KEY `coordinador_coordina_matricula_id_07bc64bd_fk_usuario_u` (`matricula_id`);

--
-- Indices de la tabla `coordinador_estado`
--
ALTER TABLE `coordinador_estado`
 ADD PRIMARY KEY (`id_estado`);

--
-- Indices de la tabla `coordinador_fecha_registro`
--
ALTER TABLE `coordinador_fecha_registro`
 ADD PRIMARY KEY (`id_registro`);

--
-- Indices de la tabla `coordinador_informacion`
--
ALTER TABLE `coordinador_informacion`
 ADD PRIMARY KEY (`id_informacion`);

--
-- Indices de la tabla `coordinador_municipio`
--
ALTER TABLE `coordinador_municipio`
 ADD PRIMARY KEY (`id_municipio`), ADD KEY `coordinador_municipi_id_estado_id_a9ca3ced_fk_coordinad` (`id_estado_id`);

--
-- Indices de la tabla `coordinador_reporteinconformidad`
--
ALTER TABLE `coordinador_reporteinconformidad`
 ADD PRIMARY KEY (`id_reporte`), ADD KEY `coordinador_reportei_id_clinica_fk_id_9bff4fef_fk_clinica_c` (`id_clinica_fk_id`), ADD KEY `coordinador_reportei_id_pasante_fk_id_7ba8ee91_fk_pasante_p` (`id_pasante_fk_id`), ADD KEY `coordinador_reportei_id_status_fk_id_19fa40e9_fk_coordinad` (`id_status_fk_id`), ADD KEY `coordinador_reportei_id_tipo_inconformida_5a8ccc88_fk_coordinad` (`id_tipo_inconformidad_FK_id`);

--
-- Indices de la tabla `coordinador_seguimiento`
--
ALTER TABLE `coordinador_seguimiento`
 ADD PRIMARY KEY (`id`), ADD KEY `coordinador_seguimie_id_reporte_id_2acbdd54_fk_coordinad` (`id_reporte_id`), ADD KEY `coordinador_seguimie_id_usuario_id_3a693a36_fk_usuario_u` (`id_usuario_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
 ADD PRIMARY KEY (`id`), ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`), ADD KEY `django_admin_log_user_id_c564eba6_fk` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
 ADD PRIMARY KEY (`session_key`), ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indices de la tabla `pasante_datosacademicos`
--
ALTER TABLE `pasante_datosacademicos`
 ADD PRIMARY KEY (`id_datos_academicos`);

--
-- Indices de la tabla `pasante_direccionpasante`
--
ALTER TABLE `pasante_direccionpasante`
 ADD PRIMARY KEY (`id_direccion`), ADD KEY `pasante_direccionpas_estado_id_4e1235f9_fk_coordinad` (`estado_id`), ADD KEY `pasante_direccionpas_municipio_id_ef4952b3_fk_coordinad` (`municipio_id`);

--
-- Indices de la tabla `pasante_generacion`
--
ALTER TABLE `pasante_generacion`
 ADD PRIMARY KEY (`id_generacion`);

--
-- Indices de la tabla `pasante_pasante`
--
ALTER TABLE `pasante_pasante`
 ADD PRIMARY KEY (`id_pasante`), ADD KEY `pasante_pasante_id_datos_academicos__07e74308_fk_pasante_d` (`id_datos_academicos_fk_id`), ADD KEY `pasante_pasante_id_direccion_fk_id_2a75e66e_fk_pasante_d` (`id_direccion_fk_id`), ADD KEY `pasante_pasante_id_usuario_fk_id_321dd118_fk_usuario_u` (`id_usuario_fk_id`);

--
-- Indices de la tabla `pasante_promocion`
--
ALTER TABLE `pasante_promocion`
 ADD PRIMARY KEY (`id_promocion`), ADD KEY `pasante_promocion_id_generacion_fk_id_e42d587f_fk_pasante_g` (`id_generacion_fk_id`), ADD KEY `pasante_promocion_id_pasante_fk_id_712b8012_fk_pasante_p` (`id_pasante_fk_id`);

--
-- Indices de la tabla `pasante_telefono`
--
ALTER TABLE `pasante_telefono`
 ADD PRIMARY KEY (`id_telefono`), ADD KEY `pasante_telefono_id_pasante_fk_id_e390ce22_fk_pasante_p` (`id_pasante_fk_id`);

--
-- Indices de la tabla `rol_asignacionrol`
--
ALTER TABLE `rol_asignacionrol`
 ADD PRIMARY KEY (`id`), ADD KEY `rol_asignacionrol_id_pasante_id_2e8ce594_fk_pasante_p` (`id_pasante_id`), ADD KEY `rol_asignacionrol_id_rotacion_id_21e48bac_fk_rol_rotac` (`id_rotacion_id`), ADD KEY `rol_asignacionrol_id_tutor_id_937cbf36_fk_rol_tutor_id_tutor` (`id_tutor_id`);

--
-- Indices de la tabla `rol_configuracionasignacion`
--
ALTER TABLE `rol_configuracionasignacion`
 ADD PRIMARY KEY (`id_configuracion`);

--
-- Indices de la tabla `rol_rol`
--
ALTER TABLE `rol_rol`
 ADD PRIMARY KEY (`id_rol`), ADD UNIQUE KEY `identificador` (`identificador`), ADD KEY `rol_rol_id_generacion_fk_id_e29cbf3a_fk_pasante_g` (`id_generacion_fk_id`);

--
-- Indices de la tabla `rol_rotacion`
--
ALTER TABLE `rol_rotacion`
 ADD PRIMARY KEY (`id_rotacion`), ADD KEY `rol_rotacion_id_clinica_fk_id_1187f851_fk_clinica_c` (`id_clinica_fk_id`), ADD KEY `rol_rotacion_id_rol_fk_id_9ccaaa3c_fk_rol_rol_id_rol` (`id_rol_fk_id`);

--
-- Indices de la tabla `rol_tutor`
--
ALTER TABLE `rol_tutor`
 ADD PRIMARY KEY (`id_tutor`), ADD UNIQUE KEY `correo_electronico` (`correo_electronico`);

--
-- Indices de la tabla `usuario_usuario`
--
ALTER TABLE `usuario_usuario`
 ADD PRIMARY KEY (`id_usuario`), ADD UNIQUE KEY `user_ptr_id` (`user_ptr_id`), ADD UNIQUE KEY `matricula` (`matricula`), ADD UNIQUE KEY `correo_electronico` (`correo_electronico`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT de la tabla `auth_user`
--
ALTER TABLE `auth_user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- AUTO_INCREMENT de la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `clinica_clinica`
--
ALTER TABLE `clinica_clinica`
MODIFY `id_clinica` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `clinica_direccionclinica`
--
ALTER TABLE `clinica_direccionclinica`
MODIFY `id_direccion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT de la tabla `clinica_institucion`
--
ALTER TABLE `clinica_institucion`
MODIFY `id_institucion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `contactos_pasante_contactospasante`
--
ALTER TABLE `contactos_pasante_contactospasante`
MODIFY `id_contacto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT de la tabla `coordinador_asignacionclinicapasante`
--
ALTER TABLE `coordinador_asignacionclinicapasante`
MODIFY `id_asignacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `coordinador_catalogoestatusinconformidad`
--
ALTER TABLE `coordinador_catalogoestatusinconformidad`
MODIFY `id_estatus` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `coordinador_catalogotipoinconformidad`
--
ALTER TABLE `coordinador_catalogotipoinconformidad`
MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `coordinador_comentariosinconformidad`
--
ALTER TABLE `coordinador_comentariosinconformidad`
MODIFY `id_comentario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `coordinador_coordinador`
--
ALTER TABLE `coordinador_coordinador`
MODIFY `id_coordinador` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `coordinador_estado`
--
ALTER TABLE `coordinador_estado`
MODIFY `id_estado` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `coordinador_fecha_registro`
--
ALTER TABLE `coordinador_fecha_registro`
MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `coordinador_informacion`
--
ALTER TABLE `coordinador_informacion`
MODIFY `id_informacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `coordinador_municipio`
--
ALTER TABLE `coordinador_municipio`
MODIFY `id_municipio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2493;
--
-- AUTO_INCREMENT de la tabla `coordinador_reporteinconformidad`
--
ALTER TABLE `coordinador_reporteinconformidad`
MODIFY `id_reporte` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `coordinador_seguimiento`
--
ALTER TABLE `coordinador_seguimiento`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `pasante_datosacademicos`
--
ALTER TABLE `pasante_datosacademicos`
MODIFY `id_datos_academicos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT de la tabla `pasante_direccionpasante`
--
ALTER TABLE `pasante_direccionpasante`
MODIFY `id_direccion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT de la tabla `pasante_generacion`
--
ALTER TABLE `pasante_generacion`
MODIFY `id_generacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `pasante_pasante`
--
ALTER TABLE `pasante_pasante`
MODIFY `id_pasante` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT de la tabla `pasante_promocion`
--
ALTER TABLE `pasante_promocion`
MODIFY `id_promocion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT de la tabla `pasante_telefono`
--
ALTER TABLE `pasante_telefono`
MODIFY `id_telefono` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=195;
--
-- AUTO_INCREMENT de la tabla `rol_asignacionrol`
--
ALTER TABLE `rol_asignacionrol`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `rol_configuracionasignacion`
--
ALTER TABLE `rol_configuracionasignacion`
MODIFY `id_configuracion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `rol_rol`
--
ALTER TABLE `rol_rol`
MODIFY `id_rol` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `rol_rotacion`
--
ALTER TABLE `rol_rotacion`
MODIFY `id_rotacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `rol_tutor`
--
ALTER TABLE `rol_tutor`
MODIFY `id_tutor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `usuario_usuario`
--
ALTER TABLE `usuario_usuario`
MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=108;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Filtros para la tabla `clinica_clinica`
--
ALTER TABLE `clinica_clinica`
ADD CONSTRAINT `clinica_clinica_id_institucion_fk_id_3fdd207f_fk_clinica_i` FOREIGN KEY (`id_institucion_fk_id`) REFERENCES `clinica_institucion` (`id_institucion`),
ADD CONSTRAINT `clinica_clinica_id_direccion_fk_id_da262c2c_fk_clinica_d` FOREIGN KEY (`id_direccion_fk_id`) REFERENCES `clinica_direccionclinica` (`id_direccion`);

--
-- Filtros para la tabla `clinica_direccionclinica`
--
ALTER TABLE `clinica_direccionclinica`
ADD CONSTRAINT `clinica_direccioncli_municipio_id_3e6a532b_fk_coordinad` FOREIGN KEY (`municipio_id`) REFERENCES `coordinador_municipio` (`id_municipio`),
ADD CONSTRAINT `clinica_direccioncli_estado_id_9582eccc_fk_coordinad` FOREIGN KEY (`estado_id`) REFERENCES `coordinador_estado` (`id_estado`);

--
-- Filtros para la tabla `contactos_pasante_contactospasante`
--
ALTER TABLE `contactos_pasante_contactospasante`
ADD CONSTRAINT `contactos_pasante_co_id_pasante_fk_id_41442e59_fk_pasante_p` FOREIGN KEY (`id_pasante_fk_id`) REFERENCES `pasante_pasante` (`id_pasante`);

--
-- Filtros para la tabla `coordinador_asignacionclinicapasante`
--
ALTER TABLE `coordinador_asignacionclinicapasante`
ADD CONSTRAINT `coordinador_asignaci_id_pasante_fk_id_3c152b9f_fk_pasante_p` FOREIGN KEY (`id_pasante_fk_id`) REFERENCES `pasante_pasante` (`id_pasante`),
ADD CONSTRAINT `coordinador_asignaci_id_clinica_fk_id_b65a53a6_fk_clinica_c` FOREIGN KEY (`id_clinica_fk_id`) REFERENCES `clinica_clinica` (`id_clinica`);

--
-- Filtros para la tabla `coordinador_comentariosinconformidad`
--
ALTER TABLE `coordinador_comentariosinconformidad`
ADD CONSTRAINT `coordinador_comentar_id_reporte_id_3ef5d9ad_fk_coordinad` FOREIGN KEY (`id_reporte_id`) REFERENCES `coordinador_reporteinconformidad` (`id_reporte`);

--
-- Filtros para la tabla `coordinador_coordinador`
--
ALTER TABLE `coordinador_coordinador`
ADD CONSTRAINT `coordinador_coordina_matricula_id_07bc64bd_fk_usuario_u` FOREIGN KEY (`matricula_id`) REFERENCES `usuario_usuario` (`id_usuario`);

--
-- Filtros para la tabla `coordinador_municipio`
--
ALTER TABLE `coordinador_municipio`
ADD CONSTRAINT `coordinador_municipi_id_estado_id_a9ca3ced_fk_coordinad` FOREIGN KEY (`id_estado_id`) REFERENCES `coordinador_estado` (`id_estado`);

--
-- Filtros para la tabla `coordinador_reporteinconformidad`
--
ALTER TABLE `coordinador_reporteinconformidad`
ADD CONSTRAINT `coordinador_reportei_id_tipo_inconformida_5a8ccc88_fk_coordinad` FOREIGN KEY (`id_tipo_inconformidad_FK_id`) REFERENCES `coordinador_catalogotipoinconformidad` (`id_tipo`),
ADD CONSTRAINT `coordinador_reportei_id_clinica_fk_id_9bff4fef_fk_clinica_c` FOREIGN KEY (`id_clinica_fk_id`) REFERENCES `clinica_clinica` (`id_clinica`),
ADD CONSTRAINT `coordinador_reportei_id_pasante_fk_id_7ba8ee91_fk_pasante_p` FOREIGN KEY (`id_pasante_fk_id`) REFERENCES `pasante_pasante` (`id_pasante`),
ADD CONSTRAINT `coordinador_reportei_id_status_fk_id_19fa40e9_fk_coordinad` FOREIGN KEY (`id_status_fk_id`) REFERENCES `coordinador_catalogoestatusinconformidad` (`id_estatus`);

--
-- Filtros para la tabla `coordinador_seguimiento`
--
ALTER TABLE `coordinador_seguimiento`
ADD CONSTRAINT `coordinador_seguimie_id_usuario_id_3a693a36_fk_usuario_u` FOREIGN KEY (`id_usuario_id`) REFERENCES `usuario_usuario` (`id_usuario`),
ADD CONSTRAINT `coordinador_seguimie_id_reporte_id_2acbdd54_fk_coordinad` FOREIGN KEY (`id_reporte_id`) REFERENCES `coordinador_reporteinconformidad` (`id_reporte`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `pasante_direccionpasante`
--
ALTER TABLE `pasante_direccionpasante`
ADD CONSTRAINT `pasante_direccionpas_municipio_id_ef4952b3_fk_coordinad` FOREIGN KEY (`municipio_id`) REFERENCES `coordinador_municipio` (`id_municipio`),
ADD CONSTRAINT `pasante_direccionpas_estado_id_4e1235f9_fk_coordinad` FOREIGN KEY (`estado_id`) REFERENCES `coordinador_estado` (`id_estado`);

--
-- Filtros para la tabla `pasante_pasante`
--
ALTER TABLE `pasante_pasante`
ADD CONSTRAINT `pasante_pasante_id_usuario_fk_id_321dd118_fk_usuario_u` FOREIGN KEY (`id_usuario_fk_id`) REFERENCES `usuario_usuario` (`id_usuario`),
ADD CONSTRAINT `pasante_pasante_id_datos_academicos__07e74308_fk_pasante_d` FOREIGN KEY (`id_datos_academicos_fk_id`) REFERENCES `pasante_datosacademicos` (`id_datos_academicos`),
ADD CONSTRAINT `pasante_pasante_id_direccion_fk_id_2a75e66e_fk_pasante_d` FOREIGN KEY (`id_direccion_fk_id`) REFERENCES `pasante_direccionpasante` (`id_direccion`);

--
-- Filtros para la tabla `pasante_promocion`
--
ALTER TABLE `pasante_promocion`
ADD CONSTRAINT `pasante_promocion_id_pasante_fk_id_712b8012_fk_pasante_p` FOREIGN KEY (`id_pasante_fk_id`) REFERENCES `pasante_pasante` (`id_pasante`),
ADD CONSTRAINT `pasante_promocion_id_generacion_fk_id_e42d587f_fk_pasante_g` FOREIGN KEY (`id_generacion_fk_id`) REFERENCES `pasante_generacion` (`id_generacion`);

--
-- Filtros para la tabla `pasante_telefono`
--
ALTER TABLE `pasante_telefono`
ADD CONSTRAINT `pasante_telefono_id_pasante_fk_id_e390ce22_fk_pasante_p` FOREIGN KEY (`id_pasante_fk_id`) REFERENCES `pasante_pasante` (`id_pasante`);

--
-- Filtros para la tabla `rol_asignacionrol`
--
ALTER TABLE `rol_asignacionrol`
ADD CONSTRAINT `rol_asignacionrol_id_tutor_id_937cbf36_fk_rol_tutor_id_tutor` FOREIGN KEY (`id_tutor_id`) REFERENCES `rol_tutor` (`id_tutor`),
ADD CONSTRAINT `rol_asignacionrol_id_pasante_id_2e8ce594_fk_pasante_p` FOREIGN KEY (`id_pasante_id`) REFERENCES `pasante_pasante` (`id_pasante`),
ADD CONSTRAINT `rol_asignacionrol_id_rotacion_id_21e48bac_fk_rol_rotac` FOREIGN KEY (`id_rotacion_id`) REFERENCES `rol_rotacion` (`id_rotacion`);

--
-- Filtros para la tabla `rol_rol`
--
ALTER TABLE `rol_rol`
ADD CONSTRAINT `rol_rol_id_generacion_fk_id_e29cbf3a_fk_pasante_g` FOREIGN KEY (`id_generacion_fk_id`) REFERENCES `pasante_generacion` (`id_generacion`);

--
-- Filtros para la tabla `rol_rotacion`
--
ALTER TABLE `rol_rotacion`
ADD CONSTRAINT `rol_rotacion_id_rol_fk_id_9ccaaa3c_fk_rol_rol_id_rol` FOREIGN KEY (`id_rol_fk_id`) REFERENCES `rol_rol` (`id_rol`),
ADD CONSTRAINT `rol_rotacion_id_clinica_fk_id_1187f851_fk_clinica_c` FOREIGN KEY (`id_clinica_fk_id`) REFERENCES `clinica_clinica` (`id_clinica`);

--
-- Filtros para la tabla `usuario_usuario`
--
ALTER TABLE `usuario_usuario`
ADD CONSTRAINT `usuario_usuario_user_ptr_id_22b63d2a_fk_auth_user_id` FOREIGN KEY (`user_ptr_id`) REFERENCES `auth_user` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
