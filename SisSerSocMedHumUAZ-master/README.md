# SSSMH-UAZ

El Sistema de Servicio Social de Medicina Humana de la UAZ, es un sistema 
para administrar las pasantías de los estudiantes de medicina de la 
universidad, los periodos de servicio y las clínicas donde lo estarán 
llevando a cabo.

## Instrucciones de instalación

### Pre-requesitos:

* Instalar MiniConda
* Instalar Visual Studio 2017
* Instalar MySQL Server y WorkBench

### Crear un entorno virtual en MiniConda:
conda create -n ssmh python=3.6

### Activar el entorno virtual:
activate ssmh

### Actualizar el pip
python -m pip install --upgrade pip

### Instalar Django
pip install Django==1.11

### Instalar dependencias de Python
pip install -r dependencias.txt

### Instalar Mysql
* Forma 1: pip install mysqlclient
* Forma 2: pip install --only-binary :all: mysqlclient

### Acceder a Mysql y crear un usuario:
mysql -u root -p

CREATE USER 'ssmh'@'localhost' IDENTIFIED BY 'ssmh';

GRANT ALL PRIVILEGES ON * . * TO 'ssmh'@'localhost';

FLUSH PRIVILEGES;

### Crear la base de datos:
create database bd_ssmh_uaz;

### Generar las Migraciones
python manage.py makemigrations

### Correr las Migraciones
python manage.py migrate

### Insertar registros en la base de datos

mysql -u ssmh -p bd_ssmh_uaz < insert_instituciones.sql

mysql -u ssmh -p bd_ssmh_uaz < insert_municipio_estados.sql

mysql -u ssmh -p bd_ssmh_uaz < insert_tutor.sql

mysql -u ssmh -p bd_ssmh_uaz < llena_tipo_y_estatus.sql

## Correr el proyecto
python manage.py runserver 0.0.0.0:8000

## Crear un Super Usuario
Crear un usuario desde el sistema, una vez creado ingresar a mysql y 
ejecutar los siguientes query:

UPDATE auth_user SET is_staff = 1 WHERE id = [id del usuario a cambiar];
UPDATE auth_user SET is_superuser = 1 WHERE id = [id del usuario a cambiar];

Una vez creado el super usuario, se podran crear coordinadores a traves 
de la pantalla de inicio del sistema.

## Dar de alta un pasante
Como coordinador se debera crear una fecha de registro en el apartado de 
pasantes.

Una vez creada la fecha, crear un nuevo usuario e iniciar sesión y dirigirse 
al apartado pasante y llenar sus datos de pasante.

Una vez registrados los datos como pasante, con el usuario coordinador se deberá
activar el pasante desde el apartado de Mostrar Pasante.

## Instrucciones para usar git

#### Clonar el repositorio
git clone https://gitlab.com/TSP_7A_2018/SisSerSocMedHumUAZ "Sistema de Medicina"

#### Obtener cambios del repositorio
git pull

#### Ver las modificaciones hechas
git status -s -b

#### Guardar todas las modificaciones hechas al proyecto
git add .

#### Realizar un commit
git commit -m "descripción del commit"

#### Subir cambios al repositorio
git push

#### Cambiar a una rama secundaria
git checkout 'nombre-de-la-rama'

#### Subir cambios de una rama secundaria al repositorio
git push origin 'nombre-de-la-rama'