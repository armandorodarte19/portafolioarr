#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.forms import TextInput
from django.contrib.auth.models import User
from django.forms import ModelForm
from usuario.models import Usuario

	#modelos de usario

class FormUsuario(forms.ModelForm):
	

	matrícula = forms.CharField(max_length=8,widget=forms.TextInput(attrs={'onkeypress':'return justNumbers(event);','title':'Ingresa tu matrícula*','oninput':'check_textm(this);'}),label='Matrícula*',required=False)
	password = forms.CharField(widget=forms.PasswordInput(attrs={'title':'Ingresa tu contraseña*'}),label='Contraseña*')
	confirmar_password = forms.CharField(widget=forms.PasswordInput(attrs={'title':'Confirma tu contraseña*'}),required=True, max_length=50,label='Confirma tu contraseña*')
	correo_electrónico = forms.EmailField(required = True, max_length=50,widget=forms.EmailInput(attrs={'title':'Ingresa tu correo electronico principal*'}),label='Correo electrónico principal*')
	correo_institucional = forms.EmailField(required=False,max_length=50,widget=forms.EmailInput(attrs={'title':'Ingresa un correo alternativo*'}),label='Correo electrónico alternativo')

	class Meta:
         model = Usuario
         fields = ()