
from django.conf.urls import include, url
from usuario.views import loginpos,AltaUsuario,IniciaSesion, SesionExperto
from django.contrib.auth.views import login, logout_then_login


urlpatterns = [
    url(r'^logout/$', logout_then_login, name="logout_user"),
    url(r'^$', IniciaSesion.iniciar_sesion, name="iniciar_sesion"),
    url(r'^registrar$', AltaUsuario.registrar_usuario, name='usuario_nuevo'),
    url(r'^recuperar$', IniciaSesion.recuperar_contrasena, name='recuperar_contrasena'),
	url(r'experto$', SesionExperto.iniciar_sesion_experto, name="experto"),
	url(r'experto2$', SesionExperto.iniciar_sesion, name="experto2"),


]
