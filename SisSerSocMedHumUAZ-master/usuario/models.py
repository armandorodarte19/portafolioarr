from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Usuario(User):
    id_usuario = models.AutoField(primary_key=True)
    matricula = models.CharField(max_length=8, unique=True)
    contrasena = models.CharField(max_length=128)
    tipo_usuario = models.CharField(max_length=50)
    correo_electronico = models.CharField(unique=True, max_length=250)
    correo_institucional = models.CharField(max_length=250)


'''
from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Usuario(User):
    id_usuario = models.AutoField(primary_key=True)
    matricula = models.CharField(u'matricula', max_length=8, unique=True)
    contrasena = models.CharField(u'contrasena', max_length=128)
    tipo_usuario = models.CharField(u'tipo_usuario', max_length=50)
    correo_electronico = models.CharField(u'correo_electronico', unique=True,
                                          max_length=250)
    correo_institucional = models.CharField(u'correo_institucional',
                                            max_length=250)

'''
