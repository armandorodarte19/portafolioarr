# !/usr/bin/env python
# -*- coding: utf-8 -*-
import json
import urllib

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.core.mail import EmailMessage, send_mail, EmailMultiAlternatives
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from usuario.models import Usuario
from coordinador.models import fecha_registro
from django.contrib.auth.hashers import *
from random import choice
from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.contrib.auth.forms import UserCreationForm
from datetime import datetime, timedelta, date
from usuario.forms import FormUsuario
from ssmh_uaz_project import settings
import re

registro = {}
registro_fallido = False
registro_fallido_experto = False


# Create your views here.
def loginpos(request):
    return render(request, 'iniciarsesion.html')	
class IniciaSesion:
    def recuperar_contrasena(request):
        if request.method != 'POST':
            return render(request, 'recuperar.html')
        mat = request.POST.get('matricula')
        correo = request.POST.get('correo')
        if mat == "" or correo == "":
            funcion = "Debe escribir su correo y matrícula."
            return render(request, 'recuperar.html', {'funcion': funcion})
        longitud = 7
        valores = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

        p = ""
        p = p.join([choice(valores) for i in range(longitud)])

        try:
            user = Usuario.objects.get(matricula=mat, correo_electronico=correo)
            message = "Tu contraseña nueva es la siguiente:    "
            message += p
            subject = 'Recuperación de contraseña'
            text_content = 'Recuperacion de contraseña'
            html_content = '<h2>Recuperación de contraseña</2><p>Medicina humana<br>Uaz</p><br> <h2 style="font-family: Andale Mono,AndaleMono,monospace; font-size:16px; font-style:normal;">' + message + '</h2>'
            from_email = '"SCSSUAMH" <ssmh.uaz@gmail.com>'
            msg = EmailMultiAlternatives(subject, text_content, from_email,
                                         [user.correo_electronico])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
            user.password = make_password(p)
            user.contrasena = make_password(p)
            # usuario=user(password = make_password(password),contrasena = make_password(password))
            user.save()

            formulario = FormUsuario()
            return render(request, 'iniciarsesion.html',
                          {'exito': 'Se envío una nueva contraseña al correo.',
                           'formulario': formulario})

        except:

            formulario = FormUsuario()
            return render(request, 'iniciarsesion.html', {
                'funcion': 'Matricula invalida y/o correo revisa tus datos.',
                'formulario': formulario})

    def iniciar_sesion(request):
        user = request.user

        if user is not None and user.is_authenticated and user.is_active:
            if user.is_staff:
                print('redireccionar a inicio coordinador')
                return redirect('inicio')
            else:
                print('redireccionar a inicio pasante')
                return redirect('inicio_pasante')
            
        global registro_fallido
        BajaAnual.desactivar_pasante_anual()
        formulario = FormUsuario()
        if (len(registro) > 0):
            registro.clear()
            return render(request, 'iniciarsesion.html',
                          {'exito': 'Se registró al usuario.',
                           'formulario': formulario})
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = ""
        print(registro_fallido == True)
        if registro_fallido:
            if username == None:
                return render(request, 'iniciarsesion.html',
                              {'formulario': formulario,
                               'login_fallido': "El campo matrícula no debe ser vacío."})
            if password == None:
                return render(request, 'iniciarsesion.html',
                              {'formulario': formulario,
                               'login_fallido': "El campo contraseña no debe ser vacío."})

        if (username != None and password != None):
            user = authenticate(username=username, password=password)

        if user is not None:
            try:
                datos_usuario = Usuario.objects.get(matricula=username)
            except Exception as e:
                return render(request, 'iniciarsesion.html',
                              {'formulario': formulario})
            if datos_usuario.is_staff == 1:
                login(request, user)
                return HttpResponseRedirect('/Coordinador/inicio/')
            else:
                login(request, user)
                return HttpResponseRedirect('/Pasante/inicio/')
        else:

            if username == "" or password == "":
                if username == "":
                    return render(request, 'iniciarsesion.html',
                                  {'formulario': formulario,
                                   'login_fallido': "El campo matrícula no debe ser ser vacío."})
                if password == "":
                    return render(request, 'iniciarsesion.html',
                                  {'formulario': formulario,
                                   'login_fallido': "El campo contraseña no debe ser vacío."})
            else:
                return render(request, 'iniciarsesion.html',
                              {'formulario': formulario,
                               'login_fallido': "Los datos introducidos son incorrectos."})
							   
	
	

class SesionExperto:
    def iniciar_sesion_experto(request):
        if request.method != 'POST':
		       return render(request, 'iniciarsesionexp.html')

           
    def iniciar_sesion(request):
        user = request.user
        global registro_fallido_experto
        formulario = FormUsuario()

        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username, password=password)
   
        if user is not None:
          datos_usuario = Usuario.objects.filter(correo_electronico=username)
          messages.success(request, 'Bienvenido al Foro')

          login(request, user)

          return HttpResponseRedirect('foro')

        else:
          return render(request, 'iniciarsesionexp.html', {'formulario': formulario,
                                  'login_fallido': "Datos Incorrectos / Datos Faltantes."})
        
class AltaUsuario:
    def registrar_usuario(request):

        if request.method == 'POST':
            formulario = FormUsuario(request.POST)

            try:
                if formulario.is_valid:

                    recaptcha_response = request.POST.get(
                        'g-recaptcha-response')
                    url = 'https://www.google.com/recaptcha/api/siteverify'
                    values = {
                        'secret': settings.GOOGLE_RECAPTCHA_SECRET_KEY,
                        'response': recaptcha_response
                    }
                    data = urllib.parse.urlencode(values).encode()
                    req = urllib.request.Request(url, data=data)
                    response = urllib.request.urlopen(req)
                    result = json.loads(response.read().decode())

                    matricula = request.POST.get('matrícula')
                    password = request.POST.get('password')
                    password2 = request.POST.get('confirmar_password')
                    email = request.POST.get('correo_electrónico')
                    email2 = request.POST.get('correo_institucional')

                    if matricula == "":
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'El campo de "Matrícula" no debe estar vacío.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})
                    if (len(matricula) != 8):
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'La "Matrícula" debe cumplir con 8 caracteres numéricos.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})

                    try:
                        int(matricula)
                    except ValueError:
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'La "Matrícula" debe cumplir con 8 caracteres numéricos.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})
                    # validacion de la matricula, para comprobar que son solo numeros
                    # import pdb;
                    # pdb.set_trace()
                    if Usuario.objects.filter(matricula=matricula).exists():
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'La "Matricula" ya fue registrada.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})

                    # El campo de contraseñas no debe ser vacío
                    if password == "":
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'El campo de "Contraseñas" no debe estar vacío.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})
                    # Tienes que confirmar la contraseña
                    if password != password2:
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'Las "Contraseñas" no coinciden.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})

                    if email == "":
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'El campo de "Correo electrónico principal" no debe estar vacío.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})

                    if re.match(
                            '^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',
                            email.lower()):
                        print("Correo correcto")
                    else:
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'El "Correo electrónico principal" está incorrecto.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})
                    if email2 != "":
                        if re.match(
                                '^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',
                                email2.lower()):
                            print("Correo correcto")
                        else:
                            return render(request, 'registrar.html',
                                          {'formulario': formulario,
                                           'funcion': 'El "Correo electrónico alternativo" está incorrecto.',
                                           "matricula": matricula,
                                           "email": email, "email2": email2})

                    print(Usuario.objects.filter(matricula=matricula))

                    if len(password) == 0:
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'La "Contraseña" debe de tener mas de un carácter.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})
                    if password == password2:
                        usuario = Usuario(matricula=matricula,
                                          password=make_password(password),
                                          correo_electronico=email,
                                          correo_institucional=email2,
                                          is_superuser=0, username=matricula,
                                          is_staff=0,
                                          tipo_usuario='Pasante',
                                          contrasena=make_password(password))
                        if result['success']:
                            usuario.save()
                            formulario = FormUsuario()
                            registro[0] = matricula
                        else:
                            return render(request, 'registrar.html',
                                          {'formulario': formulario,
                                           'funcion': 'Resuelve el captcha.',
                                           "matricula": matricula,
                                           "email": email, "email2": email2})
                        return HttpResponseRedirect("/")
                    else:
                        return render(request, 'registrar.html',
                                      {'formulario': formulario,
                                       'funcion': 'Las contraseñas no coinciden.',
                                       "matricula": matricula, "email": email,
                                       "email2": email2})
            except ValueError as ex:
                return render(request, 'registrar.html',
                              {'formulario': formulario,
                               'funcion': 'La Matrícula debe contener solo números',
                               "matricula": matricula, "email": email,
                               "email2": email2})
            except Exception as e:
                print(e)
                return render(request, 'registrar.html',
                              {'formulario': formulario,
                               'funcion': 'El correo ya está registrado.',
                               "matricula": matricula, "email": email,
                               "email2": email2})

        else:

            formulario = FormUsuario()
            return render(request, 'registrar.html', {'formulario': formulario})



from pasante.models import Pasante
    
class BajaAnual:
    def desactivar_pasante_anual():
        """metodo para verificar si un pasante ya cumplió con el limite de tiempo para
            llevar a cabo el servicio social"""

        ahora = datetime.now()

        formato_fecha = "%d-%m-%Y"

        fecha_prueba_febrero = datetime.strptime("02-03-2018",
                                                 formato_fecha)  # prueba para verificar si la fecha actual es mayor
        fecha_prueba_agosto = datetime.strptime("02-09-2018",
                                                formato_fecha)  # prueba para verificar si la fecha actual es mayor
        # al limite de 1 año y 1 mes, esto para cuando el servicio comienza en febrero.

        usuarios = User.objects.filter(is_staff=0)

        for i in usuarios:
            try:
                # obtener datos del pasante
                pasantes = Pasante.objects.get(
                    id_usuario_fk_id=Usuario.objects.get(
                        user_ptr_id=i.id).id_usuario,
                    status='activo')  # pasante a evaluar

                obtener_fechas = i.date_joined.strftime(
                    formato_fecha)  # obtener fecha del pasante

                fecha_user = datetime.strptime(obtener_fechas,
                                               formato_fecha)  # formato a la fecha del pasante
                fecha_inicial = datetime.strptime(
                    "01-02-" + str(fecha_user.year), formato_fecha)
                if (
                            fecha_user < fecha_inicial):  # en  caso de que la fecha del pasante sea menor a la fecha que inicia el servicio en febrero, verificara si la fecha actual
                    # es mayor a la fecha donde termina el servicio  social para los que iniciaron  en febrero
                    fecha_baja_febrero = datetime.strptime(
                        "01-03-" + str(fecha_user.year + 1), formato_fecha)
                    if (ahora > fecha_baja_febrero):
                        pasantes.status = "desactivado"
                        pasantes.save()
                elif (
                            fecha_user > fecha_inicial):  # si la fecha del pasante es mayor a la fecha de inicio de febrero, esto quiere decir que los pasantes van
                    # a comenzar el servicio social el 1ero de agosto
                    fecha_baja_agosto = datetime.strptime(
                        "01-09-" + str(fecha_user.year + 1), formato_fecha)
                    if (
                                ahora > fecha_baja_agosto):  # en caso de que la fecha actual sea mayor a la fecha donde termina el servicio social para los que iniciaron
                        # en agosto, el pasante se desactiva.
                        pasantes.status = "desactivado"
                        pasantes.save()

            except Pasante.DoesNotExist:
                continue


def es_alfabetica(cadena):
    separar = cadena.split(" ")
    n = ""
    for i in separar:
        n = n + i.strip()
    if (n.isalpha()):
        return True
    else:
        return False
