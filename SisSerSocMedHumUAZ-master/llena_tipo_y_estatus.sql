USE bd_ssmh_uaz;

INSERT INTO `coordinador_catalogotipoinconformidad` (`id_tipo`, `nombre`) VALUES
('1', 'Seguridad'),
('2', 'Administrativo'),
('3', 'Escolar'),
('4', 'Bajas'),
('5', 'Permisos'),
('6', 'Infraestructura'),
('7', 'Otra');

INSERT INTO coordinador_catalogoestatusinconformidad (id_estatus, nombre_estatus, color) VALUES ('1', 'Negro', '000000'), ('2', 'Rojo', 'FF0000'), ('3', 'Amarillo', 'FFFF00'), ('4', 'Naranja', 'FFBB00'), ('5', 'Verde', '0000FF')
