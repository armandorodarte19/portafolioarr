import urllib
from itertools import chain
from django.shortcuts import render, redirect, render_to_response, \
    get_object_or_404
from django.template.loader import render_to_string
from pasante.models import DireccionPasante, DatosAcademicos, Pasante, \
    Telefono, Generacion, Promocion
from contactos_pasante.models import ContactosPasante
from coordinador.models import AsignacionClinicaPasante, fecha_registro, ReporteInconformidad
from clinica.models import DireccionClinica, Clinica, Institucion, CalificacionClinica
from rol.models import AsignacionRol, Rol, Rotacion, Tutor
from usuario.models import Usuario
from clinica.forms import *
from pasante.models import ComentarioPasante,Rubro
from coordinador.forms import *
from django.contrib.auth.decorators import login_required
from coordinador.models import Estado, Municipio
from coordinador.forms import FormInformacion
import json
import json as simplejson
from django.http import HttpResponse, HttpResponseRedirect,JsonResponse
from usuario.models import Usuario
from coordinador.models import Coordinador,ComentariosInconformidad
from wsgiref.validate import validator
from django.contrib.auth.hashers import *
from django.db.models import Q
import re
from io import BytesIO
from reportlab.pdfgen import canvas
from usuario.views import BajaAnual
from datetime import datetime, timedelta
from io import BytesIO
from reportlab.platypus import Table
from reportlab.platypus import TableStyle, Paragraph, SimpleDocTemplate
from reportlab.lib import colors
from reportlab.lib.units import cm
from reportlab.lib.pagesizes import letter, landscape
from reportlab.lib.styles import getSampleStyleSheet
import xlwt
from io import StringIO
from datetime import datetime, date, time, timedelta
import calendar
from django.urls import reverse_lazy
from coordinador.models import CatalogoTipoInconformidad
import yaml
from django.core.exceptions import ObjectDoesNotExist
from pasante.views import CalificarClinica
from foro.models import Pregunta_Foro
from django.db import transaction
from django.contrib import messages


class NotificacionCoordinador:
    def notificar_coordinador(self):
        return Pregunta_Foro.objects.filter(estado=1)

# mensajes para mostrar mensajes de exito y error en mostrar pasantes
mensajes = {}
mensajes_error = {}
lista_cambio_anios = {}
clinica_global = {}
pasante_global = {}
mensaje_des_clinica = {}
mensaje_ac_clinica = {}
id_clinica1 = 0


from django.views.generic.edit import CreateView
from mensajeria.models import Pregunta, Mensaje, entrenar_bot,chatbot, conversacion
from django.views.generic.edit import ModelFormMixin
from django.views.generic.list import ListView
from django.contrib.messages.views import SuccessMessageMixin
from chatterbot import ChatBot


class CatalogoPregunta(SuccessMessageMixin, CreateView):
    model = Pregunta
    fields = ['pregunta', 'respuesta', 'id_tipo_fk_id']
    template_name= "pregunta_form.html"
    success_url =reverse_lazy('preguntas_frecuentes')
    success_message = 'Se ha registrado la pregunta con éxito'
    

    def form_valid(self, form):
        #chatbot.read_only = False
        form.instance = form.save(commit=False)        
        form.instance.coordinador= Coordinador.objects.get(matricula= Usuario.objects.get(username=self.request.user))
        
        chatbot.read_only = True
        form.instance.save()
        
        entrenar_bot()
        
        return super(CatalogoPregunta, self).form_valid(form)
    

# clase depresiada por mal manejo de recuperacion de converzaciones y falta de acceso a los mensajes
# de cada conversacion, se intercambia por clase Mensajeria continuada adelante, esta clase de deja
# aqui para futuras referencias sobre chat bot



# imports de modelos desde mensajeria, para la recuperacion de conversaciones y mensajes de conversaciones
# del coordinador
from mensajeria.models import mensaje_Conversacion
from mensajeria.models import conversacion
# clase que sustituye a la clase superior
class Mensajeria(ListView):
    model = conversacion
    template_name = "registroConversaciones.html"

    def get_context_data(self, **tipoargs):
        context = super().get_context_data(**tipoargs)
        if self.model.objects.filter(leido=1).exists():
          leidos=self.model.objects.filter(leido=1)
          context['mensajes'] = leidos
        return context


    def seleccionar_generacion(request):
      #función para mostrar los pasantes registrados
      gen_seleccionada = request.POST.get('select')
      actuales = request.POST.get('actuales') 


class Mensajes():

    def obtener_conversacion(request):
        print(request.POST)
        dato=request.POST.get("id_conversacion")
        conversa = conversacion.objects.get(id_mensaje = dato)
        mensaje = mensaje_Conversacion.objects.filter(id_conversacion_fk_id = dato)
        return render(request,'registroMensajes.html', {'mensaje':mensaje, 'conversa':conversa})

    def marcarImportante(request):
        dato = request.POST.get("id_conversacion")
        print("hellothere")
        conversa = conversacion.objects.get(id_mensaje = dato)
        mensaje = mensaje_Conversacion.objects.filter(id_conversacion_fk_id = conversa.id_mensaje)
        conversa.importante = True
        conversa.save()
        return render(request,'registroMensajes.html', {'mensaje':mensaje, 'conversa':conversa})

    def resaltar(request):
	    datConv=request.POST.get("id_conversacion")
	    datMens=request.POST.get("id_mensaje")
	    datRes=request.POST.get("resaltado")
	    print(datConv)
	    print(datMens)
	    print(datRes)
	    conversa = conversacion.objects.get(id_mensaje = datConv)
	    mensaje = mensaje_Conversacion.objects.filter(id_conversacion_fk_id = conversa.id_mensaje)
	    men=mensaje_Conversacion.objects.get(id_mensaje=(datMens))
	    if men.resaltado ==  True:
	    	men.resaltado=False
	    	men.save()
	    	return render(request,'registroMensajes.html', {'mensaje':mensaje, 'conversa':conversa})
	    else:
	    	men.resaltado=True
	    	men.save()
	    	return render(request,'registroMensajes.html', {'mensaje':mensaje, 'conversa':conversa})

class NoLeidos(ListView):
    model= Mensaje
    template_name = "mensajes_no_leidos.html"


    def get_context_data(self, **tipoargs):
        context = super(NoLeidos, self).get_context_data(**tipoargs)
        if self.model.objects.filter(leido=False).exists():
            #mensajes_vacios = self.model.objects.values('texto')
            no_leidos=self.model.objects.exclude(texto='').exclude(leido=True)


            context['mensajes'] = no_leidos
        return context
        '''pasante = pasante.filter(
                    Q(nombre__icontains=nombre_filter_1) | Q(
                        nombre__icontains=nombre_filter_1))
        '''




def actualizarReportes(request):
    reporte_in = ReporteInconformidad.objects.all()
    for reporte in reporte_in:
        if reporte.freq_rec != None and reporte.dia_rec != None and reporte.freq_rec != 0 and reporte.dia_rec != 0 and reporte.id_status_fk_id != 5:
            if (reporte.fecha_reporte + timedelta(days=(7 * reporte.freq_rec))) < datetime.now().date():
                if ComentariosInconformidad.objects.filter(id_reporte=reporte, es_coordinador=1).exists():
                    most_recent_comment = ComentariosInconformidad.objects.filter(id_reporte=reporte, es_coordinador=1).order_by('-fecha')[0]
                    if (most_recent_comment.fecha + timedelta(days=(7 * reporte.freq_rec))) < datetime.today().date():
                        if datetime.today().weekday() >= reporte.dia_rec-1:
                            reporte.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=4)
                            reporte.save()
                else:
                    # print(datetime.today().weekday() >= reporte.dia_rec)
                    if datetime.today().weekday() >= reporte.dia_rec-1:
                        reporte.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=4)
                        reporte.save()
                        # Se necesitan los comentarios para poder llevar seguimiento de cuantas semanas han pasado

@login_required
def inicio(request):
    """función para mostrar el inicio del coordinador"""
    actualizarReportes(request)
    return render(request, 'inicio.html', {})


def notificar_reportes_pendientes_inconformidad(request):
    reporteA = ReporteInconformidad.objects.filter(id_status_fk=3)
    contA = len(reporteA)

    reporteN = ReporteInconformidad.objects.filter(id_status_fk=4)
    contN = len(reporteN)

    notificador = NotificacionCoordinador()
    preguntasC = notificador.notificar_coordinador()

    contPregPend = len(preguntasC)

    return {
        'contA': contA,
        'contN': contN,
        'contPregPend': contPregPend,
    }

import sys


class MuestraPasantes:
    @login_required
    def mostrar_pasantes(request):

        rol_info_message = request.GET.get('rol_info_message', None)
        rol_error_message = request.GET.get('rol_error_message', None)
        print('rol_info_message', rol_info_message)
        print('rol_error_message', rol_error_message)

        """función para mostrar los pasantes registrados"""
        orden = request.POST.get('orden')
        gen_seleccionada = request.POST.get('select')
        actuales = request.POST.get('actuales')
        orderBy = request.POST.get('ordenar')
        telefono = Telefono.objects.all()
        contactos_pasante = ContactosPasante.objects.all()
        roles = Rol.objects.all()
        rotaciones = Rotacion.objects.all().order_by('fecha_inicio',
                                                     'fecha_fin')
        tutores = Tutor.objects.all()

        if (actuales):
            lista_cambio_anios.clear()

        ahora = datetime.now()
        BajaAnual.desactivar_pasante_anual()
        generaciones = Generacion.objects.all().order_by("-id_generacion")


        asignacion_rol = []
        roles = Rol.objects.all()
        pasantes_con_asignaciones = set()

        for rol in roles:
            asignaciones = AsignacionRol.objects.filter(
                id_rotacion__id_rol_fk__id_rol=rol.id_rol).order_by(
                'id_rotacion__fecha_inicio')

            print(str(rol.id_rol) + ', Hay ' + str(
                len(asignaciones)) + ' asignaciones en el rol')

            hoy = datetime.now().date()

            rotacion_actual = False

            for asignacion in asignaciones:
                inicio = asignacion.id_rotacion.fecha_inicio
                fin = asignacion.id_rotacion.fecha_fin

                if hoy >= inicio and hoy <= fin:
                    asignacion_rol.append(asignacion)
                    rotacion_actual = True

                pasantes_con_asignaciones.add(asignacion.id_pasante)

            if not rotacion_actual and len(asignaciones) > 0:
                pasantes = []

                for asignacion in asignaciones:
                    if asignacion.id_pasante not in pasantes:
                        asignacion_rol.append(asignacion)
                        pasantes.append(asignacion.id_pasante)

        clinicas = Clinica.objects.all()
        ordenar_por = "-id_datos_academicos_fk__promedio"
        if (request.method == 'POST'):
            lista_cambio_anios[0] = gen_seleccionada
            if '-' in request.POST:
                ordenar_por="-id_datos_academicos_fk__promedio"
            elif  'status' in request.POST:
                ordenar_por="-id_datos_academicos_fk__promedio"
            elif  'score' in request.POST:
                ordenar_por="-score"
            elif  'lastName' in request.POST:
                ordenar_por="-apellido_paterno"

        if (len(lista_cambio_anios) > 0 and lista_cambio_anios[
            0] != None):  # Verificar si se selecciono un año de la lista.
            prom_temp = Promocion.objects.filter(id_generacion_fk=lista_cambio_anios[0]).values(
                    'id_pasante_fk')
            
            pasantes = Pasante.objects.filter(
                    id_pasante__in=prom_temp).order_by(
                    ordenar_por)

            if (len(mensajes) > 0):  # mostrar mensajes de éxito
                mensaje = mensajes[0]
                mensajes.clear()
                return render(request, 'mostrar_pasantes.html',
                              {'pasantes': pasantes,
                               'contactos_pasante': contactos_pasante,
                               'roles': roles,
                               'telefono': telefono,
                               'rotaciones': rotaciones,
                               'asignacion': asignacion_rol,
                               'tutores': tutores,
                               'pasantes_asignados': pasantes_con_asignaciones,
                               # 'vacio': asignacion_vacia,
                               'clinicas': clinicas,
                               # 'values': values,
                               'mensaje_exito': mensaje,
                               'anio': gen_seleccionada,
                               'lista_anios': generaciones, 'ultimo': pasantes.last(),
                               'rol_info_message': rol_info_message,
                               'rol_error_message': rol_error_message
                               })
            if (len(mensajes_error) > 0):  # Mostrar mensajes de errores.
                mensaje = mensajes_error[0]
                mensajes_error.clear()
                return render(request, 'mostrar_pasantes.html',
                              {'pasantes': pasantes,
                               'contactos_pasante': contactos_pasante,
                               'roles': roles,
                               'rotaciones': rotaciones,
                               'asignacion': asignacion_rol,
                               'tutores': tutores,
                               'pasantes_asignados': pasantes_con_asignaciones,
                               'telefono': telefono,
                               # 'vacio': asignacion_vacia,
                               'clinicas': clinicas,
                               # 'values': values,
                               'mensaje_error': mensaje,
                               'anio': gen_seleccionada,
                               'lista_anios': generaciones, 'ultimo': pasantes.last(),
                               'rol_info_message': rol_info_message,
                               'rol_error_message': rol_error_message
                               })

            return render(request, 'mostrar_pasantes.html',
                          {'pasantes': pasantes,
                           'contactos_pasante': contactos_pasante,
                           'roles': roles,
                           'rotaciones': rotaciones,
                           'asignacion': asignacion_rol,
                           'tutores': tutores,
                           'pasantes_asignados': pasantes_con_asignaciones,
                           'telefono': telefono,
                           # 'vacio': asignacion_vacia,
                           'clinicas': clinicas,
                           # 'values': values,
                           'anio': gen_seleccionada, 'lista_anios': generaciones,
                           'ultimo': pasantes.last(),
                           'rol_info_message': rol_info_message,
                           'rol_error_message': rol_error_message
                           })
        else:  # en caso de que no seleccione un año.
            gen_temp = Generacion.objects.filter(anio=str(ahora.year))
            
            prom_temp = Promocion.objects.filter(
              id_generacion_fk__in=[x.id_generacion for x in gen_temp]).values(
                    'id_pasante_fk')
            
            
            pasantes = Pasante.objects.filter(
                    id_pasante__in=prom_temp).order_by(
                    "-id_datos_academicos_fk__promedio")

            if (len(mensajes) > 0):  # mostrar mensajes de éxito
                mensaje = mensajes[0]
                mensajes.clear()
                return render(request, 'mostrar_pasantes.html',
                              {'pasantes': pasantes,
                               'contactos_pasante': contactos_pasante,
                               'roles': roles,
                               'rotaciones': rotaciones,
                               'asignacion': asignacion_rol,
                               'tutores': tutores,
                               'pasantes_asignados': pasantes_con_asignaciones,
                               'telefono': telefono,
                               # 'vacio': asignacion_vacia,
                               'clinicas': clinicas,
                               # 'values': values,
                               'mensaje_exito': mensaje, 'anio': ahora.year,
                               'lista_anios': generaciones, 'ultimo': pasantes.last(),
                               'rol_info_message': rol_info_message,
                               'rol_error_message': rol_error_message
                               })
            if (len(mensajes_error) > 0):  # Mostrar mensajes de error
                mensaje = mensajes_error[0]
                mensajes_error.clear()
                return render(request, 'mostrar_pasantes.html',
                              {'pasantes': pasantes,
                               'contactos_pasante': contactos_pasante,
                               'roles': roles,
                               'rotaciones': rotaciones,
                               'asignacion': asignacion_rol,
                               'tutores': tutores,
                               'pasantes_asignados': pasantes_con_asignaciones,
                               'telefono': telefono,
                               # 'vacio': asignacion_vacia,
                               'clinicas': clinicas,
                               # 'values': values,
                               'mensaje_error': mensaje, 'lista_anios': generaciones,
                               'ultimo': pasantes.last(),
                               'rol_info_message': rol_info_message,
                               'rol_error_message': rol_error_message
                               })

            return render(request, 'mostrar_pasantes.html',
                          {'pasantes': pasantes,
                           'contactos_pasante': contactos_pasante,
                           'roles': roles,
                           'rotaciones': rotaciones,
                           'asignacion': asignacion_rol,
                           'tutores': tutores,
                           'pasantes_asignados': pasantes_con_asignaciones,
                           'telefono': telefono,
                           # 'vacio': asignacion_vacia,
                           'clinicas': clinicas,
                           # 'values': values,
                           'anio': ahora.year,
                           'lista_anios': generaciones,
                           'ultimo': pasantes.last(),
                           'rol_info_message': rol_info_message,
                           'rol_error_message': rol_error_message
                           })

    def limitar_fecha(request):
        asig = fecha_registro.objects.last()

        if request.method == 'POST':
            fecha_ini = request.POST.get('fechainicio')
            fecha = request.POST.get('fechafin')

            if fecha_ini == "" or fecha == "":
                alerta = "Falta seleccionar la fecha de inicio o final"
                return render(request, 'limitar_fecha.html',
                              {'fechas': asig, 'alerta': alerta})
            else:
                if fecha < fecha_ini:
                    alerta = "La fecha de inicio debe de ser menor a la fecha final"
                    return render(request, 'limitar_fecha.html',
                                  {'fechas': asig, 'alerta': alerta})
                asignacion = fecha_registro(fecha_inicio=fecha_ini,
                                            fecha_fin=fecha)
                asignacion.save()
                asig = fecha_registro.objects.last()
                funcion = "Se asignó la fecha correctamente"
                return render(request, 'limitar_fecha.html',
                              {'fechas': asig, 'funcion': funcion})

        return render(request, 'limitar_fecha.html', {'fechas': asig})

    @login_required
    def busqueda_avanzada_pasante_clinica(request):
        global pasante_global

        asignacion_rol = []
        roles = Rol.objects.all()
        pasantes_con_asignaciones = set()

        pasante = Pasante.objects.all().order_by(
            "-id_datos_academicos_fk__promedio")

        nombre_filter_1 = request.POST.get('nombre_filter_1', "")
        nombre_filter_2 = request.POST.get('nombre_filter_2', "")

        ap_paterno_filter_1 = request.POST.get('ap_paterno_filter_1', "")
        ap_paterno_filter_2 = request.POST.get('ap_paterno_filter_2', "")

        ap_materno_filter_1 = request.POST.get('ap_materno_filter_1', "")
        ap_materno_filter_2 = request.POST.get('ap_materno_filter_2', "")

        matricula_filter_1 = request.POST.get('matricula_filter_1', "")
        matricula_filter_2 = request.POST.get('matricula_filter_2', "")

        promedio_filter_1 = request.POST.get('promedio_filter_1', "")
        promedio_filter_2 = request.POST.get('promedio_filter_2', "")

        nombre_unidad_filter_1 = request.POST.get('nombre_unidad_filter_1', "")
        nombre_unidad_filter_2 = request.POST.get('nombre_unidad_filter_2', "")

        anio_filter_1 = request.POST.get('anio_filter_1', "")
        anio_filter_2 = request.POST.get('anio_filter_2', "")

        prom_filter_1 = request.POST.get('prom_filter_1', "")

        comision_filter_1 = request.POST.get('comision_filter_1', "")
        comision_filter_2 = request.POST.get('comision_filter_2', "")

        if nombre_filter_2 == "":
            if nombre_filter_1 != "":
                pasante = pasante.filter(
                    Q(nombre__icontains=nombre_filter_1) | Q(
                        nombre__icontains=nombre_filter_1))
        else:
            if nombre_filter_1 != "":
                pasante = pasante.filter(
                    Q(nombre__icontains=nombre_filter_1) | Q(
                        nombre__icontains=nombre_filter_2))

        if ap_paterno_filter_2 == "":
            if ap_paterno_filter_1 != "":
                pasante = pasante.filter(
                    Q(apellido_paterno__icontains=ap_paterno_filter_1) | Q(
                        apellido_paterno__icontains=ap_paterno_filter_1))
        else:
            if ap_paterno_filter_1 != "":
                pasante = pasante.filter(
                    Q(apellido_paterno__icontains=ap_paterno_filter_1) | Q(
                        apellido_paterno__icontains=ap_paterno_filter_2))

        if ap_materno_filter_2 == "":
            if ap_materno_filter_1 != "":
                pasante = pasante.filter(
                    Q(apellido_materno__icontains=ap_materno_filter_1) | Q(
                        apellido_materno__icontains=ap_materno_filter_1))
        else:
            if ap_materno_filter_1 != "":
                pasante = pasante.filter(
                    Q(apellido_materno__icontains=ap_materno_filter_1) | Q(
                        apellido_materno__icontains=ap_materno_filter_2))
        if matricula_filter_2 == "":
            if matricula_filter_1 != "":
                pasante = pasante.filter(
                    Q(
                        id_usuario_fk__matricula__icontains=matricula_filter_1) | Q(
                        id_usuario_fk__matricula__icontains=matricula_filter_1))
        else:
            if matricula_filter_1 != "":
                pasante = pasante.filter(Q(
                    id_usuario_fk__matricula__icontains=matricula_filter_1) | Q(
                    id_usuario_fk__matricula__icontains=matricula_filter_2))

        if promedio_filter_2 == "":
            if promedio_filter_1 != "":
                pasante = pasante.filter(Q(
                    id_datos_academicos_fk__promedio__icontains=promedio_filter_1) | Q(
                    id_datos_academicos_fk__promedio__icontains=promedio_filter_1))
        else:
            if promedio_filter_1 != "":
                pasante = pasante.filter(Q(
                    id_datos_academicos_fk__promedio__icontains=promedio_filter_1) | Q(
                    id_datos_academicos_fk__promedio__icontains=promedio_filter_2))

        if prom_filter_1 != "":
            ahora = datetime.now()
            formato_fecha = "%d-%m-%Y"
            fecha_inicial = datetime.strptime("01-08-" + str(ahora.year),
                                      formato_fecha)
            id_gen_temp = None;
            anio_activo = ""
            if prom_filter_1 == 'Agosto' and datetime.now() < fecha_inicial:
                id_gen_temp = Generacion.objects.get(generacion=prom_filter_1,
                                anio=str(datetime.now().year-1))        
            else: 
                id_gen_temp = Generacion.objects.get(generacion=prom_filter_1,
                                                        anio=str(datetime.now().year))

            #id_gen_temp = Generacion.objects.get(generacion="Agosto",
            #                                            anio=str(datetime.now().year))
            roles = Rol.objects.filter(id_generacion_fk=id_gen_temp)
            rotaciones = Rotacion.objects.filter(id_rol_fk__in=roles)
            asignaciones = AsignacionRol.objects.filter(id_rotacion_id__in=rotaciones)
            pasante = pasante.filter(id_pasante__in= asignaciones.all().values("id_pasante"))
            #asignaciones = asignaciones.filter(Q(
            #    id_rotacion__id_rol_fk__id_generacion_fk__generacion__exact=id_gen_temp))
            
        pasante_global = pasante

        for rol in roles:

            asignaciones = AsignacionRol.objects.filter(
                id_rotacion__id_rol_fk__id_rol=rol.id_rol).order_by(
                'id_rotacion__fecha_inicio')

            if comision_filter_1 != "":
                if comision_filter_2 == "":
                    asignaciones = asignaciones.filter(Q(
                        comision__icontains=comision_filter_1) | Q(
                        comision__icontains=comision_filter_1))
                else:
                    asignaciones = asignaciones.filter(Q(
                        comision__icontains=comision_filter_1) | Q(
                        comision__icontains=comision_filter_2))

            if nombre_unidad_filter_1 != "":
                if nombre_unidad_filter_2 == "":
                    asignaciones = asignaciones.filter(Q(
                        id_rotacion__id_clinica_fk__nombre_unidad__icontains=nombre_unidad_filter_1) | Q(
                        id_rotacion__id_clinica_fk__nombre_unidad__icontains=nombre_unidad_filter_1))
                else:
                    asignaciones = asignaciones.filter(Q(
                        id_rotacion__id_clinica_fk__nombre_unidad__icontains=nombre_unidad_filter_1) | Q(
                        id_rotacion__id_clinica_fk__nombre_unidad__icontains=nombre_unidad_filter_2))

            if anio_filter_1 != "":
                if anio_filter_2 == "":
                    asignaciones = asignaciones.filter(Q(
                        id_rotacion__id_rol_fk__id_generacion_fk__anio__exact=anio_filter_1))
                else:
                    asignaciones = asignaciones.filter(Q(
                        id_rotacion__id_rol_fk__id_generacion_fk__anio__exact=anio_filter_1) | Q(
                        id_rotacion__id_rol_fk__id_generacion_fk__anio__exact=anio_filter_2))

            if prom_filter_1 != "":
                ahora = datetime.now()
                formato_fecha = "%d-%m-%Y"
                fecha_inicial = datetime.strptime("01-08-" + str(ahora.year),
                                          formato_fecha)
                id_gen_temp = None;
                anio_activo = ""
                if prom_filter_1 == 'Agosto' and datetime.now() < fecha_inicial:
                    id_gen_temp = Generacion.objects.get(generacion=prom_filter_1,
                                    anio=str(datetime.now().year-1))        
                else: 
                    id_gen_temp = Generacion.objects.get(generacion=prom_filter_1,
                                                            anio=str(datetime.now().year))

                #id_gen_temp = Generacion.objects.get(generacion="Agosto",
                #                                            anio=str(datetime.now().year))
                roles = Rol.objects.filter(id_generacion_fk=id_gen_temp)
                rotaciones = Rotacion.objects.filter(id_rol_fk__in=roles)
                asignaciones = AsignacionRol.objects.filter(id_rotacion_id__in=rotaciones)
                #asignaciones = asignaciones.filter(Q(
                #    id_rotacion__id_rol_fk__id_generacion_fk__generacion__exact=id_gen_temp))
                print(len(asignaciones))

            hoy = datetime.now().date()

            rotacion_actual = False

            for asignacion in asignaciones:
                inicio = asignacion.id_rotacion.fecha_inicio
                fin = asignacion.id_rotacion.fecha_fin

                if hoy >= inicio and hoy <= fin:
                    asignacion_rol.append(asignacion)
                    rotacion_actual = True

                pasantes_con_asignaciones.add(asignacion.id_pasante)

            if not rotacion_actual and len(asignaciones) > 0:
                pasantes = []

                for asignacion in asignaciones:
                    if asignacion.id_pasante not in pasantes:
                        asignacion_rol.append(asignacion)
                        pasantes.append(asignacion.id_pasante)
        if comision_filter_1 != "" or comision_filter_2 != "" or nombre_unidad_filter_1 != "" or nombre_unidad_filter_2 != "" or anio_filter_1 != "":
            query = None

            for asignacion in asignacion_rol:
                if query is None:
                    query = Q(
                        id_pasante=asignacion.id_pasante_id)
                else:
                    query = query | Q(
                        id_pasante=asignacion.id_pasante_id)
            if query is not None:
                pasante = pasante.filter(query)
            if len(asignacion_rol) <= 0:
                pasante = None
        return render(request, 'busqueda_avanzada_pasante.html',
                      {'pasante': pasante,
                       'asignacion': asignacion_rol,
                       'pasantes_asignados': pasantes_con_asignaciones
                       })

    @login_required
    def reporte_busqueda_pasante_clinica(request):
        pasantes_filtrados = json.loads(request.POST.get(
            'pasantes_filtrados', ''))

        # Create the HttpResponse object with the appropriate PDF headers.
        # para saber si la busqueda arrojo pasantes se utiliza un len()
        if len(pasantes_filtrados) == 0:
            return render(request, 'mostrar_pasantes.html',
                          {'ErrorReporteVacio': "--"})
        else:
            response = HttpResponse(content_type='application/pdf')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_pasante_con_clínica.pdf"'
            buffer = BytesIO()
            doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=40,
                                    leftMargin=40, topMargin=40,
                                    bottomMargin=18)

            story = []
            styles = getSampleStyleSheet()
            estCel = styles["BodyText"]
            titulo = Paragraph("Reporte de pasante con clínica:",
                               styles['Heading1'])
            story.append(titulo)

            encabezados = (
                'Promedio Final', 'Nombre', 'Apellido Paterno',
                'Apellido Materno',
                'Matrícula', 'Clínica', 'Comisión')

            datos = [(Paragraph(pasante['promedio'], estCel),
                      Paragraph(pasante['nombre'], estCel),
                      Paragraph(pasante['ap_pat'], estCel),
                      Paragraph(pasante['ap_mat'], estCel),
                      Paragraph(pasante['matricula'], estCel),
                      Paragraph(pasante['clinica'], estCel),
                      Paragraph(pasante['comision'], estCel))
                     for pasante in pasantes_filtrados if 'clinica' in pasante]

            orden = Table([encabezados] + datos,
                          colWidths=[3 * cm, 3 * cm, 2.7 * cm, 2.7 * cm,
                                     2 * cm,
                                     3 * cm, 2 * cm])
            try:

                orden.setStyle(TableStyle(
                    [
                        # Encabezados centrados
                        ('ALIGN', (0, 0), (6, 0), 'CENTER'),
                        # Bordes de color negro con grosor de 1
                        ('GRID', (0, 0), (-1, -1), 1, colors.black),
                        # Tamaño de las letras de 10
                        ('FONTSIZE', (0, 0), (-1, -1), 10),
                    ]
                ))
                story.append(orden)
                doc.build(story)
                pdf = buffer.getvalue()
                response.write(pdf)
                buffer.close()
            except Exception as e:
                print(e)
            return response

    @login_required
    def reporte_mostrar_pasante_xls(request):
        pasantes_filtrados = json.loads(request.POST.get(
            'pasantes_filtrados', ''))
        if len(pasantes_filtrados) == 0:
            return render(request, 'mostrar_pasantes.html',
                          {'ErrorReporteVacio': "--"})
        else:
            response = HttpResponse(content_type='application/ms-excel')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_pasante.xls"'
            book = xlwt.Workbook()
            sh = book.add_sheet('Sheet1')
            array = ['Promedio Final', 'Nombre', 'Apellido Paterno',
                     'Apellido Materno',
                     'Matrícula']
            for i in range(array.__len__()):
                sh.write(0, i, array[i])
            n = 1
            for pasante in pasantes_filtrados:
                sh.write(n, 0, pasante['promedio'])
                sh.write(n, 1, pasante['nombre'])
                sh.write(n, 2, pasante['ap_pat'])
                sh.write(n, 3, pasante['ap_mat'])
                sh.write(n, 4, pasante['matricula'])
                n += 1
            book.save(response)
            return response

    @login_required
    def reporte_mostrar_pasante_clinica_xls(request):
        pasantes_filtrados = json.loads(request.POST.get(
            'pasantes_filtrados', ''))
        if len(pasantes_filtrados) == 0:
            return render(request, 'mostrar_pasantes.html',
                          {'ErrorReporteVacio': "--"})
        else:
            response = HttpResponse(content_type='application/ms-excel')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_pasante_con_clínica.xls"'
            titulo = "Reporte de pasante con clínica:"
            book = xlwt.Workbook()
            sh = book.add_sheet('Sheet1')
            array = ['Promedio Final', 'Nombre', 'Apellido Paterno',
                     'Apellido Materno',
                     'Matrícula', 'Clinica', 'Comisión']
            for i in range(array.__len__()):
                sh.write(0, i, array[i])
            n = 1
            for pasante in pasantes_filtrados:
                sh.write(n, 0, pasante['promedio'])
                sh.write(n, 1, pasante['nombre'])
                sh.write(n, 2, pasante['ap_pat'])
                sh.write(n, 3, pasante['ap_mat'])
                sh.write(n, 4, pasante['matricula'])
                sh.write(n, 5, pasante['clinica'])
                sh.write(n, 6, pasante['comision'])
                n += 1
            book.save(response)
            return response

    @login_required
    def reporte_busqueda_avanzada_pasante_clinica(request):
        pasantes_filtrados = json.loads(request.POST.get('pasantes_filtrados',
                                                         ''));
        if len(pasantes_filtrados) == 0:
            return render(request, 'busqueda_avanzada_pasante.html',
                          {'ErrorReporteVacio': "ErrorReporteVacio"})
        else:
            # Create the HttpResponse object with the appropriate PDF headers.
            response = HttpResponse(content_type='application/pdf')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_pasante_con_clínica.pdf"'
            buffer = BytesIO()
            doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=40,
                                    leftMargin=40, topMargin=40,
                                    bottomMargin=18)

            story = []
            styles = getSampleStyleSheet()
            estCel = styles["BodyText"]
            titulo = Paragraph("Reporte de pasante con clínica:",
                               styles['Heading1'])
            story.append(titulo)

            encabezados = (
                'Promedio Final', 'Nombre', 'Apellido Paterno',
                'Apellido Materno',
                'Matrícula', 'Clínica', 'Comisión')

            datos = [(
                Paragraph(str(pasante['promedio']), estCel),
                Paragraph(pasante['nombre'], estCel),
                Paragraph(pasante['ap_pat'], estCel),
                Paragraph(pasante['ap_mat'], estCel),
                Paragraph(pasante['matricula'], estCel),
                Paragraph(pasante['clinica'], estCel),
                Paragraph(pasante['comision'], estCel),
                Paragraph("", estCel),
                Paragraph("", estCel)) for pasante in pasantes_filtrados if 'nombre' in pasante]

            orden = Table([encabezados] + datos,
                          colWidths=[3 * cm, 3 * cm, 2.7 * cm, 2.7 * cm,
                                     2 * cm,
                                     2 * cm, 2 * cm, 0 * cm, 0 * cm])
            orden.setStyle(TableStyle(
                [
                    # Encabezados centrados
                    ('ALIGN', (0, 0), (8, 0), 'CENTER'),
                    # Bordes de color negro con grosor de 1
                    ('GRID', (0, 0), (-1, -1), 1, colors.black),
                    # Tamaño de las letras de 10
                    ('FONTSIZE', (0, 0), (-1, -1), 10),
                ]
            ))
            story.append(orden)
            doc.build(story)
            pdf = buffer.getvalue()
            response.write(pdf)
            buffer.close()
            return response

    @login_required
    def reporte_mostrar_pasante(request):
        pasantes_filtrados = json.loads(
            request.POST.get('pasantes_filtrados', ''));

        if len(pasantes_filtrados) == 0:
            return render(request, 'mostrar_pasantes.html',
                          {'ErrorReporteVacio': "--"})
        else:
            response = HttpResponse(content_type='application/pdf')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_pasante_con_clínica_asignada.pdf"'
            buffer = BytesIO()
            doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=40,
                                    leftMargin=40, topMargin=40,
                                    bottomMargin=18)

            story = []
            styles = getSampleStyleSheet()
            estCel = styles["BodyText"]
            titulo = Paragraph("Reporte de pasante:", styles['Heading1'])
            story.append(titulo)

            encabezados = (
                'Promedio Final', 'Nombre', 'Apellido Paterno',
                'Apellido Materno',
                'Matrícula')

            datos = [
                (Paragraph(pasante['promedio'], estCel),
                 Paragraph(pasante['nombre'], estCel),
                 Paragraph(pasante['ap_pat'], estCel),
                 Paragraph(pasante['ap_mat'], estCel),
                 Paragraph(pasante['matricula'], estCel),
                 ) for pasante in pasantes_filtrados
            ]

            orden = Table([encabezados] + datos,
                          colWidths=[3 * cm, 3 * cm, 2.7 * cm, 2.7 * cm,
                                     2 * cm])
            try:

                orden.setStyle(TableStyle(
                    [
                        # Encabezados centrados
                        ('ALIGN', (0, 0), (4, 0), 'CENTER'),
                        # Bordes de color negro con grosor de 1
                        ('GRID', (0, 0), (-1, -1), 1, colors.black),
                        # Tamaño de las letras de 10
                        ('FONTSIZE', (0, 0), (-1, -1), 10),
                    ]
                ))
                story.append(orden)
                doc.build(story)
                pdf = buffer.getvalue()
                response.write(pdf)
                buffer.close()
                pasante_global = {}
            except Exception as e:
                print(e)
            return response

    @login_required
    def reporte_avanzada_pasante(request):
        pasantes_filtrados = json.loads(
            request.POST.get('pasantes_filtrados', ''));
        if len(pasantes_filtrados) == 0:
            return render(request, 'mostrar_pasantes.html',
                          {'ErrorReporteVacio': "--"})
        else:
            response = HttpResponse(content_type='application/pdf')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_pasante_con_clínica_asignada.pdf"'
            buffer = BytesIO()
            doc = SimpleDocTemplate(buffer, pagesize=letter, rightMargin=40,
                                    leftMargin=40, topMargin=40,
                                    bottomMargin=18)

            story = []
            styles = getSampleStyleSheet()
            estCel = styles["BodyText"]
            titulo = Paragraph("Reporte de pasante:", styles['Heading1'])
            story.append(titulo)

            encabezados = (
                'Promedio Final', 'Nombre', 'Apellido Paterno',
                'Apellido Materno',
                'Matrícula')

            datos = [(
                Paragraph(str(pasante['promedio']), estCel),
                Paragraph(pasante['nombre'], estCel),
                Paragraph(pasante['ap_pat'], estCel),
                Paragraph(pasante['ap_mat'], estCel),
                Paragraph(pasante['matricula'], estCel),
            ) for pasante in pasantes_filtrados
            ]

            orden = Table([encabezados] + datos,
                          colWidths=[3 * cm, 3 * cm, 2.7 * cm, 2.7 * cm,
                                     2 * cm])
            try:

                orden.setStyle(TableStyle(
                    [
                        # Encabezados centrados
                        ('ALIGN', (0, 0), (4, 0), 'CENTER'),
                        # Bordes de color negro con grosor de 1
                        ('GRID', (0, 0), (-1, -1), 1, colors.black),
                        # Tamaño de las letras de 10
                        ('FONTSIZE', (0, 0), (-1, -1), 10),
                    ]
                ))
                story.append(orden)
                doc.build(story)
                pdf = buffer.getvalue()
                response.write(pdf)
                buffer.close()

            except Exception as e:
                print(e)
            return response


class BajaPasante:
    @login_required
    def desactivar_pasante(request):
        """Función para desactivar un pasante registrado. Recibe por POST el id del pasante y lo desactiva."""
        values = Pasante.objects.exclude(
            id_pasante__in=AsignacionClinicaPasante.objects.all().values(
                'id_pasante_fk'))
        id_pasante = request.POST.get('desactivar', '')
        datos = ""
        if (id_pasante != ""):
            datos = Pasante.objects.get(pk=id_pasante)
            pasante = Pasante(id_pasante=id_pasante, nombre=datos.nombre,
                              apellido_paterno=datos.apellido_paterno,
                              apellido_materno=datos.apellido_materno,
                              curp=datos.curp,
                              rfc=datos.rfc, status='desactivado',
                              id_direccion_fk=datos.id_direccion_fk,
                              # id_asignacion_fk=datos.id_asignacion_fk,
                              id_datos_academicos_fk=datos.id_datos_academicos_fk,
                              id_usuario_fk=Usuario.objects.get(
                                  pk=datos.id_usuario_fk_id), foto=datos.foto)
            pasante.save()
            mensajes[0] = "El pasante se desactivó"
            return HttpResponseRedirect("/Coordinador/")

        return render(request, 'mostrar_pasantes.html',
                      {'pasantes': Pasante.objects.all(),
                       'asignacion': AsignacionClinicaPasante.objects.all(),
                       'boolean': True, 'values': values,
                       'clinicas': Clinica.objects.all()})


class ActivaPasante:
    @login_required
    def activar_pasante(request):
        """Función para activar un pasante registrado. Recibe por POST el id del pasante y lo activa."""
        values = Pasante.objects.exclude(
            id_pasante__in=AsignacionClinicaPasante.objects.all().values(
                'id_pasante_fk'))
        id_pasante = request.POST.get('activar', '')
        datos = ""
        if (id_pasante != ""):
            datos = Pasante.objects.get(pk=id_pasante)
            pasante = Pasante(id_pasante=id_pasante, nombre=datos.nombre,
                              apellido_paterno=datos.apellido_paterno,
                              apellido_materno=datos.apellido_materno,
                              curp=datos.curp,
                              rfc=datos.rfc, status='activo',
                              id_direccion_fk=datos.id_direccion_fk,
                              # id_asignacion_fk=datos.id_asignacion_fk,
                              id_datos_academicos_fk=datos.id_datos_academicos_fk,
                              id_usuario_fk=Usuario.objects.get(
                                  pk=datos.id_usuario_fk_id), foto=datos.foto)
            pasante.save()
            mensajes[0] = "El pasante se activó"

            return HttpResponseRedirect("/Coordinador/")
        return render(request, 'mostrar_pasantes.html',
                      {'pasantes': Pasante.objects.all(),
                       # TODO Implementar la asignación del pasante a la clínica
                       'asignacion': AsignacionClinicaPasante.objects.all(),
                       'boolean': True, 'values': values,
                       'clinicas': Clinica.objects.all()})


class AsignaClinica:
    @login_required
    def asignar_clinica(request):
        """Función para asignar una clínica a un pasante. Recibe el id del pasante, clínica y la comisión."""
        asignacion_clinica_pasante = request.POST.get('comision', '')
        pasantes = Pasante.objects.all()

        clinicas = Clinica.objects.all()
        values = Pasante.objects.exclude(
            id_pasante__in=AsignacionClinicaPasante.objects.all().values(
                'id_pasante_fk'))
        guardar_asignacion = asignacion_clinica_pasante.split(":")
        if (asignacion_clinica_pasante != ""):
            guardar = AsignacionClinicaPasante(comision=guardar_asignacion[2],
                                               id_pasante_fk=Pasante.objects.get(
                                                   pk=guardar_asignacion[0]),
                                               id_clinica_fk=Clinica.objects.get(
                                                   pk=guardar_asignacion[1]))
            guardar.save()
            pasante_aux = Pasante.objects.get(pk=guardar_asignacion[0])
            pasante_aux.id_asignacion_fk = guardar
            pasante_aux.save()
            mensajes[0] = "Se le asignó la clínica al pasante."
            return HttpResponseRedirect("/Coordinador/")
        return render(request, 'mostrar_pasantes.html', {'pasantes': pasantes,
                                                         'asignacion': AsignacionClinicaPasante.objects.all(),
                                                         'vacio': len(
                                                             AsignacionClinicaPasante.objects.all()),
                                                         'clinicas': clinicas,
                                                         'values': values})


from pasante.forms import FormPasante


def recarga_form_inicial_pasante(datos_usuario, direccion_pasante, fijo, movil,
                                 datos_academicos):
    """Función para recargar el form del pasante al modificar al utilizar la función modificar pasante."""
    return {
        'nombre': datos_usuario.nombre,
        'apellido_paterno': datos_usuario.apellido_paterno,
        'apellido_materno': datos_usuario.apellido_materno,
        'curp': datos_usuario.curp,
        'rfc': datos_usuario.rfc,
        'foto': datos_usuario.foto,
        'promedio_final': datos_academicos.promedio,
        'número_fijo': fijo.numero,
        'número_móvil': movil.numero,
        'calle': direccion_pasante.calle,
        'número_exterior': direccion_pasante.numero,
        'colonia': direccion_pasante.colonia,
    }


def recarga_form_post_pasante(request, pasante):
    """recarga el form en caso de que exista algún error en los campos"""
    return {
        'nombre': request.POST.get('nombre'),
        'apellido_paterno': request.POST.get('apellido_paterno'),
        'apellido_materno': request.POST.get('apellido_materno'),
        'curp': request.POST.get('curp'),
        'rfc': request.POST.get('rfc'),
        'foto': pasante.foto,
        # Validación promedio.
        'promedio_final': request.POST.get('promedio_final'),
        'número_fijo': request.POST.get('número_fijo'),
        'número_móvil': request.POST.get('número_móvil'),
        'calle': request.POST.get('calle'),
        'número_exterior': request.POST.get('número_exterior'),
        'colonia': request.POST.get('colonia'),
    }


class ModificaPasante:
    """Función para modificar un Pasante. Se valida cada campo y en caso de que exista un error regresa un mensaje."""

    @login_required
    def modificar_pasante(request):
        estados = Estado.objects.all()
        """función que realiza la modificación de un Pasante
            si todo es correcto devuelve un mensaje de la modificación exito
            en caso contrario muestra los diferentes mensajes de error.
        """
        # se toman los datos del mostrar pasante , para diferenciar el Pasante seleccionado.
        id_pasante = request.POST.get('modificar')
        try:
            # se obtiene el objeto del Pasante seleccionado.
            pasante = Pasante.objects.get(pk=id_pasante)
            direccion_pasante = DireccionPasante.objects.get(
                pk=pasante.id_direccion_fk_id)
            telefonos_pasante = Telefono.objects.filter(
                id_pasante_fk_id=pasante.id_pasante)
            datos_academicos_pasante = DatosAcademicos.objects.get(
                pk=pasante.id_datos_academicos_fk_id)
            datos_usuario = Usuario.objects.get(
                id_usuario=pasante.id_usuario_fk_id)

            fijo = None
            movil = None
            for i in telefonos_pasante:
                if (i.tipo == "fijo"):
                    fijo = i
                elif (i.tipo == "movil"):
                    movil = i
        except Exception as e:
            return redirect("/Coordinador/")
        est = direccion_pasante.estado.id_estado
        mun = direccion_pasante.municipio.id_municipio
        # se pasan los datos del Pasante seleccionado al form que generara la vista del form
        # si se hace una peticion post se ejecutara el codigo para guardar los datos
        if request.method == 'POST':
            pasante = Pasante.objects.get(pk=id_pasante)
            form = FormPasante(
                initial=recarga_form_inicial_pasante(pasante,
                                                     direccion_pasante,
                                                     fijo, movil,
                                                     datos_academicos_pasante))
            if request.POST.get('guardar') == "True":
                id_estado = request.POST.get('estados')
                id_municipio = request.POST.get('municipios')
                # validación nombre
                nombre = ""
                if (es_alfabetica(request.POST.get('nombre'))):
                    nombre = request.POST.get('nombre')
                    if (nombre == ""):
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'nombre': 'El campo "Nombre" no puede ser vacío.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                else:
                    
                    form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'nombre': '"Nombre" contiene números o caracteres especiales.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                # validación apellido paterno
                ap = ""
                if (es_alfabetica(request.POST.get('apellido_paterno'))):
                    ap = request.POST.get('apellido_paterno')
                    if (ap == ""):
                    
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'ap': 'El campo "Apellido Paterno" no puede ser vacío.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                else:
                    
                    form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'ap': '"Apellido Paterno" contiene números o caracteres especiales.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                # validación apellido materno
                am = ""
                if (request.POST.get('apellido_materno') == ""):
                    pass
                else:
                    if (es_alfabetica(request.POST.get('apellido_materno'))):
                        am = request.POST.get('apellido_materno')
                    else:
                        
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'am': '"Apellido Materno" contiene números o caracteres especiales.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                # validación curp
                curp = request.POST.get('curp')

                
                form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                if (curp == ""):
                    
                    form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))

                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'curp': 'El campo "CURP" no debe ser vacío',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})

                if (len(curp) < 18):
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'curp': '"CURP" debe contener 18 Caracteres.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})

                # validación rfc.
                rfc = request.POST.get('rfc')
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))
                
                if (rfc == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))
                
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'rfc': 'El campo "RFC" no debe ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                if (len(rfc) < 10):
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'rfc': '"RFC" debe contener como mínimo 10 Digitos, máximo 13.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                foto = None
                try:
                    if (request.FILES.get('foto') != None):
                        if (
                                not request.FILES.get(
                                    'foto').name.lower().endswith(
                                    '.jpg')):
                            return render(request, 'modificar_pasante.html',
                                          {'form': form,
                                           'rfc': 'Debes de seleccionar una imagen jpg.',
                                           'estados': estados,
                                           'est': id_estado,
                                           'mun': id_municipio,
                                           'pasante': pasante,
                                           'usuario': datos_usuario})
                        foto = request.FILES.get('foto')
                    else:
                        pass

                except (KeyError):
                    pass
                # Validación promedio.
                promedio = request.POST.get('promedio_final')
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))
                if (request.POST.get('promedio_final') != ""):
                    try:
                        promedio = request.POST.get('promedio_final')
                        if (float(promedio) > 10 or float(promedio) < 0):
                            return render(request, 'modificar_pasante.html',
                                          {'form': form,
                                           'promedio_rango': '"Promedio" debe estar en un rango de 0 y 10.',
                                           'estados': estados,
                                           'est': id_estado,
                                           'mun': id_municipio,
                                           'pasante': pasante,
                                           'usuario': datos_usuario})
                    except ValueError:
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'promedio_num': '"Promedio" no es númerico.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                # validación número fijo.
                numero_fijo = request.POST.get('número_fijo')
                if (request.POST.get('número_fijo') != ""):
                    try:
                        numero_fijo = request.POST.get('número_fijo')
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                        numero_fijo = request.POST.get('número_fijo')
                        if (int(numero_fijo) < 0):
                            return render(request, 'modificar_pasante.html',
                                          {'form': form,
                                           'numero_fijo': '"Número fijo" no debe contener valores negativos.',
                                           'estados': estados,
                                           'est': id_estado,
                                           'mun': id_municipio,
                                           'pasante': pasante,
                                           'usuario': datos_usuario})
                        if (len(request.POST.get('número_fijo')) < 7):
                            return render(request, 'modificar_pasante.html',
                                          {'form': form,
                                           'numero_fijo': '"Número fijo" debe contener por lo menos 7 Digitos.',
                                           'estados': estados,
                                           'est': id_estado,
                                           'mun': id_municipio,
                                           'pasante': pasante,
                                           'usuario': datos_usuario})
                    except ValueError:
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'numero_fijo': '"Número fijo" no es númerico.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                # validar número móvil
                try:
                    numero_movil = request.POST.get('número_móvil')
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))
                    
                    if (numero_movil == ""):
                        form = FormPasante(
                            initial=recarga_form_post_pasante(request,
                                                              pasante))
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'numero_movil': 'El campo "Número móvil" no debe ser vacío.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})

                    if (int(numero_movil) < 0):
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'numero_movil': '"Número móvil" no debe contener valores negativos.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                    if (len(request.POST.get('número_móvil')) < 10):
                        return render(request, 'modificar_pasante.html',
                                      {'form': form,
                                       'numero_movil': '"Número móvil" debe contener por lo menos 10 Digitos.',
                                       'estados': estados, 'est': id_estado,
                                       'mun': id_municipio,
                                       'pasante': pasante,
                                       'usuario': datos_usuario})
                except ValueError:
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'numero_movil': '"Número móvil" no es númerico.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})

                calle = request.POST.get('calle')
                if (calle == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                              pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'numero_movil': 'El campo "Calle" no puede ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                numero_exterior = request.POST.get('número_exterior')
                if (numero_exterior == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                            pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'numero_movil': 'El campo "Número Exterior" no puede ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                colonia = request.POST.get('colonia')
                if (colonia == ""):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                            pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'numero_movil': 'El campo "Colonia" no puede ser vacío.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))
                
                if (id_estado == "vacio"):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'estados_error': 'Debes seleccionar un "Estado" de la lista.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                if (id_municipio == "vacio"):
                    form = FormPasante(
                        initial=recarga_form_post_pasante(request,
                                                          pasante))
                    return render(request, 'modificar_pasante.html',
                                  {'form': form,
                                   'municipios_error': 'Debes seleccionar un "Municipio" de la lista.',
                                   'estados': estados, 'est': id_estado,
                                   'mun': id_municipio,
                                   'pasante': pasante,
                                   'usuario': datos_usuario})
                direccion_pasante_modificar = DireccionPasante(
                    id_direccion=direccion_pasante.id_direccion,
                    numero=numero_exterior, calle=calle,
                    colonia=colonia,
                    municipio=Municipio.objects.get(pk=id_municipio),
                    estado=Estado.objects.get(pk=id_estado))
                direccion_pasante_modificar.save()
                # se guardan datos academicos
                if (promedio == ""):
                    datos_academicos = DatosAcademicos(
                        id_datos_academicos=datos_academicos_pasante.id_datos_academicos,
                        promedio=0)
                    datos_academicos.save()
                else:
                    datos_academicos = DatosAcademicos(
                        id_datos_academicos=datos_academicos_pasante.id_datos_academicos,
                        promedio=promedio)
                    datos_academicos.save()
                if foto:
                    foto.name
                    Pasante.objects.get(
                        id_pasante=pasante.id_pasante).foto.delete(save=True)
                    pasante_modificar = Pasante(id_pasante=pasante.id_pasante,
                                                nombre=nombre,
                                                apellido_paterno=ap,
                                                apellido_materno=am, curp=curp,
                                                rfc=rfc, status=pasante.status,
                                                id_direccion_fk=direccion_pasante,
                                                # id_asignacion_fk=pasante.id_asignacion_fk,
                                                id_datos_academicos_fk=datos_academicos,
                                                id_usuario_fk_id=datos_usuario.id_usuario,
                                                foto=foto)
                    # se guarda al pasante
                    pasante_modificar.save()
                else:
                    pasante_modificar = Pasante(id_pasante=pasante.id_pasante,
                                                nombre=nombre,
                                                apellido_paterno=ap,
                                                apellido_materno=am, curp=curp,
                                                rfc=rfc, status=pasante.status,
                                                id_direccion_fk=direccion_pasante,
                                                id_datos_academicos_fk=datos_academicos,
                                                id_usuario_fk_id=datos_usuario.id_usuario,
                                                foto=pasante.foto)
                    # se guarda al pasante
                    pasante_modificar.save()
                # se guardan teléfonos del pasante.
                telefono1 = Telefono(
                    id_telefono=telefonos_pasante[0].id_telefono,
                    numero=str(numero_fijo), tipo="fijo",
                    id_pasante_fk=pasante)
                telefono2 = Telefono(
                    id_telefono=telefonos_pasante[1].id_telefono,
                    numero=str(numero_movil), tipo="movil",
                    id_pasante_fk=pasante)
                telefono1.save()
                telefono2.save()
                pasante = Pasante.objects.get(pk=id_pasante)
                form = FormPasante(initial=recarga_form_post_pasante(request,
                                                                     pasante))
                
                direccion_pasante = DireccionPasante.objects.get(
                    pk=pasante.id_direccion_fk_id)
                est = direccion_pasante.estado.id_estado
                mun = direccion_pasante.municipio.id_municipio
                return render(request, 'modificar_pasante.html',
                              {'form': form, 'estados': estados, 'est': est,
                               'mun': mun,
                               'pasante': pasante, 'usuario': datos_usuario,
                               'funcion': 'El pasante con la matrícula ' + datos_usuario.matricula + ' ha sido modificado.'})

            else:
                form = FormPasante(
                    initial=recarga_form_inicial_pasante(pasante,
                                                         direccion_pasante,
                                                         fijo,
                                                         movil,
                                                         datos_academicos_pasante))
            return render(request, 'modificar_pasante.html',
                          {'form': form, 'estados': estados, 'est': est,
                           'mun': mun, 'pasante': pasante,
                           'usuario': datos_usuario})


from coordinador.models import Informacion as InformacionModel
from subprocess import call
from django.conf import settings

mensaje_alta_info = {}


class AltaInformacion:
    """Función para dar de alta información. El documente tiene que ser PDF, en caso de no serlo, muestra un mensaje de error."""

    @login_required
    def subir_informacion(request):
        if (len(mensaje_alta_info) > 0):
            mensaje_alta_info.clear()
            form = FormInformacion()
            return render(request, 'subir_informacion.html', {'form': form,
                                                              'exito': 'La información ha sido agregada.'})
        if request.method == 'POST':
            form = FormInformacion(request.POST, request.FILES)
            if form.is_valid():
                if (form.data['nombre_archivo'] == ""):
                    return render(request, 'subir_informacion.html',
                                  {'form': form,
                                   'error': 'El campo "Nombre del archivo" no puede ser vacío".'})
                if (form.data['descripcion'] == ""):
                    return render(request, 'subir_informacion.html',
                                  {'form': form,
                                   'error': 'El campo "Descripción" no puede ser vacío".'})
                if (request.FILES.get('ruta_archivo') == None):
                    return render(request, 'subir_informacion.html',
                                  {'form': form,
                                   'error': 'El campo "Archivo no puede ser vacío".'})
                if not request.FILES.get('ruta_archivo').name.endswith('.pdf'):
                    return render(request, 'subir_informacion.html',
                                  {'form': form,
                                   'error': 'Debes de agregar un archivo pdf.'})
                pdf = form.save()
                call(['/usr/bin/pdf2htmlEX',
                      settings.MEDIA_ROOT + '/' + str(pdf.ruta_archivo),
                      'media/' + str(pdf.ruta_archivo) + '.html'])
                pdf.ruta_archivo_html = str(pdf.ruta_archivo) + '.html'
                pdf.save()
                mensaje_alta_info[0] = "agregó"
                form = FormInformacion()
                return HttpResponseRedirect("/Coordinador/subir_informacion/")
        else:
            form = FormInformacion()
        return render(request, 'subir_informacion.html',
                      {'form': form, 'funcion': 'Nuevo'})


class MuestraInformacion:
    """Función para mostrar la información agregada"""

    @login_required
    def mostrar_informacion(request):
        info = InformacionModel.objects.all()
        if (len(info) > 0):
            return render(request, 'mostrar_informacion.html', {'info': info})
        else:
            return render(request, 'mostrar_informacion.html',
                          {'no_info': 'No hay archivos disponibles.'})


mensaje_elimina_info = {}


class BajaInformacion:
    @login_required
    def eliminar_informacion(request):
        """Función para eliminar un documento agregado previamente. En caso de que el documento haya sido eliminaó, muestra un mensaje de éxito"""
        if (len(mensaje_elimina_info) > 0):
            mensaje_elimina_info.clear()
            return render(request, 'mostrar_informacion.html',
                          {'info': InformacionModel.objects.all(),
                           'exito': 'El documento ha sido eliminado.'})
        id_informacion = request.POST.get('eliminar')
        if (id_informacion != None):
            InformacionModel.objects.get(
                id_informacion=id_informacion).ruta_archivo.delete(save=True)
            InformacionModel.objects.get(
                id_informacion=id_informacion).ruta_archivo_html.delete(
                save=True)
            eliminar = InformacionModel.objects.get(
                id_informacion=id_informacion)
            eliminar.delete()
            mensaje_elimina_info[0] = "eliminó"
            return HttpResponseRedirect("/Coordinador/eliminar_informacion/")
        return render(request, 'mostrar_informacion.html',
                      {'info': InformacionModel.objects.all()})


class SeleccionaMultiplesRegistrosPasante:
    @login_required
    def seleccionar_multiples_registros_pasante(request):
        """Función para seleccionar múltiples pasantes. El if verifica si el botón presionado fue el de desactivar o
        en caso contrario activar. Se valida si se seleccionan solo activos y quieres activarlos, muestra mensaje de error.
        Se valida si se seleccionan solo inactivos y quieres desactivarlos, muestra mensaje de error. Se valida si no seleccionas nada y presionas
        alguno de los dos botones."""
        cont_des = 0
        cont_act = 0
        if (request.POST.get('des_button') != None):
            pasantes = request.POST.getlist('checks[]')
            if (len(pasantes) == 0):
                mensajes_error[
                    0] = "Debes de seleccionar por lo menos un Pasante."
                return HttpResponseRedirect("/Coordinador/")
            else:
                for i in pasantes:
                    pasante = Pasante.objects.get(id_pasante=i)
                    if (pasante.status == "desactivado"):
                        cont_des += 1
                if (cont_des > 0):
                    mensajes_error[
                        0] = "Solo debes seleccionar pasantes activos."
                else:
                    for i in pasantes:
                        pasante = Pasante.objects.get(id_pasante=i)

                        pasante.status = "desactivado"
                        pasante.save()
                        mensajes[
                            0] = "Los pasantes seleccionados han sido desactivados."

                return HttpResponseRedirect("/Coordinador/")
        elif (request.POST.get('act_button') != None):
            pasantes = request.POST.getlist('checks[]')
            if (len(pasantes) == 0):
                mensajes_error[
                    0] = "Debes de seleccionar por lo menos un pasante."
                return HttpResponseRedirect("/Coordinador/")
            else:
                for i in pasantes:
                    pasante = Pasante.objects.get(id_pasante=i)
                    if (pasante.status == "activo"):
                        cont_act += 1
                if (cont_act > 0):
                    mensajes_error[
                        0] = "Solo debes seleccionar pasantes desactivados."
                else:
                    for i in pasantes:
                        pasante = Pasante.objects.get(id_pasante=i)
                        pasante.status = "activo"
                        pasante.save()
                        mensajes[
                            0] = "Los pasantes seleccionados han sido activados."
                return HttpResponseRedirect("/Coordinador/")


class BuscaClinica:
    clinicasBusq = {}
    global clinica_global

    @login_required
    def busca_clinica(request):
        clinicas = Clinica.objects.all()

        if (len(mensaje_des_clinica) > 0):
            mensaje_des_clinica.clear()

            return render(request, 'mostrar_clinicas.html', {
                'clinicas': clinicas,
                'hola': "La Clínica se desactivó"
            })

        if (len(mensaje_ac_clinica) > 0):
            mensaje_ac_clinica.clear()

            return render(request, 'mostrar_clinicas.html', {
                'clinicas': clinicas,
                'hola': "La Clínica se activó"
            })

        return render(request, 'mostrar_clinicas.html', {
            'clinicas': clinicas,
            'hola': request.GET.get('funcion')
        })

    @login_required
    def reporte_busqueda_avanzada_clinicas_xls(request):
        clinicasFiltradas = json.loads(request.POST.get(
            'clinicas_filtradas', ''))
        if len(clinicasFiltradas) == 0:
            return render(request, 'mostrar_clinicas.html',
                          {'ErrorReporteVacio': "ErrorReporteVacio"})
        else:
            response = HttpResponse(content_type='application/ms-excel')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_busqueda_avanzada_clinica.xls"'
            book = xlwt.Workbook()
            sh = book.add_sheet('Sheet1')
            array = ['Nombre de Unidad', 'Tipo Plaza', 'Complemento',
                     'Observación', 'Institución', 'Status', 'Localidad',
                     'Municipio', 'Estado']
            for i in range(array.__len__()):
                sh.write(0, i, array[i])
            n = 1
            for clinica in clinicasFiltradas:
                sh.write(n, 0, clinica['unidad'])
                sh.write(n, 1, clinica['tipoPlaza'])
                sh.write(n, 2, clinica['comple'])
                sh.write(n, 3, clinica['observacion'])
                sh.write(n, 4, clinica['institucion'])
                sh.write(n, 5, clinica['status'])
                sh.write(n, 6, clinica['localidad'])
                sh.write(n, 7, clinica['municipio'])
                sh.write(n, 8, clinica['estado'])
                n += 1
            book.save(response)
            return response

    @login_required
    def busqueda_avanzada_clinica(request):
        estados = Estado.objects.all()
        clinicas = Clinica.objects.all()
        ids_institucion = [clinica.id_institucion_fk for clinica in clinicas]
        instituciones = list(set(ids_institucion))

        nombre_unidad1 = request.POST.get('unidad_filter-1', '')
        nombre_unidad2 = request.POST.get('unidad_filter-2', '')

        plaza_federal1 = request.POST.get('plaza_federal_filter-1', "")
        plaza_federal2 = request.POST.get('plaza_federal_filter-2', "")

        complemento_filter1 = request.POST.get('complemento_filter-1', "")
        complemento_filter2 = request.POST.get('complemento_filter-2', "")

        institucion_filter1 = request.POST.get('institucion_filter-1', "")
        institucion_filter2 = request.POST.get('institucion_filter-2', "")

        status_filter1 = request.POST.get('status_filter-1', "")
        status_filter2 = request.POST.get('status_filter-2', "")

        localidad_filter1 = request.POST.get('localidad_filter-1', "")
        localidad_filter2 = request.POST.get('localidad_filter-2', "")

        municipio_filter1 = request.POST.get('municipios1', '')
        municipio_filter2 = request.POST.get('municipios2', '')

        estado_filter1 = request.POST.get('estados1', '')
        estado_filter2 = request.POST.get('estados2', '')

        est = Estado.objects.all()
        clinicasBusq = Clinica.objects.all()

        if nombre_unidad2 == "":
            clinicasBusq = clinicasBusq.filter(
                Q(nombre_unidad__icontains=nombre_unidad1))
        else:
            clinicasBusq = clinicasBusq.filter(
                Q(nombre_unidad__icontains=nombre_unidad1) | Q(
                    nombre_unidad__icontains=nombre_unidad2))

        if plaza_federal2 == "":
            clinicasBusq = clinicasBusq.filter(
                Q(plaza_fed__icontains=plaza_federal1))
        else:
            clinicasBusq = clinicasBusq.filter(
                Q(plaza_fed__icontains=plaza_federal1) | Q(
                    plaza_fed__icontains=plaza_federal2))

        if complemento_filter2 == "":
            clinicasBusq = clinicasBusq.filter(
                Q(complemento__icontains=complemento_filter1))
        else:
            clinicasBusq = clinicasBusq.filter(
                Q(complemento__icontains=complemento_filter1) | Q(
                    complemento__icontains=complemento_filter2))

        if institucion_filter2 == "":
            clinicasBusq = clinicasBusq.filter(
                Q(
                    id_institucion_fk__nombre_institucion__icontains=institucion_filter1))
        else:
            clinicasBusq = clinicasBusq.filter(
                Q(
                    id_institucion_fk__nombre_institucion__icontains=institucion_filter1) | Q(
                    id_institucion_fk__nombre_institucion__icontains=institucion_filter2))

        if status_filter2 == "":
            clinicasBusq = clinicasBusq.filter(
                Q(status__icontains=status_filter1))
        else:
            clinicasBusq = clinicasBusq.filter(
                Q(status__icontains=status_filter1) | Q(
                    status__icontains=status_filter2))

        if localidad_filter2 != "" or localidad_filter1 != "":
            if localidad_filter2 == "":
                clinicasBusq = clinicasBusq.filter(
                    id_direccion_fk__localidad__icontains=localidad_filter1)
            else:
                clinicasBusq = clinicasBusq.filter(Q(
                    id_direccion_fk__localidad__icontains=localidad_filter1) | Q(
                    id_direccion_fk__localidad__icontains=localidad_filter2))

        if municipio_filter2 != "" or municipio_filter1 != "":
            # .strip()
            if municipio_filter2 == "":
                m = Municipio.objects.get(pk=municipio_filter1)
                clinicasBusq = clinicasBusq.filter(
                    id_direccion_fk__municipio__nombre__icontains=m.nombre)
            else:
                if (municipio_filter1 != ""):
                    m1 = Municipio.objects.get(pk=municipio_filter1)
                m2 = Municipio.objects.get(pk=municipio_filter2)
                clinicasBusq = clinicasBusq.filter(Q(
                    id_direccion_fk__municipio__nombre__icontains=m1.nombre) | Q(
                    id_direccion_fk__municipio__nombre__icontains=m2.nombre))

        if estado_filter2 != "" or estado_filter1 != "":
            if estado_filter2 == "":
                e1 = Estado.objects.get(pk=estado_filter1)
                clinicasBusq = clinicasBusq.filter(
                    id_direccion_fk__estado__nombre__icontains=e1.nombre)
            else:
                if (estado_filter1 != ""):
                    e1 = Estado.objects.get(pk=estado_filter1)
                    e2 = Estado.objects.get(pk=estado_filter2)
                    clinicasBusq = clinicasBusq.filter(
                        Q(
                            id_direccion_fk__estado__nombre__icontains=e1.nombre) | Q(
                            id_direccion_fk__estado__nombre__icontains=e2.nombre))
        global clinica_global
        clinica_global = clinicasBusq

        asignacion_rol = []
        roles = Rol.objects.all()
        for rol in roles:
            print('Rol: ' + str(rol.id_rol))

            asignaciones = AsignacionRol.objects.filter(
                id_rotacion__id_rol_fk__id_rol=rol.id_rol).order_by(
                'id_rotacion__fecha_inicio')

            hoy = datetime.now().date()

            rotacion_actual = False

            for asignacion in asignaciones:
                inicio = asignacion.id_rotacion.fecha_inicio
                fin = asignacion.id_rotacion.fecha_fin

                if hoy >= inicio and hoy <= fin:
                    asignacion_rol.append(asignacion)
                    rotacion_actual = True
            if not rotacion_actual and len(asignaciones) > 0:
                asignacion_rol.append(asignaciones[0])

        return render(request, 'busqueda_avanzada_clinica.html',
                      {'clinicas': clinicasBusq, 'estados': estados,
                       'asignaciones': asignacion_rol,
                       'instituciones': instituciones})

    global clinica_global

    @login_required
    def reporte_busqueda_avanzada_clinicas(request):
        clinicasFiltradas = json.loads(
            request.POST.get('clinicas_filtradas', ''))

        if len(clinicasFiltradas) == 0:
            return render(request, 'busqueda_avanzada_clinica.html',
                          {'ErrorReporteVacio': "ErrorReporteVacio"})
        else:
            # Create the HttpResponse object with the appropriate PDF headers.
            response = HttpResponse(content_type='application/pdf')
            response[
                'Content-Disposition'] = 'attachment; filename="Reporte_Clínicas.pdf"'

        # Create the HttpResponse object with the appropriate PDF headers.
        response = HttpResponse(content_type='application/pdf')
        response[
            'Content-Disposition'] = 'attachment; filename="Reporte_clinicas.pdf"'

        buffer = BytesIO()
        doc = SimpleDocTemplate(buffer, pagesize=landscape(letter),
                                rightMargin=40,
                                leftMargin=40, topMargin=40, bottomMargin=18)

        story = []
        styles = getSampleStyleSheet()
        estCel = styles["BodyText"]
        titulo = Paragraph("Reporte de Clínicas:", styles['Heading1'])
        story.append(titulo)
        clinicas = clinicasFiltradas
        encabezados = ('Unidad', 'Tipo Plaza', 'Complemento', 'Observación',
                       'Institución', 'Status', 'Localidad', 'Municipio',
                       'Estado')
        datos = [(Paragraph(clinica['unidad'], estCel),
                  Paragraph(clinica['tipoPlaza'], estCel),
                  Paragraph(clinica['comple'], estCel),
                  Paragraph(clinica['observacion'], estCel),
                  Paragraph(clinica['institucion'], estCel),
                  Paragraph(clinica['status'], estCel),
                  Paragraph(clinica['localidad'], estCel),
                  Paragraph(clinica['municipio'], estCel),
                  Paragraph(clinica['estado'], estCel)) for
                 clinica in clinicas]

        orden = Table([encabezados] + datos,
                      colWidths=[3 * cm, 2 * cm, 3 * cm, 4.5 * cm, 2 * cm,
                                 1.7 * cm, 3.5 * cm, 3.5 * cm, 3.5 * cm])
        orden.setStyle(TableStyle(
            [
                # Encabezados centrados
                ('ALIGN', (0, 0), (7, 0), 'CENTER'),
                # Bordes de color negro con grosor de 1
                ('GRID', (0, 0), (-1, -1), 1, colors.black),
                # Tamaño de las letras de 10
                ('FONTSIZE', (0, 0), (-1, -1), 10),
            ]
        ))
        story.append(orden)
        doc.build(story)
        pdf = buffer.getvalue()
        response.write(pdf)
        buffer.close()
        return response


class AltaClinica:
    @login_required
    def registrar_clinica(request):
        """función que realiza el registro de una clinica
        si todo es correcto devuelve un mensaje de la clinica se registró
        en caso contrario muestra los diferentes mensajes de error.
        """

        estados = Estado.objects.all()
        datos_usuario = Clinica
        clinicas = Clinica.objects.all();

        if request.method == 'POST':
            form = FormClinica(request.POST)
            est = request.POST.get('estados')
            mun = request.POST.get('municipios')
            # "nombre_unidad":form.data['nombre_unidad'],"plaza_fed":form.data['plaza_fed'],"complemento":form.data['complemento'],"beca":form.data['beca'],"observación":form.data['observación'],"localidad":form.data['localidad']
            if form.data['nombre_unidad'] == "":
                return render(request, 'registrar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Nombre de unidad" no debe ser vacío.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "nombre_unidad1": form.data['nombre_unidad'],
                    "plaza_fed1": form.data['plaza_fed'],
                    "complemento1": form.data['complemento'],
                    # "beca1":form.data['beca'],
                    "observación1": form.data['observación'],
                    "localidad1": form.data['localidad'],
                    "est": est,
                    "mun": mun})

            if form.data['observación'] == "":
                return render(request, 'registrar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Observación" no debe ser vacío',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "nombre_unidad1": form.data['nombre_unidad'],
                    "plaza_fed1": form.data['plaza_fed'],
                    "complemento1": form.data['complemento'],
                    # "beca1":form.data['beca'],
                    "observación1": form.data['observación'],
                    "localidad1": form.data['localidad'],
                    "est": est,
                    "mun": mun
                })

            if form.data['localidad'] == "":
                return render(request, 'registrar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Localidad" no debe ser vacío',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "nombre_unidad1": form.data['nombre_unidad'],
                    "plaza_fed1": form.data['plaza_fed'],
                    "complemento1": form.data['complemento'],
                    # "beca1":form.data['beca'],
                    "observación1": form.data['observación'],
                    "localidad1": form.data['localidad'],
                    "est": est,
                    "mun": mun})

            if form.is_valid():
                id_estado = request.POST.get('estados')
                id_municipio = request.POST.get('municipios')
                nombre_unidad = ""
                nombre_unidad = form.data['nombre_unidad']
                plaza_fed = ""

                if clinicas.filter(nombre_unidad=request.POST.get(
                        "nombre_unidad")).exists():
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'alerta': 'El "Nombre de Unidad" ya está en uso',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1":form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})

                if (es_alfabetica(form.data['plaza_fed'])):
                    plaza_fed = form.data['plaza_fed']
                else:
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'plaza_fed': '"Plaza Fed." no debe contener números o caracteres especiales',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1":form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})

                complemento = ""

                if (es_alfabetica(form.data['complemento'])):
                    complemento = form.data['complemento']
                else:
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'complemento': '"Complemento" no debe contener números o caracteres especiales',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1":form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})
                # beca = form.data['beca']
                observacion = form.data['observación']

                localidad = ""

                if (es_alfabetica(form.data['localidad'])):
                    localidad = form.data['localidad']
                else:
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'localidad': '"Localidad" no debe contener números o caracteres especiales',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1": form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})

                if (id_estado == "vacio"):
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'localidad': 'Debes seleccionar un "Estado" de la lista.',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1": form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})

                if (id_municipio == "vacio"):
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'localidad': 'Debes seleccionar un "Municipio" de la lista.',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1": form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})

                institucion = request.POST.get('institucion')

                if institucion == '0':
                    return render(request, 'registrar_clinica.html', {
                        'form': form,
                        'institucion_error': 'Debes seleccionar una '
                                             '"Institución" de la lista.',
                        'usuario': datos_usuario, 'estados': estados,
                        "nombre_unidad1": form.data['nombre_unidad'],
                        "plaza_fed1": form.data['plaza_fed'],
                        "complemento1": form.data['complemento'],
                        # "beca1": form.data['beca'],
                        "observación1": form.data['observación'],
                        "localidad1": form.data['localidad'],
                        "est": est, "mun": mun})

                # se guarda dirección de clinica
                direccion_clinica = DireccionClinica(localidad=localidad,
                                                     municipio=Municipio.objects.get(
                                                         pk=id_municipio),
                                                     estado=Estado.objects.get(
                                                         pk=id_estado))
                direccion_clinica.save()

                datos_usuario = Clinica(nombre_unidad=nombre_unidad,
                                        plaza_fed=plaza_fed,
                                        # beca=beca,
                                        complemento=complemento,
                                        observacion=observacion, status=True,
                                        id_direccion_fk=direccion_clinica,
                                        id_institucion_fk_id=institucion)

                form.fields['institucion'].widget.attrs['disabled'] = True

                # se guarda la clinica
                datos_usuario.save()

                params = urllib.parse.urlencode({
                    'funcion': 'La clínica se registró'
                })

                return redirect('/Coordinador/clinicas/' + '?%s' % params)
            else:
                return render(request, 'registrar_clinica.html', {
                    'form': form, 'usuario': datos_usuario,
                    'estados': estados,
                    "nombre_unidad1": form.data['nombre_unidad'],
                    "plaza_fed1": form.data['plaza_fed'],
                    "complemento1": form.data['complemento'],
                    # "beca1":form.data['beca'],
                    "observación1": form.data['observación'],
                    "localidad1": form.data['localidad'], "est": est,
                    "mun": mun})
        else:
            form = FormClinica()
            return render(request, 'registrar_clinica.html', {
                'form': form, 'usuario': datos_usuario,
                'estados': estados})


def recarga_form_inicial(datos_usuario, direccion_clinica):
    return {
        'id_clinica': datos_usuario.id_clinica,
        'nombre_unidad': datos_usuario.nombre_unidad,
        'plaza_fed': datos_usuario.plaza_fed,
        'complemento': datos_usuario.complemento,
        'observación': datos_usuario.observacion,
        'localidad': direccion_clinica.localidad,
        'institucion': datos_usuario.id_institucion_fk_id
    }


def recarga_form_post(request):
    return {
        'id_clinica': request.POST.get('id_clinica'),
        'nombre_unidad': request.POST.get('nombre_unidad'),
        'plaza_fed': request.POST.get('plaza_fed'),
        'complemento': request.POST.get('complemento'),
        'observación': request.POST.get('observación'),
        'localidad': request.POST.get('localidad'),
        'institucion': request.POST.get('institucion')
    }


class ModificaClinica:
    @login_required
    def modificar_clinica(request):

        estados = Estado.objects.all()

        """función que realiza el registro de una clinica
          si todo es correcto devuelve un mensaje de la clinica se registró
          en caso contrario muestra los diferentes mensajes de error.
          """
        # se toman los datos del mostrar clinica , para diferenciar la clinica
        # seleccionada
        global id_clinica1

        id_clinica = request.GET.get('id_clinica')

        guardar = request.POST.get('guardar', 'False')

        try:
            print('id_clinica ' + str(id_clinica))
            # se obtiene el objeto de la clinica seleccionada y su direccion
            # clinica, buscando en base a su id
            datos_usuario = Clinica.objects.get(pk=id_clinica)
            direccion_clinica = DireccionClinica.objects.get(
                pk=datos_usuario.id_direccion_fk_id)

        except Exception as e:
            print(e)
            estados = Estado.objects.all()
            return render(request, 'modificar_clinica.html', {
                'errorNullIdModificar': 'Tiene que seleccionar una clínica, '
                                        'regrese al inicio.', })

        est = direccion_clinica.estado.id_estado
        mun = direccion_clinica.municipio.id_municipio

        # se pasan los datos de la clinica seleccionada al form que generara
        # la vista del form
        form = FormClinica(
            initial=recarga_form_inicial(datos_usuario, direccion_clinica))

        print('---- Método %s ----' % (request.method))

        # si se hace una peticion post se ejecutara el codigo para guardar los
        # datos
        if request.method == 'POST':
            guardar = False

            if datos_usuario.nombre_unidad != request.POST.get(
                    'nombre_unidad'):
                if request.POST.get('nombre_unidad') != None:
                    datos_usuario.nombre_unidad = request.POST.get(
                        'nombre_unidad')
                    guardar = True

            if datos_usuario.plaza_fed != request.POST.get('plaza_fed'):
                if request.POST.get('plaza_fed') != None:
                    datos_usuario.plaza_fed = request.POST.get('plaza_fed')
                    guardar = True

            if datos_usuario.complemento != request.POST.get('complemento'):
                if request.POST.get('complemento') != None:
                    datos_usuario.complemento = request.POST.get('complemento')
                    guardar = True

            if datos_usuario.observacion != request.POST.get('observación'):
                if request.POST.get('observación') != None:
                    datos_usuario.observacion = request.POST.get('observación')
                    guardar = True

            institucion = request.POST.get('institucion')

            if institucion == '0' or institucion is None:
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'institucion_error': 'Debes seleccionar una "Institución" '
                                         'de la lista.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })
            else:
                datos_usuario.id_institucion_fk_id = institucion
                guardar = True

            # si es el boton guardar el precionado, se guardan los respectivos
            # datos ingresados
            if request.POST.get('nombre_unidad') == "":
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Nombre de unidad" no debe estar vacío.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })

            if request.POST.get('observación') == "":
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Observación" no debe estar vacío.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })

            if request.POST.get('localidad') == "":
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'alerta': '"El campo "Localidad" no debe estar vacío.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })

            if request.POST.get('estados') == "vacio":
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Estado" no debe estar vacío.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })

            if request.POST.get('municipios') == "vacio":
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'alerta': 'El campo "Municipio" no debe estar vacío.',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })


            if (es_alfabetica(request.POST.get('localidad')) != True):
                return render(request, 'modificar_clinica.html', {
                    'form': form,
                    'alerta': '"Localidad" no debe contener números o caracteres especiales',
                    'usuario': datos_usuario,
                    'estados': estados,
                    "est": est,
                    "mun": mun
                })

            if guardar:
                print('---- GUARDAR ----')
                datos_usuario.save()

                id_estado = request.POST.get('estados')
                id_municipio = request.POST.get('municipios')

                direccion_clinica.municipio = Municipio.objects.get(pk=id_municipio)
                direccion_clinica.estado = Estado.objects.get(pk=id_estado)
                direccion_clinica.localidad = request.POST.get('localidad')
                direccion_clinica.save()

                params = urllib.parse.urlencode({
                    'funcion': 'La clínica se modificó'
                })

                return redirect('/Coordinador/clinicas/' + '?%s' % params)
            else:
                print('---- NO GUARDAR ----')

        return render(request, 'modificar_clinica.html', {
            'form': form,
            'usuario': datos_usuario,
            'direccion_clinica': direccion_clinica,
            'estados': estados,
            'est': est,
            'mun': mun
        })


class CambiarStatusClinica:
    @login_required
    def cambiar_status_clinica(request):
        try:
            clinicas = Clinica.objects.all()
            # funcion que desactiva o activa una clinicaca dependiendo de su estado anterior, simplemente lo invierte
            id_clinica = request.POST.get('id_clinica', '')
            datos = Clinica.objects.get(pk=id_clinica)
            # condicion para cambiar la clinica activa a desactiviada o de caso contrario
            salida = ""
            if datos.status:
                datos.status = False
                mensaje_des_clinica[0] = datos.status
            else:
                datos.status = True
                mensaje_ac_clinica[0] = datos.status
            # se guardan los datos de la clinica solo con el atributo de su status modificado
            clinica = Clinica(id_clinica=datos.id_clinica,
                              nombre_unidad=datos.nombre_unidad,
                              plaza_fed=datos.plaza_fed,
                              # beca=datos.beca,
                              complemento=datos.complemento,
                              observacion=datos.observacion,
                              status=datos.status,
                              id_direccion_fk=datos.id_direccion_fk
                              )
            clinica.id_institucion_fk_id = datos.id_institucion_fk_id
            clinica.save()
            return redirect('/Coordinador/clinicas')
        except ValueError:
            pass


def es_alfabetica(cadena):
    separar = cadena.split(" ")
    n = ""
    for i in separar:
        n = n + i.strip()
    if (n.isalpha() or n == ""):
        return True
    else:
        return False


def carga_municipios(request):
    estado = Estado.objects.get(pk=request.GET['id_estado'])
    municipios = Municipio.objects.filter(id_estado=estado.id_estado)
    municipios_json = []
    for municipio in municipios:
        municipio_json = {'nombre': municipio.nombre,
                          'id': municipio.id_municipio}
        municipios_json.append(municipio_json)
    data = simplejson.dumps(municipios_json)
    return HttpResponse(data, content_type="application/json")




def valida_matricula(matricula):
    return re.search(r'^[0-9]{8}$', matricula, 0)


class NuevoCoordinador:
    @login_required
    def nuevo_coordinador(request):
        '''
        Funcion que maneja la creacion de nuevos coordinadores
        '''
        if (request.method == "GET"):
            form = FormCoordinador()
            return render(request, 'nuevo_coordinador.html', {'form': form})
        if (request.method == "POST"):
            matricula = request.POST.get("matricula")
            correo_electronico = request.POST.get('correo_electronico')
            correo_institucional = request.POST.get('correo_institucional')
            nombre = request.POST.get('nombre')
            apellido_paterno = request.POST.get('apellido_paterno')
            apellido_materno = request.POST.get('apellido_materno')

            if request.POST.get('matricula') == "":
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'El campo "Matrícula" no debe estar vacío.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            try:
                int(request.POST.get("matricula"))
            except ValueError:
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'La "Matrícula" debe cumplir con 8 caracteres numéricos.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            if (len(request.POST.get("matricula")) != 8):
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'La "Matrícula" debe cumplir con 8 caracteres numéricos.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            if Usuario.objects.filter(matricula=matricula).exists():
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'La "Matricula" ya fue registrada.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            if request.POST.get("password") == "":
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'El campo de "Contraseñas" no debe estar vacío.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            if request.POST.get("password") != request.POST.get(
                    "confirmar_password"):
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'Las "Contraseñas" no coinciden.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            if request.POST.get('correo_electronico') == "":
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'El campo "Correo electrónico principal" no debe estar vacío.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})
            else:
                if re.match(
                        '^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',
                        request.POST.get('correo_electronico').lower()):
                    print("Correo correcto")
                else:
                    return render(request, 'nuevo_coordinador.html', {
                        'alerta': 'El "Correo electrónico principal" está incorrecto.',
                        "matricula": matricula,
                        "correo_electronico": correo_electronico,
                        "correo_institucional": correo_institucional,
                        "nombre": nombre,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno})

                if Usuario.objects.filter(
                        correo_electronico=correo_electronico).exists():
                    return render(request, 'nuevo_coordinador.html', {
                        'alerta': 'El "Correo electrónico principal" ya fue registrada.',
                        "matricula": matricula,
                        "correo_electronico": correo_electronico,
                        "correo_institucional": correo_institucional,
                        "nombre": nombre,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno})

            if request.POST.get('correo_institucional') != "":
                if re.match(
                        '^[(a-z0-9\_\-\.)]+@[(a-z0-9\_\-\.)]+\.[(a-z)]{2,15}$',
                        request.POST.get('correo_institucional').lower()):
                    print("Correo correcto")
                else:
                    return render(request, 'nuevo_coordinador.html', {
                        'alerta': 'El "Correo electrónico alternativo" está incorrecto.',
                        "matricula": matricula,
                        "correo_electronico": correo_electronico,
                        "correo_institucional": correo_institucional,
                        "nombre": nombre,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno})

            if request.POST.get('nombre') == "":
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'El campo "Nombre" no debe estar vacío.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            if request.POST.get('apellido_paterno') == "":
                return render(request, 'nuevo_coordinador.html', {
                    'alerta': 'El campo "Apellido Paterno" no debe estar vacío.',
                    "matricula": matricula,
                    "correo_electronico": correo_electronico,
                    "correo_institucional": correo_institucional,
                    "nombre": nombre,
                    "apellido_paterno": apellido_paterno,
                    "apellido_materno": apellido_materno})

            try:
                form = FormCoordinador(request.POST)
                if form.is_valid:

                    if not valida_matricula(request.POST.get("matricula")):
                        return render(request, 'nuevo_coordinador.html', {
                            'form': form,
                            'alerta': 'La matrícula no puede contener caracteres.',
                            "matricula": matricula,
                            "correo_electronico": correo_electronico,
                            "correo_institucional": correo_institucional,
                            "nombre": nombre,
                            "apellido_paterno": apellido_paterno,
                            "apellido_materno": apellido_materno})

                    usuario = Usuario(

                        matricula=request.POST.get('matricula'),
                        password=make_password(request.POST.get('password')),
                        correo_electronico=request.POST.get(
                            'correo_electronico'),
                        correo_institucional=request.POST.get(
                            'correo_institucional'),
                        is_superuser=0,
                        username=request.POST.get('matricula'),
                        is_staff=1,
                        tipo_usuario='Coordinador'
                    )
                    usuario.save()
                    coordinador = Coordinador(
                        nombre=request.POST.get('nombre'),
                        apellido_paterno=request.POST.get('apellido_paterno'),
                        apellido_materno=request.POST.get('apellido_materno'),
                        matricula=usuario
                    )
                    coordinador.save()
                    f = "0"
                    return render(request, 'nuevo_coordinador.html', {
                        'form': f, 'funcion': 'Coordinador creado con éxito.',
                        "matricula": matricula,
                        "correo_electronico": correo_electronico,
                        "correo_institucional": correo_institucional,
                        "nombre": nombre,
                        "apellido_paterno": apellido_paterno,
                        "apellido_materno": apellido_materno, "boolean": True})
            except ValueError as ex:
                return render(request, 'nuevo_coordinador.html',
                              {'form': form,
                               'alerta': 'La matrícula debe contener solo números.'})
            except Exception as e:
                print(e);
                return render(request, 'nuevo_coordinador.html', {'form': form,
                                                                  'alerta': 'El correo ya está registrado'})
            return True;


class EliminarClinica:
    def eliminar_clinica(request):
        try:
            if request.method == "GET":
                return redirect('/Coordinador/clinicas')
            if request.method == "POST":
                AsignacionClinicaPasante.objects.filter(
                    id_clinica_fk_id=request.POST.get('id_clinica')).delete()
                Clinica.objects.filter(
                    pk=request.POST.get('id_clinica')).delete()
                return redirect('/Coordinador/clinicas')
        except ValueError:
            pass


class ModificarCoordinador:
    @login_required
    def modificar_coordinador(request):
        auth_actual = request.user
        usuario_actual = Usuario.objects.get(user_ptr_id=auth_actual.id)
        try:
            coordinador_actual = Coordinador.objects.get(
                matricula_id=usuario_actual.id_usuario)
        except Exception as e:
            return render(request, 'modificar_coordinador.html', {})
        if (request.method == "GET"):
            form = FormModificarCoordinador({
                'nombre': coordinador_actual.nombre,
                'apellido_paterno': coordinador_actual.apellido_paterno,
                'apellido_materno': coordinador_actual.apellido_materno,
            })

            return render(request, 'modificar_coordinador.html',
                          {'form': form, })
        if (request.method == "POST"):
            try:
                form = FormModificarCoordinador(request.POST)
                if form.is_valid:
                    if len(request.POST.get("password")) > 0:
                        if request.POST.get("password") != request.POST.get(
                                "confirmar_password"):
                            return render(request,
                                          'modificar_coordinador.html',
                                          {
                                              'form': form,
                                              'alerta': 'Las contraseñas no coinciden.'})
                        usuario_actual.password = make_password(
                            request.POST.get('password'))
                        
                    else:
                        
                        usuario_actual.save()

                        if request.POST.get('nombre') == "":
                            return render(request,
                                          'modificar_coordinador.html',
                                          {
                                              'form': form,
                                              'alerta': 'El campo "Nombre" no debe estar vacío.'})
                        else:
                            coordinador_actual.nombre = request.POST.get(
                                'nombre')

                        if request.POST.get('apellido_paterno') == "":
                            return render(request,
                                          'modificar_coordinador.html',
                                          {
                                              'form': form,
                                              'alerta': 'El campo "Apellido Paterno" no debe estar vacío.'})
                        else:
                            coordinador_actual.apellido_paterno = request.POST.get(
                                'apellido_paterno')

                        if request.POST.get('apellido_materno') == "":
                            return render(request,
                                          'modificar_coordinador.html',
                                          {
                                              'form': form,
                                              'alerta': 'El campo "Apellido Materno" no debe estar vacío.'})
                        else:
                            coordinador_actual.apellido_materno = request.POST.get(
                                'apellido_materno')
                        coordinador_actual.save()
                        f = "0"
                        return render(request, 'modificar_coordinador.html', {
                            'form': f,
                            'funcion': 'Coordinador modificado con éxito.'})
            except Exception as e:
                print(e);
                return render(request, 'modificar_coordinador.html',
                              {'form': form,
                               'alerta': 'El correo ya está registrado.'})

    def eliminar_cordinador(request, id_coordinador):
        cordinadores = Coordinador.objects.all()
        cor = get_object_or_404(cordinadores, pk=id_coordinador)
        usuarios = Usuario.objects.all()
        us = get_object_or_404(usuarios, id_usuario=cor.matricula_id)
        cor.delete()

        us.delete()

        mensaje = "Se elimino el coordinador correctamente"
        return redirect('/Coordinador/mostrar_coordinadores')

    def mostrar_coordinador(request):
        cordinadores = Coordinador.objects.all()
        return render(request, 'muestra_coordinador.html',
                      {'coordinador': cordinadores})


class ReporteInconformidadCoordinador:

    def guardar_cambios(request):
        reportesIC = ReporteInconformidadCoordinador()
        reporte_in = ReporteInconformidad.objects.all()
        comentario_in = "nada"
        calificacion = None
        if request.method == 'POST':
            id_reporte = request.POST.get('id_reporte')
            reporte = ReporteInconformidad.objects.get(pk=id_reporte)
            freq_rec = request.POST.get('freq_rec_value')
            dia_rec = request.POST.get('dia_rec_value')
            status = request.POST.get('status_value')
            #Nuevo calificacion
            try:
                calificacion = int(request.POST.get('score'))
            except(Exception):
                print('No se recibio calificacion')
            #if(calificacion != '' and calificacion != None):            
                #reportesIC.calificar_reporte(id_reporte,calificacion)
                #reporte.calificacion = calificacion
                #reporte.save()
            #Nuevo calificacion
            print(status + '- ' + dia_rec + ' - ' + freq_rec)
            if freq_rec != reporte.freq_rec and freq_rec != '' and freq_rec != None:
                reporte.freq_rec = freq_rec
                reporte.save()
            if dia_rec != reporte.dia_rec and dia_rec != '' and dia_rec != None:
                reporte.dia_rec = dia_rec
                reporte.save()
            if status != reporte.descripcion_estatus and status != '' and status != None:
                reporte.descripcion_estatus = status
                if reporte.id_status_fk_id == 4 or reporte.id_status_fk_id == 2:
                    reporte.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=3)
                reporte.save()
            ReporteInconformidadCoordinador.calificar_reporte(request,id_reporte,calificacion)

            return render(request, 'Muestra_reporte_inconformidad.html',
                          {'reporte': reporte_in, 'Solucionado': True, 'comentario_in': 'Cambios Guardados'})
        else:
            return render(request, 'Muestra_reporte_inconformidad.html',
                            {'reporte': reporte_in, 'comentario_in': comentario_in})

    def calificar_reporte(request,id_reporte,calificacion):
        
        if request.method == 'POST':
            
            reporte = ReporteInconformidad.objects.get(pk=id_reporte)              
            
            if(calificacion != '' and calificacion != None and calificacion >=1 and calificacion <=5): 
                dict_cal = {}
                clinica = reporte.id_clinica_fk

                try:
                    rubro_infra = Rubro.objects.get(nombre_rubro='Infraestructura')
                    cal_infra = CalificacionClinica.objects.get(id_clinica_fk=clinica, id_rubro_fk=rubro_infra)
                    dict_cal[rubro_infra] = cal_infra.promedio

                    rubro_comunicacion = Rubro.objects.get(nombre_rubro='Comunicacion')
                    cal_comunicacion = CalificacionClinica.objects.get(id_clinica_fk=clinica, id_rubro_fk=rubro_comunicacion)
                    dict_cal[rubro_comunicacion] = cal_comunicacion.promedio

                    rubro_comunidad = Rubro.objects.get(nombre_rubro='Comunidad')
                    cal_comunidad = CalificacionClinica.objects.get(id_clinica_fk=clinica, id_rubro_fk=rubro_comunidad)
                    dict_cal[rubro_comunidad] = cal_comunidad.promedio

                    rubro_personal = Rubro.objects.get(nombre_rubro='Personal')
                    cal_personal = CalificacionClinica.objects.get(id_clinica_fk=clinica, id_rubro_fk=rubro_personal)
                    dict_cal[rubro_personal] = cal_personal.promedio
                
                    rubro_seguridad = Rubro.objects.get(nombre_rubro='Seguridad')
                    cal_seguridad = CalificacionClinica.objects.get(id_clinica_fk=clinica, id_rubro_fk=rubro_seguridad)
                    dict_cal[rubro_seguridad] = calificacion

                    rubro_general = Rubro.objects.get(nombre_rubro='General')
                    cal_general = CalificacionClinica.objects.get(id_clinica_fk=clinica, id_rubro_fk=rubro_general)
                    dict_cal[rubro_general] = cal_general.promedio

                except ObjectDoesNotExist:

                    rubro_seguridad = Rubro.objects.get(nombre_rubro='Seguridad')
                    dict_cal[rubro_seguridad] = (cal_seguridad.promedio + calificacion)/2

                CalificarClinica.asigna_calificacion(request,clinica, dict_cal)
                print(dict_cal)   
        
                if reporte.calificacion == None:
                    reporte.calificacion = calificacion
                    reporte.save()
                
        print('La cali es:' + str(calificacion))
        print('Tipo del id_reporte: ' +str(type(id_reporte)))
        print('ID del reporte: ' + str(id_reporte))


    def resuelve_inconformidad(request):
        reporte_in = ReporteInconformidad.objects.all()
        comentario_in = "nada"
        calificacion = None
        if request.method == 'POST':
            id_reporte = request.POST.get('id_reporte')
            reporte = ReporteInconformidad.objects.get(pk=id_reporte)
            reporte.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=5)
            try:
                calificacion = int(request.POST.get('score_inconformidad_value'))
                print(str(calificacion))
            except(Exception):
                print('No se recibio calificacion')
                print(str(calificacion))
            if(reporte.calificacion == None):
                reporte.calificacion = calificacion
            reporte.save()
            return render(request, 'Muestra_reporte_inconformidad.html', {'reporte': reporte_in, 'Solucionado': True, 'comentario_in':comentario_in})
        else:
            return render(request, 'Muestra_reporte_inconformidad.html',
                          {'reporte': reporte_in, 'comentario_in': comentario_in})


    def mostrar_reportes(request, id_estatus, id_tipo):


        if int(id_estatus) > 0 and int(id_estatus) < 6:
            filtrado = True
            reporte_in = ReporteInconformidad.objects.filter(id_status_fk=id_estatus)
            return render(request, 'Muestra_reporte_inconformidad.html',
                          {'reporte': reporte_in,'filtrado':filtrado,})
        elif int(id_tipo) > 0 and int(id_tipo) <= 7:
            clasificado = True
            reporte_in = ReporteInconformidad.objects.filter(id_tipo_inconformidad_FK=id_tipo)
            return render(request, 'Muestra_reporte_inconformidad.html',
                          {'reporte': reporte_in, 'clasificado':clasificado,})
        else:
          actualizarReportes(request)
          reporte_in = ReporteInconformidad.objects.all()
          return render(request, 'Muestra_reporte_inconformidad.html',
                          {'reporte': reporte_in})


    def agrega_comentario_reporte(request):
        comentario_in="nada"
        if (request.method == 'POST'):
            comentario = request.POST.get('comment')
            id_rep= int(request.POST.get('id_ii'))
            if comentario == "":
                comentario_in="El campo del comentario no debe ser vacío"
                reporte_in = ReporteInconformidad.objects.all()
                return render(request, 'Muestra_reporte_inconformidad.html',
                              {'reporte': reporte_in,'comentario_in':comentario_in})
            else:
                comentario_in="Se agregó exitosamente su comentario"
                queryset = ReporteInconformidad.objects.get(pk=id_rep)
                print(id_rep)
                comentario_save = ComentariosInconformidad (id_reporte=queryset, es_coordinador=1, comentario=comentario, fecha=date.today())
                comentario_save.save()
                if queryset.id_status_fk_id == 4 or queryset.id_status_fk_id == 2:
                    queryset.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=3)
                    queryset.save()
                reporte_in = ReporteInconformidad.objects.all()
                return render(request, 'Muestra_reporte_inconformidad.html',
                              {'reporte': reporte_in,'comentario_in':comentario_in})

    def get_reporte(request):
        idReporte=request.GET.get('id_reporte', None)
        print(idReporte)
        if idReporte is not None:
            queryset = ReporteInconformidad.objects.get(pk=idReporte)
            telefono = Telefono.objects.all()
            telef=""
            if request.user.is_staff:
                if queryset.id_status_fk_id == 1:
                    queryset.id_status_fk = CatalogoEstatusInconformidad.objects.get(pk=2)
                    queryset.save()
            comentarios = ComentariosInconformidad.objects.filter(id_reporte=idReporte)
            reporte_json=[]
            for tel in telefono:

                if tel.tipo == "movil" and tel.id_pasante_fk_id == queryset.id_pasante_fk.id_pasante:
                    telef=tel.numero


            for comentario in comentarios:



                comentario_json = {
                    'es_cordinador': comentario.es_coordinador,
                    'comentario': comentario.comentario

                }


                reporte_json.append({'comentarios':comentario_json})

            reporte_json.append({
                    "id_reporte": queryset.id_reporte,
                    "descripcion_estatus": queryset.descripcion_estatus,
                    "descripcion_inconformidad": queryset.descripcion_inconformidad,
                    "fecha_reporte": str(queryset.fecha_reporte),
                    "dia_rec": queryset.dia_rec,
                    "freq_rec": queryset.freq_rec,
                    "nombre_pasante": queryset.id_pasante_fk.nombre,
                    "apellido_pasante": queryset.id_pasante_fk.apellido_paterno,
                    "matricula_pasante": queryset.id_pasante_fk.id_usuario_fk.matricula,
                    "telefono_pasante": telef,
                    "correo_pasante": queryset.id_pasante_fk.id_usuario_fk.correo_electronico,
                    "localidad": str(queryset.id_clinica_fk.id_direccion_fk.localidad)+", "+str(queryset.id_clinica_fk.id_direccion_fk.municipio.nombre)+", "+str(queryset.id_clinica_fk.id_direccion_fk.estado.nombre),
                    "id_clinica_fk": queryset.id_clinica_fk.nombre_unidad,
                    "id_status_fk": queryset.id_status_fk.nombre_estatus,
                    "id_tipo_inconformidad_FK": queryset.id_tipo_inconformidad_FK.nombre,
                    "calificacion":queryset.calificacion
                    })

        return JsonResponse(reporte_json, safe=False, status=200)
        
class ContactosAlerta():

    @login_required
    def contactos_directivo(request):
        return render(request, 'contactos_directivo.html')

    @login_required
    def borrar_contactos_directivo(request):
        if request.method == 'GET':
            id = request.GET['id']
            contacto = ContactosPasante.objects.get(id_contacto=id)
            contacto.delete()

        return render(request, 'activar_alerta.html')

    @login_required
    def editar_contactos_directivo(request):
        id_contacto = request.GET['id']
        nombre = request.GET.get('nombre', None)
        numero = request.GET.get('numero', None)

        if request.method == 'GET' and nombre != None and numero != None:
            contacto = ContactosPasante.objects.get(id_contacto=id_contacto)

            contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk=contacto.id_pasante_fk)
            for c in contactos_pasante:
                if numero == c.numero and str(id_contacto) != str(c.id_contacto):
                    return HttpResponse("REPETIDO")

            contacto.nombre = nombre
            contacto.numero = numero
            contacto.save()

        if request.is_ajax():
          html = render_to_string('edit-contacts-dir.html',
              {
                  'contacto': ContactosPasante.objects.get(id_contacto=id_contacto)
              }
          )
          return HttpResponse(html)

    @login_required
    def obtener_contactos_directivo(request):
        contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk=None)
        html = render_to_string('get-contacts-dir.html',
            {
                'contactos': contactos_pasante,
            }
        )
        return HttpResponse(html)

    @login_required
    def insertar_contactos_directivo(request):
        nombre = request.GET.get('nombre', None)
        numero = request.GET.get('numero', None)

        if request.method == 'GET' and nombre != None and numero != None:

            contactos_pasante = ContactosPasante.objects.filter(id_pasante_fk=None)
            for contacto in contactos_pasante:
                if numero == contacto.numero:
                    return HttpResponse("REPETIDO")

            contacto = ContactosPasante(nombre=nombre, numero=numero, id_pasante_fk=None)
            contacto.save()
            return HttpResponse("OK")

        if request.is_ajax():
            html = render_to_string('add-contacts-dir.html',
                {
                    'contactos': ContactosPasante.objects.filter(id_pasante_fk_id=None)
                }
            )
            return HttpResponse(html)

def mostrar_clinica_coordinador(request,pk):
  clinica=get_object_or_404(Clinica, id_clinica=pk)
  return render(request, "MostrarInfoClinicaCoordinador.html",{'clinica':clinica})

class comentarios_coordinador:

    def get_comentarios_coordinador(request, id_rubro_sel, id_clinica_sel):            
      comentarios = ComentarioPasante.objects.filter(id_clinica_fk = id_clinica_sel, id_rubro_fk = id_rubro_sel)
      rubro = Rubro.objects.get(id_rubro = id_rubro_sel)
      clinicaCom = id_clinica_sel
      clinicaCom2=Clinica.objects.get(id_clinica=id_clinica_sel)
      
      if len(comentarios) < 0: None
      return render(request,'Muestra_comentarios_coordinador.html',
                      {'comentarios':comentarios,
                      'rubro':rubro,
                      'clinicaCom':clinicaCom,
                      'tiene_datos': True,
                      'clinicaCom2':clinicaCom2,                      
                      })

    def delete_comentarios(request,id_comentario_pasante,id_rubro_sel,id_clinica_sel):
        comentario = get_object_or_404(ComentarioPasante, id_comentario_pasante=id_comentario_pasante)
        comentario.delete()
        return comentarios_coordinador.get_comentarios_coordinador(request,id_rubro_sel,id_clinica_sel)

class CatalogoClinicas:
    @login_required
    def mostrar_clinicas(request):
        """función que muestra el catálogo de  clinicas registradas 
        """
        usuario = Pasante.objects.filter(
        id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0 else None
        clinicas = Clinica.objects.all()
        
        rubro=Rubro.objects.get(id_rubro=7)
        calificacion=CalificacionClinica.objects.filter(id_rubro_fk=rubro)
        print (rubro)

        if(len(calificacion) > 0):
            return render(request,'catalogo_clinicas_coordinador.html',
                {'calificacion': calificacion, 'clinicas':clinicas}, )
        else:
            return render(request, 'catalogo_clinicas_coordinador.html',
                          {'no_info': 'No hay clinicas calificadas.', 
                          },)
            
class MuestraCalificacion:
    @login_required
    def mostrar_calificacion(request,pk):
        usuario = Pasante.objects.filter(
            id_usuario_fk=Usuario.objects.filter(user_ptr_id=request.user.id)[0].id_usuario)
        pasante_p = usuario[0] if len(usuario) > 0  else None
        pasantes = Pasante.objects.all()

        get_clinica = pk
                      
        seguridad = CalificacionClinica.objects.all().filter(id_rubro_fk=1).filter(id_clinica_fk=get_clinica)
        infraestructura=CalificacionClinica.objects.all().filter(id_rubro_fk=2).filter(id_clinica_fk=get_clinica)
        comunicacion=CalificacionClinica.objects.all().filter(id_rubro_fk=3).filter(id_clinica_fk=get_clinica)
        personal=CalificacionClinica.objects.all().filter(id_rubro_fk=4).filter(id_clinica_fk=get_clinica)
        comunidad=CalificacionClinica.objects.all().filter(id_rubro_fk=5).filter(id_clinica_fk=get_clinica)
        general=CalificacionClinica.objects.all().filter(id_rubro_fk=6).filter(id_clinica_fk=get_clinica)
        promedio_general=CalificacionClinica.objects.all().filter(id_rubro_fk=7).filter(id_clinica_fk=get_clinica)
   

        
        if(len(promedio_general) > 0):
            return render(request,'mostrar_info_clinica_coordinador.html',
                {
                'tiene_datos': True,
            'pasante_p': pasante_p,
            'pasantes_asignados': getPasantesAsignaciones(request),
            'activo': usuario,      
                'seguridad': seguridad,
                'infraestructura':infraestructura,
                'comunicacion':comunicacion,
                'personal':personal,
                'comunidad':comunidad,
                'general':general,
                'promedio_general':promedio_general})
        
        else:
            return render(request, 'mostrar_info_clinica_coordinador.html',
                          {'no_info': 'No hay calificaciones que mostrar.'})


class CRUDExperto:

    @login_required
    def ver_catalogo_expertos(request):
        expertos = Experto.objects.all()

        info = {
            'encabezado': 'Expertos',
            'expertos': expertos
        }

        return render(request, 'inicio_expertos.html', info)

    @login_required
    @transaction.atomic
    def crear_experto(request):
        if (request.method == 'POST'):
            formulario = FormExperto(request.POST)

            if (formulario.is_valid()):
                experto = formulario.save(commit=False)

                experto.password = make_password(formulario['contrasena'].value())
                experto.is_superuser = False
                experto.username = formulario['correo_electronico'].value()
                experto.first_name = formulario['nombre'].value()
                experto.last_name = formulario['ap_paterno'].value()
                experto.email = formulario['correo_electronico'].value()
                experto.is_staff = False
                experto.is_active = formulario['activo'].value()
                experto.date_joined = datetime.now()
                
                usuarios_expertos = Usuario.objects.all().filter(tipo_usuario='Experto')
                matricula = len(usuarios_expertos) + 1  # para empezar desde 1

                experto.matricula = matricula
                experto.tipo_usuario = 'Experto'

                experto.id_experto = matricula  # ambos llevan el mismo valor

                experto.save()
                formulario.save_m2m()

                messages.add_message(request, messages.SUCCESS, 'Experto creado con exito')

                return redirect('catalogo_expertos')

        else:
            formulario = FormExperto()

        info = {
            'encabezado': 'Crear Experto',
            'formulario': formulario,
            'accion': 'CREAR',
            'nombres_botones': {
                'primer_boton': 'Crear Experto',
                'segundo_boton': 'Cancelar'
            }
        }

        return render(request, 'formulario_experto.html', info)

    @login_required
    def ver_experto(request, id):
        experto = Experto.objects.get(pk=id)

        formulario = FormExperto(instance=experto)

        formulario.fields['nombre'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['ap_paterno'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['ap_materno'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['teléfono'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['id_categoria'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['especialidad'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['correo_electronico'].widget.attrs['disabled'] = 'disabled'
        formulario.fields['contrasena'].widget.attrs['disabled'] = 'disabled'

        info = {
            'encabezado': 'Ver Experto',
            'formulario': formulario,
            'id_experto': experto.id_experto,
            'opcion_regresar': True,
            'accion': 'VER',
            'nombres_botones': {
                'primer_boton': 'Actualizar',
                'segundo_boton': 'Desactivar'
            }
        }

        return render(request, 'formulario_experto.html', info)

    @login_required
    @transaction.atomic
    def modificar_experto(request, id):
        experto = Experto.objects.get(pk=id)

        if (request.method == 'POST'):
            formulario = FormExperto(request.POST, instance=experto)

            if (formulario.is_valid()):
                experto = formulario.save(commit=False)

                experto.password = make_password(formulario['contrasena'].value())
                experto.username = formulario['correo_electronico'].value()
                experto.first_name = formulario['nombre'].value()
                experto.last_name = formulario['ap_paterno'].value()
                experto.email = formulario['correo_electronico'].value()

                experto.save()
                formulario.save_m2m()

                messages.add_message(request, messages.SUCCESS, 'Experto actualizado')

                return redirect('catalogo_expertos')

        else:
            formulario = FormExperto(instance=experto)

        info = {
            'encabezado': 'Modificar Experto',
            'formulario': formulario,
            'id_experto': experto.id_experto,
            'opcion_regresar': False,
            'accion': 'MODIFICAR',
            'nombres_botones': {
                'primer_boton': 'Guardar',
                'segundo_boton': 'Cancelar'
            }
        }

        return render(request, 'formulario_experto.html', info)

    @login_required
    def desactivar_experto(request, id):
        experto = Experto.objects.get(pk=id)

        experto.is_active = False
        experto.activo = False
        experto.save()

        messages.add_message(request, messages.SUCCESS, 'Experto desactivado')

        return redirect('catalogo_expertos')