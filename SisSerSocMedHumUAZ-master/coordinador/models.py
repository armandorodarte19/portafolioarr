from django.db import models

from clinica.models import Clinica
from usuario.models import Usuario
from pasante.models import Pasante, Generacion

import uuid
import os

def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('normatividad', filename)


class Coordinador(models.Model):
    id_coordinador = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    apellido_paterno = models.CharField(max_length=50)
    apellido_materno = models.CharField(null=True, max_length=50)
    matricula = models.ForeignKey(Usuario, on_delete=models.CASCADE,)

class CatalogoEstatusInconformidad(models.Model):
    id_estatus = models.AutoField(primary_key=True)
    nombre_estatus = models.CharField(max_length=15)
    color = models.CharField(max_length=6)

class CatalogoTipoInconformidad(models.Model):
    id_tipo = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=20)
    def __str__(self):
        return self.nombre

class ReporteInconformidad(models.Model):
    id_reporte = models.AutoField(primary_key=True)
    id_pasante_fk = models.ForeignKey(Pasante, on_delete=models.CASCADE,)
    id_clinica_fk = models.ForeignKey(Clinica, on_delete=models.CASCADE,)
    id_status_fk = models.ForeignKey(CatalogoEstatusInconformidad, default=1, on_delete=models.CASCADE,)
    id_tipo_inconformidad_FK = models.ForeignKey(CatalogoTipoInconformidad, on_delete=models.CASCADE,)
    descripcion_estatus = models.CharField(max_length=200, null=True, default='')
    descripcion_inconformidad = models.CharField(max_length=500)
    fecha_reporte = models.DateField()
    dia_rec = models.IntegerField(null=True)
    freq_rec = models.IntegerField(null=True)
    calificacion = models.IntegerField(null=True)

class ComentariosInconformidad(models.Model):
    id_comentario = models.AutoField(primary_key=True)
    id_reporte = models.ForeignKey(ReporteInconformidad, on_delete=models.CASCADE,)
    es_coordinador = models.IntegerField(blank=True, null=True)
    comentario = models.CharField(max_length=350)
    fecha = models.DateField()

class seguimiento(models.Model):
    id_reporte = models.ForeignKey(ReporteInconformidad, on_delete=models.CASCADE,)
    id_usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE,)





class Informacion(models.Model):
    id_informacion = models.AutoField(primary_key=True)
    nombre_archivo = models.CharField(max_length=100)
    descripcion = models.CharField(null=True, max_length=500)
    ruta_archivo = models.FileField(upload_to=get_file_path, blank=True,
                                    null=True)
    ruta_archivo_html = models.FileField(blank=True,null=True)

class Catalogo_Preguntas(models.Model):
    id_pregunta = models.AutoField(primary_key=True)
    pregunta = models.CharField(max_length=200)
    id_tipo_fk = models.ForeignKey(CatalogoTipoInconformidad,on_delete=models.CASCADE,)
    respuesta = models.CharField(max_length=250)

# TODO Esta tabla se eliminará!
class AsignacionClinicaPasante(models.Model):
    id_asignacion = models.AutoField(primary_key=True)
    comision = models.CharField(max_length=100)
    id_pasante_fk = models.ForeignKey(Pasante, null=True,
                                      on_delete=models.SET_NULL)
    id_clinica_fk = models.ForeignKey(Clinica, null=True,
                                      on_delete=models.SET_NULL)


class Estado(models.Model):
    id_estado = models.AutoField(primary_key=True)
    clave = models.CharField(max_length=2)
    nombre = models.CharField(max_length=45)
    abrev = models.CharField(max_length=16)
    activo = models.IntegerField(default=1)


class Municipio(models.Model):
    id_municipio = models.AutoField(primary_key=True)
    id_estado = models.ForeignKey(Estado, on_delete=models.CASCADE,)
    clave = models.CharField(max_length=3, null=False)
    nombre = models.CharField(max_length=50, null=False)
    activo = models.IntegerField(null=False, default=1)

class fecha_registro(models.Model):
	id_registro = models.AutoField(primary_key=True)
	fecha_inicio = models.DateField()
	fecha_fin=models.DateField()

'''
from django.db import models

from clinica.models import Clinica
from usuario.models import Usuario
from pasante.models import Pasante, Generacion


class Coordinador(models.Model):
    id_coordinador = models.AutoField(primary_key=True)
    nombre = models.CharField(u'nombre', max_length=50)
    apellido_paterno = models.CharField(u'apellido_paterno', max_length=50)
    apellido_materno = models.CharField(u'apellido_materno', null=True,
                                        max_length=50)
    matricula = models.ForeignKey(Usuario)


class Informacion(models.Model):
    id_informacion = models.AutoField(primary_key=True)

    nombre_archivo = models.CharField(u'nombre_archivo', max_length=50)
    descripcion = models.CharField(u'descripcion', null=True, max_length=50)
    ruta_archivo = models.FileField(u'Archivo', upload_to="normatividad/",
                                    blank=True, null=True)


# TODO Esta tabla se eliminará!
class AsignacionClinicaPasante(models.Model):
    id_asignacion = models.AutoField(primary_key=True)

    comision = models.CharField(u'comision', max_length=100)

    id_pasante_fk = models.ForeignKey(Pasante, null=True,
                                      on_delete=models.SET_NULL)
    id_clinica_fk = models.ForeignKey(Clinica, null=True,
                                      on_delete=models.SET_NULL)


class Estado(models.Model):
    id_estado = models.AutoField(primary_key=True)
    clave = models.CharField(u'clave', max_length=2)
    nombre = models.CharField(u'nombre', max_length=45)
    abrev = models.CharField(u'abrev', max_length=16)
    activo = models.IntegerField(u'activo', default=1)


class Municipio(models.Model):
    id_municipio = models.AutoField(primary_key=True)
    id_estado = models.ForeignKey(Estado)
    clave = models.CharField(u'clave', max_length=3, null=False)
    nombre = models.CharField(u'nombre', max_length=50, null=False)
    activo = models.IntegerField(u'activo', null=False, default=1)


class Rol(models.Model):
    id_rol = models.AutoField(primary_key=True)
    id_generacion_fk = models.ForeignKey(Generacion, null=False,
                                         on_delete=models.CASCADE)
    beca = models.CharField(u'beca', max_length=250, null=False)


class Rotacion(models.Model):
    id_rotacion = models.AutoField(primary_key=True)
    fecha_inicio = models.DateField(null=False)
    fecha_fin = models.DateField(null=False)
    id_rol_fk = models.ForeignKey(Rol, null=False, on_delete=models.CASCADE)
    id_clinica_fk = models.ForeignKey(Clinica, null=False,
                                      on_delete=models.CASCADE)


class Tutor(models.Model):
    id_tutor = models.AutoField(primary_key=True)
    nombre = models.CharField(u'nombre', max_length=50, null=False)
    apellido_paterno = models.CharField(u'apellido_paterno', max_length=50,
                                        null=False)
    apellido_materno = models.CharField(u'apellido_materno', max_length=50,
                                        null=False)
    telefono = models.CharField(u'telefono', max_length=15, null=False)
    correo_electronico = models.CharField(u'correo_electronico', unique=True,
                                          max_length=255)
    cede = models.CharField(u'cede', max_length=255, null=False)


class AsignacionRol(models.Model):
    id_rotacion = models.ForeignKey(Rotacion, null=False,
                                    on_delete=models.CASCADE)
    id_pasante = models.ForeignKey(Pasante, null=False,
                                   on_delete=models.CASCADE)
    id_tutor = models.ForeignKey(Tutor, null=False,
                                 on_delete=models.CASCADE)
    comision = models.CharField(max_length=100, null=False)

'''
