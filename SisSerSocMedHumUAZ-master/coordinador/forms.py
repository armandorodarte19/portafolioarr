#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django import forms
from django.forms import TextInput
from django.contrib.auth.models import User
from django.forms import ModelForm
from usuario.models import Usuario
from coordinador.models import *
from experto.models import Experto
from foro.models import Categorias


class FormCoordinador(forms.ModelForm):

    matricula = forms.CharField(label="Matrícula",min_length=8,max_length=8)
    password = forms.CharField(required=True,label='Contraseña',widget=forms.PasswordInput)
    confirmar_password = forms.CharField(required=True, max_length=50,label='Confirmar contraseña',widget=forms.PasswordInput)
    correo_electronico = forms.EmailField(label="Correo electrónico principal",required = True, max_length=50)
    correo_institucional = forms.EmailField(label="Correo electrónico alternativo",required=False,max_length=50)

    nombre = forms.CharField(label='Nombre:',required=True, max_length=50)
    apellido_paterno = forms.CharField(label='Apellido Paterno:',required=True, max_length=50)
    apellido_materno = forms.CharField(label='Apellido Materno:',required=False, max_length=50)

    class Meta:
         model = Usuario
         model_coordinador = Coordinador

         fields = (
            'matricula',
            'password',
            'confirmar_password',
            'correo_electronico',
            'correo_institucional',
            'nombre',
            'apellido_paterno',
            'apellido_materno'
             )




class FormInformacion(forms.ModelForm):
    nombre_archivo = forms.CharField(label='Nombre del archivo: *',required=False, max_length=100)
    descripcion = forms.CharField(label='Descripción: *',required=False,max_length=500, widget=forms.Textarea)
    ruta_archivo  = forms.FileField(label='Archivo: *',required=False)


    class Meta:
        model = Informacion

        fields = (
        'nombre_archivo', 'descripcion', 'ruta_archivo')


class FormModificarCoordinador(forms.ModelForm):
    password = forms.CharField(required=False,label='Nueva Contraseña',widget=forms.PasswordInput)
    confirmar_password = forms.CharField(required=False, max_length=50,label='Confirmar contraseña',widget=forms.PasswordInput)
    #correo_electronico = forms.EmailField(label="Correo electrónico 1 ",required = True, max_length=50)
    #correo_institucional = forms.EmailField(label="Correo electrónico 2",required=False,max_length=50)



    nombre = forms.CharField(label='Nombre: *',required=False,max_length=50)
    apellido_paterno = forms.CharField(label='Apellido Paterno: *',required=False, max_length=50)
    apellido_materno = forms.CharField(label='Apellido Materno: *',required=False, max_length=50)



    class Meta:
         model = Usuario
         model_coordinador = Coordinador

         fields = (
            'password',
            'confirmar_password',
            'nombre',
            'apellido_paterno',
            'apellido_materno'
             )

class FormExperto(forms.ModelForm):
    contrasena = forms.RegexField(
        label='Contraseña:',
        required=True,
        regex='.{8,32}',
        strip=True,
        widget=forms.widgets.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )
    correo_electronico = forms.EmailField(
        label='Correo:',
        required=True,
        widget=forms.widgets.EmailInput(
            attrs={
                'class': 'form-control',
            }
        )
    )

    id_experto = forms.IntegerField(
        required=False,
        widget=forms.widgets.HiddenInput()
    )
    nombre = forms.RegexField(
        label='Nombre(s):',
        required=True,
        regex='^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,50}$',
        strip=True,
        widget=forms.widgets.TextInput(
            attrs = {
                'class': 'form-control',
            }
        )
    )
    ap_paterno = forms.RegexField(
        label='Apellido Paterno:',
        required=True,
        regex='^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,50}$',
        strip=True,
        widget=forms.widgets.TextInput(
            attrs = {
                'class': 'form-control',
            }
        )
    )
    ap_materno = forms.RegexField(
        label='Apellido Materno:',
        required=False,
        regex='^[a-zA-ZáéíóúÁÉÍÓÚñÑ ]{3,50}$',
        strip=True,
        widget=forms.widgets.TextInput(
            attrs={
                'class': 'form-control',
            }
        )
    )
    teléfono = forms.RegexField(
        label="Teléfono:",
        required=True,
        regex='\d{10}',
        strip = True,
        widget=forms.widgets.TextInput(
            attrs = {
                'class': 'form-control',
            }
        )
    )
    especialidad = forms.CharField(
        label="Especialidad:",
        required=True,
        max_length=60,
        widget=forms.widgets.TextInput(
            attrs = {
                'class': 'form-control',
            }
        )
    )
    activo = forms.BooleanField(
        required=False,
        initial=True,
        widget=forms.widgets.HiddenInput()
    )
    id_categoria = forms.ModelChoiceField(
        label='Categoria:',
        required=True,
        queryset=Categorias.objects.all(),
        widget=forms.widgets.Select(
            attrs = {
                'class': 'form-control'
            }
        )
    )

    class Meta:
        model = Experto

        fields = (
            'contrasena',
            'correo_electronico',
            'id_experto',
            'nombre',
            'ap_materno',
            'ap_paterno',
            'teléfono',
            'especialidad',
            'activo',
            'id_categoria',
        )