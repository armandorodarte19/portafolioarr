#Este script asigna pasantes a otras generaciones 
#directamente en la base de datos
#para complementar las pruebas de functional_tests/test_alta_pasante.py

#1.- manualmente dar de alta las matriculas que vienen en functional_tests/test_alta_pasante.py
#2.- cambiar la linea donde se manda la imagen por una en su maquina local
#3.- correr este script con "python manage.py shell", copiar y pegar en la terminal

from pasante.models import *
#cambiar si se requiere otras generaciones
def crear_generacion(gen,anio):
    try:
        Generacion.objects.get(generacion=gen,anio=anio)
    except Exception as e:
        Generacion.objects.create(generacion=gen,anio=anio)
        


crear_generacion("Febrero","2015")
crear_generacion("Agosto","2017")
crear_generacion("Febrero","2018")
crear_generacion("Agosto","2018")
crear_generacion("Febrero","2019")
crear_generacion("Agosto","2019")
crear_generacion("Febrero","2021")
crear_generacion("Agosto","2022")
#vacia
crear_generacion("Febrero","2017")


gen = [""]*9
pasantes = [""]*9

gen[0] = Generacion.objects.get(generacion="Febrero",anio="2015")
gen[1] = Generacion.objects.get(generacion="Agosto",anio="2017")
gen[2] = Generacion.objects.get(generacion="Febrero",anio="2018")
gen[3] = Generacion.objects.get(generacion="Agosto",anio="2018")
gen[4] = Generacion.objects.get(generacion="Febrero",anio="2019")
gen[5] = Generacion.objects.get(generacion="Agosto",anio="2019")
gen[6] = Generacion.objects.get(generacion="Febrero",anio="2021")
gen[7] = Generacion.objects.get(generacion="Agosto",anio="2022")

#cambiar si e requiere otro pasante
pasantes[0] = Pasante.objects.get(curp='NHYU4158769MHRDJN4')
pasantes[1] = Pasante.objects.get(curp='BHTE416987BGFDER56')
pasantes[2] = Pasante.objects.get(curp='NHYR143258VFRESD09')
pasantes[3] = Pasante.objects.get(curp='RERS099878OIUIJU01')
pasantes[4] = Pasante.objects.get(curp='UYTY478587LOLIUI00')
pasantes[5] = Pasante.objects.get(curp='IKUI012458YYTYUI10')
pasantes[6] = Pasante.objects.get(curp='EREW343545JUJYHG00')
pasantes[7] = Pasante.objects.get(curp='MKJH876765UYUYTR03')

for i in range(0,8):
    prom = Promocion.objects.get(id_pasante_fk=pasantes[i])
    prom.id_generacion_fk = gen[i]
    prom.save()

for p in Promocion.objects.all()[1:]:
    pas =p.id_pasante_fk
    print(pas.nombre +" "+pas.apellido_paterno+"\t\t | Generacion: "+str(p.id_generacion_fk))

