from django.apps import AppConfig


class NotificacionPreguntaConfig(AppConfig):
    name = 'notificacion_pregunta'
