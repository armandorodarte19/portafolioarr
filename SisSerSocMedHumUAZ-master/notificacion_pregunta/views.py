from django.shortcuts import render
from foro.models import *
from pregunta.models import Respuesta_Foro

# Create your views here.
class NotificarRespuesta:
	def notificar_respuesta(request):
		regreso=[]
		final=""
		no_notificadas = Pregunta_Foro.objects.filter(notificada=0,id_usuario_fk_id=request.user.id)
		for p in no_notificadas:
			respuestas=Respuesta_Foro.objects.filter(id_pregunta_fk_id=p.id_pregunta)
			if(len(respuestas)>0):
				p.notificada=1
				p.save()
				regreso.append(p)
			
		return regreso	
