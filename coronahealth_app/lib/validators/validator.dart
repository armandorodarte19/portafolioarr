import 'package:covid_uaz_app/language/language.dart';

class Validators{

  Lenguaje lenguaje = new Lenguaje();

  String? validatorTextFormField(String? value){
    int num2 = 0;
  
  
    

    if (value == null || value.isEmpty || value == "") {
      
      return '${lenguaje.textEmptyValidator}';
      
    }else{
      try {
        num2 = int.parse(value);
      } catch (error) {
        return "${lenguaje.numValidator}";   
      }
      if (num2< 0 || num2> 125){
    
        return '${lenguaje.ageValidator}';
      }
    }
    
    return null;
  }


  bool validatorFrom(Map<String, String> answersSlect){
    bool isValid = true;
                             
    answersSlect.forEach(
      (key, value){ 
        if(value == lenguaje.answer){
          isValid = false;
        } 
      }
    );

    return isValid;
  }

  Map<String, String> cleanForm(Map<String, String> answersSlect){
   
                             
    answersSlect.forEach(
      (key, value){           
        answersSlect[key]= lenguaje.answer;
        //print(answersSlect[key]);
      }
    );

    answersSlect["edad"]= "";
    return answersSlect;
  }
}