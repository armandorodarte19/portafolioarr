import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {

  static final PreferenciasUsuario _instancia = new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();
  final clave = "lenguage";
  late SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  // GET y SET del lenguaje
  String get lenguage {
    return _prefs.getString(clave) ?? "en";
  }

  set lenguage( String value ) {
    _prefs.setString(clave, value);
  }
}