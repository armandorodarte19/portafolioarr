
import 'package:covid_uaz_app/language/language.dart';

class Initializers{
 
  Lenguaje lenguaje = new Lenguaje();

  Map<String, String> elements (){
    Map<String, String> elementsA = {"edad" : "", 
      "neumonia" : "", 
      "diabetes": "",
      "hipertension": "",
      "renal" : "",
      "sexo":"",
      "intubado": "",
      "embarazada":""
    };

    elementsA.forEach((key, value) { 
      elementsA[key] = lenguaje.answer;
    });

    elementsA["edad"] = "";
    elementsA["sexo"] = lenguaje.genderAnswer;
    elementsA["embarazada"] = lenguaje.answers[1];

    return elementsA;
  }

  Map<String, double?>  answers(){

    Map<String, double?> answersA = {"edad" : 0, 
      "neumonia" :0, 
      "diabetes": 0,
      "hipertension": 0,
      "renal" : 0,
      "sexo":0,
      "intubado":0,
      "embarazada":0
    };

    return answersA;
  }
}