
import 'package:covid_uaz_app/pages/login_page.dart';
import 'package:covid_uaz_app/preferencias/preferencias.dart';
import 'package:flutter/material.dart';

void main()async { 
  
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();
  runApp(MyApp());
}
 
class MyApp extends StatelessWidget {
 
  @override
  Widget build(BuildContext context) {
    /*SystemChrome.setPreferredOrientations([
     DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);*/
    
    return MaterialApp(
      
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      initialRoute: 'login',
      routes: {
        'login'    : ( BuildContext context ) =>LoginPage2(),
        // 'home'     : ( BuildContext context ) => HomePage(),
      },
      theme: ThemeData(
        primaryColor: Colors.blue,
      ),
      
    );   
  }
}
