import 'package:tflite_flutter/tflite_flutter.dart';


class ModelTlite{

  late var  interpreter = null;

  static final ModelTlite _instancia = new ModelTlite._internal();

  factory ModelTlite() {
    
    return _instancia;
  }

  ModelTlite._internal();

  loadModel() async {
    // Use Interpreter.fromAsset to create an interpreter
    this.interpreter = await Interpreter.fromAsset("cnn_model_TFlite.tflite");
    print('Interpreter loaded successfully');
  }

  Interpreter get interpreterObj {
   
    if(this.interpreter == null){
      loadModel();
      //print("nullll");
    }
    
    return this.interpreter;
  }

  void close(){    
    this.interpreter.close();
  }
}