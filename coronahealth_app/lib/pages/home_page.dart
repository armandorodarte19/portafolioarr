
import 'package:covid_uaz_app/Initializers/initializer.dart';
import 'package:covid_uaz_app/language/language.dart';
import 'package:covid_uaz_app/pages/resultado_page.dart';
import 'package:covid_uaz_app/tlite_model/tlite_modelCNN.dart';
import 'package:covid_uaz_app/validators/validator.dart';
import 'package:flutter/material.dart';

//import 'package:tflite_flutter/tflite_flutter.dart';

import 'package:flutter/services.dart';

class HomePage extends StatelessWidget {
  Lenguaje lenguaje = new Lenguaje();

    @override
    Widget build(BuildContext context) {
    
    return Scaffold(
      appBar: AppBar(
        title: Text('${lenguaje.titleForm}')
      ),
      body: ListView( children: <Widget>[MyCustomForm()]),
        
    );

  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
 

  late Initializers itz;
  late var interpreter;
  ModelTlite model = new ModelTlite();  

  var output = [[0.0]];
  Color desactivate = Colors.red;
  Icon desactivateIcon = Icon(Icons.warning, color: Colors.red);
  final _formKey = GlobalKey<FormState>();
  
  late Lenguaje lenguaje;
  Validators validadtor = new Validators();
  late Map<String, String> elementos;
  late Map<String, double?> resultados;  
  var input = [1.0, 1.0, 1.0, 1.0, 1.0,1.0,1.0,1.0];
  late bool isPregnant;

  final TextEditingController controller = TextEditingController(text: "");
  
  MyCustomFormState(){

    itz = Initializers();
    elementos = itz.elements();
    resultados = itz.answers(); 
    lenguaje = new Lenguaje();
   // validadtor = new Validators();
    isPregnant = true;
    interpreter = null;
  }

  /*
  void loadModel() async {
      
    Use Interpreter.fromAsset to create an interpreter
    this.interpreter = await Interpreter.fromAsset("cnn_model_TFlite.tflite");
    print('Interpreter loaded successfully');
    
  
  }*/

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    final size = MediaQuery.of(context).size;
    //loadModel();
    model.loadModel();  
    double sizeLatter = 18.5;
    double sizeh = size.width*0.50;
    double sizeContainer = size.width*0.78;

    return Container(  
      width:sizeContainer,
      color: Colors.white,
      child:
      Form(
        key: _formKey,
        child: Column(  
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Center( 
              child: Container(
              // color: Colors.white,
                width: size.width*0.75,
              // alignment: Alignment.centerRight,
                child:
                  TextFormField(
                    
                    keyboardType: TextInputType.number,
                    controller: controller ,
                    decoration:  InputDecoration(
                     // icon: Icon(Icons.grid_3x3),
                      hintText: '${lenguaje.ageHint}',
                      labelText:  '${lenguaje.age}',
                      labelStyle:  TextStyle( color: Colors.blue, fontSize: sizeLatter),
                      
                    ),
                    // The validator receives the text that the user has entered.
                    validator: (values) {
                     
                      String? r = validadtor.validatorTextFormField(values);
                      return r;
                    },
                  onChanged: (text) {
                    setState(() 
                      {
                        elementos["edad"] = text;
                      }   
                    );   
                  },
                )
              )
            ),
            // Separador   //
            Center(
              child:
              Container(
                width: sizeContainer,
                color: Colors.white,
                child: new Center(
                    child: new Column(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          padding: new EdgeInsets.all(6.0),
                        ),
                        new Text("${lenguaje.pneumonia}",style: TextStyle( color: Colors.blue, fontSize: sizeLatter)),
                        
                        DropdownButton<String>(
                          isExpanded: true,
                          value: elementos["neumonia"],
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 16
                          
                          ),
                          
                          underline: Container(
                            height: 2,
                            color: Colors.blueAccent,
                          ),
                          onChanged: (String? newValue) {
                            setState(() 
                              {
                                elementos["neumonia"] = newValue!;
                              
                                //print(elementos["neumonia"]);
                              }
                            );
                          },
                          items: lenguaje.answers
                            .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            })
                            .toList(),
                        ),
                      ],
                    )
                )
              ),
            ),
            // Separador
            //
            Center(
              child: Container(
                width: sizeContainer,
                color: Colors.white,
                child: new Center(
                    child: new Column(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                          new Container(
                            padding: new EdgeInsets.all(6.0),
                          ),
                          new Text("${lenguaje.diabetes}",style: TextStyle( color: Colors.blue, fontSize: sizeLatter)),
                          
                          DropdownButton<String>(
                            value: elementos["diabetes"],
                            isExpanded: true,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 16                   
                            ),    
                            underline: Container(
                              height: 2,
                              color: Colors.blueAccent,
                            ),
                            onChanged: (String? newValue) {
                              setState(() 
                                {
                                  elementos["diabetes"] = newValue!;
                                }
                              );
                            },
                            items: lenguaje.answers
                              .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              })
                              .toList(),
                          ),
                      ],
                    )
                ),
              )
            ),
            // Separador
            //
            Center(
              child:
                Container(
                  width: sizeContainer,
                  color: Colors.white,
                  child: new Center(
                      child: new Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                            padding: new EdgeInsets.all(6.0),
                          ),
                          new Text("${lenguaje.hypertension}",style: TextStyle( color: Colors.blue, fontSize: sizeLatter)),
                          
                          DropdownButton<String>(
                            value: elementos["hipertension"],
                            isExpanded: true,
                            icon: const Icon(Icons.arrow_downward),
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 16
                            ),
                            underline: Container(
                              height: 2,
                              color: Colors.blueAccent,
                            ),
                            onChanged: (String? newValue) {
                              setState(() {
                                elementos["hipertension"] = newValue!;
                              });
                            },
                            items: lenguaje.answers
                              .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }) .toList(),
                          ),
                        ],
                      )
                  ),
                )
            ),
            // Separador
            //
            Center(
              child:
              Container(
                width: sizeContainer,
                color: Colors.white,
                child: new Center(
                    child: new Column(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          padding: new EdgeInsets.all(6.0),
                        ),
                        new Text("${lenguaje.chronicKidneyDisease}",style: TextStyle( color: Colors.blue, fontSize: sizeLatter), textAlign: TextAlign.left,),
                        
                        DropdownButton<String>(
                          value: elementos["renal"],
                          isExpanded: true,
                          icon: const Icon(Icons.arrow_downward),
                          iconSize: 24,
                          elevation: 16,
                          style: const TextStyle(
                            color: Colors.black,
                            fontSize: 16
                            
                          ),
                          
                          underline: Container(
                            height: 2,
                            color: Colors.blueAccent,
                          ),
                          onChanged: (String? newValue) {
                            setState(() {
                              elementos["renal"] = newValue!;
                            });
                          },
                          items: lenguaje.answers
                            .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                        ),
                      ],
                    )
                ),
              )
            ),
            // Separador
            //
            Center(
              child:
                Container(
                    width: sizeContainer,
                    color: Colors.white,
                    child: new Center(
                        child: new Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        // mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                              padding: new EdgeInsets.all(6.0),
                            ),
                            new Text("${lenguaje.intubation}",style: TextStyle( color: Colors.blue, fontSize: sizeLatter), textAlign: TextAlign.left,),
                            
                            DropdownButton<String>(
                              value: elementos["intubado"],
                              isExpanded: true,
                              icon: const Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: const TextStyle(
                                color: Colors.black,
                                fontSize: 16
                                
                              ),
                              
                              underline: Container(
                                height: 2,
                                color: Colors.blueAccent,
                              ),
                              onChanged: (String? newValue) {
                                setState(() {
                                  elementos["intubado"] = newValue!;
                                });
                              },
                              items: lenguaje.answers
                                .map<DropdownMenuItem<String>>((String value) {
                                  return DropdownMenuItem<String>(
                                    value: value,
                                    child: Text(value),
                                  );
                                })
                                .toList(),
                            ),
                          ],
                        )
                    ),
                )
            ),
            Center(
              child:
                Container(
                  color: Colors.white,
                  width: sizeContainer,
                  child: new Center(
                      child: new Column(
                        // crossAxisAlignment: CrossAxisAlignment.center,
                        // mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                              padding: new EdgeInsets.all(6.0),
                          ),
                          new Text("${lenguaje.genderQ}",
                            style: TextStyle( color: Colors.blue, fontSize: sizeLatter), 
                            textAlign: TextAlign.left,
                          ),
                          DropdownButton<String>(
                            value: elementos["sexo"],
                            isExpanded: true,
                            icon: const Icon(Icons.arrow_downward) ,
                            iconSize: 24,
                            elevation: 16,
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 16
                            ),
                            underline: Container(
                              height: 2,
                              color: Colors.blueAccent,
                            ),
                            onChanged: (String? newValue) {
                              setState(() {
                                elementos["sexo"] = newValue!;
                                statusPregnant(newValue);
                              });
                            },
                            items:lenguaje.gender
                              .map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                          ),
                        ],
                      )
                  ),
                )
            ),
            Center(
              child:
                Container(
                  color: Colors.white,
                  width: sizeContainer,

                  child: new Center(
                      child: new Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                            padding: new EdgeInsets.all(6.0),
                          ),
                          new Text("${lenguaje.pregnantQ}", 
                            style: TextStyle( color: Colors.blue, fontSize: sizeLatter), 
                            textAlign: TextAlign.left,
                          ),
                          new IgnorePointer(
                            ignoring: this.isPregnant,
                            child:   
                              DropdownButton<String>(
                              //hint: new Text("disabled!"),
                                value: elementos["embarazada"],
                                isExpanded: true,
                                icon: desactivateIcon,
                                iconSize: 24,
                                elevation: 16,
                                style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 16 
                                ),
                                
                                underline: Container(
                                  height: 2,
                                  color: desactivate,
                                ),
                                onChanged: (String? newValue) {
                                  setState(() {
                                    elementos["embarazada"]= newValue!;  
                                  });
                                },
                                items: lenguaje.answers
                                  .map<DropdownMenuItem<String>>((String value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(value),
                                    );
                                  }).toList(),
                              ),
                          )
                        ],
                      )
                  ),
                )
            ),

            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical: 16.0),
                child: 
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      shadowColor: Colors.red,
                      padding: EdgeInsets.symmetric(horizontal: 35, ),
                    ),
                    onPressed: () {
                      
                      bool isValid = validadtor.validatorFrom(elementos);
                      
                      // Validate returns true if the form is valid, or false otherwise.
                      if (_formKey.currentState!.validate() && isValid == true) {
                        // If the form is valid, display a snackbar. In the real world,
                        // you'd often call a server or save the information in a database.
                        elementos.forEach((key, value) 
                          { 
                            //['Sí', 'No', "No sé o se desconece con exactitud", "--Sin Valor--"];
                            if(value ==lenguaje.answers[0]){ // Si
                              resultados[key] = 1.0;
                            } 
                            else if(value ==lenguaje.answers[1]){ // NO
                              resultados[key] = 2.0;
                            }
                            else if(value ==lenguaje.answers[2]){ // no lo se
                              resultados[key] = 3.0;
                            }
                            else if(value == lenguaje.gender[1]){ // mujer
                              resultados[key] = 1.0;
                            }
                            else if(value ==lenguaje.gender[0]){ // hombre
                              resultados[key] = 2.0;
                            }

                          }
                        );

                        resultados["edad"] = double.parse(controller.text);

                        input[0] =double.parse(resultados["sexo"].toString());
                        input[1] =double.parse(resultados["intubado"].toString());
                        input[2] =double.parse(resultados["neumonia"].toString());
                        input[3] =double.parse(resultados["edad"].toString());
                        input[4] =double.parse(resultados["embarazada"].toString());
                        input[5] =double.parse(resultados["diabetes"].toString());
                        input[6] =double.parse(resultados["hipertension"].toString());
                        input[7] =double.parse(resultados["renal"].toString());
                      
                        
                        interpreter = model.interpreterObj;        
                        interpreter.run(input, output);
                        //model.close();
                        //print(input.toString());
                        //print(output);
                        double r = (output[0][0])*100;
                        double r2 = 100-r;
                        //print(r);
                        //print(r2);

                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text('${lenguaje.processingInfo}')));
                        
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => ResultadoPage(pVivir:r, pMorrir: r2,)),
                        );
                      }else{
                        ScaffoldMessenger.of(context)
                            .showSnackBar(SnackBar(content: Text('${lenguaje.errorInfo}',style: TextStyle(color: Colors.red))));
                      }
                    },
                    child: Text('${lenguaje.diagnoseButton}',style: TextStyle(  fontSize: sizeLatter -1)),
                     
                  ),
              )
            ),
            
            Center(
              child:
                Container(
                  color: Colors.white,
                  width: sizeh,
                  child: 
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          
                        primary: Colors.red,
                        padding: EdgeInsets.symmetric(horizontal: 35, ),
                        shadowColor: Colors.blue,
                      ),
                      onPressed: () { 
                        setState(() {
                          cleanForm();
                        });      
                      },
                      child:  
                        Row(
                          children: <Widget>[ 
                            Text('${lenguaje.cleanButton}',style: TextStyle(  fontSize: sizeLatter -1)), 
                            Icon(Icons.delete)
                            ], 
                        ), 
                    ),
                ),
            )
          ],
        ),
      )
    );
  }

  void cleanForm(){
    elementos = validadtor.cleanForm(elementos);
    controller.text = "";
    elementos["embarazada"] = lenguaje.answers[1];

    this.isPregnant = true;
    desactivateIcon = Icon(Icons.warning, color: Colors.red,);
    desactivate = Colors.red; 
  }

  void statusPregnant(String? newValue ){
    //print(newValue);
    if(newValue == this.lenguaje.gender[0] || newValue == this.lenguaje.gender[2]){
      this.elementos["embarazada"]= lenguaje.answers[1];
      this.isPregnant = true;
      this.desactivateIcon = Icon(Icons.warning, color: Colors.red,);
      this.desactivate = Colors.red;
                        
    }else{
      this.elementos["embarazada"]=lenguaje.answer;
      this.desactivateIcon = Icon(Icons.arrow_downward,);
      this.desactivate = Colors.blueAccent;
      this.isPregnant = false;
                        
    }
  }
}