import 'package:covid_uaz_app/language/language.dart';
import 'package:flutter/material.dart';
import 'package:pie_chart/pie_chart.dart';


class ResultadoPage extends StatelessWidget {
 
  Lenguaje lenguaje = new Lenguaje();
  double pVivir;
  double pMorrir;
  ResultadoPage({@required this.pVivir =0, @required this.pMorrir = 0});

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text('${lenguaje.patientResults}')
      ),
      body:Stack(
        children: <Widget>[ 
          SizedBox( height: 30.0 ),
          Text("${lenguaje.patientResultsLabel}",style: TextStyle( color: Colors.black, fontSize: 18.0, fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
          Center (child: _showGraph(context)),
        ],
      )
    );
  }

  Widget _showGraph(BuildContext context) {
   
    Map<String, double> dataMap = new Map();
    dataMap.putIfAbsent("${lenguaje.recoveryPatient}", () => this.pVivir);
    dataMap.putIfAbsent("${lenguaje.noRecoveryPatient}", () => this.pMorrir);
    
    final List<Color> colorList = [
      Colors.blue,
      Colors.red,
      Colors.green,
      Colors.purple,
    ];

    return OrientationBuilder(
      builder: (context, orientation) {
        return  Container(
          width: double.infinity,
          height:  1200 ,
          child: PieChart(
            dataMap: dataMap,
            animationDuration: Duration(milliseconds: 800),
            chartLegendSpacing: orientation ==  Orientation.portrait ? 32 : 15,
            chartRadius: orientation ==  Orientation.portrait ? MediaQuery.of(context).size.width / 1.2 : MediaQuery.of(context).size.width / 3.5 ,
            colorList: colorList,
            initialAngleInDegree: 0,
            chartType: ChartType.disc,
            ringStrokeWidth: 32,
            legendOptions: LegendOptions(
              showLegendsInRow: false,
              legendPosition: orientation ==  Orientation.portrait ? LegendPosition.bottom : LegendPosition.left ,
              showLegends: true,
              legendTextStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: orientation ==  Orientation.portrait ? 18 : 15,
              ),
            ),
            chartValuesOptions: ChartValuesOptions(
              showChartValueBackground: true,
              showChartValues: true,
              showChartValuesInPercentage: true,
              showChartValuesOutside: false,
              decimalPlaces: 4,
              
            ), 
          )
        );
      } 
    );
  }
}