
import 'package:covid_uaz_app/language/language.dart';
import 'package:flutter/material.dart';
import 'home_page.dart';

class LoginPage2 extends StatefulWidget {
  @override
  LoginPage createState() {
    return LoginPage();
  }
}

class LoginPage extends State<LoginPage2 > {
  
  Lenguaje lenguaje = new Lenguaje();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      body:  Stack(
      
        children: <Widget>[
          _crearFondo( context ),
          _loginForm( context ),
        ],
      )
    );
  }

  Widget _loginForm(BuildContext context) {
    double sizeh = 0.0;
   // final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;
    
    if(size.shortestSide > 600) {
    // Do something for tablets here
      sizeh =size.height * 0.22;
     
    } else {
      // Do something for phones
      sizeh =size.height * 0.22;
    }
    
    return  OrientationBuilder(
      builder: (context, orientation) {  
        return SingleChildScrollView(
          child: Column(
            
            children: <Widget>[
              SafeArea(
                child:  Container(
                  height: orientation == Orientation.portrait ? sizeh  : (size.shortestSide > 600 ?  (size.height *0.25): sizeh+ (size.height *0.25)) ,
                )
              ),
              Container(
                width: size.width * 0.85,
                margin: EdgeInsets.symmetric(vertical: size.height * 0.05),
                padding: EdgeInsets.symmetric( vertical: size.height * 0.05),
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0
                    )
                  ]
                ),
                child: Column(
                  children: <Widget>[
                    Text('${lenguaje.messageLogin}', style: TextStyle(fontSize: 18.0)),
                    SizedBox( height: 30.0 ),
                    _crearBoton()
                  ],
                ),
              ),

              Text('${lenguaje.legend}'),
              SizedBox( height: 30.0 ),
              Container(
                width: size.width*0.5,
                color: Colors.white,
                child: new Center(
                  child: new Row(
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    // mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                      new Container(
                        padding: new EdgeInsets.all(6.0),
                      ),
                      new Text("${lenguaje.language}  ",
                        style: TextStyle( color: Colors.blue, fontSize: 15.0)
                      ),
                      DropdownButton<String>(
                        value: "${lenguaje.idiomaSelec}",
                        icon: const Icon(Icons.arrow_downward),
                        iconSize: 24,
                        elevation: 16,
                      
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16
                        
                        ),
                        
                        underline: Container(
                          height: 2,
                          color: Colors.blueAccent,
                        ),
                        onChanged: (String? newValue) {
                          setState(
                            () {
                             changeLanguage(newValue);
                            }
                          );
                        },
                        items: lenguaje.languagesOptions
                          .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }
                        ).toList(),
                      ),
                    ],
                  )
                ),
              ),
              SizedBox( height: 50.0 ),
              Text('${lenguaje.description}',
              style: TextStyle(fontSize: 15.0), textAlign:TextAlign.center),
              SizedBox( height: 100.0 )
            ],
          ),
        ); 
      }
    );
  }

  Widget _crearBoton() {

    // formValidStream
    // snapshot.hasData
    //  true ? algo si true : algo si false

    return StreamBuilder(
      
      builder: (BuildContext context, AsyncSnapshot snapshot){
        
        return ElevatedButton(
          child: Container(
            padding: EdgeInsets.symmetric( horizontal: 85.0, vertical: 15.0),
            child: Text('${lenguaje.start}', style: TextStyle(fontSize: 18.0)),
            
          ),
          style: ElevatedButton.styleFrom(shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(15.0),),
            elevation: 0.0, 
            shadowColor: Colors.red,  
            primary: Colors.blue
          ),
          
          //color: Colors.blue,
          //textColor: Colors.white, 
          onPressed: () { 
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          },
         // onPressed: snapshot.hasData ? ()=> _login(bloc, context) : null
        );
      },
    );
  }


  Widget _crearFondo(BuildContext context) {

    final size = MediaQuery.of(context).size;
    double sizeh2 = 0.0;
    
    if(size.shortestSide > 600) {
    // Do something for tablets here
      sizeh2 =size.height * 0.46;
    } else {
      // Do something for phones
      sizeh2 =size.height * 0.73;
    }

    final fondoModaro = OrientationBuilder(
      builder: (context, orientation) {
        return Container(
          height: orientation == Orientation.portrait ? size.height * 0.4 : sizeh2,
          width: double.infinity,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: <Color> [
                Colors.blue,
                Color.fromRGBO(90, 70, 178, 1.0)
              ]
            )
          ),
        );
      } 
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100.0),
        color: Color.fromRGBO(255, 255, 255, 0.05)
      ),
    );

    return Stack(
      children: <Widget>[
        fondoModaro,
        Positioned( top: 90.0, left: 30.0, child: circulo ),
        Positioned( top: -40.0, right: -30.0, child: circulo ),
        Positioned( bottom: -50.0, right: -10.0, child: circulo ),
        Positioned( bottom: 120.0, right: 20.0, child: circulo ),
        Positioned( bottom: -50.0, left: -20.0, child: circulo ),
        
        Container(
          padding: EdgeInsets.only(top: 80.0),
          child: Column(
            children: <Widget>[
              Image.asset("assets/iconop2.png", width: 100.0, height: 100.0,),
             // Icon( Icons.coronavirus, color: Colors.white, size: 100.0 ),
              SizedBox( height: 10.0, width: double.infinity ),
              Text('${lenguaje.title}', style: TextStyle( color: Colors.white, fontSize: 25.0 ))            
            ],
          ),
        )
      ],
    );
  }

  void changeLanguage(String? newValue){
    if(newValue ==lenguaje.optionsES[0] ){
        lenguaje.lenguajes ="es";
                                
    }
    if(newValue == lenguaje.optionsES[1]){
      lenguaje.lenguajes ="en";
    }
    if(newValue == lenguaje.optionseEn[0]){
      lenguaje.lenguajes ="es";                         
    }
    if(newValue == lenguaje.optionseEn[1]){
      lenguaje.lenguajes ="en";
    }
  }
}

class StatefullWidget {
}