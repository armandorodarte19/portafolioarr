
import 'package:covid_uaz_app/preferencias/preferencias.dart';


class Lenguaje {
  
  late String lenguaje;
  PreferenciasUsuario _instancia = new PreferenciasUsuario();

  final List<String> optionsES = ["Español", "Inglés"];
  final List<String> optionseEn = ["Spanish", "English"];
  final List<String> answersES = ['Sí', 'No', "No sé o se desconece con exactitud", "--Seleccionar--"];
  final List<String> answersEn = ['Yes', 'No', "I dont know it", "--Select--"];
  final List<String> genderES = ['Hombre', 'Mujer',"--Seleccionar--"];
  final List<String> genderEn = ['Male', 'Famale',"--Select--"];

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'start':'Get started',
      'title': 'CoronaHealth',
      'description':'The mobile application developed will help to predict the probability of recovering and not recovering in patients infected by COVID-19, based on the factors with the greatest influence on death from COVID-19 in Mexico.',
      'messageLogin':'Click on get started to diagnose',
      'es':'Spanish',
      'en':'English',
      'legend':'APP COVID-19 UAZ',
      'language':'Language:',
      'age': 'Enter your age:',
      'ageHint': 'How old are you?',
      'answer':'--Select--',
      'gender':'--Select--',
      'titleForm': 'Diagnosis',
      'ageValidator':"The value is not valid for Age",
      'numValidator':"The value is not valid",
      'textEmptyValidator':"Age not valid",
      'CleanButton':'Clean form',
      'diagnoseButton':'Diagnose',
      'errorInfo':'Error: wrong information',
      'processingInfo':'Processing information',
      'diabetes':'Do I have diabetes?',
      'pneumonia':'Do I suffer from pneumonia?',
      'hypertension':'Am I hypertensive?',
      'chronicKidneyDisease':'Do I have chronic kidney disease?',
      'intubation':'Am I requiring intubation?',
      'genderQ':'Gender:',
      'pregnantQ':'Am I pregnant?',
      'recoveryPatient':'Probability of recovery',
      'noRecoveryPatient':'Probability of NOT recovering',
      'patientResults':'Patient Results',
      'patientResultsLabel':'Results: Probabilities of recovery from COVID-19',
    },
    'es': {
      'title': 'CoronaHealth',
      'start':'Iniciar',
      'description':'La aplicación móvil desarrollada apoyará en lograr predecir la probabilidad de recuperarse y de no recuperarse en pacientes infectados por COVID-19, a partir de los factores con mayor influencia en la muerte por COVID-19 en México.',
      'messageLogin':'Da clic en iniciar para diagnosticar',
      'es':'Español',
      'en':'Inglés',
      'legend':'APP COVID-19 UAZ',
      'language':'Lenguaje:',
      'age': 'Ingresa tu edad:',
      'ageHint': '¿Cuál es tu edad?',
      'answer':'--Seleccionar--',
      'gender':'--Seleccionar--',
      'titleForm': 'Diagnóstico',
      'ageValidator':"No es un número valido",
      'numValidator':"No es un número",
      'textEmptyValidator':"Edad no valida",
      'CleanButton':'Borrar datos',
      'diagnoseButton':'Diagnosticar',
      'errorInfo':'Error: información incorrecta',
      'processingInfo':'Procesando Información',
      'diabetes':'¿Padezco diabetes?',
      'pneumonia':'¿Sufro de neumonía?',
      'hypertension':'¿Soy hipertenso?',
      'chronicKidneyDisease':'¿Tengo insuficiencia renal crónica?',
      'intubation':'¿Estoy requiriendo intubación?',
      'genderQ':'Género:',
      'pregnantQ':'¿Estoy embarazada?',
      'recoveryPatient':'Probabilidad de recuperación ',
      'noRecoveryPatient':'Probabilidad de NO recuperarse',
      'patientResults':'Resultados del Paciente',
      'patientResultsLabel':'Resultados: Probabilidades de recuperación por COVID-19',
    },
    
  };

  Lenguaje(){
     this.lenguaje = _instancia.lenguage;
    
  }

  void saveState(String valor)async{
    
    _instancia.lenguage = valor;
  }

  set lenguajes(String lenguaje) {
    saveState(lenguaje);
    this.lenguaje = lenguaje;
  }

  String get patientResultsLabel{
    return _localizedValues[lenguaje]!["patientResultsLabel"].toString();
  }
  String get patientResults{
    return _localizedValues[lenguaje]!["patientResults"].toString();
  }

  String get noRecoveryPatient{
    return _localizedValues[lenguaje]!["noRecoveryPatient"].toString();
  }

  String get recoveryPatient{
    return _localizedValues[lenguaje]!["recoveryPatient"].toString();
  }

  String get genderQ{
    return _localizedValues[lenguaje]!["genderQ"].toString();
  }

  String get pregnantQ{
    return _localizedValues[lenguaje]!["pregnantQ"].toString();
  }

  String get intubation{
    return _localizedValues[lenguaje]!["intubation"].toString();
  }
  
  String get chronicKidneyDisease{
    return _localizedValues[lenguaje]!["chronicKidneyDisease"].toString();
  }
  
  String get hypertension{
    return _localizedValues[lenguaje]!["hypertension"].toString();
  }
  String get pneumonia{
    return _localizedValues[lenguaje]!["pneumonia"].toString();
  }

  String get diabetes{
    return _localizedValues[lenguaje]!["diabetes"].toString();
  }

  String get processingInfo{
    return _localizedValues[lenguaje]!["processingInfo"].toString();
  }

  String get errorInfo{
    return _localizedValues[lenguaje]!["errorInfo"].toString();
  }

  String get cleanButton {
   
    return _localizedValues[lenguaje]!["CleanButton"].toString();
  }
   String get diagnoseButton {
   
    return _localizedValues[lenguaje]!["diagnoseButton"].toString();
  }
  
  String get ageValidator {
   
    return _localizedValues[lenguaje]!["ageValidator"].toString();
  }
   
  String get numValidator {
   
    return _localizedValues[lenguaje]!["numValidator"].toString();
  }
  
  String get textEmptyValidator {
   
    return _localizedValues[lenguaje]!["textEmptyValidator"].toString();
  }

  String get ageHint {
   
    return _localizedValues[lenguaje]!["ageHint"].toString();
  }

  String get titleForm {
   
    return _localizedValues[lenguaje]!["titleForm"].toString();
  }

  String get genderAnswer {
   
    return _localizedValues[lenguaje]!["gender"].toString();
  }

   String get title {
   
    return _localizedValues[lenguaje]!["title"].toString();
  }

  String get description {
   
    return _localizedValues[lenguaje]!["description"].toString();
  }

  String get answer {
   
    return _localizedValues[lenguaje]!["answer"].toString();
  }

   String get start {
    
    return _localizedValues[lenguaje]!["start"].toString();
  }

  String get messageLogin {
    
    return _localizedValues[lenguaje]!["messageLogin"].toString();
  }

  String get es {
    
    return _localizedValues[lenguaje]!["es"].toString();
  }

  String get en {
    
    return _localizedValues[lenguaje]!["en"].toString();
  }

  String get legend {
    
    return _localizedValues[lenguaje]!["legend"].toString();
  }

  String get language {
    
    return _localizedValues[lenguaje]!["language"].toString();
  }

  String get age {
    
    return _localizedValues[lenguaje]!["age"].toString();
  }

  String get idiomaSelec {

    if(this.lenguaje == "es" ){
      return "Español";
    }
    
    return "English";
  }

  List<String> get languagesOptions{
   
    if(this.lenguaje == "es" ){
      return this.optionsES;
    }
    return this.optionseEn;
  }

  List<String> get answers{
   
    if(this.lenguaje == "es" ){
      return this.answersES;
    }
    return this.answersEn;
  }

  List<String> get gender{
   
    if(this.lenguaje == "es" ){
      return this.genderES;
    }
    return this.genderEn;
  }
}