import numpy as np
import matplotlib.pyplot as plt
plt.figure()  # Comenzamos un nuevo gráfico (figura)
lista1 = [0.8552051428763758,0.8510858324715617,0.8546004143599497,0.8558447347666563,0.8570655299570351,0.8525426858956063]
lista2 = [0.8558793969849247,0.8552764058113098 ,0.8565829145728643,0.8591624790619765,0.8584589614740369,0.8528810720268006]
lista3 = [0.8796302707193796,0.8983408528598458,0.8875485008818342,0.8981288233177402,0.8863620460429641,0.8747028628518276]
lista4 = [0.8320998166099031,0.808553838092743,0.8240110034058161,0.8173631123919308,0.8296436992402411,0.8314776002095887]
plt.grid(True, which='both')
plt.title(" Comparison of Model's performance With 16 features." , fontsize=26.25)
plt.xlabel("MODELS" , fontsize=23.5)
plt.ylabel("Accuracy", fontsize=23.5)
"""
plt.title("Comparación de rendimiento de los modelos con 16 características." , fontsize=26.25)
plt.xlabel("MODELOS" , fontsize=23.5)
plt.ylabel("RENDIMIENTO", fontsize=23.5)
"""
indice = np.arange(6)   # Declara un array
"""
plt.xticks(indice, ("KNN", "DNN 1", "Clasificador\nMayoría\nde votos", " Árbol de\nDecisión", "Bosques\naleatorios",
"Regresión\nlogística"),fontsize=23.5)

"""
plt.yticks(fontsize=23.5)
plt.xticks(indice, ("KNN", "DNN 1", "Majority Vote\nClassifier", "Decision\nTree", "Random\nForest",
"Logistic\nRegression"),fontsize=23.5)
#plt.yticks(np.arange(0,51,10))
plt.plot(lista1, marker='x', linestyle=':', color='b', label = "F1 Score")
plt.plot(lista2, marker='*', linestyle='-', color='g', label = "Accuracy")
plt.plot(lista3, marker='o', linestyle='--', color='r', label = "Precision")
plt.plot(lista4, marker='+', linestyle='-.', color='y', label = "Recall")
plt.legend(loc="lower left", fontsize=14.1)
plt.show()

"""
Estilos de Líneas (linestyle=):
-, Línea Sólida
--, Línea discontinua
:, Línea punteada
-., Línea punteada discontinua. y
None, Ninguna línea

Marcadores (marker=):
+, Cruz
., Punto
o,Círculo
*, Estrellas
p, Pentágonos
s, cuadrados
x, Tachados
D, Diamantes
h, Hexágonos y
^, Triángulos

Colores (color=):
b, blue
g, green
r, red
c, cyan
m, magenta
y, yellow
k, black
w, white
"""














##################### 8 Caracteristicas################################
plt.figure()  # Comenzamos un nuevo gráfico (figura)
lista1 = [0.8528999064546305,0.8525415888570987,0.8559872740810938,0.8575752937775284,0.8511288805268109]
lista2 = [0.8524958123953099,0.855829119682312 ,0.8574539363484087,0.859715242881072,0.8515577889447237]
lista3 = [0.8704650845608293,0.8939388495670607,0.885663258159406,0.8919941981816252,0.873792770419426]
lista4 = [0.8360296044013623,0.8148087503274823,0.8282355252816348,0.8257139114487818,0.8296109510086456]
plt.grid()
plt.title(" Comparison of Model's performance With 8 features.", fontsize=26.25)
plt.xlabel("MODELS", fontsize=23.5)
plt.ylabel("Accuracy",fontsize=23.5)
indice = np.arange(5)   # Declara un array
plt.xticks(indice, ("KNN", "DNN 2", "Majority Vote\nClassifier", "Decision Tree",
"Logistic\nRegression"), fontsize=23.5)
"""

plt.title("Comparación de rendimiento de los modelos con 8 características." , fontsize=26.25)
plt.xlabel("MODELOS" , fontsize=23.5)
plt.ylabel("RENDIMIENTO", fontsize=23.5)
plt.xticks(indice, ("KNN", "DNN 2", "Clasificador Mayoría\nde votos", " Árbol de\nDecisión",
"Regresión\nlogística"),fontsize=23.5)
plt.yticks(fontsize=23.5)
plt.yticks(fontsize=23.5)
"""
plt.yticks(fontsize=23.5)
#plt.yticks(np.arange(0,51,10))
plt.plot(lista1, marker='x', linestyle=':', color='b', label = "F1 Score")
plt.plot(lista2, marker='*', linestyle='-', color='g', label = "Accuracy")
plt.plot(lista3, marker='o', linestyle='--', color='r', label = "Precision")
plt.plot(lista4, marker='+', linestyle='-.', color='y', label = "Recall")
plt.legend(loc="lower left", fontsize=14.1)
plt.show()
