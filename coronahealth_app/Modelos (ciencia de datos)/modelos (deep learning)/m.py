import numpy as np
import matplotlib.pyplot as plt

#cm = matrix1 = np.matrix([[25899, 3265,], [5245, 25291,]])   fig 33
#cm = matrix1 = np.matrix([[25976, 3188,], [5374, 25162,]]) fig 32
#cm = matrix1 = np.matrix([[26212, 2952,], [5655, 24881,]]) fig 30
cm = matrix1 = np.matrix([[26370, 2794,], [5846, 24690,]]) #fig 28
#cm = matrix1 = np.matrix([[25365, 3799,], [5007, 25529,]]) #fig 26
#cm = matrix1 = np.matrix([[25687, 3477,], [5127, 25409,]]) fig 24
#cm = matrix1 = np.matrix([[25505, 3659,], [5203, 25333,]]) #fig 22
#cm = matrix1 = np.matrix([[25527, 3637,], [5146, 25390,]]) #fig 20
#cm = matrix1 = np.matrix([[25111, 36053,], [5322, 25214,]]) #fig 18
#cm = matrix1 = np.matrix([[26333, 2831,], [5577, 24959,]]) #fig 16
#cm = matrix1 = np.matrix([[25916, 3248,], [5202, 25334,]]) #fig 14


print(cm)
fig, ax = plt.subplots(figsize=(3,3))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)

for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center",fontsize=21.5)
plt.xlabel("Predicted label",fontsize=21.5)
plt.ylabel("True label",fontsize=21.5)
plt.yticks(fontsize=21.5)
plt.xticks(fontsize=21.5)
plt.show()



print(cm)
fig, ax = plt.subplots(figsize=(2,2))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
cm = matrix1 = np.matrix([["Verdadero positivo\n(TP)", "Falso Negativo\n(FN)",],
["Falso positivo\n(FP)", "Verdadero negativo\n(TN)",]])
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center",fontsize=21.5)
plt.xlabel("Etiqueta Predicha",fontsize=21.5)
plt.ylabel("Etiqueta Real",fontsize=21.5)
plt.yticks(fontsize=21.5)
plt.xticks(fontsize=21.5)
plt.show()
