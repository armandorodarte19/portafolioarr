from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from sklearn.preprocessing import LabelEncoder
import pandas as pnd
from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.preprocessing import StandardScaler
from tensorflow.keras.optimizers import Adam
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from sklearn.metrics import precision_score, recall_score, f1_score
import time
import random

pnd.set_option('display.max_columns',None)

datosAlumnos = pnd.read_csv("dataset.csv",index_col=0)
#datosAlumnos.to_csv("datos.csv")

print("cargado x...")
#print(X.info())
Y = datosAlumnos.loc[:,['Death']]
X = datosAlumnos.drop(['Death'], axis='columns')
X = X.drop(['CLASIFICACION_FINAL'], axis='columns')

#X = X.drop(['TABAQUISMO:'], axis='columns')
#################################################################
###### VARIVLES A BORRAR PARA MODELO DE 8 CARACTERISTICAS
"""
X = X.drop(['TABAQUISMO'], axis='columns')
X = X.drop(['UCI'], axis='columns')
X = X.drop(['OTRA_COM'], axis='columns')
X = X.drop(['CARDIOVASCULAR'], axis='columns')
X = X.drop(['INMUSUPR'], axis='columns')

X = X.drop(['ASMA'], axis='columns')
X = X.drop(['EPOC'], axis='columns')
X = X.drop(['OBESIDAD'], axis='columns')
"""
#############################################################
print(X.info())

sc = StandardScaler()
X = sc.fit_transform(X)

#X_APRENDIZAJE = sc.transform(X_APRENDIZAJE)
#Instanciamos objeto PCA y aplicamos
"""
#Seleccionar el numero de componentes
pca=PCA(n_components= 0.95) # Otra opción es instanciar pca sólo con dimensiones nuevas hasta obtener un mínimo "explicado" ej.: pca=PCA(.85)
pca.fit(X) # obtener los componentes principales
X=pca.transform(X) # convertimos nuestros datos con las nuevas dimensiones de PCA
print("Nuevo dataset")
print(X)
"""
lb = LabelEncoder()
#print(Y.values)
Y= lb.fit_transform(Y['Death'].values)


X_APRENDIZAJE, X_VALIDACION, Y_APRENDIZAJE, Y_VALIDACION =  train_test_split(X, Y, test_size = 0.25, random_state = 0)
#BOSQUES ALEATORIOS
y = lb.inverse_transform(Y_VALIDACION)

print("")
print("Para Y_APRENDIZAJE, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_APRENDIZAJE, return_counts=True)) # contar valores para array numpy
print("Para Y_VALIDACION, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_VALIDACION, return_counts=True)) # contar valores para array numpy


##################################################################################
####################################################################################
####################################################################################
init=time.time()
model = Sequential()
model.add(Dense(9, input_dim=16, kernel_initializer = "uniform",activation='tanh'))
model.add(Dense(6,kernel_initializer = "uniform", activation='relu'))
model.add(Dense(5,kernel_initializer = "uniform", activation='relu'))
model.add(Dense(5,kernel_initializer = "uniform", activation='relu'))
model.add(Dense(1,kernel_initializer = "uniform", activation='sigmoid'))
model.compile(loss='binary_crossentropy',
               optimizer=Adam(learning_rate=0.00095), metrics =["accuracy"])

model.fit(X_APRENDIZAJE, Y_APRENDIZAJE, epochs=120)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de entrenmiento')

ra = model.predict(X_APRENDIZAJE)
init=time.time()
r = model.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')

# guardar modelo

model.save('modelo_cnn_8_variables.h5')

r=(r>0.5)
ra=(ra>0.5)
ps = precision_score(y_true= Y_VALIDACION, y_pred = r)
ps2 = precision_score(y_true= Y_APRENDIZAJE, y_pred = ra)

recall = recall_score(y_true= Y_VALIDACION, y_pred = r)
recall2 = recall_score(y_true= Y_APRENDIZAJE, y_pred = ra)

f1 = f1_score(y_true= Y_VALIDACION, y_pred = r)
f12 = f1_score(y_true= Y_APRENDIZAJE, y_pred = ra)
print(r)
# evaluamos el modelo
scores = model.evaluate(X_APRENDIZAJE, Y_APRENDIZAJE)

print("Resultados de entrenmiento:")
print(scores)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)
scores2 = model.evaluate(X_VALIDACION, Y_VALIDACION)
print("Resultados de prueba:")
print(scores2)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)


cm = confusion_matrix(Y_VALIDACION, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")
plt.show()


import numpy as np

#Definimos una lista con paises como string
datos = ['Accuracy', 'Precision', 'Recall', 'F1 Score', "Loss"]
#Definimos una lista con ventas como entero
resultados = [scores2[1], ps, recall, f1,scores2[0]]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For DNN 1')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_prueba_dnn_1.png')
#Finalment

resultados = [scores[1], ps2, recall2, f12,scores[0]]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For DNN 1')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_entrenmiento_dnn1.png')
#Finalment
