import pandas as pnd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import validation_curve
from sklearn.model_selection import learning_curve
from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
import time
import random

pnd.set_option('display.max_columns',None)
#pnd.options.mode.chained_assignment = None  # default='warn'

datosAlumnos = pnd.read_csv("datosN6.csv",encoding = "ISO-8859-1")
#datosAlumnos.to_csv("datos.csv")

print("cargado...")
print(datosAlumnos.info())

datosAlumnos = datosAlumnos.drop(datosAlumnos.columns[[0,1,2,3,4,6,7,8,9,10,11,16,18,19,31, 36,37,38]], axis='columns')


datosAlumnos.FECHA_DEF[datosAlumnos.FECHA_DEF != "9999-99-99"] = 0
datosAlumnos.FECHA_DEF[datosAlumnos.FECHA_DEF == "9999-99-99"] = 1
datosAlumnos.FECHA_DEF= datosAlumnos.FECHA_DEF.astype(int)
datosAlumnos.to_csv("dataset.csv") # convertimos el excel en csv
#datosAlumnos = pnd.read_csv("dataset.csv")

datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['CLASIFICACION_FINAL'] > 3].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['RESULTADO_LAB']>1].index)

datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['INTUBADO']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['NEUMONIA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['DIABETES']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['EPOC']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['ASMA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['INMUSUPR']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['HIPERTENSION']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OTRA_COM']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['CARDIOVASCULAR']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OBESIDAD']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['RENAL_CRONICA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['TABAQUISMO']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OTRO_CASO']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['TOMA_MUESTRA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['UCI']>97].index)

datosAlumnos.NEUMONIA[datosAlumnos.INTUBADO >2] = 3
datosAlumnos.NEUMONIA[datosAlumnos.NEUMONIA >2] = 3
datosAlumnos.DIABETES[datosAlumnos.DIABETES > 2] = 3
datosAlumnos.EPOC[datosAlumnos.EPOC > 2] = 3
datosAlumnos.ASMA[datosAlumnos.ASMA > 2] = 3
datosAlumnos.INMUSUPR[datosAlumnos.INMUSUPR > 2] = 3

datosAlumnos.HIPERTENSION[datosAlumnos.HIPERTENSION > 2] = 3
datosAlumnos.OTRA_COM[datosAlumnos.OTRA_COM > 2] = 3
datosAlumnos.CARDIOVASCULAR[datosAlumnos.CARDIOVASCULAR > 2] = 3
datosAlumnos.OBESIDAD[datosAlumnos.OBESIDAD > 2] = 3
datosAlumnos.RENAL_CRONICA[datosAlumnos.RENAL_CRONICA > 2] = 3

datosAlumnos.TABAQUISMO[datosAlumnos.TABAQUISMO > 2] = 3
datosAlumnos.OTRO_CASO[datosAlumnos.OTRO_CASO > 2] = 3
datosAlumnos.OTRO_CASO[datosAlumnos.EMBARAZO > 2] = 3
#datosAlumnos.TOMA_MUESTRA[datosAlumnos.TOMA_MUESTRA > 2] = 3
datosAlumnos.UCI[ datosAlumnos.UCI > 2 ] = 3
#datosAlumnos.UCI[datosAlumnos.UCI == 1 ] = 2
#datosAlumnos.UCI[datosAlumnos.UCI > 2 ] = 0
#datosAlumnos.TOMA_MUESTRA[datosAlumnos.TOMA_MUESTRA >2] = 0
##
p = datosAlumnos.loc[datosAlumnos['FECHA_DEF'] == 0]
print("p columnas")
print(str(len(p.index)))

p2 = datosAlumnos.loc[datosAlumnos['FECHA_DEF'] == 1]
p2 = p2.iloc[0:round(len(p.index)*1.05)]
print("p2 columnas ")
print(str(len(p2.index)))

p3 = pnd.concat([p, p2])
print("p3 columnas ")
print(str(len(p3.index)))
datosAlumnos = p3

datosAlumnos =datosAlumnos.sample(frac=1).reset_index(drop=True) # ordenar aleatorio lso elementos
datosAlumnos.dropna(axis=0, how='any',inplace=True) # eliminar datos Nan

#datosAlumnos = datosAlumnos.drop(['UCI'], axis='columns')
#datosAlumnos2 = datosAlumnos['FECHA_DEF'].astype('int')
print(datosAlumnos.info())
print(datosAlumnos.head(20))

#datosAlumnos = datosAlumnos.iloc[0:200000]
datosAlumnos = datosAlumnos.drop(['RESULTADO_LAB'], axis='columns')
datosAlumnos = datosAlumnos.drop(['OTRO_CASO'], axis='columns')
datosAlumnos = datosAlumnos.drop(['TOMA_MUESTRA_ANTIGENO'], axis='columns')
datosAlumnos = datosAlumnos.drop(['RESULTADO_ANTIGENO'], axis='columns')
datosAlumnos = datosAlumnos.rename(columns={'FECHA_DEF':'Death'})

print("cargado...")
print(datosAlumnos.info())
print("corelacion:")
print(datosAlumnos.corr())
datosAlumnos.to_csv("dataset.csv") # convertimos el excel en csv



datosAlumnos2 = pnd.read_csv("dataset.csv", index_col=0)
print("info")
print(datosAlumnos2)


datosAlumnos2.rename(columns={'SEXO':'SEX'},
                              inplace=True)
datosAlumnos2.rename(columns={'INTUBADO':'INTUBATED'},
                              inplace=True)
datosAlumnos2.rename(columns={'NEUMONIA':'PNEUMONIA'},
                              inplace=True)
datosAlumnos2.rename(columns={'EDAD':'AGE'},
                              inplace=True)
datosAlumnos2.rename(columns={'EMBARAZO':'PREGNANCY'},
                              inplace=True)
datosAlumnos2.rename(columns={'DIABETES':'DIABETES'},
                              inplace=True)
datosAlumnos2.rename(columns={'EPOC':'EPOC'},
                              inplace=True)
datosAlumnos2.rename(columns={'ASMA':'ASTHMA'},
                              inplace=True)
datosAlumnos2.rename(columns={'INMUSUPR':'INMUSUPR'},
                              inplace=True)
datosAlumnos2.rename(columns={'HIPERTENSION':'HYPERTENSION'},
                              inplace=True)
datosAlumnos2.rename(columns={'OTRA_COM':'ANOTHER_COM'},
                              inplace=True)
datosAlumnos2.rename(columns={'CARDIOVASCULAR':'CARDIOVASCULAR'},
                              inplace=True)
datosAlumnos2.rename(columns={'OBESIDAD':'OBESITY'},
                              inplace=True)
datosAlumnos2.rename(columns={'RENAL_CRONICA':'CHRONIC KIDNEY'},
                              inplace=True)
datosAlumnos2.rename(columns={'TABAQUISMO':'SMOKING'},
                              inplace=True)
datosAlumnos2.rename(columns={'CLASIFICACION_FINAL':'CLASSIFICATION'},
                              inplace=True)
datosAlumnos2.rename(columns={'UCI':'UCI'},
                              inplace=True)
