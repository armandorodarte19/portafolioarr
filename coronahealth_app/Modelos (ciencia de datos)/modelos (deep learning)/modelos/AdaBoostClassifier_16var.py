import pandas as pnd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import validation_curve
from sklearn.model_selection import learning_curve
from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
import time
import random

pnd.set_option('display.max_columns',None)
datosAlumnos = pnd.read_csv("dataset.csv", index_col=0)
print("info")
print(datosAlumnos)

#datosAlumnos = datosAlumnos.drop(['SEXO'], axis='columns')
ana = datosAlumnos.describe()
print(ana)
ana.to_csv("analisis_art.csv")
correlation_mat =datosAlumnos.corr()
sns.heatmap(correlation_mat, annot = True, annot_kws={"size": 14 },fmt='.2f')
plt.title("Correlation matrix between Risk factor's",fontsize=22)
plt.xticks(fontsize=16)
#plt.text(fontsize=13)
plt.yticks(fontsize=16)
plt.show()


print("Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(datosAlumnos["Death"].value_counts())

Y = datosAlumnos.loc[:,['Death']]
X = datosAlumnos.drop(['Death'], axis='columns')
#X = X.drop(['TOMA_MUESTRA'], axis='columns')

X = X.drop(['CLASIFICACION_FINAL'], axis='columns')

# quitar para 8 características
"""
X = X.drop(['ASMA'], axis='columns')
X = X.drop(['HIPERTENSION'], axis='columns')
X = X.drop(['OBESIDAD'], axis='columns')
X = X.drop(['UCI'], axis='columns')
X = X.drop(['CARDIOVASCULAR'], axis='columns')
X = X.drop(['TABAQUISMO'], axis='columns')
X = X.drop(['OTRA_COM'], axis='columns')
X = X.drop(['EPOC'], axis='columns')
X = X.drop(['INMUSUPR'], axis='columns')
"""

print("cargado x...")
print(X.info())


lb = LabelEncoder()
#print(Y.values)
Y= lb.fit_transform(Y['Death'].values)


X_APRENDIZAJE, X_VALIDACION, Y_APRENDIZAJE, Y_VALIDACION =  train_test_split(X, Y, test_size = 0.25, random_state = 0)
#BOSQUES ALEATORIOS
y = lb.inverse_transform(Y_VALIDACION)
y2 = lb.inverse_transform(Y_APRENDIZAJE)
print("")
print("Para Y_APRENDIZAJE, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_APRENDIZAJE, return_counts=True)) # contar valores para array numpy
print("Para Y_VALIDACION, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_VALIDACION, return_counts=True)) # contar valores para array numpy

def exactitud(valoresVerdaderos, predic):
    print("\n Exactitud-------------------------------------")
    print("Cantidad de pruebas: "+ str(len(predic)))

    print("\n Etiquetas")
    print(valoresVerdaderos)
    print("valores predichos")
    print(predic)


    contAciertos = 0
    for pos in range(0, len(predic)):

        if valoresVerdaderos[pos] == predic[pos]:
            contAciertos +=1

    print("\n Cantidad de pruebas acertadas:" + str(contAciertos))
    print(" de exactitud: " + str((contAciertos*100)/len(predic)) +"%")
####################################################################################
####################################################################################


from sklearn.ensemble import AdaBoostClassifier

tree =  RandomForestClassifier(criterion="gini", max_depth = 3, n_estimators=100, random_state=1)
#da = AdaBoostClassifier(base_estimator = tree, n_estimators = 600, learning_rate =0.2, random_state = 1)

tree.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
#ada.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("Algoritmo ADa ##########################################################################################")

print("Arbol")
print("Metrica del modelo " +str(tree.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(tree.score(X_VALIDACION, Y_VALIDACION)))
