import pandas as pnd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import validation_curve
from sklearn.model_selection import learning_curve
from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
import time
import random

pnd.set_option('display.max_columns',None)
datosAlumnos = pnd.read_csv("dataset.csv", index_col=0)
print("info")
print(datosAlumnos)

#datosAlumnos = datosAlumnos.drop(['SEXO'], axis='columns')
ana = datosAlumnos.describe()
print(ana)
ana.to_csv("analisis_art.csv")
correlation_mat =datosAlumnos.corr()
sns.heatmap(correlation_mat, annot = True, annot_kws={"size": 14 },fmt='.2f')
plt.title("Correlation matrix between Risk factor's",fontsize=22)
plt.xticks(fontsize=16)
#plt.text(fontsize=13)
plt.yticks(fontsize=16)
plt.show()


print("Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(datosAlumnos["Death"].value_counts())

Y = datosAlumnos.loc[:,['Death']]
X = datosAlumnos.drop(['Death'], axis='columns')
#X = X.drop(['TOMA_MUESTRA'], axis='columns')

X = X.drop(['CLASIFICACION_FINAL'], axis='columns')

# quitar para 8 características
"""
X = X.drop(['ASMA'], axis='columns')
X = X.drop(['HIPERTENSION'], axis='columns')
X = X.drop(['OBESIDAD'], axis='columns')
X = X.drop(['UCI'], axis='columns')
X = X.drop(['CARDIOVASCULAR'], axis='columns')
X = X.drop(['TABAQUISMO'], axis='columns')
X = X.drop(['OTRA_COM'], axis='columns')
X = X.drop(['EPOC'], axis='columns')
X = X.drop(['INMUSUPR'], axis='columns')
"""

print("cargado x...")
print(X.info())


lb = LabelEncoder()
#print(Y.values)
Y= lb.fit_transform(Y['Death'].values)


X_APRENDIZAJE, X_VALIDACION, Y_APRENDIZAJE, Y_VALIDACION =  train_test_split(X, Y, test_size = 0.25, random_state = 0)
#BOSQUES ALEATORIOS
y = lb.inverse_transform(Y_VALIDACION)
y2 = lb.inverse_transform(Y_APRENDIZAJE)
print("")
print("Para Y_APRENDIZAJE, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_APRENDIZAJE, return_counts=True)) # contar valores para array numpy
print("Para Y_VALIDACION, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_VALIDACION, return_counts=True)) # contar valores para array numpy

def exactitud(valoresVerdaderos, predic):
    print("\n Exactitud-------------------------------------")
    print("Cantidad de pruebas: "+ str(len(predic)))

    print("\n Etiquetas")
    print(valoresVerdaderos)
    print("valores predichos")
    print(predic)


    contAciertos = 0
    for pos in range(0, len(predic)):

        if valoresVerdaderos[pos] == predic[pos]:
            contAciertos +=1

    print("\n Cantidad de pruebas acertadas:" + str(contAciertos))
    print(" de exactitud: " + str((contAciertos*100)/len(predic)) +"%")
####################################################################################
####################################################################################


from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
sc = StandardScaler()
print("Regresión LOG")
#X_VALIDACION = X_VALIDACION.values
#X_APRENDIZAJE = X_APRENDIZAJE.values
#Y_APRENDIZAJE = Y_APRENDIZAJE.values
#Y_VALIDACION = Y_VALIDACION.values

X_VALIDACION = sc.fit_transform(X_VALIDACION)
X_APRENDIZAJE = sc.transform(X_APRENDIZAJE)

penalizacion = [{"C": [0.001, 0.002, 0.01, 0.02, 0.03,0.1,0.2, 0.3 ,0.4, 1.0, 2.0,3.0,4.0,5.0,10.0, 20.0,30.0,40.0,50.0,60.0,
70.0,80.0,90.0,100.0]}]

print("Inicio reg logistica")
init=time.time()
#Pruebas con 5 muestras de Validación Cruzada
busqueda_optimizaciones = GridSearchCV(LogisticRegression(random_state = 42), penalizacion, cv=10)
busqueda_optimizaciones.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Entrenamiento')

print("El mejor parámetro es:")
print()
pDT = (busqueda_optimizaciones.best_params_).get("C")
print(busqueda_optimizaciones.best_params_)
print()

regresion_logistica = busqueda_optimizaciones.best_estimator_
init=time.time()
r = regresion_logistica.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')


#regresion_logistica = LogisticRegression( C= 100.0,random_state = 42)
regresion_logistica.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("Metrica del modelo " +str(regresion_logistica.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(regresion_logistica.score(X_VALIDACION, Y_VALIDACION)))
scores = cross_val_score(regresion_logistica, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)

print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))
predicciones = regresion_logistica.predict(X_VALIDACION)

print("\n Regresion Logistica predicciones:  ")
#print(X_VALIDACION)
print(predicciones)
p = lb.inverse_transform(predicciones)
print(p)
exactitud(y,p)



r = regresion_logistica.predict(X_VALIDACION)
ra = regresion_logistica.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)

print(r)
exactitud(y,r)
print("Matriz de confusion")

param_range = [0.001, 0.002, 0.01, 0.02, 0.03,0.1,0.2, 0.3 ,0.4, 1.0, 2.0,3.0,4.0,5.0,10.0, 20.0,30.0,40.0,50.0,60.0,
70.0,80.0,90.0,100.0]
#param_range =  [3,7,9]
train_scores, test_scores = validation_curve(
    LogisticRegression(), X=X_APRENDIZAJE, y=Y_APRENDIZAJE, param_name="C", param_range=param_range,
     cv=10)
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)
indice = np.arange(3)
print("graficar")

lw = 2
plt.semilogx(param_range, train_scores_mean, label="Training accuracy",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Validation accuracy",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Validation Curve with Logistic Regression Classifier",fontsize=25.75)
plt.xlabel("Paramater C",fontsize=22.5)
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.ylabel("Accuracy",fontsize=22.5)
plt.ylim([0.800, 1.030])

plt.legend(loc="best",fontsize=16.25)
plt.show()
print("fin") ###########################################
plt.semilogx(param_range, train_scores_mean, label="Exactitud de entrenamiento",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Exactitud la validación",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Curva de Validación con Regresión Logística",fontsize=25.75)
plt.xlabel("Parámetro C",fontsize=22.5)
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.ylabel("Exactitud",fontsize=22.5)
plt.ylim([0.800, 1.030])

plt.legend(loc="best",fontsize=16.25)
plt.show()
########################### Metricas
score = regresion_logistica.score(X_VALIDACION, Y_VALIDACION)
score2 = regresion_logistica.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)

print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()

#Definimos una lista con paises como string
datos = ['Accuracy', 'Precision', 'Recall', 'F1 Score']
#Definimos una lista con ventas como entero
resultados = [score, ps, recall, f1]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Logistic Regression')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_prueba_LogReg_2.png')


resultados = [score2, ps2, recall2, f12]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Logistic Regression ')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_entrenamiento_LogReg_2.png')
