import pandas as pnd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import validation_curve
from sklearn.model_selection import learning_curve
from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
import time
import random

pnd.set_option('display.max_columns',None)
datosAlumnos = pnd.read_csv("dataset.csv", index_col=0)
print("info")
print(datosAlumnos)

#datosAlumnos = datosAlumnos.drop(['SEXO'], axis='columns')
ana = datosAlumnos.describe()
print(ana)
ana.to_csv("analisis_art.csv")
correlation_mat =datosAlumnos.corr()
sns.heatmap(correlation_mat, annot = True, annot_kws={"size": 14 },fmt='.2f')
plt.title("Correlation matrix between Risk factor's",fontsize=22)
plt.xticks(fontsize=16)
#plt.text(fontsize=13)
plt.yticks(fontsize=16)
plt.show()


print("Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(datosAlumnos["Death"].value_counts())

Y = datosAlumnos.loc[:,['Death']]
X = datosAlumnos.drop(['Death'], axis='columns')
#X = X.drop(['TOMA_MUESTRA'], axis='columns')

X = X.drop(['CLASIFICACION_FINAL'], axis='columns')

# quitar para 8 características
"""
X = X.drop(['ASMA'], axis='columns')
X = X.drop(['HIPERTENSION'], axis='columns')
X = X.drop(['OBESIDAD'], axis='columns')
X = X.drop(['UCI'], axis='columns')
X = X.drop(['CARDIOVASCULAR'], axis='columns')
X = X.drop(['TABAQUISMO'], axis='columns')
X = X.drop(['OTRA_COM'], axis='columns')
X = X.drop(['EPOC'], axis='columns')
X = X.drop(['INMUSUPR'], axis='columns')
"""

print("cargado x...")
print(X.info())


lb = LabelEncoder()
#print(Y.values)
Y= lb.fit_transform(Y['Death'].values)


X_APRENDIZAJE, X_VALIDACION, Y_APRENDIZAJE, Y_VALIDACION =  train_test_split(X, Y, test_size = 0.25, random_state = 0)
#BOSQUES ALEATORIOS
y = lb.inverse_transform(Y_VALIDACION)
y2 = lb.inverse_transform(Y_APRENDIZAJE)
print("")
print("Para Y_APRENDIZAJE, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_APRENDIZAJE, return_counts=True)) # contar valores para array numpy
print("Para Y_VALIDACION, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_VALIDACION, return_counts=True)) # contar valores para array numpy

def exactitud(valoresVerdaderos, predic):
    print("\n Exactitud-------------------------------------")
    print("Cantidad de pruebas: "+ str(len(predic)))

    print("\n Etiquetas")
    print(valoresVerdaderos)
    print("valores predichos")
    print(predic)


    contAciertos = 0
    for pos in range(0, len(predic)):

        if valoresVerdaderos[pos] == predic[pos]:
            contAciertos +=1

    print("\n Cantidad de pruebas acertadas:" + str(contAciertos))
    print(" de exactitud: " + str((contAciertos*100)/len(predic)) +"%")
####################################################################################
####################################################################################

print("Voto mayoritario----------------------------------------------------------")

from sklearn.ensemble import VotingClassifier

sc = StandardScaler()
X_VALIDACION = sc.fit_transform(X_VALIDACION)
X_APRENDIZAJE = sc.transform(X_APRENDIZAJE)
pca = PCA(n_components = 0.95)
sc = StandardScaler()
X = sc.fit_transform(X)
X = pca.fit(X)
X_VALIDACION = pca.transform(X_VALIDACION)
X_APRENDIZAJE = pca.transform(X_APRENDIZAJE)

knnv= KNeighborsClassifier(n_neighbors = 31)
#logregv = LogisticRegression( C= 0.2,random_state = 42)
#treedv = DecisionTreeClassifier(criterion="gini",max_depth  = 8, random_state=1)


penalizacion = [{"max_depth": range(1,12)}]


#Pruebas con 5 muestras de Validación Cruzada
treedv = GridSearchCV(DecisionTreeClassifier(criterion="gini", random_state=1), penalizacion, cv=10)
treedv.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("El mejor parámetro es:")
print()
pDT = (treedv.best_params_).get("max_depth")
print(pDT)
print()

treedv = treedv.best_estimator_

penalizacion = [{"C": [0.001, 0.002, 0.003,0.004, 0.01, 0.02, 0.03,0.1
,0.2, 0.3 ,0.4, 1.0, 2.0,3.0,4.0]}]


#Pruebas con 5 muestras de Validación Cruzada
logregv = GridSearchCV(LogisticRegression(random_state = 42), penalizacion, cv=10)
logregv.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("El mejor parámetro es:")
print()
pDT = (logregv.best_params_).get("C")
print(logregv.best_params_)
print()

logregv = logregv.best_estimator_


print("Hard")
votingM = VotingClassifier(
       estimators=[('lr', knnv),# ('lr2', bosque_aleatorio),
        ('rf', treedv), ('gnb', logregv)],
       )


penalizacion = [{"voting": ["hard","soft"]}]

print("Inicio voto mayoritario")

init=time.time()
#Pruebas con 5 muestras de Validación Cruzada
votingM=  GridSearchCV(votingM, penalizacion, cv= 10)
votingM.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Entrenamiento')


print("El mejor parámetro es:")
print()
pDT = (votingM.best_params_).get("voting")
print(pDT)
print()

votingM = votingM.best_estimator_
#GRADIENT BOOSTING
init=time.time()
r = votingM.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')
votingM.fit(X_APRENDIZAJE, Y_APRENDIZAJE)


print("Metrica del modelo " +str(votingM.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(votingM.score(X_VALIDACION, Y_VALIDACION)))
scores = cross_val_score(votingM, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)
print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))

r = votingM.predict(X_VALIDACION)
ra = votingM.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)
#scores = cross_val_score(eclf1, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)
#print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))
score = votingM.score(X_VALIDACION, Y_VALIDACION)
score2 = votingM.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)
print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()
