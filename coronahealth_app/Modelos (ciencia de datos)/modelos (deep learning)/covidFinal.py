import pandas as pnd
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LinearRegression
from sklearn.cluster import KMeans
from sklearn.cluster import MeanShift
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn.model_selection import cross_val_score
from sklearn.svm import SVC
from sklearn.ensemble import RandomForestClassifier
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import GridSearchCV
from sklearn.decomposition import PCA
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.tree import DecisionTreeClassifier
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import precision_score, recall_score, f1_score
from sklearn.model_selection import validation_curve
from sklearn.model_selection import learning_curve
from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression
import time
import random

pnd.set_option('display.max_columns',None)
#pnd.options.mode.chained_assignment = None  # default='warn'
"""
datosAlumnos = pnd.read_csv("datosN6.csv",encoding = "ISO-8859-1")
#datosAlumnos.to_csv("datos.csv")

print("cargado...")
print(datosAlumnos.info())

datosAlumnos = datosAlumnos.drop(datosAlumnos.columns[[0,1,2,3,4,6,7,8,9,10,11,16,18,19,31, 36,37,38]], axis='columns')


datosAlumnos.FECHA_DEF[datosAlumnos.FECHA_DEF != "9999-99-99"] = 0
datosAlumnos.FECHA_DEF[datosAlumnos.FECHA_DEF == "9999-99-99"] = 1
datosAlumnos.FECHA_DEF= datosAlumnos.FECHA_DEF.astype(int)
datosAlumnos.to_csv("dataset.csv") # convertimos el excel en csv
#datosAlumnos = pnd.read_csv("dataset.csv")

datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['CLASIFICACION_FINAL'] > 3].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['RESULTADO_LAB']>1].index)

#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['INTUBADO']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['NEUMONIA']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['DIABETES']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['EPOC']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['ASMA']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['INMUSUPR']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['HIPERTENSION']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OTRA_COM']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['CARDIOVASCULAR']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OBESIDAD']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['RENAL_CRONICA']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['TABAQUISMO']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OTRO_CASO']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['TOMA_MUESTRA']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['UCI']>97].index)

datosAlumnos.NEUMONIA[datosAlumnos.INTUBADO >2] = 3
datosAlumnos.NEUMONIA[datosAlumnos.NEUMONIA >2] = 3
datosAlumnos.DIABETES[datosAlumnos.DIABETES > 2] = 3
datosAlumnos.EPOC[datosAlumnos.EPOC > 2] = 3
datosAlumnos.ASMA[datosAlumnos.ASMA > 2] = 3
datosAlumnos.INMUSUPR[datosAlumnos.INMUSUPR > 2] = 3

datosAlumnos.HIPERTENSION[datosAlumnos.HIPERTENSION > 2] = 3
datosAlumnos.OTRA_COM[datosAlumnos.OTRA_COM > 2] = 3
datosAlumnos.CARDIOVASCULAR[datosAlumnos.CARDIOVASCULAR > 2] = 3
datosAlumnos.OBESIDAD[datosAlumnos.OBESIDAD > 2] = 3
datosAlumnos.RENAL_CRONICA[datosAlumnos.RENAL_CRONICA > 2] = 3

datosAlumnos.TABAQUISMO[datosAlumnos.TABAQUISMO > 2] = 3
datosAlumnos.OTRO_CASO[datosAlumnos.OTRO_CASO > 2] = 3
datosAlumnos.OTRO_CASO[datosAlumnos.EMBARAZO > 2] = 3
#datosAlumnos.TOMA_MUESTRA[datosAlumnos.TOMA_MUESTRA > 2] = 3
datosAlumnos.UCI[ datosAlumnos.UCI > 2 ] = 3
#datosAlumnos.UCI[datosAlumnos.UCI == 1 ] = 2
#datosAlumnos.UCI[datosAlumnos.UCI > 2 ] = 0
#datosAlumnos.TOMA_MUESTRA[datosAlumnos.TOMA_MUESTRA >2] = 0
##
p = datosAlumnos.loc[datosAlumnos['FECHA_DEF'] == 0]
print("p columnas")
print(str(len(p.index)))

p2 = datosAlumnos.loc[datosAlumnos['FECHA_DEF'] == 1]
p2 = p2.iloc[0:round(len(p.index)*1.05)]
print("p2 columnas ")
print(str(len(p2.index)))

p3 = pnd.concat([p, p2])
print("p3 columnas ")
print(str(len(p3.index)))
datosAlumnos = p3

datosAlumnos =datosAlumnos.sample(frac=1).reset_index(drop=True) # ordenar aleatorio lso elementos
datosAlumnos.dropna(axis=0, how='any',inplace=True) # eliminar datos Nan

#datosAlumnos = datosAlumnos.drop(['UCI'], axis='columns')
#datosAlumnos2 = datosAlumnos['FECHA_DEF'].astype('int')
print(datosAlumnos.info())
print(datosAlumnos.head(20))

#datosAlumnos = datosAlumnos.iloc[0:200000]
datosAlumnos = datosAlumnos.drop(['RESULTADO_LAB'], axis='columns')
datosAlumnos = datosAlumnos.drop(['OTRO_CASO'], axis='columns')
datosAlumnos = datosAlumnos.drop(['TOMA_MUESTRA_ANTIGENO'], axis='columns')
datosAlumnos = datosAlumnos.drop(['RESULTADO_ANTIGENO'], axis='columns')
datosAlumnos = datosAlumnos.rename(columns={'FECHA_DEF':'Death'})

print("cargado...")
print(datosAlumnos.info())
print("corelacion:")
print(datosAlumnos.corr())
datosAlumnos.to_csv("dataset.csv") # convertimos el excel en csv
"""


datosAlumnos = pnd.read_csv("dataset.csv", index_col=0)
print("info")
print(datosAlumnos)
"""
p = 100 * datosAlumnos['NEUMONIA'].value_counts() / len(datosAlumnos['NEUMONIA'])
print("Porcentaje sexo")
print(p)

q =pnd.crosstab(index=datosAlumnos['Death'], columns=datosAlumnos['NEUMONIA']
           ).apply(lambda r: r/r.sum() *100,
                                axis=1)
print(q)
plot = pnd.crosstab(index=datosAlumnos['Death'],
            columns=datosAlumnos['SEXO']).apply(lambda r: r/r.sum() *100,
                                              axis=1).plot(kind='bar')
plt.show()
"""

#datosAlumnos.rename(columns={'CLASIFICACION_FINAL':'CLASIFICACION'},
#               inplace=True)
#datosAlumnos.rename(columns={'Death':'Muerte'},
#                              inplace=True)

"""
datosAlumnos.rename(columns={'SEXO':'SEX'},
                              inplace=True)
datosAlumnos.rename(columns={'INTUBADO':'INTUBATED'},
                              inplace=True)
datosAlumnos.rename(columns={'NEUMONIA':'PNEUMONIA'},
                              inplace=True)
datosAlumnos.rename(columns={'EDAD':'AGE'},
                              inplace=True)
datosAlumnos.rename(columns={'EMBARAZO':'PREGNANCY'},
                              inplace=True)
datosAlumnos.rename(columns={'DIABETES':'DIABETES'},
                              inplace=True)
datosAlumnos.rename(columns={'EPOC':'EPOC'},
                              inplace=True)
datosAlumnos.rename(columns={'ASMA':'ASTHMA'},
                              inplace=True)
datosAlumnos.rename(columns={'INMUSUPR':'INMUSUPR'},
                              inplace=True)
datosAlumnos.rename(columns={'HIPERTENSION':'HYPERTENSION'},
                              inplace=True)
datosAlumnos.rename(columns={'OTRA_COM':'ANOTHER_COM'},
                              inplace=True)
datosAlumnos.rename(columns={'CARDIOVASCULAR':'CARDIOVASCULAR'},
                              inplace=True)
datosAlumnos.rename(columns={'OBESIDAD':'OBESITY'},
                              inplace=True)
datosAlumnos.rename(columns={'RENAL_CRONICA':'CHRONIC KIDNEY'},
                              inplace=True)
datosAlumnos.rename(columns={'TABAQUISMO':'SMOKING'},
                              inplace=True)
datosAlumnos.rename(columns={'CLASIFICACION_FINAL':'CLASSIFICATION'},
                              inplace=True)
datosAlumnos.rename(columns={'UCI':'UCI'},
                              inplace=True)

"""

#datosAlumnos = datosAlumnos.drop(['SEXO'], axis='columns')
ana = datosAlumnos.describe()
print(ana)
ana.to_csv("analisis_art.csv")
correlation_mat =datosAlumnos.corr()
sns.heatmap(correlation_mat, annot = True, annot_kws={"size": 14 },fmt='.2f')
plt.title("Correlation matrix between Risk factor's",fontsize=22)
plt.xticks(fontsize=16)
#plt.text(fontsize=13)
plt.yticks(fontsize=16)
plt.show()


print("Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(datosAlumnos["Death"].value_counts())

Y = datosAlumnos.loc[:,['Death']]
X = datosAlumnos.drop(['Death'], axis='columns')
#X = X.drop(['TOMA_MUESTRA'], axis='columns')

X = X.drop(['CLASIFICACION_FINAL'], axis='columns')
############### características menos
###################################################



#X = X.drop(['UCI'], axis='columns')-
#X = X.drop(['OTRA_COM'], axis='columns')

#X = X.drop(['INMUSUPR'], axis='columns')

#X = X.drop(['ASMA'], axis='columns')
#X = X.drop(['EPOC'], axis='columns')
#X = X.drop(['OBESIDAD'], axis='columns')

#X = X.drop(['SEXO'], axis='columns')
#X = X.drop(['INTUBADO'], axis='columns')

#X = X.drop(['RENAL_CRONICA'], axis='columns')
#X = X.drop(['TABAQUISMO:'], axis='columns')
#X = X.drop(['CARDIOVASCULAR:'], axis='columns')


# quitar para 8 características

X = X.drop(['ASMA'], axis='columns')
X = X.drop(['HIPERTENSION'], axis='columns')
X = X.drop(['OBESIDAD'], axis='columns')
X = X.drop(['UCI'], axis='columns')
X = X.drop(['CARDIOVASCULAR'], axis='columns')
X = X.drop(['TABAQUISMO'], axis='columns')
X = X.drop(['OTRA_COM'], axis='columns')
X = X.drop(['EPOC'], axis='columns')
X = X.drop(['INMUSUPR'], axis='columns')


print("cargado x...")
print(X.info())

"""
sc = StandardScaler()
X = sc.fit_transform(X)
"""
#X_APRENDIZAJE = sc.transform(X_APRENDIZAJE)
#Instanciamos objeto PCA y aplicamos
"""
#Seleccionar el numero de componentes
pca=PCA(n_components= 0.95) # Otra opción es instanciar pca sólo con dimensiones nuevas hasta obtener un mínimo "explicado" ej.: pca=PCA(.85)
pca.fit(X) # obtener los componentes principales
X=pca.transform(X) # convertimos nuestros datos con las nuevas dimensiones de PCA
print("Nuevo dataset")
print(X)
"""
lb = LabelEncoder()
#print(Y.values)
Y= lb.fit_transform(Y['Death'].values)


X_APRENDIZAJE, X_VALIDACION, Y_APRENDIZAJE, Y_VALIDACION =  train_test_split(X, Y, test_size = 0.25, random_state = 0)
#BOSQUES ALEATORIOS
y = lb.inverse_transform(Y_VALIDACION)
y2 = lb.inverse_transform(Y_APRENDIZAJE)
print("")
print("Para Y_APRENDIZAJE, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_APRENDIZAJE, return_counts=True)) # contar valores para array numpy
print("Para Y_VALIDACION, Conteo de datos para fallecidos: 0 para muertos y 1 sobrevivimentes")
print(np.unique(Y_VALIDACION, return_counts=True)) # contar valores para array numpy

def exactitud(valoresVerdaderos, predic):
    print("\n Exactitud-------------------------------------")
    print("Cantidad de pruebas: "+ str(len(predic)))

    print("\n Etiquetas")
    print(valoresVerdaderos)
    print("valores predichos")
    print(predic)


    contAciertos = 0
    for pos in range(0, len(predic)):

        if valoresVerdaderos[pos] == predic[pos]:
            contAciertos +=1

    print("\n Cantidad de pruebas acertadas:" + str(contAciertos))
    print(" de exactitud: " + str((contAciertos*100)/len(predic)) +"%")
####################################################################################
####################################################################################
"""
from sklearn.ensemble import AdaBoostClassifier

tree =  RandomForestClassifier(criterion="gini", max_depth = 3, n_estimators=100, random_state=1)
#da = AdaBoostClassifier(base_estimator = tree, n_estimators = 600, learning_rate =0.2, random_state = 1)

tree.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
#ada.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("Algoritmo ADa ##########################################################################################")

print("Arbol")
print("Metrica del modelo " +str(tree.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(tree.score(X_VALIDACION, Y_VALIDACION)))
"""
#print("ADA")
#print("Metrica del modelo " +str(ada.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
#print("Metrica del modelo prueba " +str(ada.score(X_VALIDACION, Y_VALIDACION)))


######################################################################################
####################################################################################
###############################################################################
################################# BOSQUES ALEATORIOS """""""""""""
"""
penalizacion = [{"max_depth": range(1,9), "n_estimators": range(3,20)}]


#Pruebas con 5 muestras de Validación Cruzada
print("Inicio bosques aleatorios")
init=time.time()
busqueda_optimizaciones = GridSearchCV(RandomForestClassifier( random_state=1), penalizacion, cv=10)
busqueda_optimizaciones.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de entrenmiento')


print("El mejor parámetro es:")
print()
pDT = (busqueda_optimizaciones.best_params_).get("max_depth")
print(busqueda_optimizaciones.best_params_)
print()

bosque_aleatorio = busqueda_optimizaciones.best_estimator_

init=time.time()
r = bosque_aleatorio.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')

print("------Importancia de las características-----------:")
labels =X.columns[0:]
importancias = bosque_aleatorio.feature_importances_

c = 1
#indices = np.argsort
for f in range(len(labels)):
    print(str(c) +") "+labels[f] +":  " + str(importancias[f]*100))
    c = c +1

#bosque_aleatorio= RandomForestClassifier(n_estimators = 5,max_depth  = 5)
print("entranado...")
bosque_aleatorio.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
print("parametros")
print(bosque_aleatorio.get_params(deep=True))
print("Cantidad de arboles")
print(len(bosque_aleatorio))

print("Metrica datos de entrenmiento " +str(bosque_aleatorio.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica datos de prueba " +str(bosque_aleatorio.score(X_VALIDACION, Y_VALIDACION)))


scores = cross_val_score(bosque_aleatorio, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)

print("valudacion cruzada: " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))

predicciones = bosque_aleatorio.predict(X_VALIDACION)
print("\n Bosques aleatorios predicciones:  ")
p = lb.inverse_transform(predicciones)
print(p)
exactitud(y,p)

#visualizar datos
#from matplotlib import pyplot as plt
estimator = bosque_aleatorio.estimators_[0]

r = bosque_aleatorio.predict(X_VALIDACION)
ra = bosque_aleatorio.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)

score = bosque_aleatorio.score(X_VALIDACION, Y_VALIDACION)
score2 = bosque_aleatorio.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)

print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()

#Definimos una lista con paises como string
datos = ['Accuracy', 'Precision', 'Recall', 'F1 Score']
#Definimos una lista con ventas como entero
resultados = [score, ps, recall, f1]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Random Forest Classifier')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_prueba_bosques_2.png')


resultados = [score2, ps2, recall2, f12]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Random Forest Classifier ')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_entrenamiento_bosques_2.png')
"""





##############################################################
########################################
################################################################################
################################################################################

"""

from sklearn.model_selection import cross_val_score


print("########################################################################")


penalizacion = [{"max_depth": range(1,16)}]

print("Inicio árbol de desicón")
#Pruebas con 5 muestras de Validación Cruzada
init=time.time()
busqueda_optimizaciones = GridSearchCV(DecisionTreeClassifier(criterion="gini", random_state=1), penalizacion, cv=10)
busqueda_optimizaciones.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Entrenamiento')
print("El mejor parámetro es:")
print()
pDT = (busqueda_optimizaciones.best_params_).get("max_depth")
print(pDT)
print()

algoritmo_dt = busqueda_optimizaciones.best_estimator_

init=time.time()
r = algoritmo_dt.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')


scores = cross_val_score(algoritmo_dt, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)

print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))



r = algoritmo_dt.predict(X_VALIDACION)
ra = algoritmo_dt.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)

print(r)
exactitud(y,r)
print("Matriz de confusion")

param_range = [1,2,3,4,5,6,7,8,9,10,11,12,13,13,14,15,16]
#param_range =  [3,7,9]
train_scores, test_scores = validation_curve(
    DecisionTreeClassifier(), X=X_APRENDIZAJE, y=Y_APRENDIZAJE, param_name="max_depth", param_range=param_range,
     cv=10)
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)
indice = np.arange(3)
print("graficar")

lw = 2
plt.semilogx(param_range, train_scores_mean, label="Training accuracy",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Validation accuracy",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Validation Curve with Decision Tree Classifier V2",fontsize=25.75)
plt.xlabel("Paramater max_depth",fontsize=22.5)

plt.ylabel("Accuracy",fontsize=22.5)
plt.ylim([0.800, 1.030])
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.legend(loc="best",fontsize=16.25)
plt.show()
print("fin")#######################################################################
plt.semilogx(param_range, train_scores_mean, label="Exactitud de entrenamiento",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Exactitud de la validación",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Curva de Validación con Clasificador de Árbol de Decisión V2",fontsize=25.75)
plt.xlabel("Parámetro max_depth",fontsize=22.5)

plt.ylabel("Exactitud",fontsize=22.5)
plt.ylim([0.800, 1.030])
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.legend(loc="best",fontsize=16.25)
plt.show()

########################### Metricas
score = algoritmo_dt.score(X_VALIDACION, Y_VALIDACION)
score2 = algoritmo_dt.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)

print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()

#Definimos una lista con paises como string
datos = ['Accuracy', 'Precision', 'Recall', 'F1 Score']
#Definimos una lista con ventas como entero
resultados = [score, ps, recall, f1]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Decision Tree Classifier')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_prueba_treedes_2_2.png')


resultados = [score2, ps2, recall2, f12]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Decision Tree Classifier ')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_entrenamiento_tredes_2_2.png')


import os

os.environ['PATH'] = os.environ['PATH']+';'+os.environ['CONDA_PREFIX']+r"\envs\practicas\Library\bin\graphviz"
# Export as dot file
dot_data = export_graphviz(algoritmo_dt,
                filled = True,
                #, out_file='tree.dot',

                feature_names =["SEXO",
                "INTUBADO","NEUMONIA","EDAD","EMBARAZO","DIABETES",

                "HIPERTENSION",
                "RENAL_CRONICA"],

                class_names = ["Muerto","Vivo"],
                rounded = True,
                out_file=None)
print("s")

g = graph_from_dot_data(dot_data)
print("s")


g.write_png("pruebatree2.jpg")
"""





#algoritmo_dt = DecisionTreeClassifier(criterion="gini",max_depth  = pDT, random_state=1)
print(">> ----------- ARBOLES DE DECISIONES  --------------------------")
"""
from sklearn.decomposition import PCA
pca = PCA(n_components = 10)
X_APRENDIZAJE = pca.fit_transform(X_APRENDIZAJE)
X_VALIDACION = pca.transform(X_VALIDACION)
"""
"""
algoritmo_dt.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
print("Metrica del modelo " +str(algoritmo_dt.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(algoritmo_dt.score(X_VALIDACION, Y_VALIDACION)))
scores = cross_val_score(algoritmo_dt, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)

print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))
predicciones = algoritmo_dt.predict(X_VALIDACION)
print("total predicciones " +str(len(predicciones)))
print(predicciones)
precision = accuracy_score(Y_VALIDACION, predicciones)
#A = np.array([1,2,2,73,1,2,2,2,2,1,2,2,2,1,1,1,1,1,1])
#B = np.reshape(A, (-1, 19))
#print('caso muestra')
#print(algoritmo.predict(B))


print(">> Precision = "+str(precision))
print(predicciones)
p = lb.inverse_transform(predicciones)
print(p)
exactitud(y,p)
print("Matriz de confusion")
print(confusion_matrix(Y_VALIDACION, predicciones))
print("cantidad elementos por variable")
print(np.unique(Y_VALIDACION, return_counts=True))



from pydotplus import graph_from_dot_data
from sklearn.tree import export_graphviz
#import pydotplus

import os

os.environ['PATH'] = os.environ['PATH']+';'+os.environ['CONDA_PREFIX']+r"\envs\practicas\Library\bin\graphviz"
# Export as dot file
dot_data = export_graphviz(algoritmo_dt,
                filled = True,
                #, out_file='tree.dot',

                #feature_names =[ #"Sexo",
                #"NEUMONIA","Edad","Diabetes","EPOC","ASMA","INMUSUPR",
                #"HIPERTENSION","OTRA_COM",
                #"CARDIOVASCULAR","OBESIDAD","RENAL_CRONICA","TABAQUISMO",
                #"OTRO_CASO","UCI"],

                class_names = ["Muerto","Vivo"],
                rounded = True,
                out_file=None)
print("s")

g = graph_from_dot_data(dot_data)
print("s")

"""
#from IPython.display import Image
#Image(g.create_png())

"""
import matplotlib.pyplot as plt
plt.imshow(g.create_png())
plt.show()"""

#g.write_png("tree2.jpg")
#-----



####################################################################################
###################################################################################
("########################################################################")
"""
from sklearn.ensemble import GradientBoostingClassifier

penalizacion = [{"n_estimators": range(1,200,10)}]


#Pruebas con 5 muestras de Validación Cruzada
gradientBoosting = GridSearchCV(GradientBoostingClassifier(), penalizacion, cv=10)
gradientBoosting.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("El mejor parámetro es:")
print()
pDT = (gradientBoosting.best_params_).get("n_estimators")
print(pDT)
print()

gradientBoosting = busqueda_optimizaciones.best_estimator_
#GRADIENT BOOSTING
gradientBoosting = GradientBoostingClassifier()
gradientBoosting.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
#predicciones = gradientBoosting.predict(X_VALIDACION)

print("\n GradientBoostingClassifier predicciones:  ")
print("Metrica del modelo " +str(gradientBoosting.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(gradientBoosting.score(X_VALIDACION, Y_VALIDACION)))
scores = cross_val_score(gradientBoosting, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)


print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))
predicciones = gradientBoosting.predict(X_VALIDACION)

"""
print("########################################################################")



##############################################################################
#############################################################################
#################################### StandardScaler ###########################
#############################################################################
"""
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
sc = StandardScaler()
print("Regresión LOG")
#X_VALIDACION = X_VALIDACION.values
#X_APRENDIZAJE = X_APRENDIZAJE.values
#Y_APRENDIZAJE = Y_APRENDIZAJE.values
#Y_VALIDACION = Y_VALIDACION.values

X_VALIDACION = sc.fit_transform(X_VALIDACION)
X_APRENDIZAJE = sc.transform(X_APRENDIZAJE)

penalizacion = [{"C": [0.001, 0.002, 0.01, 0.02, 0.03,0.1,0.2, 0.3 ,0.4, 1.0, 2.0,3.0,4.0,5.0,10.0, 20.0,30.0,40.0,50.0,60.0,
70.0,80.0,90.0,100.0]}]

print("Inicio reg logistica")
init=time.time()
#Pruebas con 5 muestras de Validación Cruzada
busqueda_optimizaciones = GridSearchCV(LogisticRegression(random_state = 42), penalizacion, cv=10)
busqueda_optimizaciones.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Entrenamiento')

print("El mejor parámetro es:")
print()
pDT = (busqueda_optimizaciones.best_params_).get("C")
print(busqueda_optimizaciones.best_params_)
print()

regresion_logistica = busqueda_optimizaciones.best_estimator_
init=time.time()
r = regresion_logistica.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')


#regresion_logistica = LogisticRegression( C= 100.0,random_state = 42)
regresion_logistica.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("Metrica del modelo " +str(regresion_logistica.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(regresion_logistica.score(X_VALIDACION, Y_VALIDACION)))
scores = cross_val_score(regresion_logistica, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)

print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))
predicciones = regresion_logistica.predict(X_VALIDACION)

print("\n Regresion Logistica predicciones:  ")
#print(X_VALIDACION)
print(predicciones)
p = lb.inverse_transform(predicciones)
print(p)
exactitud(y,p)



r = regresion_logistica.predict(X_VALIDACION)
ra = regresion_logistica.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)

print(r)
exactitud(y,r)
print("Matriz de confusion")

param_range = [0.001, 0.002, 0.01, 0.02, 0.03,0.1,0.2, 0.3 ,0.4, 1.0, 2.0,3.0,4.0,5.0,10.0, 20.0,30.0,40.0,50.0,60.0,
70.0,80.0,90.0,100.0]
#param_range =  [3,7,9]
train_scores, test_scores = validation_curve(
    LogisticRegression(), X=X_APRENDIZAJE, y=Y_APRENDIZAJE, param_name="C", param_range=param_range,
     cv=10)
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)
indice = np.arange(3)
print("graficar")

lw = 2
plt.semilogx(param_range, train_scores_mean, label="Training accuracy",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Validation accuracy",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Validation Curve with Logistic Regression Classifier",fontsize=25.75)
plt.xlabel("Paramater C",fontsize=22.5)
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.ylabel("Accuracy",fontsize=22.5)
plt.ylim([0.800, 1.030])

plt.legend(loc="best",fontsize=16.25)
plt.show()
print("fin") ###########################################
plt.semilogx(param_range, train_scores_mean, label="Exactitud de entrenamiento",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Exactitud la validación",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Curva de Validación con Regresión Logística",fontsize=25.75)
plt.xlabel("Parámetro C",fontsize=22.5)
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.ylabel("Exactitud",fontsize=22.5)
plt.ylim([0.800, 1.030])

plt.legend(loc="best",fontsize=16.25)
plt.show()
########################### Metricas
score = regresion_logistica.score(X_VALIDACION, Y_VALIDACION)
score2 = regresion_logistica.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)

print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()

#Definimos una lista con paises como string
datos = ['Accuracy', 'Precision', 'Recall', 'F1 Score']
#Definimos una lista con ventas como entero
resultados = [score, ps, recall, f1]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Logistic Regression')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_prueba_LogReg_2.png')


resultados = [score2, ps2, recall2, f12]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For Logistic Regression ')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_entrenamiento_LogReg_2.png')
"""






###################################################################################
###################################################################################
##################################################################################
##################################################################################
print("")
print(" ########################################################################")
"""
print("Voto mayoritario----------------------------------------------------------")

from sklearn.ensemble import VotingClassifier

sc = StandardScaler()
X_VALIDACION = sc.fit_transform(X_VALIDACION)
X_APRENDIZAJE = sc.transform(X_APRENDIZAJE)
pca = PCA(n_components = 0.95)
sc = StandardScaler()
X = sc.fit_transform(X)
X = pca.fit(X)
X_VALIDACION = pca.transform(X_VALIDACION)
X_APRENDIZAJE = pca.transform(X_APRENDIZAJE)

knnv= KNeighborsClassifier(n_neighbors = 31)
#logregv = LogisticRegression( C= 0.2,random_state = 42)
#treedv = DecisionTreeClassifier(criterion="gini",max_depth  = 8, random_state=1)


penalizacion = [{"max_depth": range(1,12)}]


#Pruebas con 5 muestras de Validación Cruzada
treedv = GridSearchCV(DecisionTreeClassifier(criterion="gini", random_state=1), penalizacion, cv=10)
treedv.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("El mejor parámetro es:")
print()
pDT = (treedv.best_params_).get("max_depth")
print(pDT)
print()

treedv = treedv.best_estimator_

penalizacion = [{"C": [0.001, 0.002, 0.003,0.004, 0.01, 0.02, 0.03,0.1
,0.2, 0.3 ,0.4, 1.0, 2.0,3.0,4.0]}]


#Pruebas con 5 muestras de Validación Cruzada
logregv = GridSearchCV(LogisticRegression(random_state = 42), penalizacion, cv=10)
logregv.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

print("El mejor parámetro es:")
print()
pDT = (logregv.best_params_).get("C")
print(logregv.best_params_)
print()

logregv = logregv.best_estimator_


print("Hard")
votingM = VotingClassifier(
       estimators=[('lr', knnv),# ('lr2', bosque_aleatorio),
        ('rf', treedv), ('gnb', logregv)],
       )


penalizacion = [{"voting": ["hard","soft"]}]

print("Inicio voto mayoritario")

init=time.time()
#Pruebas con 5 muestras de Validación Cruzada
votingM=  GridSearchCV(votingM, penalizacion, cv= 10)
votingM.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Entrenamiento')


print("El mejor parámetro es:")
print()
pDT = (votingM.best_params_).get("voting")
print(pDT)
print()

votingM = votingM.best_estimator_
#GRADIENT BOOSTING
init=time.time()
r = votingM.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')
votingM.fit(X_APRENDIZAJE, Y_APRENDIZAJE)


print("Metrica del modelo " +str(votingM.score(X_APRENDIZAJE, Y_APRENDIZAJE)))
print("Metrica del modelo prueba " +str(votingM.score(X_VALIDACION, Y_VALIDACION)))
scores = cross_val_score(votingM, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)
print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))

r = votingM.predict(X_VALIDACION)
ra = votingM.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)
#scores = cross_val_score(eclf1, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)
#print("valudacion cruzada> " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))
score = votingM.score(X_VALIDACION, Y_VALIDACION)
score2 = votingM.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)
print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()
"""
##############################################################################
###############################################################################
from sklearn.ensemble import RandomForestRegressor
"""
print("########################################################################")
print(">> ----------- BOSQUES ALEATORIOS Regresion -----------------------------")
algoritmo = RandomForestRegressor()

algoritmo.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

predicciones = algoritmo.predict(X_VALIDACION)
print("total predicciones " +str(len(predicciones)))
print(predicciones)

precision = r2_score(Y_VALIDACION, predicciones)


print(">> Precision = "+str(precision*100))
print("------------------------------------------")
"""
"""
#############################################################################
###############################################################################
#-------------------------------------------------------------------------------------
print("########################################################################")
print(">> ----------- Regresion Polynomial -------------------------------------")
from sklearn.preprocessing import PolynomialFeatures
poli_reg = PolynomialFeatures(degree = 3)

#X_APRENDIZAJE, X_VALIDACION, Y_APRENDIZAJE, Y_VALIDACION

X_APRENDIZAJE = poli_reg.fit_transform(X_APRENDIZAJE)
X_VALIDACION = poli_reg.fit_transform(X_VALIDACION)
#Y_APRENDIZAJE = poli_reg.fit_transform(Y_APRENDIZAJE)
#Y_VALIDACION = poli_reg.fit_transform(Y_VALIDACION)

algoritmo = LinearRegression()
algoritmo.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

predicciones = algoritmo.predict(X_VALIDACION)


print("predict")
print("total predicciones " +str(len(predicciones)))
print(predicciones)
precision = r2_score(Y_VALIDACION, predicciones)
print(">> Precision%: = "+str(precision*100))

################################################################################
##############################################################################
print("--------------------------------------- Resultados Regresion lineal----------------------------")
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression

algoritmo = LinearRegression()
algoritmo.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

predicciones = algoritmo.predict(X_VALIDACION)
print("predict")
print("total predicciones " +str(len(predicciones)))
print(predicciones)
precision = r2_score(Y_VALIDACION, predicciones)
print(">> Precision%: = "+str(precision*100))
print("------------------------------------------")

###############################################################################
##############################################################################

from sklearn.tree import DecisionTreeRegressor
algoritmo = DecisionTreeRegressor()

algoritmo.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

predicciones = algoritmo.predict(X_VALIDACION)
print("total predicciones " +str(len(predicciones)))
print(predicciones)
precision = r2_score(Y_VALIDACION, predicciones)

print("########################################################################")
print(">> ----------- ARBOLES DE DECISIONES Regresion --------------------------")
print(">> Precision = "+str(precision*100))

############################################################################
###########################################################################
"""

pca = PCA(n_components = 0.95)
sc = StandardScaler()
X = sc.fit_transform(X)
X = pca.fit(X)
X_VALIDACION = pca.transform(X_VALIDACION)
X_APRENDIZAJE = pca.transform(X_APRENDIZAJE)
X_APRENDIZAJE =sc.fit_transform(X_APRENDIZAJE)
X_VALIDACION =sc.fit_transform(X_VALIDACION)
#X_APRENDIZAJE = pca.transform(X_APRENDIZAJE)
#print("Componenres PCA " + str(X_APRENDIZAJE.info()))
print("Escalado terminado")
"""
#MÁQUINA DE VECTORES DE SOPORTE
SVM = SVC( gamma= 0.2, kernel ="rbf", random_state = 1, C=1)
SVM.fit(X_APRENDIZAJE, Y_APRENDIZAJE)
print("Escalado terminado")
predicciones = SVM.predict(X_VALIDACION)
print("\n SVM predicciones:  ")
p = lb.inverse_transform(predicciones)
print(p)
exactitud(y,p)

"""


##############################################################################
#############################################################################
#############################################################################
##################          KNN   ###########################################

print("########################################################################")
#K VECINOS MÁS CERCANOS
print("KNN")
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier()
#penalizacion = [{"n_neighbors": [3]}]

print("KNN iniciado")
init=time.time()
penalizacion = [{"n_neighbors": [3,5,7,9,11,13,15,17,19,21,23,25,27,29,31]}]
knn = GridSearchCV(knn, penalizacion, cv=10)
knn.fit(X_APRENDIZAJE, Y_APRENDIZAJE)


end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Entrenamiento')

print("El mejor parámetro es:")
print()
pDT = (knn.best_params_).get("n_neighbors")
print(knn.best_params_)
print()

knn = knn.best_estimator_

init=time.time()
r = knn.predict(X_VALIDACION)
end=time.time()
print ('Done in '+str(end-init)+' secs. Fase de Pruebas')

scores = cross_val_score(knn, X_APRENDIZAJE, Y_APRENDIZAJE, cv=10)

print("valudacion cruzada: " + str(np.mean(scores)) + " STD: "  + str(np.std(scores)))

#knn = MultiOutputClassifier(knn, n_jobs=2)
knn.fit(X_APRENDIZAJE, Y_APRENDIZAJE)

r = knn.predict(X_VALIDACION)
ra = knn.predict(X_APRENDIZAJE)
print("\n KNN predicciones:  ")
r = lb.inverse_transform(r)
ra = lb.inverse_transform(ra)

print(r)
exactitud(y,r)
print("Matriz de confusion")

#################################################### Curva de VALIDACION
param_range =  [3,5,7,9,11,13,15,17,19,21,23,25,27,29,31]
#param_range =  [3,7,9]
train_scores, test_scores = validation_curve(
    KNeighborsClassifier(), X=X_APRENDIZAJE, y=Y_APRENDIZAJE, param_name="n_neighbors", param_range=param_range,
     cv=10)
train_scores_mean = np.mean(train_scores, axis=1)
train_scores_std = np.std(train_scores, axis=1)
test_scores_mean = np.mean(test_scores, axis=1)
test_scores_std = np.std(test_scores, axis=1)
indice = np.arange(3)
print("graficar")

lw = 2
plt.semilogx(param_range, train_scores_mean, label="Training accuracy",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Validation accuracy",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Validation Curve with KNN V2",fontsize=25.75)
plt.xlabel("Paramater K (NEIGHBORS)",fontsize=22.5)
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.ylabel("Accuracy",fontsize=22.5)
plt.ylim([0.8, 1.03])

plt.legend(loc="best",fontsize=16.25)
plt.show()
print("fin") ###########################################

plt.semilogx(param_range, train_scores_mean, label="Exactitud de entrenamiento",
             color="blue", marker ="o", markersize=5)
plt.fill_between(param_range, train_scores_mean + train_scores_std,
                 train_scores_mean - train_scores_std, alpha=0.15,
                 color="blue")
plt.semilogx(param_range, test_scores_mean, label="Exactitud de validación",
             color="green", marker ="s", markersize=5,linestyle="--")
plt.fill_between(param_range, test_scores_mean + test_scores_std,
                 test_scores_mean - test_scores_std, alpha=0.15,
                 color="green" )
plt.grid()
plt.xscale("log")
plt.title("Curva de Validación con KNN V2",fontsize=25.75)
plt.xlabel("Parámetro K(VECINOS)",fontsize=22.5)
plt.yticks(fontsize=22.5)
plt.xticks(fontsize=22.5)
plt.ylabel("Accuracy",fontsize=22.5)
plt.ylim([0.8, 1.03])

plt.legend(loc="best",fontsize=16.25)
plt.show()





########################### Metricas
score = knn.score(X_VALIDACION, Y_VALIDACION)
score2 = knn.score(X_APRENDIZAJE, Y_APRENDIZAJE)
ps = precision_score(y_true= y, y_pred = r)
ps2 = precision_score(y_true= y2, y_pred = ra)

recall = recall_score(y_true= y, y_pred = r)
recall2 = recall_score(y_true= y2, y_pred = ra)

f1 = f1_score(y_true= y, y_pred = r)
f12 = f1_score(y_true= y2, y_pred = ra)

print("Resultados de entrenmiento:")
print(score2)
print("precision")
print(ps2)
print("recall")
print(recall2)
print("f1")
print(f12)

print("Resultados de prueba:")
print(score)
print("precision")
print(ps)
print("recall")
print(recall)
print("f1")
print(f1)

cm = confusion_matrix(y, r)
print(cm)
fig, ax = plt.subplots(figsize=(2.5,2.5))
ax.matshow(cm, cmap=plt.cm.Blues, alpha=0.3)
for i in range(cm.shape[0]):
    for j in range(cm.shape[1]):
        ax.text(x=j, y=i, s=cm[i,j], va="center", ha="center")
plt.xlabel("Predicted label")
plt.ylabel("True label")

plt.show()

#Definimos una lista con paises como string
datos = ['Accuracy', 'Precision', 'Recall', 'F1 Score']
#Definimos una lista con ventas como entero
resultados = [score, ps, recall, f1]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For KNN 1')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_prueba_knn_2.png')


resultados = [score2, ps2, recall2, f12]

fig, ax = plt.subplots()
#Colocamos una etiqueta en el eje Y
ax.set_ylabel('Precision')
#Colocamos una etiqueta en el eje X
ax.set_title('Evaluation Metrics For KNN 1')
#Creamos la grafica de barras utilizando 'paises' como eje X y 'ventas' como eje y.
plt.bar(datos, resultados)
plt.savefig('resultados_entrenamiento_knn_2.png')
