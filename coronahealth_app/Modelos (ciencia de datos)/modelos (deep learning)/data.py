import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
import pandas as pnd


datosAlumnos = pnd.read_csv("dataset.csv", index_col=0)


datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['INTUBADO']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['NEUMONIA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['DIABETES']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['EPOC']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['ASMA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['INMUSUPR']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['HIPERTENSION']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OTRA_COM']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['CARDIOVASCULAR']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OBESIDAD']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['RENAL_CRONICA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['TABAQUISMO']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['OTRA_COM']>2].index)
#datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['TOMA_MUESTRA']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['UCI']>2].index)
datosAlumnos = datosAlumnos.drop(datosAlumnos[datosAlumnos['EMBARAZO']>2].index)

datosAlumnos.to_csv("dataset2.csv")
