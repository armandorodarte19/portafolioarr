/*
  Recordar instalar el paquete de:
    shared_preferences:
  Inicializar en el main
    final prefs = new PreferenciasUsuario();
    prefs.initPrefs();
*/

import 'package:apiri/models/medico_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferenciasUsuario {
  static final PreferenciasUsuario _instancia =
      new PreferenciasUsuario._internal();

  factory PreferenciasUsuario() {
    return _instancia;
  }

  PreferenciasUsuario._internal();

  SharedPreferences _prefs;

  initPrefs() async {
    this._prefs = await SharedPreferences.getInstance();
  }

  get token {
    return _prefs.getString('token') ?? '';
  }

  set nombreAdmin(String value) {
    String nombre = value.split("@")[0];
    _prefs.setString('nombreAdmin', nombre);
  }

  get nombreAdmin {
    return _prefs.getString('nombreAdmin') ?? '';
  }

  set token(String value) {
    _prefs.setString('token', value);
  }

  logOut() {
    _prefs.remove('role');
    _prefs.remove('medico');
    _prefs.remove('token');
    _prefs.remove('nombreAdmin');
    _prefs.remove('ultimaPagina');
  }

  get role {
    return _prefs.getString('role') ?? '';
  }

  set role(String value) {
    _prefs.setString('role', value);
  }

  get medico {
    return _prefs.getStringList('medico') ?? '';
  }

  get nombremedico {
    List med = _prefs.getStringList('medico') ?? '';

    return med[1];
  }

  set medico(Medico medico) {
    List<String> datosMedico = new List();
    datosMedico.add(medico.id.toString());
    datosMedico.add(medico.nombreMedico + ' ' + medico.apellidoPaterno);
    datosMedico.add(medico.correo);
    datosMedico.add(medico.centroClinicoId.toString());
    _prefs.setStringList('medico', datosMedico);
  }

  set email(String email) {
    _prefs.setString('email', email);
  }

  clearUser() {
    _prefs.remove('email');
    _prefs.remove('pass');
  }

  get email {
    try {
      return _prefs.getString('email') ?? '';
    } catch (Exception) {
      return '';
    }
  }

  set pass(String pass) {
    _prefs.setString('pass', pass);
  }

  get pass {
    try {
      return _prefs.getString('pass') ?? '';
    } catch (Exception) {
      return '';
    }
  }

  get centroClinico {
    List med = _prefs.getStringList('medico') ?? '';

    return med[3];
  }

  // GET y SET de la última página
  get ultimaPagina {
    return _prefs.getString('ultimaPagina') ?? 'login';
  }

  set ultimaPagina(String value) {
    _prefs.setString('ultimaPagina', value);
  }
}
