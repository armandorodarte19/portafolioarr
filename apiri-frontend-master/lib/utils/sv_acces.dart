import 'package:apiri/providers/medicos_provider.dart';

//final String urlSv = '192.168.1.70\:56912';
final String urlSv = 'apiri.azurewebsites.net';

MedicoProvider _medicoProvider = new MedicoProvider();

obtenerUsuarioLogin(String role, String email) async {
  if (role == 'medico') {
    return await _medicoProvider.getMedicoByEmail(email);
  }
}
