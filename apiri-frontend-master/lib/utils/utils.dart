import 'dart:io';
import 'dart:math';

import 'package:apiri/models/etapas_visuales_model.dart';
import 'package:apiri/models/factores_sanacion_model.dart';
import 'package:apiri/providers/etapas_visuales_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

DateFormat dateFormat = DateFormat("yyyy-MM-dd");
List<EtapasVisuales> etapasVisuales = new List();
EtapasVisualesProvider _etapasVisualesProvider = new EtapasVisualesProvider();
String infoEtapas;
bool isNumeric(String s) {
  if (s.isEmpty) return false;
  final n = num.tryParse(s);
  return (n == null) ? false : true;
}

bool isDataClosed(String fecha) {
  //print(fecha);
  if (fecha == '01-01-0001' || fecha == '0001-01-01') {
    return true;
  }
  return false;
}

String formatDate(DateTime fecha) {
  return dateFormat.format(fecha);
}

String formatDateMonth(DateTime fecha) {
  return DateFormat.yMMMd('es_ES').format(fecha);
}

TextStyle styleTitle = TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0);
void cargarEtapasVisuales() async {
  etapasVisuales = await _etapasVisualesProvider.getEtapasVisuales();
}

Future<List<EtapasVisuales>> getEtapasVisuales() async {
  if (etapasVisuales != null) return etapasVisuales;
  etapasVisuales = await _etapasVisualesProvider.getEtapasVisuales();

  return etapasVisuales;
}

String doMayus(String work) {
  return work.toUpperCase();
}

String passRandom() {
  const _chars =
      'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
  Random _rnd = Random.secure();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
      length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));
  String pass = getRandomString(10);
  pass = pass + 'A';
  return pass;
}

Map<String, bool> getMapFactores() {
  return {
    "Fumar": false,
    "Beber Alcohol": false,
    "Enfermedades Crónico Degenerativas": false,
    "Enfermedades Géneticas": false,
    "Edad Mayor a 60": false,
    "Buena Alimentación": false,
    "Desórdenes Alimenticios": false,
    "Realiza Ejercicio": false,
    "Drogas o medicamentos con impacto negativo": false,
    "Medicamento con impacto positivo": false,
    "Endocrinopatias": false,
    "Estres": false,
    "Insuficiencia Vascular": false,
    "Cicatriz": false,
    "Herida con puntos": false,
    "Cicatriz con deformidad": false,
  };
}

Map<String, bool> mapFactores(FactoresSanacion fs) {
  return {
    "Fumar": fs.fumar,
    "Beber Alcohol": fs.bebeAlcohol,
    "Enfermedades Crónico Degenerativas": fs.enfermedadesCronicoDegenerativas,
    "Enfermedades Géneticas": fs.enfermedadesGeneticas,
    "Edad Mayor a 60": fs.mayor60,
    "Buena Alimentación": fs.buenaAlimentacion,
    "Desórdenes Alimenticios": fs.desordenesAlimenticios,
    "Realiza Ejercicio": fs.realizaEjercicio,
    "Drogas o medicamentos con impacto negativo": fs.medicamentosNegativos,
    "Medicamento con impacto positivo": fs.medicamentosPositivos,
    "Endocrinopatias": fs.endocrinopatias,
    "Estres": fs.estres,
    "Insuficiencia Vascular": fs.insuficienciaVascular,
    "Cicatriz": fs.cicatriz,
    "Herida con puntos": fs.heridaConPuntos,
    "Cicatriz con deformidad": fs.cicatrizConDeformidad
  };
}

FactoresSanacion convertFactoresMap(Map<String, bool> factoresSanacionMap) {
  FactoresSanacion factor = new FactoresSanacion();
  factor.fumar = factoresSanacionMap['Fumar'];
  factor.bebeAlcohol = factoresSanacionMap['Beber Alcohol'];
  factor.enfermedadesCronicoDegenerativas =
      factoresSanacionMap['Enfermedades Crónico Degenerativas'];
  factor.enfermedadesGeneticas = factoresSanacionMap['Enfermedades Géneticas'];
  factor.mayor60 = factoresSanacionMap['Edad Mayor a 60'];
  factor.buenaAlimentacion = factoresSanacionMap['Buena Alimentación'];
  factor.desordenesAlimenticios =
      factoresSanacionMap['Desórdenes Alimenticios'];
  factor.realizaEjercicio = factoresSanacionMap['Realiza Ejercicio'];
  factor.medicamentosNegativos =
      factoresSanacionMap['Drogas o medicamentos con impacto negativo'];
  factor.medicamentosPositivos =
      factoresSanacionMap['Medicamento con impacto positivo'];
  factor.endocrinopatias = factoresSanacionMap['Endocrinopatias'];
  factor.estres = factoresSanacionMap['Estres'];
  factor.insuficienciaVascular = factoresSanacionMap['Insuficiencia Vascular'];
  factor.cicatriz = factoresSanacionMap['Cicatriz'];
  factor.heridaConPuntos = factoresSanacionMap['Herida con puntos'];
  factor.cicatrizConDeformidad = factoresSanacionMap['Cicatriz con deformidad'];

  return factor;
}

String infoInicialEtapas =
    "Etapas de sanación o de cicatrización de las heridas cutáneas \nEl proceso de cicatrización transcurre por 4 grandes fases generales, cada una de éstas se componen por una serie de subprocesos. Estas fases son\n1.	Fase de coagulación (hemostasia).\n2.	Fase inflamatoria. \n3.	Fase proliferativa (reparación de tejido).\n4.	 Fase de remodelación (maduración).\n";
