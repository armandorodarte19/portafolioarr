import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:rich_alert/rich_alert.dart';

mostrarAlerta(String path, bool correcto, BuildContext context,
    [dynamic argument]) {
  print('Entro');
  String title = "Guardado";
  var alert = RichAlertType.SUCCESS;
  Color color = Colors.green;
  String mensaje = 'Operación realizada con exito';
  if (!correcto) {
    title = 'Error';
    alert = RichAlertType.ERROR;
    mensaje = 'Ups... Ocurrio un error inesperado';
  }

  showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return RichAlertDialog(
          actions: [
            RaisedButton(
              color: color,
              elevation: 2.0,
              child: Text(
                "Aceptar",
                style: TextStyle(color: Colors.white),
              ),
              onPressed: () {
                if (argument == null) {
                  Navigator.popAndPushNamed(context, path);
                } else {
                  Navigator.popAndPushNamed(context, path, arguments: argument);
                }
              },
            ),
          ],
          //uses the custom alert dialog
          alertTitle: richTitle(title),
          alertSubtitle: richSubtitle(mensaje),
          alertType: alert,
        );
      });
}

mostrarAlertaPop(BuildContext context, String mensaje, String title) {
  showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(title),
          content: Text(mensaje),
          actions: <Widget>[
            FlatButton(
              child: Text('Ok'),
              onPressed: () => Navigator.of(context).pop(),
            )
          ],
        );
      });
}
