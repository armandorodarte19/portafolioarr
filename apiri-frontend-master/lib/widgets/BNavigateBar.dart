import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BNNavigateBar extends StatefulWidget {
  BuildContext _pageactual;
  BNNavigateBar(BuildContext pageactual) {
    _pageactual = pageactual;
  }

  @override
  _BNNavigateBarState createState() => _BNNavigateBarState(_pageactual);
}

class _BNNavigateBarState extends State<BNNavigateBar> {
  var routes = new Map<int, String>();
  PreferenciasUsuario _prefs = new PreferenciasUsuario();
  int _currentIndex;
  BuildContext _pageactual;
  _BNNavigateBarState(BuildContext pageactual) {
    this._pageactual = pageactual;
    _llenarMapa();
    _currentIndex = _obtenerIndex(pageactual.toString());
  }
  @override
  Widget build(BuildContext context) {
    var bottomNavyBar = BottomNavyBar(
      selectedIndex: _currentIndex,
      showElevation: true,
      itemCornerRadius: 8,
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      curve: Curves.easeInBack,
      items: _llenarNVBar(),
      onItemSelected: (index) => setState(() {
        if (_currentIndex == index) return;

        _currentIndex = index;

        String pagina = _pageactual.toString().toLowerCase();
        if (!pagina.contains(routes[index])) {
          Navigator.of(_pageactual).popAndPushNamed(routes[index]);
        }
      }),
    );
    return bottomNavyBar;
  }

  List<BottomNavyBarItem> _llenarNVBar() {
    List<BottomNavyBarItem> listNVB = new List();

    if (_prefs.role == "admin") {
      listNVB.add(BottomNavyBarItem(
        icon: Icon(Icons.apps),
        title: Text('Inicio'),
        activeColor: Colors.green,
      ));
      listNVB.add(BottomNavyBarItem(
        icon: Icon(Icons.person),
        title: Text('Médicos'),
        activeColor: Colors.purpleAccent,
        textAlign: TextAlign.center,
      ));
      return listNVB;
    }
    listNVB.add(BottomNavyBarItem(
      icon: Icon(Icons.apps),
      title: Text('Inicio'),
      activeColor: Colors.green,
    ));
    listNVB.add(
      BottomNavyBarItem(
        icon: Icon(Icons.people),
        title: Text('Pacientes'),
        activeColor: Colors.purpleAccent,
        textAlign: TextAlign.center,
      ),
    );
    listNVB.add(
      BottomNavyBarItem(
        icon: Icon(Icons.assignment),
        title: Text(
          'Casos Medicos',
        ),
        activeColor: Colors.pink,
        textAlign: TextAlign.center,
      ),
    );
    return listNVB;
  }

  void _llenarMapa() {
    if (_prefs.role == "admin") {
      routes = {0: "home", 1: "medicos"};
    } else {
      routes = {
        0: "home",
        1: "pacientes",
        2: "casosmedicos",
      };
    }
  }

  int _obtenerIndex(String pagina) {
    for (int i = 0; i < routes.length; i++) {
      if (pagina.toLowerCase().contains(routes[i])) {
        _currentIndex = i;
      }
    }
    return _currentIndex;
  }
}
