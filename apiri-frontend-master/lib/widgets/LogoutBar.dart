import 'package:apiri/providers/usuario_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LogOutApiri extends StatelessWidget {
  const LogOutApiri({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    UsuarioProvider _usuarioProvider = new UsuarioProvider();
    return Container(
      child: IconButton(
        icon: const Icon(Icons.exit_to_app),
        tooltip: 'Cerrar Sesión',
        onPressed: () async {
          await _usuarioProvider.logout();
          Navigator.pushReplacementNamed(context, 'login');
        },
      ),
    );
  }
}
