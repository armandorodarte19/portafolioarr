import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/centro_clinico_models.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/utils/sv_acces.dart' as sv;
import 'package:http/http.dart' as http;

class CentroClinicosProvider {
  final String _url = sv.urlSv;
  final _centrosClinicoStreamController =
      StreamController<List<CentroClinico>>.broadcast();

  Function(List<CentroClinico>) get centrosClinicoSink =>
      _centrosClinicoStreamController.sink.add;

  Stream<List<CentroClinico>> get centrosClinicoStream =>
      _centrosClinicoStreamController.stream;

  void disposeStrams() {
    _centrosClinicoStreamController?.close();
  }

  Future<List<CentroClinico>> getCentrosClinicos() async {
    List<CentroClinico> _centrosClinicos = new List();
    final url = Uri.https(_url, 'api/centroclinico');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    final List<dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((caso) {
      final centro = CentroClinico.fromJson(caso);

      _centrosClinicos.add(centro);
    });
    return _centrosClinicos;
  }

  Future<CentroClinico> getCentroClinico() async {
    final url = Uri.https(_url, 'api/centroclinico/centros', {});
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    CentroClinico newCentro = CentroClinico.fromJson(json.decode(resp.body));
    print(newCentro.id);
    return newCentro;
  }
}
