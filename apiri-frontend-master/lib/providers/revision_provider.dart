import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'package:apiri/data/factores_sanacion_data.dart';
import 'package:apiri/models/factores_sanacion_model.dart';
import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/models/revision_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/utils/sv_acces.dart' as sv;
import 'package:apiri/utils/utils.dart' as utils;
import 'package:http/http.dart' as http;

class RevisionProvider {
  List<Revision> _casoMedico = new List();
  final String _url = sv.urlSv;
  final _casosMedicosStreamController =
      StreamController<List<Revision>>.broadcast();

  Function(List<Revision>) get casosMedicosSink =>
      _casosMedicosStreamController.sink.add;

  Stream<List<Revision>> get casosMedicosStream =>
      _casosMedicosStreamController.stream;

  void disposeStrams() {
    _casosMedicosStreamController?.close();
  }

  Future<List<Revision>> getRevisiones(int id) async {
    List<Revision> _revisiones = new List();
    final url = Uri.https(_url, 'api/revision/caso/$id');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    final List<dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((element) {
      final rev = Revision.fromJson(element);

      _revisiones.add(rev);
    });

    return _revisiones;
  }

  Future<Revision> getRevision(int id) async {
    final url = Uri.https(_url, 'api/revision/$id');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    final Map<String, dynamic> decodedData = json.decode(resp.body);

    final _revision = Revision.fromJson(decodedData);

    return _revision;
  }

  Future<bool> postRevision(Revision revision) async {
    try {
      final url = Uri.https(
        _url,
        'api/revision/',
      );
      print(revision.fecha);
      final resp = await http
          .post(url, body: revisionModelToJsonCreate(revision), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.statusCode);
      print(resp.headers);
      print(resp.body);
      print(revision.toJsonCreate());
      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return true;
      }

      final url2 = Uri.https(
        _url,
        'api/revision/deleteImagen',
      );
      final resp2 = await http.delete(url2);
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> putRevision(Revision revisionUpdate) async {
    final url = Uri.https(_url, 'api/revision/${revisionUpdate.id}');

    final resp = await http
        .put(url, body: revisionModelToJsonUpdate(revisionUpdate), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    if (resp.statusCode == 200 ||
        resp.statusCode == 201 ||
        resp.statusCode == 204) {
      final urlfr = Uri.https(
          _url, 'api/factoresSanacion/${revisionUpdate.factoresSanacionId}');

      revisionUpdate.factoresSanacion.id = revisionUpdate.factoresSanacionId;
      final resp2 = await http.put(urlfr,
          body: factoresSanacionModelToJsonUpdate(
              revisionUpdate.factoresSanacion),
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': 'Bearer ${PreferenciasUsuario().token}'
          });

      if (resp2.statusCode == 200 ||
          resp2.statusCode == 201 ||
          resp2.statusCode == 204) return true;
      return false;
    }
    return false;
  }

  //Metodo para subir fotografias
  Future<String> subirImagen(
      File imagen, Revision revision, String nameCaso) async {
    final url = Uri.https(
      _url,
      'api/revision/upload',
    );

    String fileName = _newNameImage(revision, nameCaso);

    var request = http.MultipartRequest(
      'POST',
      url,
    );
    request.headers
        .addAll({'Authorization': 'Bearer ${PreferenciasUsuario().token}'});
    request.files.add(http.MultipartFile.fromBytes(
        'files', imagen.readAsBytesSync(),
        filename: fileName));
    var res = await request.send();

    String ruta = await res.stream.bytesToString();
    return ruta;
  }

  Future<bool> deleteRevision(int id) async {
    try {
      final url = Uri.https(
        _url,
        'api/revision/$id',
      );
      final resp = await http.delete(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      if (resp.statusCode == 201 ||
          resp.statusCode == 200 ||
          resp.statusCode == 204) return true;

      return false;
    } catch (e) {
      return false;
    }
  }

  String downloadImage(String rutaImagen) {
    final url = Uri.https(_url, 'api/revision/download/$rutaImagen');

    return url.toString();
  }

  //Metodo para que el sv le coloque el nonbre a la imagen
  String _newNameImage(Revision revision, String namecaso) {
    List<String> name = namecaso.split(" ");
    print(name);
    String nSE = "";
    name.forEach((element) {
      nSE += element;
    });
    String infectada = "";
    (revision.tipoHerida.gradoGravedad.infectada)
        ? infectada = "YI"
        : infectada = "NI";
    return revision.casoMedico.pacienteCaso.id.toString() +
        '-' +
        revision.casoMedico.id.toString() +
        '-' +
        utils.formatDate(revision.fecha) +
        "-" +
        revision.tipoHerida.nombreHerida +
        "-" +
        revision.tipoHerida.gradoGravedad.grado.toString() +
        '-' +
        infectada +
        '-FS0' +
        revision.etapasVisuales.id.toString() +
        '-' +
        Random().nextInt(9999).toString() +
        '.jpg';
  }
}
