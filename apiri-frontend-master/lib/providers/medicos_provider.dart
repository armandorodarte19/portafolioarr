import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/centro_clinico_models.dart';
import 'package:apiri/models/medico_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/providers/centros_clinicos_provider.dart';
import 'package:apiri/providers/usuario_provider.dart';
import 'package:http/http.dart' as http;
import 'package:apiri/utils/sv_acces.dart' as sv;

class MedicoProvider {
  final _medicosStreamController = StreamController<List<Medico>>.broadcast();
  List<Medico> _medicos;
  Function(List<Medico>) get medicosSink => _medicosStreamController.sink.add;

  Stream<List<Medico>> get medicosStream => _medicosStreamController.stream;

  void disposeStreams() {
    _medicosStreamController?.close();
  }

  void clearData() {
    _medicos = [];
  }

  Future<Medico> getMedico(int id) async {
    final url = Uri.https(sv.urlSv, 'api/medico/$id');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    final Map<String, dynamic> decodedData = json.decode(resp.body);
    final paciente = Medico.fromJson(decodedData);

    return paciente;
  }

  Future<List<Medico>> getMedicos() async {
    _medicos = new List();
    final url = Uri.https(sv.urlSv, 'api/medico/');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    if (resp.statusCode != 200 &&
        resp.statusCode != 201 &&
        resp.statusCode != 204) return [];
    final List<dynamic> decodedData = json.decode(resp.body);
    if (decodedData == null) return [];
    decodedData.forEach((prod) {
      final prodTemp = Medico.fromJson(prod);

      _medicos.add(prodTemp);
    });

    return _medicos;
  }

  Future<Medico> getMedicoByEmail(String email) async {
    final url = Uri.https(sv.urlSv, 'api/medico/email/$email');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    final paciente = Medico.fromJson(decodedData);

    return paciente;
  }

  CentroClinico _centroClinico = new CentroClinico();
  CentroClinicosProvider _centroClinicosProvider = new CentroClinicosProvider();
  Future<bool> postMedico(
    Medico medico,
  ) async {
    try {
      _centroClinico = await _centroClinicosProvider.getCentroClinico();

      if (_centroClinico == null) {
        return false;
      }
      medico.centroClinicoId = _centroClinico.id;
      var jsonMedico = medicoModelToJsonCreate(medico);
      print(jsonMedico);
      final url = Uri.https(sv.urlSv, 'api/medico/');

      final resp = await http.post(url, body: jsonMedico, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.body);
      print(resp.statusCode);
      print(resp.headers);
      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return true;
      }
      return false;
    } catch (e) {
      print(e);
      return false;
    }
  }

  deleteMedico(Medico medico) async {
    try {
      final url = Uri.https(
        sv.urlSv,
        'api/medico/${medico.id}',
      );
      bool respUser = await UsuarioProvider().deleteUser(medico.correo);
      if (respUser == false) {
        return false;
      }
      final resp = await http.delete(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.statusCode);
      if (resp.statusCode == 201 || resp.statusCode == 200) return true;

      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> updateMedico(Medico medico) async {
    print(medico.toJson());
    final url = Uri.https(sv.urlSv, 'api/medico/${medico.id}');

    final resp = await http.put(url, body: medicoModelToJson(medico), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    print(resp.body);
    print(resp.statusCode);
    print(resp.headers);
    if (resp.statusCode == 200 ||
        resp.statusCode == 201 ||
        resp.statusCode == 204) {
      return true;
    }

    return false;
  }
}
