import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/etapas_visuales_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/utils/sv_acces.dart' as sv;
import 'package:http/http.dart' as http;

class EtapasVisualesProvider {
  final String _url = sv.urlSv;
  final _etapasVisualesStreamController =
      StreamController<List<EtapasVisuales>>.broadcast();

  Function(List<EtapasVisuales>) get etapasVisualesSink =>
      _etapasVisualesStreamController.sink.add;

  Stream<List<EtapasVisuales>> get etapasVisualesStream =>
      _etapasVisualesStreamController.stream;

  void disposeStrams() {
    _etapasVisualesStreamController?.close();
  }

  Future<List<EtapasVisuales>> getEtapasVisuales() async {
    List<EtapasVisuales> _etapasVisuales = new List();
    final url = Uri.https(_url, 'api/etapasvisuales');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    final List<dynamic> decodedData = json.decode(resp.body);
    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((etapa) {
      final etpa = EtapasVisuales.fromJson(etapa);

      _etapasVisuales.add(etpa);
    });
    return _etapasVisuales;
  }
}
