import 'dart:async';

import 'package:apiri/models/factores_sanacion_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:http/http.dart' as http;
import 'package:apiri/utils/sv_acces.dart' as sv;

final String _url = sv.urlSv;

final _factoresSanacionStreamController =
    StreamController<List<FactoresSanacion>>.broadcast();

Function(List<FactoresSanacion>) get factoresSanacionSink =>
    _factoresSanacionStreamController.sink.add;

Stream<List<FactoresSanacion>> get factoresSanacionStream =>
    _factoresSanacionStreamController.stream;

void disposeStreams() {
  _factoresSanacionStreamController?.close();
}

Future<bool> updateFactores(FactoresSanacion factoresSanacion) async {
  final url = Uri.https(_url, 'api/factoresSanacion/${factoresSanacion.id}');
  final resp = await http.put(url,
      body: factoresSanacionModelToJsonUpdate(factoresSanacion),
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

  if (resp.statusCode == 200 ||
      resp.statusCode == 201 ||
      resp.statusCode == 204) return true;
  return false;
}
