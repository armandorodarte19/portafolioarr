import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:apiri/utils/sv_acces.dart' as sv;

class PacientesProvider {
  List<Paciente> _pacientes = new List();
  bool _cargando = false;
  final String _url = sv.urlSv;
  int _ultimoCount = 0;
  bool limite = false;
  bool hasMore = true;
  final _pacientesStreamController =
      StreamController<List<Paciente>>.broadcast();

  Function(List<Paciente>) get pacientesSink =>
      _pacientesStreamController.sink.add;

  Stream<List<Paciente>> get pacientesStream =>
      _pacientesStreamController.stream;

  void disposeStreams() {
    _pacientesStreamController?.close();
  }

  void clearData() {
    _ultimoCount = 0;
    _pacientes = [];
    hasMore = true;
    limite = false;
  }

  Future<List<Paciente>> getPacientes() async {
    if (_pacientes.length > 0) {
      _ultimoCount = _pacientes.length;
    }
    final url = Uri.https(
        _url, 'api/paciente', {'countActual': _ultimoCount.toString()});

    if (limite == true) {
      return _pacientes;
    }
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}',
    });

    if (resp.statusCode != 200 &&
        resp.statusCode != 201 &&
        resp.statusCode != 204) return [];

    if (resp.body == null || resp.body.isEmpty) {
      hasMore = false;
      limite = true;

      return _pacientes;
    }
    final List<dynamic> decodedData = json.decode(resp.body);

    if (decodedData == null) return [];
    print(decodedData.length);
    decodedData.forEach((prod) {
      final prodTemp = Paciente.fromJson(prod);

      if (prodTemp.apellidoMaterno == null) prodTemp.apellidoMaterno = "";
      if (_pacientes.length > 0) {
        if (!_pacientes.contains(prodTemp)) {
          _pacientes.add(prodTemp);
          print(prodTemp.id);
        }
      } else {
        _pacientes.add(prodTemp);
      }
    });

    return _pacientes;
  }

  Future<Paciente> getPaciente(int id) async {
    final url = Uri.https(_url, 'api/paciente/$id');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    final Map<String, dynamic> decodedData = json.decode(resp.body);

    final paciente = Paciente.fromJson(decodedData);

    return paciente;
  }

  Future<List<Paciente>> getPacientesSearched(String search) async {
    List<Paciente> searchedPac = new List();
    try {
      final url = Uri.https(_url, 'api/paciente/search/$search');
      final resp = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      if (resp.statusCode != 200 &&
          resp.statusCode != 201 &&
          resp.statusCode != 204) return [];

      final List<dynamic> decodedData = json.decode(resp.body);

      if (decodedData == null || decodedData.isEmpty) return [];

      decodedData.forEach((prod) {
        final prodTemp = Paciente.fromJson(prod);
        if (prodTemp.apellidoMaterno == null) prodTemp.apellidoMaterno = "";
        if (searchedPac.length > 0) {
          if (!searchedPac.contains(prodTemp)) searchedPac.add(prodTemp);
          print(prodTemp.toJson());
        } else {
          searchedPac.add(prodTemp);
        }
      });

      return searchedPac;
    } catch (e) {
      return null;
    }
  }

  Future<bool> deletePaciente(int id) async {
    try {
      final url = Uri.https(
        _url,
        'api/paciente/$id',
      );
      final resp = await http.delete(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      if (resp.statusCode == 201 || resp.statusCode == 200) return true;

      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> postPaciente(
    Paciente paciente,
  ) async {
    try {
      var jsonPaciente = pacienteModelToJsonCreate(paciente);
      final url = Uri.https(_url, 'api/paciente/');

      final resp = await http.post(url, body: jsonPaciente, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> updatePaciente(Paciente paciente) async {
    final url = Uri.https(_url, 'api/paciente/${paciente.id}');

    final resp =
        await http.put(url, body: pacienteModelToJson(paciente), headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    if (resp.statusCode == 200 ||
        resp.statusCode == 201 ||
        resp.statusCode == 204) {
      return true;
    }

    return false;
  }
}
