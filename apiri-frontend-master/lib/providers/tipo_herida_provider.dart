import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/tipo_herida_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/utils/sv_acces.dart' as sv;
import 'package:http/http.dart' as http;

class TipoHeridaProvider {
  final String _url = sv.urlSv;
  final _tipoHeridaStreamController =
      StreamController<List<TipoHerida>>.broadcast();

  Function(List<TipoHerida>) get etapasVisualesSink =>
      _tipoHeridaStreamController.sink.add;

  Stream<List<TipoHerida>> get etapasVisualesStream =>
      _tipoHeridaStreamController.stream;

  void disposeStrams() {
    _tipoHeridaStreamController?.close();
  }

  Future<List<TipoHerida>> getTipoHeridas() async {
    List<TipoHerida> _etapasVisuales = new List();
    final url = Uri.https(_url, 'api/heridas');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    final List<dynamic> decodedData = json.decode(resp.body);
    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((etapa) {
      final etpa = TipoHerida.fromJson(etapa);

      _etapasVisuales.add(etpa);
    });
    return _etapasVisuales;
  }

  Future<List<String>> getNombresHeridas() async {
    List<String> nHeridas = new List();
    final url = Uri.https(_url, 'api/tipoherida/heridas');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    final List<dynamic> decodedData = json.decode(resp.body);
    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((etapa) {
      final etpa = etapa;

      nHeridas.add(etpa);
    });

    return nHeridas;
  }

  //Obtiene los nombres de las heridas
  Future<List<String>> getTipoHeridaNames() async {
    List<String> nHeridas = new List();
    final url = Uri.https(_url, 'api/tipoherida/heridas');
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    final List<dynamic> decodedData = json.decode(resp.body);
    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((etapa) {
      final etpa = etapa;

      nHeridas.add(etpa);
    });

    return nHeridas;
  }

  //Obtiene la herida de lcatalogo, mediante el nombre de la herida, su grado de gravedad y si esta se encuentra infectada
  Future<TipoHerida> getTipoHeridaSeleccionada(
      String nombre, int grado, bool infeccion) async {
    final url = Uri.https(_url, 'api/tipoherida/byName', {
      "nombreHerida": nombre,
      "gradoGravedad": grado.toString(),
      "infectada": infeccion.toString()
    });
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });

    final Map<String, dynamic> decodedData = json.decode(resp.body);

    final nHeridas = TipoHerida.fromJson(decodedData);
    nHeridas.gradoGravedadID = nHeridas.gradoGravedad.id;
    return nHeridas;
  }
}
