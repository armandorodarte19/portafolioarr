import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/utils/sv_acces.dart' as sv;
import 'package:http/http.dart' as http;

class CasosMedicosProvider {
  List<CasoMedico> _casoMedico = new List();
  List<CasoMedico> _casosMedicos = new List();
  int _ultimoId = 0;
  int _ultimoCount = 0;
  bool limite = false;
  bool hasMore = true;
  final String _url = sv.urlSv;
  final _casosMedicosStreamController =
      StreamController<List<CasoMedico>>.broadcast();

  Function(List<CasoMedico>) get casosMedicosSink =>
      _casosMedicosStreamController.sink.add;

  Stream<List<CasoMedico>> get casosMedicosStream =>
      _casosMedicosStreamController.stream;

  void disposeStrams() {
    _casosMedicosStreamController?.close();
  }

  void clearData() {
    _ultimoCount = 0;
    _casosMedicos = [];
    hasMore = true;
    limite = false;
  }

  Future<List<CasoMedico>> getCasosMedicos() async {
    if (_casosMedicos.length > 0) {
      _ultimoCount = _casosMedicos.length;
    }
    final url = Uri.https(_url, 'api/casoMedico', {
      'countActual': _ultimoCount.toString(),
      'centroClinicodId': PreferenciasUsuario().centroClinico
    });

    if (limite == true) {
      return _casosMedicos;
    }
    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    if (resp.statusCode != 200 &&
        resp.statusCode != 201 &&
        resp.statusCode != 204) return [];
    if (resp.body == null || resp.body.isEmpty) {
      hasMore = false;
      limite = true;
      return _casosMedicos;
    }
    final List<dynamic> decodedData = json.decode(resp.body);
    if (decodedData == []) return [];
    decodedData.forEach((caso) {
      final casoTemp = CasoMedico.fromJson(caso);
      if (_casosMedicos.length > 0) {
        if (!_casosMedicos.contains(casoTemp)) {
          _casosMedicos.add(casoTemp);
        }
      } else {
        _casosMedicos.add(casoTemp);
      }
    });
    //_casosMedicos.sort();
    return _casosMedicos;
  }

  Future<List<CasoMedico>> getCasoMedicoSearched(String search) async {
    List<CasoMedico> searchedCas = new List();

    try {
      final url = Uri.https(_url, 'api/casomedico/search/$search',
          {'centroClinicodId': PreferenciasUsuario().centroClinico});
      final resp = await http.get(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.body);
      print(resp.statusCode);
      if (resp.statusCode != 200 &&
          resp.statusCode != 201 &&
          resp.statusCode != 204) return [];

      final List<dynamic> decodedData = json.decode(resp.body);
      if (decodedData == null || decodedData.isEmpty) return [];

      decodedData.forEach((prod) {
        final casTemp = CasoMedico.fromJson(prod);
        if (searchedCas.length > 0) {
          if (!searchedCas.contains(casTemp)) searchedCas.add(casTemp);
        } else {
          searchedCas.add(casTemp);
        }
      });
      return searchedCas;
    } catch (e) {
      return null;
    }
  }

  Future<List<CasoMedico>> getCasosMedicosPaciente(int idPaciente) async {
    final url = Uri.https(_url, 'api/casomedico/paciente/$idPaciente',
        {'centroClinicoId': PreferenciasUsuario().centroClinico});

    final resp = await http.get(url, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer ${PreferenciasUsuario().token}'
    });
    if (resp.body == null) return [];

    final List<dynamic> decodedData = json.decode(resp.body);
    final List<CasoMedico> casoMedicoList = new List();
    if (decodedData == null || decodedData == []) return [];
    decodedData.forEach((caso) {
      final casoTemp = CasoMedico.fromJson(caso);
      casoMedicoList.add(casoTemp);
    });
    return casoMedicoList;
  }

  Future<bool> postCasoMedico(CasoMedico caso) async {
    try {
      final url = Uri.https(_url, 'api/casomedico/');

      final resp = await http
          .post(url, body: casoMedicoModelToJsonCreate(caso), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      final decodedData = json.decode(resp.body);

      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> putCasoMedico(CasoMedico caso) async {
    try {
      final url = Uri.https(_url, 'api/casomedico/${caso.id}');

      final resp =
          await http.put(url, body: casoMedicoModelToJson(caso), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      if (resp.statusCode == 200 ||
          resp.statusCode == 201 ||
          resp.statusCode == 204) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> cerrarCasoMedico(CasoMedico caso) async {
    try {
      final url = Uri.https(_url, 'api/casomedico/${caso.id}');

      final resp = await http
          .put(url, body: casoMedicoModelToJsonCloseCase(caso), headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      final decodedData = json.decode(resp.body);

      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> deleteCasoMedico(int id) async {
    try {
      final url = Uri.https(_url, 'api/casomedico/$id');

      final resp = await http.delete(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });

      bool res = json.decode(resp.body);
      if (res) return true;
      return false;
    } catch (e) {
      return false;
    }
  }
}
