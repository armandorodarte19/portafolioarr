import 'dart:convert';

import 'package:apiri/models/user_info.models.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:http/http.dart' as http;
import 'package:apiri/utils/sv_acces.dart' as sv;

class UsuarioProvider {
  final _prefs = new PreferenciasUsuario();

  final String _url = sv.urlSv;
  Future<Map<String, dynamic>> login(String email, String password) async {
    final authData = {
      'email': email.toString(),
      'password': password.toString(),
    };

    final url = Uri.https(sv.urlSv, 'api/cuentas/login');
    print(url);
    final resp = await http.post(
      url,
      body: json.encode(authData),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    );

    if (resp.statusCode == 403 || resp.body == null) {
      return {
        'ok': false,
        'mensaje': "Upps... parece ser que se nos perdio el servidor "
      };
    }

    Map<String, dynamic> decodedResp = json.decode(resp.body);

    if (decodedResp.containsKey('token')) {
      _prefs.token = decodedResp['token'];
      _prefs.role = decodedResp['role'];
      print(_prefs.role);
      if (_prefs.role == 'Medico') {
        _prefs.medico = await sv.obtenerUsuarioLogin('medico', email);
      } else {
        _prefs.nombreAdmin = email;
      }

      return {'ok': true, 'token': decodedResp['token']};
    } else {
      return {'ok': false, 'mensaje': decodedResp.values.first[0].toString()};
    }
  }

  Future<bool> logout() async {
    final url = Uri.https(_url, 'api/cuentas/logout');

    print(url);
    final resp = await http.post(url);
    print(resp.body);
    _prefs.logOut();
    return true;
  }

  Future<bool> signOut(
    String email,
    String pass,
  ) async {
    try {
      var jsonUser =
          userInfoModelToJson(new UserInfo(email: email, password: pass + '+'));
      final url = Uri.https(sv.urlSv, 'api/cuentas/crear');

      final resp = await http.post(url, body: jsonUser, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.statusCode);
      print(resp.headers);
      print(resp.body);
      if (resp.statusCode == 200 || resp.statusCode == 201) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  Future<bool> deleteUser(String email) async {
    try {
      final url = Uri.https(
        _url,
        'api/cuentas/$email',
      );
      final resp = await http.delete(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.statusCode);
      if (resp.statusCode == 201 || resp.statusCode == 200) return true;

      return false;
    } catch (e) {
      return false;
    }
  }

  resendPass(String email) async {
    try {
      final url = Uri.https(
        _url,
        'api/cuentas/ResendPass/$email',
      );
      final resp = await http.post(url, headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer ${PreferenciasUsuario().token}'
      });
      print(resp.statusCode);
      print(resp.headers);
      print(resp.body);
      return true;
    } catch (e) {
      return false;
    }
  }
}
