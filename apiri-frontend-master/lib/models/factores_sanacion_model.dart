import 'dart:convert';

FactoresSanacion factoresSanacioModelFromJson(String str) =>
    FactoresSanacion.fromJson(json.decode(str));

String factoresSanacionModelToJson(FactoresSanacion data) =>
    json.encode(data.toJson());
String factoresSanacionModelToJsonUpdate(FactoresSanacion data) =>
    json.encode(data.toJsonUpdate());

class FactoresSanacion {
  FactoresSanacion(
      {this.id,
      this.fumar,
      this.bebeAlcohol,
      this.iMC,
      this.enfermedadesCronicoDegenerativas,
      this.enfermedadesGeneticas,
      this.mayor60,
      this.buenaAlimentacion,
      this.desordenesAlimenticios,
      this.realizaEjercicio,
      this.medicamentosNegativos,
      this.medicamentosPositivos,
      this.endocrinopatias,
      this.estres,
      this.insuficienciaVascular,
      this.cicatriz,
      this.heridaConPuntos,
      this.cicatrizConDeformidad});
  int id;
  bool fumar;
  bool bebeAlcohol;
  int iMC;
  bool enfermedadesCronicoDegenerativas;
  bool enfermedadesGeneticas;
  bool mayor60;
  bool buenaAlimentacion;
  bool desordenesAlimenticios;
  bool realizaEjercicio;
  bool medicamentosNegativos;
  bool medicamentosPositivos;
  bool endocrinopatias;
  bool estres;
  bool insuficienciaVascular;
  bool cicatriz;
  bool heridaConPuntos;
  bool cicatrizConDeformidad;

  factory FactoresSanacion.fromJson(Map<String, dynamic> json) =>
      new FactoresSanacion(
        id: json['id'],
        fumar: json['fumar'],
        bebeAlcohol: json['bebeAlcohol'],
        iMC: json['imc'],
        enfermedadesCronicoDegenerativas:
            json['enfermedadesCronicoDegenerativas'],
        enfermedadesGeneticas: json['enfermedadesGeneticas'],
        mayor60: json['mayor60'],
        buenaAlimentacion: json['buenaAlimentacion'],
        desordenesAlimenticios: json['desordenesAlimenticios'],
        realizaEjercicio: json['realizaEjercicio'],
        medicamentosNegativos: json['medicamentosNegativos'],
        medicamentosPositivos: json['medicamentosPositivos'],
        endocrinopatias: json['endocrinopatias'],
        estres: json['estres'],
        insuficienciaVascular: json['insuficienciaVascular'],
        cicatriz: json['cicatriz'],
        heridaConPuntos: json['heridaConPuntos'],
        cicatrizConDeformidad: json['cicatrizConDeformidad'],
      );

  Map<String, dynamic> toJson() => {
        "fumar": fumar,
        "bebeAlcohol": bebeAlcohol,
        "iMC": iMC,
        "enfermedadesCronicoDegenerativas": enfermedadesCronicoDegenerativas,
        "enfermedadesGeneticas": enfermedadesGeneticas,
        "mayor60": mayor60,
        "buenaAlimentacion": buenaAlimentacion,
        "desordenesAlimenticios": desordenesAlimenticios,
        "realizaEjercicio": realizaEjercicio,
        "medicamentosNegativos": medicamentosNegativos,
        "medicamentosPositivos": medicamentosPositivos,
        "endocrinopatias": endocrinopatias,
        "estres": estres,
        "insuficienciaVascular": insuficienciaVascular,
        "cicatriz": cicatriz,
        "heridaConPuntos": heridaConPuntos,
        "cicatrizConDeformidad": cicatrizConDeformidad,
      };

  Map<String, dynamic> toJsonUpdate() => {
        "id": id,
        "fumar": fumar,
        "bebeAlcohol": bebeAlcohol,
        "iMC": iMC,
        "enfermedadesCronicoDegenerativas": enfermedadesCronicoDegenerativas,
        "enfermedadesGeneticas": enfermedadesGeneticas,
        "mayor60": mayor60,
        "buenaAlimentacion": buenaAlimentacion,
        "desordenesAlimenticios": desordenesAlimenticios,
        "realizaEjercicio": realizaEjercicio,
        "medicamentosNegativos": medicamentosNegativos,
        "medicamentosPositivos": medicamentosPositivos,
        "endocrinopatias": endocrinopatias,
        "estres": estres,
        "insuficienciaVascular": insuficienciaVascular,
        "cicatriz": cicatriz,
        "heridaConPuntos": heridaConPuntos,
        "cicatrizConDeformidad": cicatrizConDeformidad,
      };
}
