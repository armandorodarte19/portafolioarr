import 'dart:io';
import 'dart:convert';
import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/models/etapas_visuales_model.dart';
import 'package:apiri/models/factores_sanacion_model.dart';
import 'package:apiri/models/medico_model.dart';
import 'package:apiri/models/tipo_herida_model.dart';
import 'package:flutter/cupertino.dart';

Revision revisionModelFromJson(String str) =>
    Revision.fromJson(json.decode(str));

String revisionModelToJsonCreate(Revision data) =>
    json.encode(data.toJsonCreate());
String revisionModelToJsonUpdate(Revision data) => json.encode(data.toJson());

class Revision {
  Revision(
      {this.id,
      this.imagen,
      this.urlImage,
      this.fecha,
      this.factoresSanacion,
      this.factoresSanacionId,
      this.etapasVisuales,
      this.etapasVisualesId,
      this.casoMedico,
      this.casoMedicoid,
      this.tipoHerida,
      this.medico});

  int id;
  File imagen;
  String urlImage;
  DateTime fecha;
  FactoresSanacion factoresSanacion;
  int factoresSanacionId;
  EtapasVisuales etapasVisuales;
  int etapasVisualesId;
  CasoMedico casoMedico;
  int casoMedicoid;
  TipoHerida tipoHerida;
  int tipoHeridaId;
  Medico medico;
  int medicoId;

  factory Revision.fromJson(Map<String, dynamic> json) => new Revision(
        id: json['id'],
        urlImage: json['urlImagen'],
        fecha: DateTime.parse(json['fecha']),
        factoresSanacion: FactoresSanacion.fromJson(json['factoresSanacion']),
        etapasVisuales: EtapasVisuales.fromJson(json['etapasVisuales']),
        etapasVisualesId: json['etapasVisualesId'],
        casoMedicoid: json['casoMedicoId'],
        factoresSanacionId: json['factoresSanacionId'],
        tipoHerida: TipoHerida.fromJson(json['tipoHerida']),
      );

  Map<String, dynamic> toJsonCreate() => {
        "fecha": fecha.toIso8601String(),
        "UrlImagen": urlImage,
        "FactoresSanacion": factoresSanacion.toJson(),
        "etapasVisualesId": etapasVisuales.id,
        "casoMedicoId": casoMedico.id,
        "tipoHeridaId": tipoHerida.id,
        "medicoId": medico.id
      };

  Map<String, dynamic> toJson() => {
        "id": id,
        "fecha": fecha.toIso8601String(),
        "UrlImagen": urlImage,
        "factoresSanacionId": factoresSanacionId,
        "etapasVisualesId": etapasVisuales.id,
        "casoMedicoId": casoMedicoid,
        "tipoHeridaId": tipoHerida.id,
        "medicoId": medico.id
      };
}
