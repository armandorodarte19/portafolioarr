import 'dart:convert';

import 'package:apiri/models/centro_clinico_models.dart';

Medico medicoModelFromJson(String str) => Medico.fromJson(json.decode(str));

String medicoModelToJson(Medico data) => json.encode(data.toJson());
String medicoModelToJsonCreate(Medico data) => json.encode(data.toJsonCreate());

class Medico {
  Medico(
      {this.id,
      this.apellidoPaterno,
      this.apellidoMaterno,
      this.nombreMedico,
      this.genero,
      this.correo,
      this.telefono,
      this.centroClinicoId,
      this.centrosClinicos});
  int id;
  String apellidoPaterno;
  String apellidoMaterno;
  String nombreMedico;
  String genero;
  String correo;
  int telefono;

  int centroClinicoId;
  CentroClinico centrosClinicos;

  factory Medico.fromJson(Map<String, dynamic> json) {
    return new Medico(
        id: json['id'],
        apellidoPaterno: json['apellidoPaterno'],
        apellidoMaterno: json['apellidoMaterno'],
        nombreMedico: json['nombreMedico'],
        genero: json['genero'],
        telefono: json['telefono'],
        correo: json['correo'],
        centroClinicoId: json['centroClinicoId'],
        centrosClinicos: CentroClinico.fromJson(json['centrosClinicos']));
  }

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombreMedico": nombreMedico,
        "apellidoPaterno": apellidoPaterno,
        "apellidoMaterno": apellidoMaterno,
        "genero": genero,
        "telefono": telefono,
        "correo": correo,
        "centroClinicoId": centroClinicoId,
      };
  Map<String, dynamic> toJsonCreate() => {
        "apellidoPaterno": apellidoPaterno,
        "telefono": telefono,
        "correo": correo,
        "apellidoMaterno": apellidoMaterno,
        "nombreMedico": nombreMedico,
        "genero": genero,
        "centroClinicoId": centroClinicoId,
      };
}
