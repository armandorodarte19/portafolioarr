import 'dart:convert';

import 'package:apiri/models/centro_clinico_models.dart';
import 'package:apiri/models/paciente_model.dart';

CasoMedico CasoMedicoModelFromJson(String str) =>
    CasoMedico.fromJson(json.decode(str));

String casoMedicoModelToJson(CasoMedico data) => json.encode(data.toJson());
String casoMedicoModelToJsonCreate(CasoMedico data) =>
    json.encode(data.toJsonCreate());
String casoMedicoModelToJsonCloseCase(CasoMedico data) =>
    json.encode(data.toJsonCloseCase());

class CasoMedico {
  CasoMedico(
      {this.id,
      this.nombreCasoMedico,
      this.fechaInicio,
      this.fechaFin,
      this.tiempoSanacion,
      this.casoAbierto,
      this.pacienteId,
      this.centroClinicoId,
      this.centroClinico,
      this.pacienteCaso});
  int id;
  String nombreCasoMedico;
  DateTime fechaInicio;
  DateTime fechaFin;
  int tiempoSanacion;
  bool casoAbierto;
  int pacienteId;
  int centroClinicoId;
  CentroClinico centroClinico;
  Paciente pacienteCaso;
  //int tiempoSanacion;
  //bool casoAbierto;

  //String correo;
  //int telefono;

  factory CasoMedico.fromJson(Map<String, dynamic> json) => new CasoMedico(
      id: json['id'],
      nombreCasoMedico: json['nombreCasoMedico'],
      fechaInicio: DateTime.parse(json['fechaInicio']),
      fechaFin: DateTime.parse(json['fechaFin']),
      casoAbierto: json['casoAbierto'],
      pacienteId: json['pacienteId'],
      tiempoSanacion: json['tiempoSanacion'],
      pacienteCaso: Paciente.fromJson(json['paciente']),
      centroClinico: CentroClinico.fromJson(json['centroClinico']),
      centroClinicoId: json['centroClinicoId']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombreCasoMedico": nombreCasoMedico,
        "fechaInicio": fechaInicio.toIso8601String(),
        //"fechaFin": fechaFin,
        "pacienteId": pacienteId,
        "centroClinicoId": centroClinicoId
      };
  Map<String, dynamic> toJsonCloseCase() => {
        "id": id,
        "nombreCasoMedico": nombreCasoMedico,
        "fechaInicio": fechaInicio.toIso8601String(),
        "fechaFin": fechaFin.toIso8601String(),
        "tiempoSanacion": fechaInicio.difference(fechaFin).inDays,
        "casoAbierto": false,
        "pacienteId": pacienteId,
        "centroClinicoId": centroClinicoId
      };
  Map<String, dynamic> toJsonCreate() => {
        //"id": id,
        "nombreCasoMedico": nombreCasoMedico,
        "fechaInicio": fechaInicio.toIso8601String(),
        "casoAbierto": true,
        "pacienteId": pacienteId,
        "centroClinicoId": centroClinicoId
      };
}
