import 'dart:convert';

String userInfoModelToJson(UserInfo data) => json.encode(data.toJson());

class UserInfo {
  UserInfo({
    this.email,
    this.password,
  });
  String email;
  String password;

  Map<String, dynamic> toJson() => {
        "email": email,
        "password": password,
      };
}
