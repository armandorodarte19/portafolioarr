import 'dart:convert';

CentroClinico CentroClinicoModelFromJson(String str) =>
    CentroClinico.fromJson(json.decode(str));

class CentroClinico {
  CentroClinico({
    this.id,
    this.nombreCentroClinico,
  });
  int id;
  String nombreCentroClinico;

  factory CentroClinico.fromJson(Map<String, dynamic> json) {
    return new CentroClinico(
      id: json['id'],
      nombreCentroClinico: json['nombreCentroClinico'],
    );
  }
}
