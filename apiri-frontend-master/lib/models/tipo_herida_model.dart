import 'dart:convert';

TipoHerida tipoHeridaModelFromJson(String str) =>
    TipoHerida.fromJson(json.decode(str));

String tipoHeridanModelToJson(TipoHerida data) => json.encode(data.toJson());

class TipoHerida {
  TipoHerida({
    this.id,
    this.nombreHerida,
    this.gradoGravedad,
    this.gradoGravedadID,
  });

  int id;
  String nombreHerida;
  String tiempoCuracion;
  GradoGravedad gradoGravedad;
  int gradoGravedadID;
  factory TipoHerida.fromJson(Map<String, dynamic> json) => new TipoHerida(
      id: json['id'],
      nombreHerida: json['nombreHerida'],
      gradoGravedad: GradoGravedad.fromJson(json['gradoGravedad']),
      gradoGravedadID: json['gradoGravedadID']);

  Map<String, dynamic> toJson() => {
        "id": id,
        "nombreHerida": nombreHerida,
        "gradoGravedadID": gradoGravedadID,
        "gradoGravedad": gradoGravedad.toJson(),
      };
}

class GradoGravedad {
  GradoGravedad({
    this.id,
    this.grado,
    this.infectada,
  });
  int id;
  int grado;
  bool infectada;

  factory GradoGravedad.fromJson(Map<String, dynamic> json) =>
      new GradoGravedad(
        id: json['id'],
        grado: json['grado'],
        infectada: json['infectada'],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "grado": grado,
        "infectada": infectada,
      };
}
