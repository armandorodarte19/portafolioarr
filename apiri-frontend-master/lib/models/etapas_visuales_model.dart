import 'dart:convert';

EtapasVisuales etapasVisualesModelFromJson(String str) =>
    EtapasVisuales.fromJson(json.decode(str));

class EtapasVisuales {
  EtapasVisuales({
    this.id,
    this.nombreFase,
    this.descripcion,
  });
  int id;
  String nombreFase;
  String descripcion;

  factory EtapasVisuales.fromJson(Map<String, dynamic> json) =>
      new EtapasVisuales(
          id: json['id'],
          nombreFase: json['nombreFase'],
          descripcion: json['descripcion']);
}
