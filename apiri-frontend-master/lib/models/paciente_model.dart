// To parse this JSON data, do
//
//     final paciente = pacienteFromJson(jsonString);

import 'dart:convert';

Paciente pacienteModelFromJson(String str) =>
    Paciente.fromJson(json.decode(str));

String pacienteModelToJson(Paciente data) => json.encode(data.toJson());
String pacienteModelToJsonCreate(Paciente data) =>
    json.encode(data.toJsonCreate());

class Paciente {
  Paciente({
    this.id,
    this.apellidoPaterno,
    this.nss,
    this.curp,
    this.apellidoMaterno,
    this.nombrePaciente,
    this.genero,
    this.edad = 0,
  });
  int id;
  String apellidoPaterno;
  String nss;
  String curp;
  String apellidoMaterno;
  String nombrePaciente;
  String genero;
  int edad;

  factory Paciente.fromJson(Map<String, dynamic> json) => new Paciente(
        id: json['id'],
        nss: json['nss'],
        curp: json['curp'],
        apellidoPaterno: json['apellidoPaterno'],
        apellidoMaterno: json['apellidoMaterno'],
        nombrePaciente: json['nombrePaciente'],
        genero: json['genero'],
        edad: json['edad'],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "nss": nss,
        "curp": curp,
        "nombrePaciente": nombrePaciente,
        "apellidoPaterno": apellidoPaterno,
        "apellidoMaterno": apellidoMaterno,
        "genero": genero,
        "edad": edad,
      };
  Map<String, dynamic> toJsonCreate() => {
        "nss": nss,
        "nombrePaciente": nombrePaciente,
        "apellidoPaterno": apellidoPaterno,
        "apellidoMaterno": apellidoMaterno,
        "curp": curp,
        "genero": genero,
        "edad": edad,
      };
}
