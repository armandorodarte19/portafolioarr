import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/providers/casos_medicos_provider.dart';
import 'package:apiri/providers/pacientes_provider.dart';
import 'package:flutter/material.dart';

class DataSearch extends SearchDelegate {
  String seleccion = '';
  final _pacienteProvider = new PacientesProvider();
  final _casosProvider = new CasosMedicosProvider();
  String pagina;
  DataSearch(this.pagina);

  @override
  List<Widget> buildActions(BuildContext context) {
    // Acciones de nuestro AppBar

    return [
      IconButton(
        icon: Icon(Icons.clear),
        onPressed: () {
          query = '';
        },
      )
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    // Icono a la izquierda del navbar
    return IconButton(
        icon: AnimatedIcon(
          icon: AnimatedIcons.menu_arrow,
          progress: transitionAnimation,
        ),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // Crea los resultados que vamos a mostrar
    return Center(
        child: Container(
      height: 100.0,
      width: 100.0,
      color: Colors.amberAccent,
      child: Text(seleccion),
    ));
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Son las sugerencias que aparecen cuando la persona escribe
    var ftr;
    print(pagina);
    if (pagina == "pacientes") {
      ftr = _pacienteProvider.getPacientesSearched(query);
    } else {
      print(pagina);
      ftr = _casosProvider.getCasoMedicoSearched(query);
    }
    return FutureBuilder(
      future: ftr,
      builder: (
        BuildContext context,
        AsyncSnapshot<List<dynamic>> snapshot,
      ) {
        print(snapshot);
        if (snapshot.hasData) {
          final data = snapshot.data;
          if (data.length == 0) {
            return Container(
              child: Text(
                "Sin resultados",
                style: TextStyle(fontSize: 30),
              ),
              alignment: Alignment.center,
            );
          }
          return listviewRtr(data, context);
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  ListView listviewRtr(dynamic data, BuildContext context) {
    if (pagina == "pacientes") {
      List<Paciente> paclist = data;
      return ListView(
        children: paclist.map((dt) {
          return ListTile(
            title: Text(dt.nombrePaciente +
                " " +
                dt.apellidoPaterno +
                " " +
                dt.apellidoMaterno),
            subtitle: Text("CURP ${dt.curp}"),
            leading: Icon(
              Icons.person_outline,
            ),
            onTap: () {
              close(context, null);
              Navigator.pushNamed(context, 'pacienteinfo', arguments: dt);
            },
          );
        }).toList(),
      );
    } else {
      List<CasoMedico> listCas = data;
      return ListView(
        children: listCas.map((dt) {
          return ListTile(
            title: Text(dt.nombreCasoMedico),
            subtitle: Text("Fecha ${dt.fechaInicio}"),
            leading: Icon(
              Icons.assignment,
            ),
            onTap: () {
              close(context, null);

              Navigator.pushNamed(context, 'revisiones', arguments: dt);
            },
          );
        }).toList(),
      );
    }
  }
}
