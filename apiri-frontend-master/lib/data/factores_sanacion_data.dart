Map<String, String> infoFactoresSanacion = {
  "Fumar":
      "En la práctica clínica, a menudo se ha hecho responsable al tabaquismo de un retraso de la cicatrización, pero este efecto está poco documentado. ",
  "Beber Alcohol":
      "La evidencia clínica y los experimentos con animales han demostrado que la exposición al alcohol perjudica la cicatrización de heridas y aumenta la incidencia de infección.",
  "Enfermedades Crónico Degenerativas": _a,
  "Enfermedades Géneticas": _a,
  "Edad Mayor a 60":
      "La edad en sí no es un factor que retrase la cicatrización, pero este proceso suele ser más rápido que en las personas jóvenes y más lento con una edad avanzada, ya que retarda procesos de la cicatrización. se ha observado frecuentemente que la dehiscencia de las heridas es un fenómeno que predomina en la población mayor de 60 o 65 años.",
  "Buena Alimentación":
      "Una buena alimentación influye positivamente en la sanación de una herida y la ausencia de una buena alimentación afecta este proceso, la nutrición tiene un gran efecto en la sanación . Las proteínas, vitaminas y carbohidratos juegan un papel importante en este proceso. Se identifica el paciente con una alimentación sana.",
  "DesordenesAlimenticios":
      "El paciente presenta algun desórden alimenticío como lo son: Obesidad, sobrepeso, desnutrición, anorexía,bulimia,etc. ",
  "Realiza Ejercicio":
      "El paciente realiza con regularidad ( al menos 3 días por semana) ejercicio.",
  "Drogas o medicamentos con impacto negativo":
      "Muchos medicamentos, como los que interfieren con la formación de coágulos o la función plaquetaria, o las respuestas inflamatorias y la proliferación celular tienen la capacidad de afectar la cicatrización de heridas.\n\n1)	Corticosteroides. \n2)	Anticoagulantes. \n3)	La colchicina. \n4)	Los antiinﬂamatorios no esteroideos y los quimioterapéuticos.\n5)	Penicilamina. \n6)	Esteroides. \n7)	Inmunosupresores.\n8)	Fármacos para el tratamiento del cáncer (antineoplásicos)\n9)	Anticolvulsivantes\n10)	Antidepresivos, ansiolíticos",
  "Medicamento con impacto positivo":
      "Para evitar la infección de la herida deben emplearse antisépticos. Éstos son productos químicos que se aplican sobre los tejidos vivos con la finalidad de eliminar todos los microorganismos patógenos o inactivar los virus.\n\n1)	Antibióticos. Aquí hay como 10 grupos: pencilinas, cefalosporinas de primera a quinta generación, amino glucósidos, macrolidos, tetraciclinas, glicopeptidos, carbapenemes, sulfas, quinolonas \n2)	Antisépticos.\n3)	Analgésicos, antinflamatorios, antipiréticos \n4)	Cicatrizantes \n5)	Desecantes \n6)	Apósitos.",
  "Endocrinopatias": _a,
  "Estres":
      "Numerosos estudios han confirmado que la disrupción inducida por el estrés del equilibrio inmunológico neuroendocrino es consecuencia de la salud. Los estudios tanto en humanos como en animales han demostrado que el estrés psicológico provoca un retraso sustancial en la cicatrización de heridas.",
  "Insuficiencia Vascular":
      "La disminución del suministro de sangre es una causa común de estas úlceras",
  "Cicatriz": "",
  "Herida con puntos": "",
  "Cicatriz con deformidad": "",
};

String _a =
    "Algunas enfermedades que influyen en la cicatrización de las heridas son:\n\n1.	Hipotiroidismo. \n2.	Diabetes.\n3.	hiperlipidemias (colesterol alto).\n4.	Hipertensión. \n5.	obesidad mórbida.\n6.insuficiencia renal.\n7.	síndrome de down.\n8.	lupus eritema todo sistémico.\n9.	artritis \n10.	Enfermedades de colágena. \n11.	Inmunosupresión.\n12.	Traumatismo sistemático. \n13.	enfermedades metabólicas. \n14.	Edema. \n15.	Desvitalización de los tejidos.\n16.	Isquemia.\n17.	Desequilibrio hídrico y electrolítico\n18.	Desnutrición.\n19.	Insuficiencia hepática.\n20.	Insuficiencia respiratoria\n\nLas enfermedades metabólicas como la diabetes mellitus, el cáncer y enfermedades vasculares interfieren en la cicatrización normal de los tejidos causando heridas crónicas\nLa insuficiencia renal aguda o crónica puede requerir tratamiento de diálisis. Niveles elevados de urémica, las toxinas y la acidosis metabólica afectarán la curación de la herida al influir tanto en el sistema inmunológico innato como en el adaptativo. Hemodiálisis y la diálisis se asocia con una mayor susceptibilidad a las infecciones. Regulación a la baja de los neutrófilos circulantes debido a su activación repetida que desencadena por membranas de diálisis. Las especies de oxígeno reactivo circulante también aumentan como resultado de la activación de la membrana de diálisis. Respuestas deficientes de los linfocitos B y T también se asocian con la diálisis renal\nLa falla hepática, provoca disminución de factores de coagulación, proteínas plasmáticas bajas, actividad bactericida disminuida y falla de la regulación de la glucosa puede contribuir a la falla de las heridas en los tejidos blandos\nTambién La Insuficiencia respiratoria y falla intestinal pueden provocar problemas en la sanación de la herida.";
