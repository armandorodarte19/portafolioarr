import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/widgets/BNavigateBar.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:apiri/utils/utils.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  PreferenciasUsuario _preferenciasUsuario = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    cargarEtapasVisuales();

    return Scaffold(
      appBar: AppBar(
        title: Text("APIRI-HC"),
        automaticallyImplyLeading: false,
        actions: <Widget>[LogOutApiri()],
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
      ),
      body: Stack(
        children: <Widget>[_fondoApp(), _titulos()],
      ),
      bottomNavigationBar: BNNavigateBar(context),
    );
  }

  Widget _titulos() {
    String nombreMedico = "";
    (_preferenciasUsuario.role == 'Medico')
        ? nombreMedico = _preferenciasUsuario.nombremedico
        : nombreMedico = _preferenciasUsuario.nombreAdmin;

    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(20.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text(
              'Sistema de recolección de datos APIRI',
              style: TextStyle(color: Colors.black, fontSize: 30.0),
              textAlign: TextAlign.center,
            ),
            Text(
                'Bienvenido \n $nombreMedico a la aplicación de recolección de información sobre heridas cutáneas o APIRI-HC',
                style: TextStyle(color: Colors.black, fontSize: 30.0),
                textAlign: TextAlign.center),
            Icon(
              Icons.local_hospital,
              color: Colors.redAccent,
              size: 200,
            )
          ],
        ),
      ),
    );
  }

  Widget _fondoApp() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white30,
    );
  }
}
