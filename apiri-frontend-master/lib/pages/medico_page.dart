import 'dart:async';

import 'package:apiri/bloc/login_bloc.dart';
import 'package:apiri/models/centro_clinico_models.dart';
import 'package:apiri/models/medico_model.dart';
import 'package:apiri/providers/medicos_provider.dart';
import 'package:apiri/providers/usuario_provider.dart';
import 'package:apiri/widgets/AnimationsLoadersAPIRI.dart';
import 'package:apiri/widgets/BNavigateBar.dart';
import 'package:flutter/material.dart';
import 'package:apiri/utils/utils.dart' as validatorUtils;

class MedicoPage extends StatefulWidget {
  MedicoPage({Key key}) : super(key: key);

  @override
  _MedicoPageState createState() => _MedicoPageState();
}

enum Genero { hombre, mujer }

class _MedicoPageState extends State<MedicoPage> {
  bool _guardar = true;
  Medico _medicoNuevo = new Medico();
  bool _guardando = false;
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  Genero _genero = Genero.hombre;
  MedicoProvider _medicoProvider = new MedicoProvider();

  UsuarioProvider _usuarioProvider = new UsuarioProvider();
  LoginBloc bloc = new LoginBloc();

  @override
  Widget build(BuildContext context) {
    final Medico medData = ModalRoute.of(context).settings.arguments;

    if (medData != null && _medicoNuevo.id == null) {
      _medicoNuevo = medData;
      _precargarGenero();

      _guardar = false;
    }

    return Scaffold(
      key: scaffoldKey,
      appBar: AppBar(
        title: Text("APIRI-HC"),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
      ),
      body: listFields(),
    );
  }

  Widget listFields() {
    return SafeArea(
        child: SingleChildScrollView(
      child: Container(
        child: Form(
            key: formKey,
            child: Column(
              children: [
                SizedBox(
                  height: 15,
                ),
                Divider(),
                Text(
                  "Información del Médico",
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 15,
                ),
                _crearInputTexto(
                    "Nombre", 1, (!_guardar) ? _medicoNuevo.nombreMedico : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputTexto("Apellido Paterno", 2,
                    (!_guardar) ? _medicoNuevo.apellidoPaterno : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputTexto("Apellido Materno", 3,
                    (!_guardar) ? _medicoNuevo.apellidoMaterno : ""),
                SizedBox(
                  height: 10,
                ),
                _crearRBGenero(),
                SizedBox(
                  height: 10,
                ),
                (!_guardar)
                    ? _crearEmailClosed("Correo", _medicoNuevo.correo)
                    : _crearEmail(),
                SizedBox(
                  height: 10,
                ),
                _crearInputNumber("Telefono", 4, _medicoNuevo.telefono),
                SizedBox(
                  height: 10,
                ),
                (_guardando) ? animationCargandoWave() : _crearBoton(context),
                RaisedButton.icon(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  color: Colors.blueAccent,
                  textColor: Colors.white,
                  label: Text('Re-enviar contraseña'),
                  icon: Icon(Icons.vpn_key),
                  onPressed: () =>
                      _usuarioProvider.resendPass(_medicoNuevo.correo),
                ),
              ],
            )),
      ),
    ));
  }

  Widget _crearInputTexto(String texto, int element, String valueInit) {
    return TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: _decoratorInput(texto),
        initialValue: (valueInit == null) ? "" : valueInit,
        onSaved: (value) => _guardarValor(value, element),
        validator:
            (texto != "Apellido Materno") ? _validatorText(texto) : null);
  }

  Widget _crearEmailClosed(String texto, String valueInit) {
    return TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: _decoratorInput(texto),
        initialValue: valueInit,
        readOnly: true);
  }

  Widget _crearEmail() {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          //padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextFormField(
            keyboardType: TextInputType.emailAddress,
            decoration: InputDecoration(
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0),
                ),
                suffixIcon:
                    Icon(Icons.alternate_email, color: Colors.deepPurple),
                hintText: 'ejemplo@correo.com',
                labelText: 'Correo electrónico',
                errorText: snapshot.error),
            onChanged: bloc.changeEmail,
          ),
        );
      },
    );
  }

  _validatorText(String text) {
    if (text == 'CURP') {
      return (value) {
        if (value.length != 18) {
          return 'CURP debe tener 18 caracteres';
        } else {
          return null;
        }
      };
    }
    return (value) {
      if (value.length < 3) {
        return 'El campo no puede ir vacío';
      } else {
        return null;
      }
    };
  }

  Widget _crearInputNumber(String texto, int element, dynamic valueInit) {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: _decoratorInput(texto),
      initialValue:
          (valueInit == null || valueInit == 0) ? null : valueInit.toString(),
      onSaved: (value) => _guardarValor(value, element),
      validator: (value) => _validatorNumber(value),
    );
  }

  _precargarGenero() {
    (_medicoNuevo.genero == 'H')
        ? _genero = Genero.hombre
        : _genero = Genero.mujer;
  }

  _asignarGenero() {
    (_genero == Genero.hombre)
        ? _medicoNuevo.genero = 'H'
        : _medicoNuevo.genero = 'M';
  }

  Widget _crearRBGenero() {
    _asignarGenero();
    return Column(
      children: <Widget>[
        ListTile(
          title: const Text('Hombre'),
          leading: Radio(
            value: Genero.hombre,
            groupValue: _genero,
            onChanged: (Genero value) {
              setState(() {
                _genero = value;
              });
            },
          ),
        ),
        ListTile(
          title: const Text('Mujer'),
          leading: Radio(
            value: Genero.mujer,
            groupValue: _genero,
            onChanged: (Genero value) {
              setState(() {
                _genero = value;
              });
            },
          ),
        ),
      ],
    );
  }

  _validatorNumber(dynamic value2) {
    if (validatorUtils.isNumeric(value2)) {
      int value = int.parse(value2);
      if (value.isNegative) return "Debe ser un numero valido";

      if (value.toString().length == 10) {
        return null;
      } else {
        return "El número debe contener 10 digitos";
      }
    } else {
      return 'Debe ser un numero valido';
    }
  }

  _decoratorInput(String texto) {
    return InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      labelText: texto,
    );
  }

  _guardarValor(dynamic value, int element) {
    switch (element) {
      case 1:
        _medicoNuevo.nombreMedico = value;
        break;
      case 2:
        _medicoNuevo.apellidoPaterno = value;
        break;
      case 3:
        _medicoNuevo.apellidoMaterno = value;
        break;
      case 4:
        _medicoNuevo.telefono = int.parse(value);
        break;
    }
  }

  _crearBoton(BuildContext context) {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      color: Colors.blueAccent,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      onPressed: (_guardando) ? null : _submit,
    );
  }

  void mostrarSnackbar(String mensaje) {
    final snackbar = SnackBar(
      content: Text(mensaje),
      duration: Duration(seconds: 5),
      backgroundColor: Colors.greenAccent,
    );
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });

    bool exito = false;

    if (!_guardar) {
      exito = await _medicoProvider.updateMedico(_medicoNuevo);
      if (!exito) {
        mostrarSnackbar('Upps ha ocurrido un error ');
        setState(() {
          _guardando = false;
        });
      } else {
        mostrarSnackbar('Registro guardado');
        Timer(Duration(seconds: 2), () => {Navigator.pop(context)});
      }
    } else {
      String ps = validatorUtils.passRandom();
      _medicoNuevo.correo = bloc.email;
      exito = await _medicoProvider.postMedico(_medicoNuevo);
      if (exito)
        exito = await _usuarioProvider.signOut(_medicoNuevo.correo, ps);

      if (!exito) {
        mostrarSnackbar('Upps ha ocurrido un error ');
        setState(() {
          _guardando = false;
        });
      } else {
        mostrarSnackbar('Registro guardado');
        Timer(Duration(seconds: 2), () => {Navigator.pop(context)});
      }

      //
    }
  }
}
