import 'dart:async';

import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/providers/pacientes_provider.dart';
import 'package:apiri/search/search_delegate.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/BNavigateBar.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PacientesPage extends StatefulWidget {
  const PacientesPage({Key key}) : super(key: key);
  @override
  _PacientesPageState createState() => _PacientesPageState();
}

class _PacientesPageState extends State<PacientesPage> {
  final _pacientesProvider = new PacientesProvider();
  final scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("APIRI-HC"),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: DataSearch("pacientes"),
              );
            },
          ),
          LogOutApiri()
        ],
      ),
      body: _pacientesList(),
      bottomNavigationBar: BNNavigateBar(context),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.person_add),
        onPressed: () {
          Navigator.pushNamed(context, 'paciente').then((value) =>
              {_pacientesProvider.clearData(), print(value), setState(() {})});
        },
      ),
    );
  }

  @override
  void initState() {
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        setState(() {});
      }
    });
    super.initState();
  }

  _pacientesList() {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        //_buscador(),
        SizedBox(
          height: 10,
        ),
        Expanded(child: _createList())
      ],
    );
  }

  _createList() {
    return FutureBuilder(
      future: _pacientesProvider.getPacientes(),
      builder: (BuildContext context, AsyncSnapshot<List<Paciente>> snapshot) {
        if (snapshot.hasData) {
          final pacientes = snapshot.data;
          print(pacientes.length);
          if (pacientes.length == 0) {
            return Container(
              child: Text(
                "Sin resultados",
                style: TextStyle(fontSize: 30),
              ),
              alignment: Alignment.center,
            );
          }

          return RefreshIndicator(
              child: ListView.builder(
                itemCount: pacientes.length,
                itemBuilder: (context, i) {
                  if (i < snapshot.data.length) {
                    return _crearItem(context, pacientes[i]);
                  } else if (_pacientesProvider.hasMore) {
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 32.0),
                      child: Center(child: CircularProgressIndicator()),
                    );
                  } else {
                    return Padding(
                      padding: EdgeInsets.symmetric(vertical: 32.0),
                      child: Center(
                          child: Text('No quedan pacientes por mostrar')),
                    );
                  }
                },
                controller: scrollController,
              ),
              onRefresh: _pacientesProvider.getPacientes);
          //return  _listas(snapshot.data);

        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _crearItem(BuildContext context, Paciente paciente) {
    bool _borradoExito = false;
    return Dismissible(
      background: Container(
        color: Colors.redAccent,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 50,
        ),
      ),
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      onDismissed: (direction) async {
        _borradoExito = await _pacientesProvider.deletePaciente(paciente.id);
        if (!_borradoExito) {
          mostrarAlertaPop(context, "No se pueden borrar pacientes con casos",
              "Error al eliminar");
          setState(() {});
        }
      },
      confirmDismiss: (DismissDirection direction) async {
        return await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Confirmación"),
              content:
                  const Text("Estas seguro que deseas borrar este paciente?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: const Text("Cancelar")),
                FlatButton(
                  onPressed: () => {Navigator.of(context).pop(true)},
                  child: const Text("Borrar"),
                ),
              ],
            );
          },
        );
      },
      child: Card(
        shadowColor: Color.fromRGBO(50, 50, 50, 1),
        elevation: 10.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(
                  '${paciente.apellidoPaterno} ${paciente.apellidoMaterno} ${paciente.nombrePaciente} '),
              subtitle: Text("NSS: " + paciente.nss),
              trailing: Text(
                "Edad: " + paciente.edad.toString(),
              ),
              leading: Icon(Icons.person, color: Colors.blue, size: 50),
              onTap: () {
                Navigator.pushNamed(context, "pacienteinfo",
                        arguments: paciente)
                    .then((value) => setState(() {}));
              },
            ),
          ],
        ),
      ),
    );
  }
}
