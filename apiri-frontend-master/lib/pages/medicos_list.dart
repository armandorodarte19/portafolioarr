import 'package:apiri/models/medico_model.dart';
import 'package:apiri/providers/medicos_provider.dart';
import 'package:apiri/providers/usuario_provider.dart';
import 'package:apiri/widgets/BNavigateBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MedicosListPage extends StatefulWidget {
  MedicosListPage({Key key}) : super(key: key);

  @override
  _MedicosListPageState createState() => _MedicosListPageState();
}

class _MedicosListPageState extends State<MedicosListPage> {
  MedicoProvider _medicoProvider = new MedicoProvider();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("APIRI-HC"),
        automaticallyImplyLeading: false,
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
      ),
      bottomNavigationBar: BNNavigateBar(context),
      body: _medicosList(),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.person_add),
        onPressed: () {
          //Navigator.pushNamed(context, 'medicoinfo').;
          Navigator.pushNamed(context, "medicoinfo")
              .then((value) => setState(() {}));
        },
      ),
    );
  }

  _medicosList() {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        Expanded(child: _createList())
      ],
    );
  }

  _createList() {
    return FutureBuilder(
      future: _medicoProvider.getMedicos(),
      builder: (BuildContext context, AsyncSnapshot<List<Medico>> snapshot) {
        //_buscador();
        if (snapshot.hasData) {
          final medicos = snapshot.data;
          if (medicos.length == 0) {
            return Container(
              child: Text(
                "Sin resultados",
                style: TextStyle(fontSize: 30),
              ),
              alignment: Alignment.center,
            );
          }
          return ListView.builder(
              itemCount: medicos.length,
              itemBuilder: (context, i) {
                if (i < snapshot.data.length) {
                  return _crearItem(context, medicos[i]);
                } else
                  return Container();
              });
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  void borrarUsuario(Medico medico) async {
    await _medicoProvider.deleteMedico(medico);
    setState(() {});
  }

  Widget _crearItem(BuildContext context, Medico medico) {
    bool _borradoExito = false;
    return Dismissible(
      background: Container(
        color: Colors.redAccent,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 50,
        ),
      ),
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      confirmDismiss: (DismissDirection direction) async {
        return await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Confirmación"),
              content:
                  const Text("Estas seguro que deseas borrar este médico?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: const Text("Cancelar")),
                FlatButton(
                  onPressed: () async => {
                    _borradoExito = false,
                    borrarUsuario(medico),
                    Navigator.of(context).pop(_borradoExito)
                  },
                  child: const Text("Borrar"),
                ),
              ],
            );
          },
        );
      },
      child: Card(
        shadowColor: Color.fromRGBO(50, 50, 50, 1),
        elevation: 10.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text(
                  '${medico.apellidoPaterno} ${medico.apellidoMaterno} ${medico.nombreMedico} '),
              subtitle: Text("Telefono: " + medico.telefono.toString()),
              trailing: Text(
                medico.centrosClinicos.nombreCentroClinico,
              ),
              onTap: () {
                Navigator.pushNamed(context, "medicoinfo", arguments: medico)
                    .then((value) => setState(() {}));
              },
            ),
          ],
        ),
      ),
    );
  }
}
