import 'dart:async';
import 'dart:io';
import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/models/etapas_visuales_model.dart';
import 'package:apiri/models/factores_sanacion_model.dart';
import 'package:apiri/models/revision_model.dart';
import 'package:apiri/models/tipo_herida_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/providers/medicos_provider.dart';
import 'package:apiri/providers/revision_provider.dart';
import 'package:apiri/providers/tipo_herida_provider.dart';
import 'package:apiri/utils/sv_acces.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/AnimationsLoadersAPIRI.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:apiri/data/factores_sanacion_data.dart';
import 'package:apiri/utils/utils.dart' as utils;
import 'package:apiri/utils/sv_acces.dart' as _sv;

class RevisionPage extends StatefulWidget {
  @override
  _RevisionPageState createState() => _RevisionPageState();
}

class _RevisionPageState extends State<RevisionPage> {
  String _fecha = '';
  EtapasVisuales _etapaSel;
  List<EtapasVisuales> etapasVisuales = new List();
  List<String> _heridas = new List();
  String _heridaSel;
  TextEditingController _inputFieldDateController = new TextEditingController();
  CasoMedico casoM = new CasoMedico();
  File _foto;
  int _gradoSeleccionado = 1;
  bool _infectada = false;
  Revision _newRevision = new Revision();
  final formKey = GlobalKey<FormState>();
  FactoresSanacion _factorSanacion = new FactoresSanacion();
  List<String> _labelsList = new List();
  final scaffoldKey = GlobalKey<ScaffoldState>();
  Map<String, bool> _factoresSanacion;
  RevisionProvider _revisionProvider = new RevisionProvider();
  bool _editar = false;
  bool _guardando = false;
  Map _infoFactoresSanacion;
  bool cargado = false;
  bool _casoAbierto = true;
  int _imc;
  @override
  Widget build(BuildContext context) {
    _infoFactoresSanacion = infoFactoresSanacion;
    _labelsList = utils.getMapFactores().keys.toList();
    final data = ModalRoute.of(context).settings.arguments;
    if (_factoresSanacion == null) {
      _factoresSanacion = utils.getMapFactores();
      _imc = 0;
    }

    if (data is CasoMedico) {
      casoM = data;
    } else if (!cargado) {
      _cargarInit(data);
      _casoAbierto = _newRevision.casoMedico.casoAbierto;
    }

    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text("Revisión"),
          centerTitle: true,
          backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
          actions: <Widget>[LogOutApiri()],
        ),
        body: SafeArea(child: _factoresSanacionChecklist()));
  }

  _cargarInit(Revision data) {
    _newRevision = data;
    _gradoSeleccionado = _newRevision.tipoHerida.gradoGravedad.grado;
    _imc = _newRevision.factoresSanacion.iMC;
    _infectada = _newRevision.tipoHerida.gradoGravedad.infectada;
    _factoresSanacion = utils.mapFactores(_newRevision.factoresSanacion);
    _editar = true;
    if (_fecha == '') _fecha = utils.formatDate(_newRevision.fecha);
    _revisionProvider.downloadImage(_newRevision.urlImage);
    _inputFieldDateController.text = _fecha;
    _heridaSel = _newRevision.tipoHerida.nombreHerida;
    cargado = true;
  }

  Widget _factoresSanacionChecklist() {
    return SingleChildScrollView(
      child: Container(
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SizedBox(height: 15.0),
              SizedBox(height: 10.0),
              _crearFecha(context, 'Fecha revision'),
              SizedBox(height: 20.0),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "Fotografía",
                    style: utils.styleTitle,
                  ),
                  SizedBox(width: 15.0),
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    iconSize: 40,
                    color: Colors.blue,
                    onPressed: (_casoAbierto) ? _tomarFoto : null,
                  ),
                ],
              ),
              SizedBox(height: 10.0),
              Divider(),
              _mostrarFoto(),
              SizedBox(height: 10.0),
              Container(
                padding: const EdgeInsets.only(left: 14.0, top: 14.0),
                child: Text(
                  "Información de herida",
                  style: utils.styleTitle,
                ),
              ),
              (_casoAbierto)
                  ? _selectorCB(etapasVisuales, 'Tipo de Herida', 0)
                  : _crearInputTexto('Tipo de Herida', _heridaSel),
              SizedBox(height: 10.0),
              _radioButtonGrados(),
              Container(
                padding: const EdgeInsets.only(left: 14.0, top: 14.0),
                child: Text(
                  "Factores de Sanación",
                  style: utils.styleTitle,
                ),
              ),
              Divider(),
              _crearInputNumber("Índice de masa corporal", _imc),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: CheckboxGroup(
                  labels: _labelsList,
                  itemBuilder: (checkBox, label, index) {
                    return _buildCheckList(index);
                  },
                ),
              ),
              Divider(),
              SizedBox(height: 10.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    "Etapas de sanación de \nla herida (etapas visuales)",
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style: utils.styleTitle,
                  ),
                  buttonHelp('Ayuda', _cargarInfoEtapas()),
                ],
              ),
              SizedBox(height: 10.0),
              (_casoAbierto)
                  ? _selectorCB(etapasVisuales, 'Etapas Visuales', 1)
                  : _crearInputTexto(
                      'Etapa Visuale', _newRevision.etapasVisuales.nombreFase),
              SizedBox(height: 15.0),
              (_guardando) ? animationCargandoWave() : _crearBoton(context),
            ],
          ),
        ),
      ),
    );
  }

  _radioButtonGrados() {
    if (_heridaSel == null) return Container();
    return Column(
      children: [
        ButtonBar(
          alignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Text("Grado de Gravedad"),
            _crearRadioBtn(1),
            _crearRadioBtn(2),
            _crearRadioBtn(3),
            _crearRadioBtn(4),
          ],
        ),
        CheckboxListTile(
          value: _infectada,
          onChanged: (_casoAbierto)
              ? (value) {
                  _infectada = value;

                  setState(() {});
                }
              : null,
          title: Text("Infectada"),
        ),
      ],
    );
  }

  Widget _crearRadioBtn(int value) {
    return Column(
      children: [
        Text(value.toString()),
        Radio(
            value: value,
            groupValue: _gradoSeleccionado,
            onChanged: (_casoAbierto)
                ? (val) {
                    setSelectedRadio(val);
                  }
                : null)
      ],
    );
  }

  setSelectedRadio(int val) {
    setState(() {
      _gradoSeleccionado = val;
    });
  }

  Widget _buildCheckList(int index) {
    bool _selectedFactor = _factoresSanacion.values.elementAt(index);

    String _itemSelFac = _factoresSanacion.keys.elementAt(index);

    Checkbox cb = Checkbox(
        value: _selectedFactor,
        onChanged: (_casoAbierto)
            ? (
                bool valueSel,
              ) {
                setState(() {
                  _factoresSanacion.update(_itemSelFac, (value) => valueSel);
                  _factoresSanacion.forEach((key, value) {});
                });
              }
            : null);
    String factor = utils.getMapFactores().keys.elementAt(index);
    Text t = Text.rich(TextSpan(text: factor));

    return Row(
      children: [
        buttonHelp(factor, _infoFactoresSanacion.values.elementAt(index)),
        cb,
        Flexible(
          child: t,
        )
      ],
    );
  }

  _cargarDatosIniciales(int nSelector, List<dynamic> list) {
    if (nSelector == 0) {
      //Heridas
      if (list.length < 1) _cargarHeridas();
    }
    if (nSelector == 1) {
      //Etapas Visuales
      if (list.length < 1) _cargarEtapasVisuales();
    }
  }

  Widget _crearInputTexto(String texto, String value) {
    return TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: _decoratorInput(texto),
        initialValue: value,
        enabled: _casoAbierto);
  }

  Widget _selectorCB(List<dynamic> list, String title, int nSelector) {
    _cargarDatosIniciales(nSelector, list);

    return Column(
      children: [
        DropDownFormField(
          titleText: title,
          hintText: (_editar && nSelector == 1)
              ? _newRevision.etapasVisuales.nombreFase
              : 'Escoge una opción',
          value: (nSelector == 0) ? _heridaSel : _etapaSel,
          onSaved: (value) {
            setState(() {
              (nSelector == 0) ? _heridaSel = value : _etapaSel = value;
            });
          },
          required: true,
          validator: (value) {
            if (nSelector == 1 &&
                value == null &&
                _newRevision.etapasVisuales != null) {
              return null;
            }
            if (value == null) return 'Seleccione una opcion';
            return null;
          },
          onChanged: (value) {
            setState(() {
              (nSelector == 0) ? _heridaSel = value : _etapaSel = value;
            });
          },
          dataSource:
              (nSelector == 0) ? _mapListTipoHeridas() : _mapListEtapas(),
          textField: 'display',
          valueField: 'value',
        ),
      ],
    );
  }

  //Borrar

  _mapListEtapas() {
    List<dynamic> _etapasMapList = new List();

    etapasVisuales.forEach((element) {
      _etapasMapList.add({
        "display": element.nombreFase,
        "value": element,
      });
    });

    return _etapasMapList;
  }

  _mapListTipoHeridas() {
    List<dynamic> _etapasMapList = new List();

    _heridas.forEach((element) {
      _etapasMapList.add({
        "display": element,
        "value": element,
      });
    });

    return _etapasMapList;
  }

  _cargarHeridas() async {
    if (_heridas.length > 0) return;
    _heridas = await TipoHeridaProvider().getNombresHeridas();
    setState(() {});
  }

  Widget _crearInputNumber(String texto, dynamic valueInit) {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: _decoratorInput(texto),
      enabled: _casoAbierto,
      initialValue:
          (valueInit == null || valueInit == 0) ? null : valueInit.toString(),
      onSaved: (value) => _imc = int.parse(value),
      validator: (value) => _validatorNumber(texto, value),
    );
  }

  _decoratorInput(String texto) {
    return InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      labelText: texto,
    );
  }

  _validatorNumber(String text, dynamic value2) {
    if (utils.isNumeric(value2)) {
      int value = int.parse(value2);
      if (value.isNegative) return "Debe ser un numero positivo";
      if (text == "Edad") {
        if (value >= 0 && value <= 110) {
          return null;
        } else {
          return "El rango de edad debe ser de 0 a 110";
        }
      }
      if (text == "Código Postal") {
        if (value.toString().length == 5) {
          return null;
        } else {
          return "El Código Postal debe tener 5 dígitos";
        }
      }

      return null;
    } else {
      return 'Solo números';
    }
  }

  void _cargarEtapasVisuales() {
    etapasVisuales = utils.etapasVisuales;
  }

  _crearBoton(BuildContext context) {
    return RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        color: Colors.blueAccent,
        textColor: Colors.white,
        label: Text('Guardar'),
        icon: Icon(Icons.save),
        onPressed: (_guardando) ? null : _submit);
  }

  //Para Borrar

  _tomarFoto() async {
    _procesarImagen(ImageSource.camera);
  }

  _procesarImagen(ImageSource origen) async {
    final picker = await ImagePicker().getImage(source: origen);

    if (picker != null) {
      //Limpieza
      _foto = File(picker.path);
    }

    setState(() {});
  }

  _mostrarFoto() {
    if (_newRevision.urlImage != null) {
      return FadeInImage(
        image: NetworkImage(
            _revisionProvider.downloadImage(_newRevision.urlImage),
            headers: {
              'Authorization': 'Bearer ${PreferenciasUsuario().token}'
            }),
        placeholder: AssetImage('assets/jar-loading.gif'),
        height: 300.0,
        fit: BoxFit.contain,
      );
    } else {
      return Image(
        image: _mostrarFotoImage(),
        height: 200.0,
        fit: BoxFit.cover,
      );
    }
  }

  _mostrarFotoImage() {
    if (_foto == null) {
      return AssetImage('assets/no-image.png');
    } else {
      return FileImage(_foto);
    }
  }

  Widget buttonHelp(String title, String info) {
    if (info == "")
      return IconButton(
          icon: Icon(Icons.help),
          color: Colors.deepPurpleAccent,
          onPressed: null);

    return IconButton(
      icon: Icon(Icons.help),
      color: Colors.deepPurpleAccent,
      onPressed: () => showDialog(
          context: context,
          child: AlertDialog(
            title: Text(title),
            content: Text.rich(TextSpan(text: info)),
            scrollable: true,
            actions: <Widget>[
              FlatButton(
                  color: Colors.deepPurpleAccent,
                  onPressed: () =>
                      {setState(() {}), Navigator.of(context).pop(false)},
                  child: const Text("Ok")),
            ],
          )),
    );
  }

  String _cargarInfoEtapas() {
    String info = utils.infoInicialEtapas;
    etapasVisuales.forEach((element) {
      info += element.nombreFase + ": " + element.descripcion + "\n";
    });

    return info;
  }

  Widget _crearFecha(BuildContext context, String text) {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      enableInteractiveSelection: false,
      enabled: _casoAbierto,
      controller: _inputFieldDateController,
      decoration: InputDecoration(
        hintText: text,
        labelText: text,
        suffixIcon: Icon(Icons.calendar_today),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
      ),
      onTap: () {
        if (_casoAbierto) {
          FocusScope.of(context).requestFocus(new FocusNode());

          _selectDate(context);
        }
      },
      validator: (value) {
        if (_fecha == "") {
          return 'Este campo es obligatorio';
        }
        return null;
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime initialDate = (_editar) ? _newRevision.fecha : new DateTime.now();
    DateTime firstDate = _firstDate();

    DateTime picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: firstDate,
      lastDate: new DateTime(2100),
      locale: Locale('es', 'ES'),
    );
    if (picked != null) {
      setState(() {
        _fecha = utils.formatDate(picked);
        _inputFieldDateController.text = _fecha;
      });
    }
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });
    bool exito;
    await _llenarRevision();

    if (_editar) {
      exito = await _revisionProvider.putRevision(_newRevision);
    } else {
      exito = await _revisionProvider.postRevision(_newRevision);
    }
    if (!exito) {
      setState(() {
        _guardando = false;
      });
      mostrarSnackbar('Upps ocurrio un problema', 2);
    } else {
      mostrarSnackbar('Registro guardado', 1);
      Timer(Duration(seconds: 2), () => {Navigator.pop(context)});
    }
  }

  void mostrarSnackbar(String mensaje, int opt) {
    final snackbar = SnackBar(
        content: Text(mensaje),
        duration: Duration(seconds: 5),
        backgroundColor: (opt == 1) ? Colors.green : Colors.red);
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  _llenarRevision() async {
    MedicoProvider _medicoProvider = new MedicoProvider();
    PreferenciasUsuario _user = new PreferenciasUsuario();
    _newRevision.casoMedico = casoM;

    _newRevision.fecha = DateTime.parse(_fecha);
    _newRevision.imagen = _foto;

    _newRevision.medico =
        await _medicoProvider.getMedico(int.parse(_user.medico[0]));

    if (_etapaSel != null && _editar) {
      _newRevision.etapasVisuales = _etapaSel;
    }
    if (!_editar) {
      _newRevision.etapasVisuales = _etapaSel;
    }
    _newRevision.factoresSanacion = utils.convertFactoresMap(_factoresSanacion);
    _newRevision.factoresSanacion.iMC = _imc;
    TipoHeridaProvider tipoHeridaProvider = new TipoHeridaProvider();
    _newRevision.tipoHerida = await tipoHeridaProvider
        .getTipoHeridaSeleccionada(_heridaSel, _gradoSeleccionado, _infectada);
    if (_newRevision.imagen != null) {
      String ruta = await _revisionProvider.subirImagen(
          _newRevision.imagen, _newRevision, casoM.nombreCasoMedico);
      _newRevision.urlImage = ruta;
    }
  }

  DateTime _firstDate() {
    if (_editar) {
      return DateTime(_newRevision.fecha.year, _newRevision.fecha.month,
          _newRevision.fecha.day);
    }
    return DateTime(
        casoM.fechaInicio.year, casoM.fechaInicio.month, casoM.fechaInicio.day);
  }
}
