import 'dart:async';
import 'dart:convert';

import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/providers/pacientes_provider.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/AnimationsLoadersAPIRI.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:apiri/utils/utils.dart' as validatorUtils;

class PacienteCreatePage extends StatefulWidget {
  @override
  _PacienteCreatePageState createState() => _PacienteCreatePageState();
}

enum Genero { hombre, mujer }

class _PacienteCreatePageState extends State<PacienteCreatePage> {
  Paciente pacienteNuevo = new Paciente();

  PacientesProvider _pacientesProvider = new PacientesProvider();
  bool _guardar = true;
  bool _guardando = false;
  Genero _genero = Genero.hombre;

  final formKey = GlobalKey<FormState>();
  final _scflKeyPc = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    final Paciente pacData = ModalRoute.of(context).settings.arguments;
    String title = "Crear Paciente";

    if (pacData != null) {
      pacienteNuevo = pacData;

      _asignarGenero();
      _guardar = false;
      title = "Editar paciente";
    }

    return Scaffold(
      key: _scflKeyPc,
      appBar: AppBar(
        title: Text(title),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
        actions: <Widget>[LogOutApiri()],
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context)),
      ),
      body: SafeArea(
          child: SingleChildScrollView(
        child: Container(
          child: Form(
            key: formKey,
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 15,
                ),
                Divider(),
                Text(
                  "Información del paciente",
                  textAlign: TextAlign.center,
                ),
                _crearInputTexto(
                    "NSS", 1, (!_guardar) ? pacienteNuevo.nss : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputTexto(
                    "CURP", 6, (!_guardar) ? pacienteNuevo.curp : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputTexto("Nombre", 2,
                    (!_guardar) ? pacienteNuevo.nombrePaciente : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputTexto("Apellido Paterno", 3,
                    (!_guardar) ? pacienteNuevo.apellidoPaterno : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputTexto("Apellido Materno", 4,
                    (!_guardar) ? pacienteNuevo.apellidoMaterno : ""),
                SizedBox(
                  height: 10,
                ),
                _crearInputNumber(
                    "Edad", 5, (!_guardar) ? pacienteNuevo.edad : 0),
                SizedBox(
                  height: 10,
                ),
                _crearRBGenero(),
                Divider(),
                (_guardando) ? animationCargandoWave() : _crearBoton(context)
              ],
            ),
          ),
        ),
      )),
    );
  }

  Widget _crearInputTexto(String texto, int element, String valueInit) {
    return TextFormField(
        textCapitalization: TextCapitalization.sentences,
        decoration: _decoratorInput(texto),
        initialValue: (valueInit == null) ? "" : valueInit,
        onSaved: (value) => _guardarValor(value, element),
        onEditingComplete: () => print("object"),
        validator:
            (texto != "Apellido Materno") ? _validatorText(texto) : null);
  }

  Widget _crearInputNumber(String texto, int element, dynamic valueInit) {
    return TextFormField(
      keyboardType: TextInputType.number,
      decoration: _decoratorInput(texto),
      initialValue:
          (valueInit == null || valueInit == 0) ? null : valueInit.toString(),
      onSaved: (value) => _guardarValor(value, element),
      validator: (value) => _validatorNumber(texto, value),
    );
  }

  _asignarGenero() {
    (_genero == Genero.hombre)
        ? pacienteNuevo.genero = 'H'
        : pacienteNuevo.genero = 'M';
  }

  Widget _crearRBGenero() {
    print(pacienteNuevo.genero);
    _asignarGenero();
    return Column(
      children: <Widget>[
        ListTile(
          title: const Text('Hombre'),
          leading: Radio(
            value: Genero.hombre,
            groupValue: _genero,
            onChanged: (Genero value) {
              setState(() {
                _genero = value;
              });
            },
          ),
        ),
        ListTile(
          title: const Text('Mujer'),
          leading: Radio(
            value: Genero.mujer,
            groupValue: _genero,
            onChanged: (Genero value) {
              setState(() {
                _genero = value;
              });
            },
          ),
        ),
      ],
    );
  }

  _validatorNumber(String text, dynamic value2) {
    if (validatorUtils.isNumeric(value2)) {
      int value = int.parse(value2);
      if (value.isNegative) return "Debe ser un numero positivo";
      if (text == "Edad") {
        if (value >= 0 && value <= 110) {
          return null;
        } else {
          return "El rango de edad debe ser de 0 a 110";
        }
      }
      if (text == "Código Postal") {
        if (value.toString().length == 5) {
          return null;
        } else {
          return "El Código Postal debe tener 5 dígitos";
        }
      }

      return null;
    } else {
      return 'Solo números';
    }
  }

  _guardarValor(dynamic value, int element) {
    switch (element) {
      case 1:
        pacienteNuevo.nss = value;
        break;
      case 2:
        pacienteNuevo.nombrePaciente = value;
        break;
      case 3:
        pacienteNuevo.apellidoPaterno = value;
        break;
      case 4:
        pacienteNuevo.apellidoMaterno = value;
        break;
      case 5:
        pacienteNuevo.edad = int.parse(value);
        break;
      case 6:
        pacienteNuevo.curp = value.toString().toUpperCase();
        break;
    }
  }

  _validatorText(String text) {
    if (text == 'CURP') {
      return (value) {
        if (value.length != 18) {
          return 'CURP debe tener 18 caracteres';
        } else {
          return null;
        }
      };
    }
    return (value) {
      if (value.length < 3) {
        return 'El campo no puede ir vacío';
      } else {
        return null;
      }
    };
  }

  _decoratorInput(String texto) {
    return InputDecoration(
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      labelText: texto,
    );
  }

  _crearBoton(BuildContext context) {
    return RaisedButton.icon(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(20.0),
      ),
      color: Colors.blueAccent,
      textColor: Colors.white,
      label: Text('Guardar'),
      icon: Icon(Icons.save),
      onPressed: (_guardando) ? null : _submit,
    );
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    setState(() {
      _guardando = true;
    });
    bool exito = false;

    if (!_guardar) {
      exito = await _pacientesProvider.updatePaciente(pacienteNuevo);
    } else {
      exito = await _pacientesProvider.postPaciente(
        pacienteNuevo,
      );
    }

    if (exito) {
      mostrarSnackbar('Registro guardado', 1);

      Timer(Duration(seconds: 2), () => {Navigator.pop(context, true)});
    } else {
      mostrarSnackbar('Upps ha ocurrido un error ', 2);
      setState(() {
        _guardando = false;
      });
    }
  }

  void mostrarSnackbar(String mensaje, int opt) {
    final snackbar = SnackBar(
        content: Text(mensaje),
        duration: Duration(seconds: 5),
        backgroundColor: (opt == 1) ? Colors.green : Colors.red);
    print(_scflKeyPc);
    _scflKeyPc.currentState.showSnackBar(snackbar);
  }
}
