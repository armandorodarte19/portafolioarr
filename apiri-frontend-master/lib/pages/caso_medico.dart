import 'dart:async';

import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/models/centro_clinico_models.dart';
import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/providers/casos_medicos_provider.dart';
import 'package:apiri/providers/centros_clinicos_provider.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/AnimationsLoadersAPIRI.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:dropdown_formfield/dropdown_formfield.dart';
import 'package:apiri/utils/utils.dart' as utils;

class CasoMedicoPage extends StatefulWidget {
  @override
  _CasoMedicoPageState createState() => _CasoMedicoPageState();
}

class _CasoMedicoPageState extends State<CasoMedicoPage> {
  final formKey = GlobalKey<FormState>();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  String _fecha = '';
  TextEditingController _inputFieldDateController = new TextEditingController();
  bool _guardando = false;

  List<CentroClinico> _centrosClinicos = [];
  CentroClinico _centroSel;
  CasoMedico _newCasoMedico;
  Paciente _paciente;
  CentroClinicosProvider _centroClinicosProvider = new CentroClinicosProvider();
  CasosMedicosProvider _casosMedicosProvider = new CasosMedicosProvider();
  bool _editar = false;
  String _labelTitle = '';
  @override
  Widget build(BuildContext context) {
    var data = ModalRoute.of(context).settings.arguments;
    _newCasoMedico = new CasoMedico();
    if (data is CasoMedico) {
      _cargarInitial(data);
    } else {
      _paciente = data;
      _labelTitle = "Nuevo Casó";
    }

    return Scaffold(
        key: scaffoldKey,
        appBar: AppBar(
          title: Text(_labelTitle),
          leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context)),
          centerTitle: true,
          backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
          actions: <Widget>[LogOutApiri()],
        ),
        body: interfaz());
  }

  _cargarInitial(CasoMedico data) {
    _paciente = data.pacienteCaso;
    _editar = true;
    _newCasoMedico = data;
    if (_fecha == '') _fecha = utils.formatDate(_newCasoMedico.fechaInicio);
    _inputFieldDateController.text = _fecha;
    _labelTitle = 'Editar Caso';
  }

  Widget interfaz() {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.all(15.0),
        child: Form(
          key: formKey,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Text(
                  'NSS: ${_paciente.nss} \n${_paciente.nombrePaciente} ${_paciente.apellidoPaterno} ${_paciente.apellidoMaterno}',
                  style: TextStyle(fontSize: 20),
                  textAlign: TextAlign.center),
              Divider(),
              Text(
                "Info del casó",
                textAlign: TextAlign.center,
              ),
              _crearNombre(context),
              SizedBox(
                height: 10,
              ),
              _crearFecha(context, "Fecha de inicio"),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 10,
              ),
              //  _crearSelectorInstituto(),
              (_guardando) ? animationCargandoWave() : _crearBoton(context)
            ],
          ),
        ),
      ),
    );
  }

  Widget _crearNombre(BuildContext context) {
    return TextFormField(
      // initialValue: _newCasoMedico.nombreCasoMedico,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(labelText: 'Nombre del caso'),
      initialValue: (_editar) ? _newCasoMedico.nombreCasoMedico : '',
      onSaved: (value) => _newCasoMedico.nombreCasoMedico = value,
      enabled: !_editar,

      validator: (value) {
        if (value.length < 3) {
          return 'Ingrese el nombre del caso';
        } else {
          return null;
        }
      },
    );
  }

  _crearBoton(BuildContext context) {
    return RaisedButton.icon(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0),
        ),
        color: Colors.blueAccent,
        textColor: Colors.white,
        label: Text('Guardar'),
        icon: Icon(Icons.save),
        onPressed: (_guardando) ? null : _submit);
  }

  void _submit() async {
    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();

    if (!formKey.currentState.validate()) return;

    formKey.currentState.save();
    _newCasoMedico.fechaInicio = DateTime.parse(_fecha);
    setState(() {
      _guardando = true;
    });

    _newCasoMedico.pacienteId = _paciente.id;
    String _mensaje = 'Guardado';
    bool exito = true;
    if (!_editar) {
      print("Guardando");
      _newCasoMedico.centroClinicoId =
          int.parse(PreferenciasUsuario().centroClinico);
      exito = await _casosMedicosProvider.postCasoMedico(_newCasoMedico);

      //
    } else {
      if (_newCasoMedico.centroClinico != _centroSel && _centroSel != null) {
        _newCasoMedico.centroClinicoId = _centroSel.id;
      }
      exito = await _casosMedicosProvider.putCasoMedico(_newCasoMedico);
    }

    if (exito) {
      mostrarSnackbar('Registro guardado', 1);
      Timer(Duration(seconds: 2), () => {Navigator.pop(context)});
    } else {
      mostrarSnackbar('Upps ha ocurrido un error ', 2);
      setState(() {
        _guardando = false;
      });
    }
  }

  void mostrarSnackbar(String mensaje, int opt) {
    final snackbar = SnackBar(
        content: Text(mensaje),
        duration: Duration(seconds: 5),
        backgroundColor: (opt == 1) ? Colors.green : Colors.red);
    scaffoldKey.currentState.showSnackBar(snackbar);
  }

  Widget _crearFecha(BuildContext context, String text) {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      enableInteractiveSelection: false,
      controller: _inputFieldDateController,
      decoration: InputDecoration(
        hintText: text,
        labelText: text,
        suffixIcon: Icon(Icons.calendar_today),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());

        _selectDate(context);
      },
      validator: (value) {
        if (_fecha == "") {
          return 'Este campo es obligatorio';
        }
        return null;
      },
    );
  }

  void _selectDate(BuildContext context) async {
    DateTime initialDate =
        (_editar) ? _newCasoMedico.fechaInicio : new DateTime.now();

    DateTime picked = await showDatePicker(
      context: context,
      initialDate: initialDate,
      firstDate: new DateTime(2018),
      lastDate: new DateTime.now(),
      locale: Locale('es', 'ES'),
    );

    if (picked != null) {
      setState(() {
        _fecha = utils.formatDate(picked);
        _inputFieldDateController.text = _fecha;
      });
    }
  }
}
