import 'dart:async';

import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/models/centro_clinico_models.dart';
import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/models/revision_model.dart';
import 'package:apiri/providers/casos_medicos_provider.dart';
import 'package:apiri/providers/revision_provider.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:apiri/utils/utils.dart' as utils;

class RevisionesCasoPage extends StatefulWidget {
  @override
  _RevisionesCasoPageState createState() => _RevisionesCasoPageState();
}

class _RevisionesCasoPageState extends State<RevisionesCasoPage> {
  CasoMedico _casoMedico = new CasoMedico();
  Paciente _pacienteCaso = new Paciente();

  String _abiertoStr = 'Abierto';
  CentroClinico _centroClinico = new CentroClinico();
  CasosMedicosProvider _casoMedicoProvider = new CasosMedicosProvider();
  RevisionProvider _revisionProvider = new RevisionProvider();
  bool _dePaciente = true;
  int _cantRev = 0;
  @override
  Widget build(BuildContext context) {
    final data = ModalRoute.of(context).settings.arguments;
    CasoMedico casMed;

    //Permite saber de que ruta se mando llamar la revision, si de paciente o desde casos
    if (data is CasoMedico) {
      casMed = data;
    } else {
      List data2 = data;
      casMed = data2.elementAt(0);
      _dePaciente = false;
    }

    _initDatos(casMed);

    return Scaffold(
      appBar: AppBar(
        title: Text("Caso: ${casMed.nombreCasoMedico}"),
        centerTitle: true,
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context)),
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
        actions: <Widget>[LogOutApiri()],
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Text("Información del casó",
              style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
          Divider(
            thickness: 2,
          ),
          SizedBox(height: 10.0),
          _crearInput(
              "${_pacienteCaso.nombrePaciente} ${_pacienteCaso.apellidoPaterno} ${_pacienteCaso.apellidoMaterno}",
              "Paciente"),
          _crearInput(utils.formatDateMonth(_casoMedico.fechaInicio),
              "Fecha de inicio"),
          _crearInput("$_abiertoStr", "Estado"),
          _crearInput(
              "${_centroClinico.nombreCentroClinico}", "Centro Clinico"),
          _crearBotones(_casoMedico.casoAbierto),
          SizedBox(height: 15.0),
          Text(
            "Revisiones",
            style: TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
          Divider(
            thickness: 2,
          ),
          _botonAgregarRevision(_casoMedico.casoAbierto),
          SizedBox(height: 10.0),
          _interfazRevisiones(),
        ],
      ),
    );
  }

  _initDatos(CasoMedico casMed) {
    _casoMedico = casMed;
    _pacienteCaso = _casoMedico.pacienteCaso;
    _centroClinico = _casoMedico.centroClinico;

    if (_casoMedico.casoAbierto) {
      _abiertoStr = 'Abierto';
    } else {
      _abiertoStr = 'Cerrado';
    }
  }

  Widget _crearInput(String texto, String nombreLabel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(nombreLabel),
        TextField(
            autofocus: true,
            enabled: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              labelText: texto,
            )),
        SizedBox(height: 10.0),
      ],
    );
  }

  Widget _crearBotones(bool casoAbierto) {
    if (!casoAbierto) {
      return Column(
        children: [
          Container(
            width: double.infinity,
            height: 25,
            color: Colors.redAccent[200],
            child: Text(
              "Caso Cerrado",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
              textAlign: TextAlign.center,
            ),
          )
        ],
      );
    }
    return Row(
      mainAxisAlignment:
          MainAxisAlignment.center, //Center Row contents horizontally,

      children: [
        _botonEditarCaso(),
        SizedBox(
          width: 50,
        ),
        _botonCerrarCaso()
      ],
    );
  }

  Widget _botonCerrarCaso() {
    return Center(
      child: FlatButton(
          child: Text(
            'Cerrar caso',
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.red,
          onPressed: alertCaso),
    );
  }

  alertCaso() {
    if (_cantRev == 0) {
      return mostrarAlertaPop(context,
          "No puedes cerrar un caso sin revisiones", "Error al cerrar caso");
    }
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text("Confirmación"),
          content: const Text("Estas seguro que deseas cerrar este caso?"),
          actions: <Widget>[
            FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: const Text("Cancelar")),
            FlatButton(
              onPressed: () => {
                _casoMedico.casoAbierto = false,
                _casoMedico.fechaFin = DateTime.now(),
                setState(() {}),
                Navigator.of(context)
                    .pop(_casoMedicoProvider.cerrarCasoMedico(_casoMedico))
              },
              child: const Text("Aceptar"),
            ),
          ],
        );
      },
    );
  }

  Widget _botonEditarCaso() {
    return Center(
        child: FlatButton(
            child: Text(
              'Editar',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.deepPurpleAccent,
            onPressed: () => {
                  Navigator.pushNamed(context, 'casomedico',
                          arguments: _casoMedico)
                      .then((value) => setState(() {}))
                }));
  }

  Widget _botonAgregarRevision(bool casoAbierto) {
    if (!casoAbierto) {
      return Container();
    }
    return Row(
      children: <Widget>[
        FlatButton(
            child: Text(
              'Agregar Revisión',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.deepPurpleAccent,
            onPressed: () =>
                Navigator.pushNamed(context, 'revision', arguments: _casoMedico)
                    .then((value) => setState(() {})))
      ],
    );
  }

  Widget _interfazRevisiones() {
    return FutureBuilder(
      future: _revisionProvider.getRevisiones(_casoMedico.id),
      builder: (BuildContext context, AsyncSnapshot<List<Revision>> snapshot) {
        if (snapshot.hasData) {
          final revisiones = snapshot.data;
          if (revisiones.length == 0) {
            return Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                "Sin resultados",
                style: TextStyle(fontSize: 30),
              ),
              alignment: Alignment.center,
            );
          }
          return ListView.builder(
            itemCount: revisiones.length,
            shrinkWrap: true,
            physics: ScrollPhysics(),
            itemBuilder: (context, i) {
              return _addItem(revisiones[i], i + 1);
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _addItem(Revision revision, int index) {
    if (!_casoMedico.casoAbierto) {
      return _card(revision, index);
    }
    _cantRev = index + 1;
    return Dismissible(
      child: _card(revision, index),
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      background: Card(
        elevation: 10.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        color: Colors.redAccent,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
      ),
      onDismissed: (direction) {
        setState(() {
          Icon(Icons.delete);
        });
      },
      confirmDismiss: (DismissDirection direction) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            bool borradoExito;
            return AlertDialog(
              title: const Text("Confirmación"),
              content: const Text("Estas seguro que deseas borrar este caso?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: const Text("Cancelar")),
                FlatButton(
                  onPressed: () async => {
                    borradoExito =
                        await _revisionProvider.deleteRevision(revision.id),
                    Navigator.of(context).pop(borradoExito)
                  },
                  child: const Text("Borrar"),
                ),
              ],
            );
          },
        );
      },
    );
  }

  _card(Revision revision, int index) {
    var color = Colors.white;
    revision.casoMedico = _casoMedico;
    return Card(
      color: color,
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(_asignarNombreGrado(revision, index)),
            subtitle: Text('Fase: ' + revision.etapasVisuales.nombreFase),
            leading:
                Icon(Icons.playlist_add_check, color: Colors.blue, size: 40),
            trailing: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  utils.formatDate(revision.fecha),
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, 'revision', arguments: revision)
                  .then((value) => setState(() {}));
              ;
            },
          ),
        ],
      ),
    );
  }

  String _asignarNombreGrado(Revision revision, int index) {
    String title = index.toString() +
        "-" +
        revision.tipoHerida.nombreHerida +
        " " +
        revision.tipoHerida.gradoGravedad.grado.toString() +
        "°";
    return title;
  }
}
