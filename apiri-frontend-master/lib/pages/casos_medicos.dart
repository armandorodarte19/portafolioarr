import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/providers/casos_medicos_provider.dart';
import 'package:apiri/search/search_delegate.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/BNavigateBar.dart';
import 'package:apiri/widgets/LogoutBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:apiri/utils/utils.dart' as utils;

class CasosMedicosPage extends StatefulWidget {
  @override
  _CasosMedicosPageState createState() => _CasosMedicosPageState();
}

class _CasosMedicosPageState extends State<CasosMedicosPage> {
  CasosMedicosProvider _casosMedicosProvider = new CasosMedicosProvider();
  bool _casoAbierto = true;

  final scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text("APIRI-HC"),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.search),
            onPressed: () {
              showSearch(
                context: context,
                delegate: DataSearch("casos"),
              );
            },
          ),
          LogOutApiri()
        ],
      ),
      body: _interfazCasosMedicos(),
      bottomNavigationBar: BNNavigateBar(context),
    );
  }

  Widget _addItem(CasoMedico casoM) {
    String fechaCaso = utils.formatDate(casoM.fechaFin);
    _casoAbierto = true;
    if (!utils.isDataClosed(fechaCaso)) {
      _casoAbierto = false;
      return _cardCaso(casoM);
    }

    return Dismissible(
      child: _cardCaso(casoM),
      key: Key(casoM.id.toString()),
      direction: DismissDirection.startToEnd,
      background: Card(
        elevation: 10.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        color: Colors.redAccent,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
      ),
      onDismissed: (direction) {
        Icon(Icons.delete);
      },
      confirmDismiss: (DismissDirection direction) async {
        bool borrado = true;

        return await showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Confirmación"),
              content: const Text("Estas seguro que deseas borrar este caso?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () =>
                        {setState(() {}), Navigator.of(context).pop(false)},
                    child: const Text("Cancelar")),
                FlatButton(
                  onPressed: () async => {
                    borrado =
                        await _casosMedicosProvider.deleteCasoMedico(casoM.id),
                    Navigator.of(context).pop(borrado),
                    if (!borrado)
                      mostrarAlertaPop(
                          context,
                          "Asegurate de que el caso no tenga revisiones",
                          'No se pudo borrar el caso')
                  },
                  child: const Text("Borrar"),
                ),
              ],
            );
          },
        );
      },
    );
  }

  _cardCaso(CasoMedico casoM) {
    var color = Colors.white;
    if (!_casoAbierto) {
      color = Colors.red[100];
    }
    return Card(
      color: color,
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(casoM.nombreCasoMedico),
            subtitle: Text(
                '${casoM.pacienteCaso.apellidoPaterno} ${casoM.pacienteCaso.nombrePaciente}'),
            leading: Icon(Icons.folder_shared, color: Colors.blue, size: 50),
            trailing: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  utils.formatDate(casoM.fechaInicio),
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
                Text(
                  "${casoM.centroClinico.nombreCentroClinico} ",
                  style: TextStyle(fontSize: 12),
                ),
                (!casoM.casoAbierto)
                    ? Text(" Estado: Cerrado", style: TextStyle(fontSize: 12))
                    : Text(
                        "Estado: Abierto",
                        style: TextStyle(fontSize: 12),
                      )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, 'revisiones', arguments: casoM)
                  .then((value) => setState(() {}));
              ;
            },
          ),
        ],
      ),
    );
  }

  _interfazCasosMedicos() {
    return Column(
      children: [
        SizedBox(
          height: 10,
        ),
        SizedBox(
          height: 10,
        ),
        Expanded(child: _listadoCasosMedicos())
      ],
    );
  }

  @override
  void initState() {
    scrollController.addListener(() {
      if (scrollController.position.maxScrollExtent ==
          scrollController.offset) {
        setState(() {});
      }
    });
    super.initState();
  }

  _listadoCasosMedicos() {
    return FutureBuilder(
      future: _casosMedicosProvider.getCasosMedicos(),
      builder:
          (BuildContext context, AsyncSnapshot<List<CasoMedico>> snapshot) {
        if (snapshot.hasData) {
          final casoMedico = snapshot.data;
          if (casoMedico.length == 0) {
            return Container(
              child: Text(
                "Sin resultados",
                style: TextStyle(fontSize: 30),
              ),
              alignment: Alignment.center,
            );
          }
          return RefreshIndicator(
            child: ListView.builder(
              itemCount: casoMedico.length,
              itemBuilder: (context, i) {
                if (i < snapshot.data.length) {
                  return _addItem(casoMedico[i]);
                } else if (_casosMedicosProvider.hasMore) {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 32.0),
                    child: Center(child: CircularProgressIndicator()),
                  );
                } else {
                  return Padding(
                    padding: EdgeInsets.symmetric(vertical: 32.0),
                    child: Center(child: Text('No quedan casos por mostrar')),
                  );
                }
              },
              controller: scrollController,
            ),
            onRefresh: _casosMedicosProvider.getCasosMedicos,
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }
}
