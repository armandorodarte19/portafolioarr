import 'package:apiri/bloc/provider.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:apiri/providers/usuario_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  UsuarioProvider _usuarioProvider = new UsuarioProvider();
  PreferenciasUsuario _preferenciasUsuario = new PreferenciasUsuario();

  bool _recordarme = true;
  bool _inProgress;

  @override
  Widget build(BuildContext context) {
    if (_inProgress == null) _inProgress = false;

    return Scaffold(
        body: Stack(
      children: <Widget>[
        _crearFondo(context),
        _loginForm(context),
      ],
    ));
  }

  Widget _loginForm(BuildContext context) {
    final bloc = Provider.of(context);
    final size = MediaQuery.of(context).size;

    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          SafeArea(
            child: Container(
              height: 180.0,
            ),
          ),
          Container(
            width: size.width * 0.85,
            margin: EdgeInsets.symmetric(vertical: 30.0),
            padding: EdgeInsets.symmetric(vertical: 50.0),
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                      color: Colors.black26,
                      blurRadius: 3.0,
                      offset: Offset(0.0, 5.0),
                      spreadRadius: 3.0)
                ]),
            child: Column(
              children: <Widget>[
                Text('Ingreso', style: TextStyle(fontSize: 20.0)),
                SizedBox(height: 60.0),
                _crearEmail(bloc),
                SizedBox(height: 30.0),
                _crearPassword(bloc),
                SizedBox(height: 30.0),
                _rememberPass(),
                if (_inProgress) _animationCargando() else _crearBoton(bloc),

                SizedBox(height: 10.0),
                // Text('¿Olvido la contraseña?'),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _rememberPass() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Checkbox(
          value: _recordarme,
          onChanged: (value) {
            setState(() {
              _recordarme = !_recordarme;
            });
          },
        ),
        Text("Recuérdame"),
      ],
    );
  }

  Widget _animationCargando() {
    return SpinKitWave(
      itemBuilder: (BuildContext context, int index) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: index.isEven ? Colors.deepPurpleAccent : Colors.blueAccent,
          ),
        );
      },
    );
  }

  Widget _crearEmail(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.emailStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextFormField(
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                icon: Icon(Icons.alternate_email, color: Colors.deepPurple),
                hintText: 'ejemplo@correo.com',
                labelText: 'Correo electrónico',
                errorText: snapshot.error,
              ),
              initialValue: _preferenciasUsuario.email,
              onChanged: bloc.changeEmail),
        );
      },
    );
  }

  Widget _crearPassword(LoginBloc bloc) {
    return StreamBuilder(
      stream: bloc.passwordStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20.0),
          child: TextFormField(
            obscureText: true,
            decoration: InputDecoration(
                icon: Icon(Icons.lock_outline, color: Colors.deepPurple),
                labelText: 'Contraseña',
                errorText: snapshot.error),
            initialValue: _preferenciasUsuario.pass,
            onChanged: bloc.changePassword,
          ),
        );
      },
    );
  }

  Widget _crearBoton(LoginBloc bloc) {
    // formValidStream
    // snapshot.hasData
    //  true ? algo si true : algo si false

    return StreamBuilder(
      stream: bloc.formValidStream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        return RaisedButton(
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 80.0, vertical: 15.0),
              child: Text('Ingresar'),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)),
            elevation: 0.0,
            color: Colors.deepPurple,
            textColor: Colors.white,
            onPressed: (snapshot.hasData || _preferenciasUsuario.email != '')
                ? () => _login(bloc, context)
                : null);
      },
    );
  }

  _login(LoginBloc bloc, BuildContext context) async {
    setState(() {
      _inProgress = true;
    });

    Map info = new Map();
    (bloc.email == null && _preferenciasUsuario.email != '')
        ? info = await _usuarioProvider.login(
            _preferenciasUsuario.email, _preferenciasUsuario.pass)
        : info = await _usuarioProvider.login(bloc.email, bloc.password);

    if (info['ok']) {
      if (_recordarme) {
        _preferenciasUsuario.email = bloc.email;
        _preferenciasUsuario.pass = bloc.password;
      } else {
        _preferenciasUsuario.clearUser();
      }

      await Navigator.pushReplacementNamed(this.context, 'home');
    } else {
      mostrarAlerta(info['mensaje']);
      setState(() {
        _inProgress = false;
      });
    }

    //Borrar comentario en cuanto pueda
  }

  void mostrarAlerta(String mensaje) {
    showDialog(
        context: this.context,
        builder: (context) {
          return AlertDialog(
            title: Text('Información incorrecta'),
            content: Text(mensaje),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                onPressed: () => Navigator.of(context).pop(),
              )
            ],
          );
        });
  }

  Widget _crearFondo(BuildContext context) {
    final size = MediaQuery.of(context).size;

    final fondo = Container(
      width: double.infinity,
      height: size.height,
      decoration: BoxDecoration(
          color: Color.fromRGBO(63, 63, 156, 1.0),
          gradient: LinearGradient(colors: <Color>[
            Color.fromRGBO(63, 63, 156, 1.0),
            Color.fromRGBO(90, 70, 178, 1.0)
          ], begin: Alignment.topRight, end: Alignment.bottomLeft)),
    );

    final circulo = Container(
      width: 100.0,
      height: 100.0,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(100.0),
          color: Color.fromRGBO(255, 255, 255, 0.05)),
    );

    return Stack(
      children: <Widget>[
        fondo,
        Container(
          padding: EdgeInsets.only(top: 60.0),
          child: Column(
            children: <Widget>[
              SizedBox(height: 10.0, width: double.infinity),
              Text('APIRI-HC',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 38,
                    fontWeight: FontWeight.w900,
                  )),
              Text(
                  'Sistema de Recolección de \nInformación sobre \nHeridas Cutáneas ',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 24,
                    fontWeight: FontWeight.w900,
                  )),
            ],
          ),
        )
      ],
    );
  }
}
