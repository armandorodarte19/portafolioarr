import 'package:apiri/models/caso_medico_models.dart';
import 'package:apiri/models/paciente_model.dart';
import 'package:apiri/providers/casos_medicos_provider.dart';
import 'package:apiri/widgets/Alertas.dart';
import 'package:apiri/widgets/LogoutBar.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:apiri/utils/utils.dart' as utils;

class PacienteInfoPage extends StatefulWidget {
  @override
  _PacienteInfoPageState createState() => _PacienteInfoPageState();
}

class _PacienteInfoPageState extends State<PacienteInfoPage> {
  List<String> _nombres = ['IMSS', 'Juan Melendez'];
  String _opcionSeleccionada = 'IMSS';
  String _fecha = '';
  bool _creandoCaso = false;
  TextEditingController _inputFieldDateController = new TextEditingController();
  Paciente paciente = new Paciente();

  CasosMedicosProvider _casosMedicosProvider = new CasosMedicosProvider();
  @override
  Widget build(
    BuildContext context,
  ) {
    final Paciente pacData = ModalRoute.of(context).settings.arguments;
    if (pacData != null) {
      paciente = pacData;
    }
    if (_creandoCaso) setState(() {});
    return Scaffold(
      appBar: AppBar(
        title: Text("Paciente"),
        leading: IconButton(
          icon: Icon(Icons.people),
          onPressed: () => Navigator.pop(context),
        ),
        centerTitle: true,
        backgroundColor: Color.fromRGBO(51, 153, 255, 1.0),
        actions: <Widget>[
          LogOutApiri(),
        ],
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Text("Información del paciente",
              style: TextStyle(fontSize: 20), textAlign: TextAlign.center),
          Divider(
            thickness: 2,
          ),
          SizedBox(height: 10.0),
          _crearInput(paciente.nss, "NSS"),
          _crearInput(paciente.nombrePaciente, "Nombre"),
          _crearInput(paciente.apellidoPaterno, "Apellido Paterno"),
          paciente.apellidoMaterno != null
              ? SizedBox(
                  height: 1,
                )
              : _crearInput(paciente.apellidoMaterno, "Apellido Materno"),
          _crearInput(paciente.genero, "Genero"),
          _crearInput(paciente.edad.toString(), "Edad"),
          _boton(),
          SizedBox(height: 15.0),
          Text(
            "Casos",
            style: TextStyle(
              fontSize: 20,
            ),
            textAlign: TextAlign.center,
          ),
          Divider(
            thickness: 2,
          ),
          _botonAgregar(),
          SizedBox(height: 10.0),
          _interfazCasosMedicos()
        ],
      ),
    );
  }

  Widget _boton() {
    return Center(
      child: FlatButton(
          child: Text(
            'Editar',
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.deepPurpleAccent,
          onPressed: () =>
              Navigator.pushNamed(context, "paciente", arguments: paciente)
                  .then((value) => setState(() {}))),
    );
  }

  Widget _botonAgregar() {
    return Row(
      children: <Widget>[
        FlatButton(
            child: Text(
              'Agregar Caso',
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.deepPurpleAccent,
            onPressed: () =>
                Navigator.pushNamed(context, 'casomedico', arguments: paciente)
                    .then((value) => setState(() {})))
      ],
    );
  }

  Widget _crearInput(String texto, String nombreLabel) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Text(nombreLabel),
        TextField(
            autofocus: true,
            enabled: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              labelText: texto,
            )),
        SizedBox(height: 10.0),
      ],
    );
  }

  Widget _interfazCasosMedicos() {
    return FutureBuilder(
      future: _casosMedicosProvider.getCasosMedicosPaciente(paciente.id),
      builder:
          (BuildContext context, AsyncSnapshot<List<CasoMedico>> snapshot) {
        if (snapshot.hasData) {
          final casoMedico = snapshot.data;
          if (casoMedico.length == 0) {
            return Container(
              margin: EdgeInsets.only(top: 10),
              child: Text(
                "Sin resultados",
                style: TextStyle(fontSize: 30),
              ),
              alignment: Alignment.center,
            );
          }

          return ListView.builder(
            itemCount: casoMedico.length,
            shrinkWrap: true,
            primary: true,
            physics: ScrollPhysics(),
            itemBuilder: (context, i) {
              return _addItem(casoMedico[i]);
            },
          );
        } else {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
      },
    );
  }

  Widget _addItem(CasoMedico casoM) {
    String fechaCaso = utils.formatDate(casoM.fechaFin);
    bool borrado = false;

    if (!casoM.casoAbierto) {
      return _cardCaso(casoM);
    }

    return Dismissible(
      child: _cardCaso(casoM),
      key: UniqueKey(),
      direction: DismissDirection.startToEnd,
      background: Card(
        elevation: 10.0,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
        color: Colors.redAccent,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
      ),
      onDismissed: (direction) {
        //  setState(() {});
      },
      confirmDismiss: (DismissDirection direction) {
        return showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text("Confirmación"),
              content: const Text("Estas seguro que deseas borrar este caso?"),
              actions: <Widget>[
                FlatButton(
                    onPressed: () => Navigator.of(context).pop(false),
                    child: const Text("Cancelar")),
                FlatButton(
                  onPressed: () async => {
                    borrado =
                        await _casosMedicosProvider.deleteCasoMedico(casoM.id),
                    if (!borrado)
                      mostrarAlertaPop(
                          context,
                          "Asegurate de que el caso no tenga revisiones",
                          'No se pudo borrar el caso'),
                    Navigator.of(context).pop(borrado),
                  },
                  child: const Text("Borrar"),
                ),
              ],
            );
          },
        );
      },
    );
  }

  _cardCaso(CasoMedico casoM) {
    var color = Colors.white;
    if (!casoM.casoAbierto) {
      color = Colors.red[100];
    }
    return Card(
      color: color,
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            title: Text(casoM.nombreCasoMedico),
            subtitle: Text(
                '${casoM.pacienteCaso.apellidoPaterno} ${casoM.pacienteCaso.nombrePaciente}'),
            leading: Icon(Icons.folder_shared, color: Colors.blue, size: 50),
            trailing: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  utils.formatDate(casoM.fechaInicio),
                  style: TextStyle(
                    fontSize: 12,
                  ),
                ),
                Text(
                  "${casoM.centroClinico.nombreCentroClinico}",
                  style: TextStyle(fontSize: 12),
                ),
                (!casoM.casoAbierto)
                    ? Text(" Estado: Cerrado", style: TextStyle(fontSize: 12))
                    : Text(
                        "Estado: Abierto",
                        style: TextStyle(fontSize: 12),
                      )
              ],
            ),
            onTap: () {
              Navigator.pushNamed(context, 'revisiones', arguments: [casoM, 2])
                  .then((value) => setState(() {}));
            },
          ),
        ],
      ),
    );
  }
}
