import 'package:apiri/models/medico_model.dart';
import 'package:apiri/pages/caso_medico.dart';
import 'package:apiri/pages/casos_medicos.dart';
//import 'package:apiri/pages/casos_medicos_vista2.dart';
import 'package:apiri/pages/home.dart';
import 'package:apiri/pages/login_page.dart';
import 'package:apiri/pages/medicos_list.dart';
import 'package:apiri/pages/medico_page.dart';
import 'package:apiri/pages/paciente_info.dart';
import 'package:apiri/pages/paciente_create.dart';
import 'package:apiri/pages/pacientes.dart';
import 'package:apiri/pages/revision.dart';
import 'package:apiri/pages/revisiones_caso.dart';
import 'package:apiri/preferencias_usuario/preferencias_usuario.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'bloc/provider.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  final prefs = new PreferenciasUsuario();
  await prefs.initPrefs();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final prefs = new PreferenciasUsuario();
  @override
  Widget build(BuildContext context) {
    return Provider(
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [const Locale('en', 'US'), const Locale('es', 'ES')],
        title: 'APIRI',
        initialRoute: 'login',
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'home': (BuildContext context) => HomePage(),
          'pacientes': (BuildContext context) => PacientesPage(),
          'casosmedicos': (BuildContext context) => CasosMedicosPage(),
          'paciente': (BuildContext context) =>
              PacienteCreatePage(), //Ventana de editar, guardar

          'casomedico': (BuildContext context) => CasoMedicoPage(),
          'pacienteinfo': (BuildContext context) =>
              PacienteInfoPage(), //Ventana de listado de casos y boton de edicion de apciente
          'revisiones': (BuildContext context) => RevisionesCasoPage(),
          'revision': (BuildContext context) => RevisionPage(),
          'medicos': (BuildContext context) => MedicosListPage(),
          'medicoinfo': (BuildContext context) => MedicoPage(),
        },
        theme: ThemeData(
          primaryColor: Colors.deepPurple,
        ),
      ),
    );
  }
}
