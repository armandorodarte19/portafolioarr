# APIRI-HC
_Aplicación para la Recolección de Información sobre Heridas Cutáneas (APIRI-HC)_


### Pre-requisitos 📋

_Es necesario tener instalado el framework Flutter, para corroborar esto bastara con escribir el comando_

```
flutter doctor
```
_Ademas es necesario contar con un emulador de Androi, IOS o algun dispositivo fisico para correr la aplicación_
### Instalación 🔧

_1.- Para la instalación del proyecto, es necesario descargar el repositorio, ya sea en formato ZIP o utilizando el comando_

```
git clone https://gitlab.com/alejandrofenix/apiri-frontend.git
```
_2.- Una vez hecho esto bastara con dirigirse mediante la consola de comandos a la raiz del proyecto y ejecutar._

```
flutter pub get 
```
_Este comando nos permitira instalar todas las dependencias de la aplicación_

_3.-Una vez hecho esto, bastara con dirigirnos a Visual Studio Code y seleccionar el emulador con el cual queremos correrla, una vez hecho esto se desplegara la Aplicación._


## Despliegue 📦

_Para desplegar bastara con seguir la documentación oficial de flutter encontrada en la página https://esflutter.dev/docs/deployment/android esto para realizar el despliegue en android y https://esflutter.dev/docs/deployment/ios para el despliegue en IOS_

## Autor ✒️

* **Armando Rodarte Rodríguez** 

