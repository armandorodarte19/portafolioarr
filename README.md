# Portafolio 

# Armando Rodarte Rodríguez

## Portafolio Personal de GITLAB

## Acerca de mí.

I am software engineer and data scientist
with experience in scientific research.
Proficient in predictive modeling,
statistical analysis, optimization, data
processing, numerical analysis, as well as
scripting languages such as Python, R and
MATLAB. Finally, I possess mathematical
skills for developing new intelligent
models.

## TECHNOLOGIES:

- Python, C# , C++, Flutter, Dart, Django and Java.
- Matlab and R.
-  SQL.
-  Git.
-  Jupyter.
-  Rapids.
-  Scikit-learn, Tensorflow and PyTorch.
-  OpenCV.
-  Pandas, matplotlib and numpy.

##COURSES:

• Udemy course: "Deep Learning with
TensorFlow for Machine Learning and
AI".
• Udemy course: "Deep Learning and
Artificial Intelligence with
Keras/Tensorflow".
• Udemy course: "Tensorflow 2.0:
Complete Guide to the New
Tensorflow".
• Udemy course: "Master's in Machine
Learning - Learn R and Python from
Scratch".
• Udemy course: "TypeScript: Your
Complete Guide and Handbook.".
• Udemy course: "Flutter: Your
Complete Guide for iOS and
Android.".

## Descripción de los proyectos

Mi portafolio incluye cuatro aplicaciones destacadas que he desarrollado como parte de mi trayectoria personal, académica y de proyectos de investicación científica. Estos proyectos fueron implementados en entornos reales.

En primer lugar, he creado una sólida API REST utilizando C# (apiri-master (API REST)), diseñada específicamente para la recopilación y gestión eficiente de datos médicos. Este proyecto refleja mis habilidades en el desarrollo backend y mi capacidad para crear sistemas robustos. Esta API REST se implementó en hospitales y formó parte del trabajo del proyecto de investigación.

La segunda aplicación es un frontend desarrollado con Flutter (apiri-frontend-master), que es compatible con Android e iOS. Esta aplicación móvil se enfoca en la recopilación de datos médicos y ha sido diseñada para contribuir a investigaciones relacionadas con pacientes que tienen heridas médicas. Este proyecto destaca mi capacidad para desarrollar aplicaciones móviles intuitivas y efectivas. Esta app móvil se considera el frontend del proyecto mencionado y fue implementada en hospitales como parte de la investigación.

También, he desarrollado una tercera aplicación móvil con Flutter (coronahealth_app), centrada en el ámbito de la salud. Esta aplicación tiene como objetivo predecir la probabilidad de riesgo de mortalidad por COVID-19, utilizando un modelo de aprendizaje automático. Este proyecto demuestra mi experiencia en ciencia de datos y mi capacidad para abordar cuestiones de salud pública mediante la tecnología. Fue el prototipo de un proyecto científico.

Como cuarto proyecto (SisSerSocMedHumUAZ-master), creé una aplicación para pasantes de la Licenciatura de Medicina de la UAZ. El Sistema de Servicio Social de Medicina Humana de la UAZ es un sistema web diseñado para administrar las pasantías de los estudiantes de medicina de la universidad, los periodos de servicio y las clínicas donde llevarán a cabo sus actividades. Esta aplicación fue desarrollada mientras yo era estudiante y contribuyó de manera significativa a la mejora de la gestión de pasantías en la institución. Otros alumnos colaborarton en este proyecto utlizando herramientas como python y Django.

En resumen, estos proyectos personales en mi portafolio demuestran mi experiencia en el desarrollo de aplicaciones móviles y web, la gestión de datos médicos, así como mi experiencia en ciencia de datos. Además, destacan mi contribución al avance de la investigación médica y la mejora de la salud pblica.

## LINKEDIN
**www.linkedin.com/in/armandorodarte19****
